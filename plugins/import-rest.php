<?php

class Nethr_Import {

    private $base = 'https://public-api.wordpress.com/rest/v1.1/sites/90557224/';
    private $code = 'czFSajFRcl';

    private $token = 'vjAH5wZKTthQ#)jSZaUu#QiD@GwsOzO9ClAo^)I^gYZ6Re3FsCaIfSg5t3(#!Ck8';

    private $blog_id = 90557224;

    private $request = [
        'headers' => [
            'Authorization' => 'Bearer vjAH5wZKTthQ#)jSZaUu#QiD@GwsOzO9ClAo^)I^gYZ6Re3FsCaIfSg5t3(#!Ck8'
        ]
    ];

    public function pull_posts() {
        $progress = \WP_CLI\Utils\make_progress_bar( 'Processing posts...', 1000 );
        for ($offset=0; $offset<1000; $offset+=100) {
            $r = wp_remote_get($this->base . 'posts?context=edit&type=any&order=asc&number=100&offset=' . $offset, $this->request);
            $body = json_decode($r['body'], true);
            foreach ($body['posts'] as $post) {

                $id = wp_insert_post([
                    'post_author' => $this->get_author($post['author']['ID']),
                    'post_content' => $post['content'],
                    'post_title' => $post['title'],
                    'post_status' => 'publish',
                    'post_type' => $post['type'],
                    'post_date' => $post['date'],
                    'post_modified' => $post['modified'],
                    'guid' => $post['URL'],
                ]);
                wp_set_post_terms($id, $post['format'], 'post_format');

                update_post_meta($id, '_old_id', $post['ID']);
                $image_id = $this->uploadRemoteImageAndAttach($post['post_thumbnail']['guid'], $id);
                update_post_meta($image_id, '_old_id', $post['post_thumbnail']['ID']);
                set_post_thumbnail($id, $image_id);
                $this->set_categories($id, $post['categories']);
                $this->set_terms($id, $post['tags'], 'post_tag');
                $this->update_meta_data($id, $post['metadata']);
                $this->set_terms($id, $post['terms']['author'], 'author');
                $progress->tick();
                WP_CLI\Utils\wp_clear_object_cache();
            }

        }
        $progress->finish();
    }

    private function set_categories($id, $categories) {
        $cats = [];
        foreach ($categories as $category) {
            $cats[] = intval($this->get_category($category['ID']));
        }
        wp_set_post_terms($id, $cats, 'category');
    }

    private function set_terms($id, $categories, $taxonomy = 'category') {
        $cats = [];
        foreach ($categories as $category) {
            $cats[] = $category['slug'];
        }
        wp_set_post_terms($id, $cats, $taxonomy);
    }

    private function get_author($id) {
        global $wpdb;
        $row = $wpdb->get_row('SELECT * FROM wp_usermeta WHERE meta_key="_old_id" AND meta_value="'.$id.'"');
        if ($row) {
            return $row->user_id;
        }
        return 1;
    }

    private function update_meta_data($id, $meta) {
        $metas = ['extra_titles', 'main-category', 'nethr_color', 'oglasi'];
        foreach ($meta as $one) {
            if ( in_array( $one['key'], $metas) ) {
                update_post_meta($id, $one['key'], $one['value']);
            }
        }
    }

    private function get_category($id) {
        global $wpdb;
        $row = $wpdb->get_row('SELECT * FROM wp_termmeta WHERE meta_key = "_old_id" AND meta_value="'.$id.'"');
        return $row->term_id;
    }

    public function pull_categories() {
        $r = wp_remote_get($this->base . 'categories?offset=100', $this->request);
        $body = json_decode($r['body'], true);
        foreach ($body['categories'] as $category) {
            $id = wp_insert_category([
                'cat_name' => $category['slug'],
                'category_description' => $category['description'],
                'category_nicename' => $category['name'],
                'category_parent' => 0,
                'taxonomy' => 'category'
            ]);

            update_term_meta($id, '_old_id', $category['ID']);
            update_term_meta($id, '_old_parent_id', $category['parent']);
        }
    }

    public function pull_tags() {
        $r = wp_remote_get($this->base . 'tags', $this->request);
        $body = json_decode($r['body'], true);
        foreach ($body['tags'] as $tag) {
            $id = wp_insert_term($tag['name'], 'post_tag', [
                'slug' => $tag['slug'],
                'description' => $tag['description']
            ]);

            update_term_meta($id['term_id'], '_old_id', $tag['ID']);
        }
    }

    public function fix_categories() {
        global $wpdb;
        $cats = new WP_Term_Query([
            'taxonomy'               => 'category',
            'orderby'                => 'name',
            'order'                  => 'ASC',
            'hide_empty'             => false,
            'meta_key'   => '_old_parent_id',
            'meta_value'   => 0,
            'meta_compare'   => '!=',
        ]);

        foreach ($cats->terms as $cat) {
            $parent = get_term_meta($cat->term_id, '_old_parent_id', true);
            $row = $wpdb->get_row('SELECT * FROM wp_termmeta WHERE meta_key = "_old_id" AND meta_value = "'.$parent.'"');
            wp_update_category([
                'cat_ID' => $cat->term_id,
                'category_parent' => $row->term_id
            ]);
        }


    }


    private function uploadRemoteImageAndAttach($image_url, $parent_id){

        $image = $image_url;

        $get = wp_remote_get( $image );

        $type = wp_remote_retrieve_header( $get, 'content-type' );

        if (!$type)
            return false;

        $mirror = wp_upload_bits( basename( $image ), '', wp_remote_retrieve_body( $get ) );

        $attachment = array(
            'post_title'=> basename( $image ),
            'post_mime_type' => $type
        );

        $attach_id = wp_insert_attachment( $attachment, $mirror['file'], $parent_id );

        require_once(ABSPATH . 'wp-admin/includes/image.php');

        $attach_data = wp_generate_attachment_metadata( $attach_id, $mirror['file'] );

        wp_update_attachment_metadata( $attach_id, $attach_data );

        return $attach_id;

    }

    public function pull_authors() {
        $r = wp_remote_get($this->base . 'users?number=60&context=edit', $this->request);
        $authors = json_decode($r['body'], true);
        foreach ($authors['users'] as $author) {
            $id = wp_insert_user([
                'user_pass' => 'secret123.',
                'user_login' => $author['login'],
                'user_nicename' => $author['nice_name'],
                'user_email' => $author['email'],
                'display_name' => $author['name'],
                'role' => 'editor'
            ]);
            update_user_meta($id, '_old_id', $author['ID']);
        }
    }

    public function pull_coauthors() {

        $r = wp_remote_get($this->base . 'taxonomies/author/terms?offset=100', $this->request);
        $authors = json_decode($r['body'], true);
        $co = new CoAuthors_Guest_Authors();
        foreach ($authors['terms'] as $author) {
            $display = explode($author['name'], $author['description']);
            $display = explode(' ', trim($display[0]));
            if (count($display) === 1) {
                $display_name = $display[0];
            }
            else if (count($display)===2) {
                $display_name = implode(' ', $display);
            }
            else {
                $display = array_slice($display, 0, count($display)/2);
                $display_name = implode(' ', $display);
            }
            if ($display_name) {
                $co->create([
                    'display_name' => $display_name,
                    'user_login' => $author['name'],
                ]);
            }
        }
    }

}

WP_CLI::add_command( 'net', 'Nethr_Import' );