<?php

if ( ! wp_next_scheduled( 'nethr_hourly_tasks' ) ) {
	wp_schedule_event( time(), 'hourly', 'nethr_hourly_tasks' );
}

add_action( 'nethr_hourly_tasks', 'nethr_hourly' );

function nethr_hourly() {
	nethr_get_stats();
	nethr_query_weather_api();
	nethr_get_backup_promos();
	nethr_get_telegram();
}

function nethr_get_stats() {
	$r = wp_remote_get( 'http://services.net.hr/api/google/net' );
	if ( $r && !is_wp_error( $r ) ) {
		$list = json_decode( $r['body'], true );
		if ( is_array( $list ) ) {
			foreach ( $list as $key => $array ) {
				set_transient( 'nethr_most_read_' . $key, $array, DAY_IN_SECONDS );
			}
		}
	}
}

function nethr_strpos_arr($haystack, $needle) {
	if(!is_array($needle)) $needle = array($needle);
	foreach($needle as $what) {
		if(($pos = strpos($haystack, $what))!==false) return $pos;
	}
	return false;
}

function nethr_query_weather_api() {
	$data = vip_safe_wp_remote_get( 'http://services.net.hr/api/weather' );

	if ( ! is_wp_error( $data ) ) {
		$data   = json_decode( $data['body'], true );
		if ( is_array( $data ) && isset( $data[0] ) && isset( $data[0]['city'] ) ) {
			set_transient( 'nethr_weather_data', $data, DAY_IN_SECONDS );
		}
	}
}

function nethr_get_backup_promos() {
	$q = new WP_Query(
		array(
			'posts_per_page' => 10,
			'post_type'      => array( 'promo', 'igre' ),
			'post_status'    => 'publish',
			'meta_query' => array(
				array(
					'key' => 'home-feed-backup-on',
					'value' => 1,
					'type' => 'BOOLEAN'
				)
			),
			'no_found_rows'  => true,
			'fields'         => 'ids',
			'ignore_sticky_posts' => true
		)
	);
	$promos = get_option( 'nethr_promos', array() );
	$promos['backup'] = $q->posts;
	update_option( 'nethr_promos', $promos );
}

function nethr_get_telegram() {
	$rss = fetch_feed( 'https://www.telegram.hr/nethr' );

	if ( ! is_wp_error( $rss ) ) {
		$maxitems  = $rss->get_item_quantity( 10 );
		$articles  = array();
		$rss_items = $rss->get_items( 0, $maxitems );
		$i         = 0;
		foreach ( $rss_items as $item ) {
			$e = $item->get_enclosure();
			if ( $e->get_link() ) {
				$articles[] = array(
					'title' => $item->get_title(),
					'link'  => $item->get_permalink(),
					'image' => $e->get_link()
				);
				$i ++;
			}
			if ( $i === 5 ) {
				break;
			}
		}

		set_transient( 'nethr_telegram_posts', $articles, DAY_IN_SECONDS );
	}
}
