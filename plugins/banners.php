<?php
add_action( 'wp_footer', 'nethr_xclaim', 99 );

function nethr_xclaim() {
	if ( is_single() && !has_post_format( 'gallery' ) ) {
		global $post;
		$oglasi = get_post_meta( $post->ID, 'oglasi', true );
		if ( isset( $oglasi['xclaim'] ) && 1 === intval( $oglasi['xclaim'] ) ) {
			return;
		} else {
			?>
			<script id="xclaim_script" type="text/javascript" language="javascript">
				(function () {
					function async_load() {
						var s = document.createElement('script');
						s.type = 'text/javascript';
						s.async = true;
						s.src ='https://hr-engine.xclaimwords.net/script.aspx?partnerid=200166';
						var x = document.getElementsByTagName('script')[0];
						x.parentNode.insertBefore(s, x);
					}

					if (window.attachEvent)
						window.attachEvent('onload', async_load);
					else
						window.addEventListener('load', async_load, false);
				})();
			</script>
			<script>
				function onXClaimSettingsLoaded(){}
			</script>
			<?php
		}
	}
}

