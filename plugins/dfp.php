<?php

class Nethr_DFP {

    public function __construct() {
        add_action( 'admin_enqueue_scripts', [ $this, 'scripts' ] );
        add_action( 'admin_menu', [ $this, 'menu' ] );
        add_action( 'rest_api_init', [ $this, 'rest_init' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_script' ] );
    }

    public function menu() {
        add_options_page( 'DFP2', 'DFP2', 'edit_posts', 'nethr-dfp', array( $this, 'admin_page' ) );
    }

    public function scripts() {
        if ( $_GET['page'] === 'nethr-dfp' ) {
            wp_enqueue_script( 'nethr-vue-script', get_template_directory_uri() . '/js/dfp-vue.min.js', null, null, true );
            wp_localize_script( 'nethr-vue-script', 'nethrApiSettings', array(
                'root'  => esc_url_raw( rest_url() ),
                'nonce' => wp_create_nonce( 'wp_rest' )
            ) );
        }
    }

    public function admin_page() {
        ?>
        <div id="dfp-app">
            <adunits-component></adunits-component>
        </div>
        <?php
    }

    public function rest_init() {
        register_rest_route( 'tmg/v1', 'dfp/units', [
            'methods'  => \WP_REST_Server::READABLE,
            'callback' => [ $this, 'get_units' ],
            'permission_callback' => function () {
                return current_user_can( 'manage_options' );
            }
        ] );
        register_rest_route( 'tmg/v1', 'dfp/mappings', [
            'methods'  => \WP_REST_Server::READABLE,
            'callback' => [ $this, 'get_mappings' ],
            'permission_callback' => function () {
                return current_user_can( 'manage_options' );
            }
        ] );
        register_rest_route( 'tmg/v1', 'dfp/setup', [
            'methods'  => \WP_REST_Server::READABLE,
            'callback' => [ $this, 'get_setup' ],
            'permission_callback' => function () {
                return current_user_can( 'manage_options' );
            }
        ] );
        register_rest_route( 'tmg/v1', 'dfp/units', [
            'methods'  => \WP_REST_Server::CREATABLE,
            'callback' => [ $this, 'post_units' ],
            'permission_callback' => function () {
                return current_user_can( 'manage_options' );
            }
        ] );
        register_rest_route( 'tmg/v1', 'dfp/mappings', [
            'methods'  => \WP_REST_Server::CREATABLE,
            'callback' => [ $this, 'post_mappings' ],
            'permission_callback' => function () {
                return current_user_can( 'manage_options' );
            }
        ] );
        register_rest_route( 'tmg/v1', 'dfp/setup', [
            'methods'  => \WP_REST_Server::CREATABLE,
            'callback' => [ $this, 'post_setup' ],
            'permission_callback' => function () {
                return current_user_can( 'manage_options' );
            }
        ] );
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return mixed|\WP_REST_Response
     */
    public function get_units( $request ) {
        $units = get_option( 'nethr_dfp2_units', [
            [
                'name'     => 'nethr_desktop_homepage_billboard_v1',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [ [ 'x' => 1200, 'y' => 500 ], [ 'x' => 970, 'y' => 500 ], [ 'x' => 1200, 'y' => 250 ], [ 'x' => 970, 'y' => 250 ], [ 'x' => 1024, 'y' => 250 ], [ 'x' => 1024, 'y' => 500 ] ],
                'a'        => 2,
                'b'        => [8,7],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_homepage_billboard_v2',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [ [ 'x' => 1200, 'y' => 500 ], [ 'x' => 970, 'y' => 500 ], [ 'x' => 1200, 'y' => 250 ], [ 'x' => 970, 'y' => 250 ], [ 'x' => 1024, 'y' => 250 ], [ 'x' => 1024, 'y' => 500 ] ],
                'a'        => 2,
                'b'        => 7,
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_homepage_300x250_v1',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [ [ 'x' => 300, 'y' => 600 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_homepage_300x250_v2',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [ [ 'x' => 300, 'y' => 600 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => [ 7,8 ],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_homepage_300x250_v3',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [ [ 'x' => 300, 'y' => 600 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => 7,
                'j'        => 0,
                'k'        => 1,
                'h'        => ''
            ],
                [
                    'name'     => 'nethr_desktop_homepage_300x250_v4',
                    'location' => 'desktop',
                    'front'    => 'yes',
                    'sizes'    => [ [ 'x' => 300, 'y' => 600 ], [ 'x' => 300, 'y' => 250 ] ],
                    'a'        => 2,
                    'b'        => 7,
                    'j'        => 0,
                    'k'        => 1,
                    'h'        => ''
                ],
            [
                'name'     => 'nethr_desktop_homepage_wallpaper_left',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [
                    [ 'x' => 200, 'y' => 900 ],
                    [ 'x' => 300, 'y' => 1050 ],
                    [ 'x' => 340, 'y' => 1050 ],
                    [ 'x' => 300, 'y' => 900 ]
                ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_homepage_wallpaper_right',
                'location' => 'desktop',
                'front'    => 'yes',
                'sizes'    => [
                    [ 'x' => 201, 'y' => 901 ],
                    [ 'x' => 301, 'y' => 1051 ],
                    [ 'x' => 341, 'y' => 1051 ],
                    [ 'x' => 301, 'y' => 901 ]
                ],
                'a'        => 2,
                'b'        => [7,8],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_billboard_v1',
                'location' => 'desktop',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 1200, 'y' => 500 ], [ 'x' => 970, 'y' => 500 ], [ 'x' => 1200, 'y' => 250 ], [ 'x' => 970, 'y' => 250 ], [ 'x' => 1024, 'y' => 250 ], [ 'x' => 1024, 'y' => 500 ] ],
                'a'        => 2,
                'b'        => [7,8],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_300x250_v1',
                'location' => 'desktop',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 600 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => [ 9, 8],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_intext_v1',
                'location' => 'desktop',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 660, 'y' => 350 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => 7,
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_desktop_intext_v2',
                'location' => 'desktop',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 660, 'y' => 350 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 0,
                'h'        => 'contentbelow'
            ],
            [
                'name'     => 'nethr_desktop_intext_v3',
                'location' => 'desktop',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 660, 'y' => 350 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 1,
                'h'        => 'contentbelow'
            ],
            [
                'name'     => 'nethr_desktop_intext_v4',
                'location' => 'desktop',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 660, 'y' => 350 ], [ 'x' => 300, 'y' => 250 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 0,
                'h'        => 'contentbelow'
            ],
                [
                    'name'     => 'nethr_desktop_wallpaper_left',
                    'location' => 'desktop',
                    'front'    => 'yes',
                    'sizes'    => [
                        [ 'x' => 200, 'y' => 900 ],
                        [ 'x' => 300, 'y' => 1050 ],
                        [ 'x' => 340, 'y' => 1050 ],
                        [ 'x' => 300, 'y' => 900 ]
                    ],
                    'a'        => 2,
                    'b'        => [8,7],
                    'j'        => 0,
                    'k'        => 0,
                    'h'        => ''
                ],
                [
                    'name'     => 'nethr_desktop_wallpaper_right',
                    'location' => 'desktop',
                    'front'    => 'yes',
                    'sizes'    => [
                        [ 'x' => 201, 'y' => 901 ],
                        [ 'x' => 301, 'y' => 1051 ],
                        [ 'x' => 341, 'y' => 1051 ],
                        [ 'x' => 301, 'y' => 901 ]
                    ],
                    'a'        => 2,
                    'b'        => 8,
                    'j'        => 0,
                    'k'        => 1,
                    'h'        => ''
                ],
            [
                'name'     => 'nethr_desktop_bf_300x250_v1',
                'location' => 'desktop',
                'front'    => 'both',
                'sizes'    => [],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 1,
                'h'        => ''
            ],
                [
                    'name'     => 'nethr_desktop_bf_300x250_v2',
                    'location' => 'desktop',
                    'front'    => 'both',
                    'sizes'    => [],
                    'a'        => 2,
                    'b'        => 8,
                    'j'        => 0,
                    'k'        => 1,
                    'h'        => ''
                ],
            [
                'name'     => 'nethr_mobile_intext_v1',
                'location' => 'mobile',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                'a'        => 2,
                'b'        => [8,7],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_mobile_intext_v2',
                'location' => 'mobile',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 0,
                'h'        => 'contentbelow'
            ],
            [
                'name'     => 'nethr_mobile_intext_v3',
                'location' => 'mobile',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                'a'        => 2,
                'b'        => 7,
                'j'        => 0,
                'k'        => 1,
                'h'        => 'contentbelow'
            ],
            [
                'name'     => 'nethr_mobile_intext_v4',
                'location' => 'mobile',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 0,
                'h'        => 'contentbelow'
            ],
            [
                'name'     => 'nethr_mobile_header',
                'location' => 'mobile',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ] ],
                'a'        => 2,
                'b'        => [7,8],
                'j'        => 0,
                'k'        => 0,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_mobile_footer',
                'location' => 'mobile',
                'front'    => 'no',
                'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 1,
                'h'        => ''
            ],
                [
                    'name'     => 'nethr_mobile_homepage_v1',
                    'location' => 'mobile',
                    'front'    => 'yes',
                    'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 100 ] ],
                    'a'        => 2,
                    'b'        => 7,
                    'j'        => 0,
                    'k'        => 0,
                    'h'        => ''
                ],
                [
                    'name'     => 'nethr_mobile_homepage_v2',
                    'location' => 'mobile',
                    'front'    => 'yes',
                    'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                    'a'        => 2,
                    'b'        => 8,
                    'j'        => 0,
                    'k'        => 0,
                    'h'        => ''
                ],
                [
                    'name'     => 'nethr_mobile_homepage_v3',
                    'location' => 'mobile',
                    'front'    => 'yes',
                    'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                    'a'        => 2,
                    'b'        => 7,
                    'j'        => 0,
                    'k'        => 0,
                    'h'        => ''
                ],
                [
                    'name'     => 'nethr_mobile_homepage_v4',
                    'location' => 'mobile',
                    'front'    => 'yes',
                    'sizes'    => [ [ 'x' => 300, 'y' => 250 ], [ 'x' => 320, 'y' => 50 ], [ 'x' => 300, 'y' => 600 ] ],
                    'a'        => 2,
                    'b'        => 7,
                    'j'        => 0,
                    'k'        => 1,
                    'h'        => 'contentbelow'
                ],

            [
                'name'     => 'nethr_mobile_bf_300x250_v1',
                'location' => 'mobile',
                'front'    => 'both',
                'sizes'    => [],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 1,
                'h'        => ''
            ],
            [
                'name'     => 'nethr_mobile_bf_300x250_v2',
                'location' => 'mobile',
                'front'    => 'both',
                'sizes'    => [],
                'a'        => 2,
                'b'        => 8,
                'j'        => 0,
                'k'        => 1,
                'h'        => ''
            ],
        ] );

        return rest_ensure_response( [ 'units' => $units ] );
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return mixed|\WP_REST_Response
     */
    public function get_mappings( $request ) {
        $mappings = get_option( 'nethr_dfp2_mappings', [
            [
                'name'  => 'contentbelow',
                'sizes' => [
                    [
                        'viewport_width'  => 0,
                        'viewport_height' => 0,
                        'slotsize_width'  => 300,
                        'slotsize_height' => 250
                    ],
                    [
                        'viewport_width'  => 360,
                        'viewport_height' => 0,
                        'slotsize_width'  => 340,
                        'slotsize_height' => 280
                    ],
                    [
                        'viewport_width'  => 375,
                        'viewport_height' => 0,
                        'slotsize_width'  => 354,
                        'slotsize_height' => 280
                    ],
                    [
                        'viewport_width'  => 403,
                        'viewport_height' => 0,
                        'slotsize_width'  => 382,
                        'slotsize_height' => 280
                    ],
                    [
                        'viewport_width'  => 412,
                        'viewport_height' => 0,
                        'slotsize_width'  => 392,
                        'slotsize_height' => 280
                    ],
                    [
                        'viewport_width'  => 480,
                        'viewport_height' => 0,
                        'slotsize_width'  => 460,
                        'slotsize_height' => 280
                    ],
                    [
                        'viewport_width'  => 600,
                        'viewport_height' => 0,
                        'slotsize_width'  => 580,
                        'slotsize_height' => 280
                    ],
                    [
                        'viewport_width'  => 620,
                        'viewport_height' => 0,
                        'slotsize_width'  => 660,
                        'slotsize_height' => 350
                    ],
                    [
                        'viewport_width'  => 1024,
                        'viewport_height' => 0,
                        'slotsize_width'  => 660,
                        'slotsize_height' => 350
                    ],
                ]
            ]
        ] );

        return rest_ensure_response( [ 'mappings' => $mappings ] );
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return mixed|\WP_REST_Response
     */
    public function get_setup( $request ) {
        $setup               = get_option( 'nethr_dfp2_setup', [
            'a' => [ 'type' => 'string', 'value' => 'Orange' ],
            'b' => [ 'type' => 'string', 'value' => 'f' ],
            'c' => [ 'type' => 'float', 'value' => 1.4 ],
            'd' => [ 'type' => 'int', 'value' => '' ],
            'e' => [ 'type' => 'int', 'value' => '' ],
            'f' => [ 'type' => 'json', 'value' => [[3.4,2,"-"],[2.5,1,"-"],[-1.3,3,"+"],[-0.3,2,"+"],[0.4,1,"+"]] ],
            'g' => [ 'type' => 'int', 'value' => '' ],
        ] );
        $setup['f']['value'] = wp_json_encode( $setup['f']['value'] );

        return rest_ensure_response( [ 'setup' => $setup ] );
    }

    private function sanitize( $type, $value ) {
        switch ( $type ) {
            case 'int':
                $value = intval( $value );
                break;
            case 'float':
                $value = floatval( $value );
                break;
            case 'string':
                $value = sanitize_text_field( $value );
                break;
            case 'array':
                $value = array_map( 'intval', explode( ',', $value ) );
                break;
            case 'json':
                $value = json_decode( $value, true );
                break;
            default:
                $value = 0;
                break;
        }

        return $value;
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return mixed|\WP_REST_Response
     */
    public function post_setup( $request ) {
        $setup_vars = $request->get_json_params();
        $out        = [];
        foreach ( $setup_vars as $key => $var ) {
            $value       = $this->sanitize( $var['type'], $var['value'] );
            $out[ $key ] = [
                'type'  => $var['type'],
                'value' => $value
            ];
        }
        update_option( 'nethr_dfp2_setup', $out );

        return rest_ensure_response( 'success' );
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return mixed|\WP_REST_Response
     */
    public function post_units( $request ) {
        $data  = $request->get_json_params();
        $units = [];
        foreach ( $data as $unit ) {
            $sizes = [];
            foreach ( $unit['sizes'] as $size ) {
                $sizes[] = [ 'x' => $this->sanitize( 'int', $size['x'] ), 'y' => $this->sanitize( 'int', $size['y'] ) ];
            }
            $units[] = [
                'name'     => $this->sanitize( 'string', $unit['name'] ),
                'location' => $this->sanitize( 'string', $unit['location'] ),
                'front'    => $this->sanitize( 'string', $unit['front'] ),
                'sizes'    => $sizes,
                'a'        => $this->sanitize( 'int', $unit['a'] ),
                'b'        => $this->sanitize( 'string', $unit['b'] ),
                'j'        => $this->sanitize( 'int', $unit['j'] ),
                'k'        => $this->sanitize( 'int', $unit['k'] ),
                'h'        => $this->sanitize( 'string', $unit['h'] ),
            ];
        }
        update_option( 'nethr_dfp2_units', $units );

        return rest_ensure_response( 'success' );
    }

    /**
     * @param \WP_REST_Request $request
     *
     * @return mixed|\WP_REST_Response
     */
    public function post_mappings( $request ) {
        $vars     = $request->get_json_params();
        $mappings = [];
        foreach ( $vars as $mapping ) {
            $sizes = [];
            foreach ( $mapping['sizes'] as $size ) {
                $sizes[] = [
                    'viewport_width'  => $this->sanitize( 'int', $size['viewport_width'] ),
                    'viewport_height' => $this->sanitize( 'int', $size['viewport_height'] ),
                    'slotsize_width'  => $this->sanitize( 'int', $size['slotsize_width'] ),
                    'slotsize_height' => $this->sanitize( 'int', $size['slotsize_height'] )
                ];
            }
            $mappings[] = [
                'name'  => $this->sanitize( 'string', $mapping['name'] ),
                'sizes' => $sizes
            ];
        }
        update_option( 'nethr_dfp2_mappings', $mappings );

        return rest_ensure_response( 'success' );
    }

    public function enqueue_script() {
        wp_enqueue_script( 'nethr-gpt', 'https://www.googletagservices.com/tag/js/gpt.js');
        wp_add_inline_script('nethr-gpt', $this->header_script());
    }

    private function unit($unit) {
        $sizes = [];
        foreach ( $unit['sizes'] as $size ) {
            $sizes[] = [ $size['x'], $size['y'] ];
        }
        if ( strpos( $unit['b'], ',' ) !== false ) {
            $b = array_map( 'intval', explode( ',', $unit['b'] ) );
        } else {
            $b = intval( $unit['b'] );
        }
        return [
            'a' => intval( $unit['a'] ),
            'b' => $b,
            'c' => "/1092744/nethr/" . $unit['name'],
            'e' => $sizes,
            'g' => $unit['name'],
            'h' => $unit['h'],
            'j' => intval( $unit['j'] ),
            'k' => intval( $unit['k'] )
        ];
    }

    public function header_script() {
        $mappings            = get_option( 'nethr_dfp2_mappings' );
        $out_mappings        = [];
        $out_mappings_object = [];
        foreach ( $mappings as $mapping ) {
            $out_mappings[ $mapping['name'] ] = [];
            $out_mappings_object[]            = $mapping['name'];
            foreach ( $mapping['sizes'] as $size ) {
                $out_mappings[ $mapping['name'] ][] = [
                    [ $size['viewport_width'], $size['viewport_height'] ],
                    [ $size['slotsize_width'], $size['slotsize_height'] ]
                ];
            }
        }
        $setup     = get_option( 'nethr_dfp2_setup' );
        $out_setup = [];
        foreach ( $setup as $key => $value ) {
            $out_setup[ $key ] = $value['value'];
        }
        $out_units = [];
        $units     = get_option( 'nethr_dfp2_units' );
        foreach ( $units as $unit ) {
            if ( jetpack_is_mobile() && in_array( $unit['location'], [ 'both', 'mobile' ], true ) ) {
                if ( is_front_page() && in_array( $unit['front'], [ 'both', 'yes' ], true ) ) {
                    $out_units[] = $this->unit( $unit );
                }
                if ( ! is_front_page() && in_array( $unit['front'], [ 'both', 'no' ], true ) ) {
                    $out_units[] = $this->unit( $unit );
                }
            }
            if ( ! jetpack_is_mobile() && in_array( $unit['location'], [ 'both', 'desktop' ], true ) ) {
                if ( is_front_page() && in_array( $unit['front'], [ 'both', 'yes' ], true ) ) {
                    $out_units[] = $this->unit( $unit );
                }
                if ( ! is_front_page() && in_array( $unit['front'], [ 'both', 'no' ], true ) ) {
                    $out_units[] = $this->unit( $unit );
                }
            }
        }
        ob_start();
        ?>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            var googleSlots = {}, activeSlots = [];
            googletag.cmd.push(function () {
                var tg_sizes = <?php echo wp_json_encode( $out_mappings ) ?>;
                var tg_mappings_object = <?php echo wp_json_encode( $out_mappings_object ); ?>;
                var tg_mappings = {};
                for (var i in tg_mappings_object) {
                    var size = tg_mappings_object[i];
                    var mapping = googletag.sizeMapping();
                    for (var j in tg_sizes[size]) {
                        mapping.addSize(tg_sizes[size][j][0], tg_sizes[size][j][1]);
                    }
                    tg_mappings[size] = mapping.build();
                }
                var mf_setup = <?php echo wp_json_encode( $out_setup ); ?>;

                var mf_views_array = <?php echo wp_json_encode( $out_units ); ?>;

                for (var i in mf_views_array) {
                    if (mf_views_array.hasOwnProperty(i)) {
                        if (mf_views_array[i].h) {
                            mf_views_array[i].h = tg_mappings[mf_views_array[i].h];
                        }
                        googleSlots[mf_views_array.g] = null;
                    }
                }

                function mf_init(e) {
                    for (i = 0; i < e.length; i++) {
                        var t = googletag.defineSlot(e[i].c, e[i].e, e[i].g);
                        googleSlots[e[i].g] = t;
                        if (void 0 !== e[i].h && 0 < e[i].h.length && t.defineSizeMapping(e[i].h), t.addService(googletag.pubads()), e[i].b.constructor === Array) if (e[i].b.length < 2) e[i].b = e[i].b[0]; else {
                            var g = Math.floor(Math.random() * e[i].b.length);
                            e[i].b = e[i].b[g], e[i].i = e[i].b, t.setTargeting("split", e[i].i)
                        }
                        t.setTargeting(mf_setup.a, mf_setup.b + Math.round(e[i].b)), 1 == e[i].k && (t.setTargeting("no_show", 1), e[i].b = e[i].b + mf_setup.c, e[i].a = e[i].a + 1)
                    }
                }

                function mf_loop(e, t) {
                    googletag.pubads().addEventListener("slotRenderEnded", function (o) {
                        var n = o.slot[Object.keys(o.slot)[0]];
                        for (i = 0; i < e.length; i++) if (o.isEmpty && n === e[i].c && document.getElementById(o.slot.getSlotElementId()) && e[i].a > 0) {
                            e[i].a = e[i].a - 1, e[i].b = e[i].b - t.c, e[i].g = e[i].g + Math.round(e[i].b), document.getElementById(o.slot.getSlotElementId()).innerHTML = "", document.getElementById(o.slot.getSlotElementId()).removeAttribute("data-google-query-id"), document.getElementById(o.slot.getSlotElementId()).removeAttribute("style"), g = document.createElement("div"), g.setAttribute("id", e[i].g), document.getElementById(o.slot.getSlotElementId()).appendChild(g);
                            var a = googletag.defineSlot(e[i].c, e[i].e, e[i].g);
                            void 0 !== e[i].h && 0 < e[i].h.length && a.defineSizeMapping(e[i].h), a.addService(googletag.pubads()), a.setTargeting(t.a, t.b + Math.round(e[i].b)), void 0 !== e[i].i && a.setTargeting("split", e[i].i), 0 === e[i].a && 1 === e[i].j && a.setTargeting("bf", 1), googletag.display(e[i].g)
                        }
                    })
                }

                mf_setup.ver = "v_7.3", mf_init(mf_views_array), mf_loop(mf_views_array, mf_setup);

                googletag.pubads().enableSingleRequest();
                googletag.pubads().collapseEmptyDivs();
                <?php
                    $targeting = [
                        'wp_post_type' => [],
                        'post_slug' => [],
                        'post_tag' => [],
                        'nethr_category' => [],
                    ];
                    if (is_single()) {
                        global $post;

                        $targeting['wp_post_type'] = [ 'single', $post->post_type ];
                        $targeting['post_slug'] = [ $post->post_name ];
                        $cat = nethr_get_top_category();
                        if ($cat) {
                            $targeting['nethr_category'] = [ $cat->slug ];
                        }
                        $tags = get_the_tags();
                        $tkeys = [];
                        if ( $tags && ! is_wp_error( $tags ) ) {
                            foreach ( $tags as $tag ) {
                                $tkeys[] = $tag->slug;
                            }
                            $targeting['post_tag'] = $tkeys;
                        }

                        $oglasi = get_post_meta( $post->ID, 'oglasi', true );
                        if ( isset( $oglasi['adocean'] ) && 1 === intval( $oglasi['adocean'] ) ) {
                            ?>googletag.pubads().setCategoryExclusion('NePromo');<?php
                        }
                    }
                    if (is_archive()) {
                        $targeting['wp_post_type'] = [ 'archive' ];
                    }
                    if (is_tag()) {
                        $targeting['post_tag'] = [ get_queried_object()->slug ];
                    }
                    if (is_category()) {
                        $cat = nethr_get_top_category();
                        if ($cat) {
                            $targeting['nethr_category'] = [ $cat->slug ];
                        }
                    }
                    if (is_search()) {
                        $targeting['wp_post_type'] = [ 'search' ];
                    }
                    if (is_front_page()) {
                        $targeting['wp_post_type'] = [ 'home' ];
                    }
                    foreach ( $targeting as $key => $value ) {
                        if ( count( $value ) ) {
                            ?>googletag.pubads().setTargeting( <?php echo wp_json_encode( $key ) ?>, <?php echo wp_json_encode( $value ) ?> );<?php
                        }
                    }
                ?>
                googletag.enableServices();
            });
        <?php
        return ob_get_clean();
    }

    public function write_footer() {
        ?><script>
            var refreshSlots = [];
            for (var slot in googleSlots) {
                if (googleSlots.hasOwnProperty(slot) && activeSlots.indexOf(slot) === -1 && document.getElementById(slot)) {
                    refreshSlots.push(googleSlots[slot]);
                }
            }
            googletag.pubads().refresh(refreshSlots);
        </script><?php
    }
}

new Nethr_DFP();