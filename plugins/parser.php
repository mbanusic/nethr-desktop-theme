<?php
$tags = "
  googletag.cmd.push(function() {
    googletag.defineSlot('/1092744/super1/super1_300x250_v1', [[300, 600], [300, 250]], 'div-gpt-ad-1526017156184-0').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_300x250_v2', [[300, 600], [300, 250]], 'div-gpt-ad-1526017156184-1').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_300x250_v3', [[300, 600], [300, 250]], 'div-gpt-ad-1526017156184-2').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_billboard_v1', [[1024, 768], [1200, 500], [1024, 250], [970, 250], [1200, 250], [300, 250]], 'div-gpt-ad-1526017156184-3').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_billboard_v2', [[1000, 500], [970, 500], [1200, 500], [1024, 250], [1024, 500], [970, 250], [1200, 250], [300, 250]], 'div-gpt-ad-1526017156184-4').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_intext_v1', [[320, 200], [720, 250], [300, 250], [670, 350]], 'div-gpt-ad-1526017156184-5').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_wallpaper_left', [[200, 900], [300, 900], [300, 1050], [340, 1050]], 'div-gpt-ad-1526017156184-6').addService(googletag.pubads());
    googletag.defineSlot('/1092744/super1/super1_wallpaper_right', [[200, 900], [300, 900], [300, 1050], [340, 1050]], 'div-gpt-ad-1526017156184-7').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });";

$matches = [];
preg_match_all("/googletag\.defineSlot\(\'(.*)\'\,\s(.*?),\s*\'([^\']*)\'\)/", $tags, $matches);

$out = [];
foreach ($matches[1] as $key => $match) {
	$id = explode('/', $match);
	$id = $id[count($id)-1];
?>var <?php echo $id ?> = {
    a:2,
    b:13,
    c:"<?php echo $match ?>",
    e:<?php echo $matches[2][$key] ?>,
    g:"<?php echo $id ?>",
    j:1,
    k:0
};
<?php
}