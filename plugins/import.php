<?php

/**
 * Implements transfer command.
 */
class Nethr_Command extends WP_CLI_Command {
	private $odb = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 213.202.98.202)(PORT = 1521)))(CONNECT_DATA=(SID=portaldb)))";
	private $oconn;

	//private $dbh;

	public function __construct() {
		//add_action('init', array($this, 'init'));

		//$this->dbh = new PDO( "mysql:host=127.0.0.1;dbname=lee", 'lee', 'lee' );
		libxml_use_internal_errors(true);
		$this->oconn = oci_connect( "lee", "ele", $this->odb, 'AL32UTF8' );
	}

	function __destruct() {
		oci_close( $this->oconn );
		//$this->dbh = null;
	}

	function run() {
		//$this->get_articles();
		$this->assign_authors();
		$this->assing_coauthors();
		$this->pull_categories();
		$offset = 100;
		for ($i=0; $i<1000; $i = $i+$offset) {
			$this->parse_embeds($i, $offset);
			$this->replace_placeholders($i, $offset);
		}
	}

	/**
	 * @param int $typeid
	 * @param string $objid
	 *
	 * Types:
	 * 6 - article
	 * 4 - category
	 * 7 - image
	 * 32 - html
	 * 41 - gallery
	 *
	 * @return array
	 */
	private function get_objects_by_type( $objid, $typeid = 6 ) {
		$stid = oci_parse( $this->oconn, "SELECT * FROM LEE_OBJECTS WHERE TYPEID = :typeid AND OBJID like :objid AND STATUS = 2 AND LASTPUBLISHED IS NOT NULL" );
		oci_bind_by_name( $stid, ':typeid', $typeid);
		oci_bind_by_name( $stid, ':objid', $objid);
		oci_execute( $stid );
		$out = array();
		while ( $row = oci_fetch_array( $stid, OCI_ASSOC + OCI_RETURN_NULLS ) ) {
			$out[] = $row;
		}

		return $out;
	}

	function get_article($objid) {
		$stid = oci_parse( $this->oconn, "SELECT * FROM LO_ARTICLES WHERE OBJID = :objid" );
		oci_bind_by_name( $stid, ':objid', $objid);
		oci_execute( $stid );
		return oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS);
	}

	function embeds($id, $content) {
			$conns = $this->get_conns($id);

			foreach ($conns as $one) {
				switch ($one['TYPEID']) {
					case 32: //html
						$replace = $this->parse_embed( $this->parse_html($one['OBJID']) );
						break;
					case 41:
						$replace = '';
						break;
					case 7:
						//$replace = $this->parse_image($one['OBJID']);
						$replace = '';
						break;
					case 35:
						$replace = $this->parse_text($one['OBJID']);
						break;
					case 1:
						$replace = $this->parse_link($one['OBJID']);
						break;
					case 27:
						$replace = '';
						break;
					case 13:
						$replace = '';
						break;
					default:
						$replace = '';
				}

				$content = str_replace('<#o'.$one['ORDNUM'].'>',$replace, $content);
			}

		return $content;

	}

	function get_conns($objid) {
		$stid = oci_parse( $this->oconn,
			"SELECT OBJID, ORDNUM, TYPEID, NAME, SKINID FROM LEE_CONNS
			LEFT JOIN LEE_OBJECTS ON LEE_CONNS.TO_OBJID = LEE_OBJECTS.OBJID
			WHERE FROM_OBJID = :objid ORDER BY ORDNUM"
		);
		oci_bind_by_name( $stid, ':objid', $objid);
		oci_execute( $stid );
		$out = array();
		while ( $row = oci_fetch_array( $stid, OCI_ASSOC + OCI_RETURN_NULLS ) ) {
			$out[] = $row;
		}

		return $out;
	}

	/**
	 * Pulls articles.
	 *
	 * ## OPTIONS
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     wp transfer get_articles
	 *
	 * @synopsis <objid_from> <objid_to>
	 */
	function get_articles($args) {
		list( $objid_from, $objid_to ) = $args;
		global $wpdb;
		$type = 6;
		for ($i = intval($objid_from); $i<=intval($objid_to); $i++) {
		    if (substr($i, -2) == 32) {
		        $i += 69;
            }

			$objid = $i . '%';
			$rows = $this->get_objects_by_type( $objid, $type );
			WP_CLI::line($i . ' - ' . count($rows));
			foreach ( $rows as $row ) {
				$objid = $row['OBJID'];

				$article = $this->get_article( $objid );
				/*$conns = $this->get_conns($objid);
				$html = array();
				$gallery = array();
				$image = array();
				$other = array();
				$text = array();
				$link = array();
				foreach ($conns as $conn) {
					if ($conn['TYPEID'] == 32) {
						$html[] = $conn;
					}
					else if ($conn['TYPEID'] == 41) {
						$gallery[] = $conn;
					}
					else if ($conn['TYPEID'] == 7) {
						$image[] = $conn;
					}
					else if ($conn['TYPEID'] == 35) {
						$text[] = $conn;
					}
					else if ($conn['TYPEID'] == 1) {
						$link[] = $conn;
					}
					else if ($conn['TYPEID'] == 27 || $conn['TYPEID'] == 13) {

					}
					else {
						$other[] = $conn;
					}
				}
				try {
					var_dump($row['OBJID']);
					if (trim($article['ATEXT']) && $row['PUBLISHED_URL']) {
						$sth->execute( array(
							'objid'          => $objid,
							'post_title'     => $row['NAME'],
							'post_content'   => $article['LEAD'] ? '<h4>' . $article['LEAD'] . '</h4>' . $article['ATEXT'] : trim($article['ATEXT']),
							'post_overtitle' => $article['TITLE'],
							'post_name'      => $row['PUBLISHED_URL'],
							'post_date'      => $article['PUBLISH_DATE'],
							'post_category'  => $row['CATID'] . '/' . $row['LO_SITEID'],
							'post_author'    => $row['USERID'],
							'post_coauthor'  => $article['SIGNATURE'],
							'post_tags'      => $row['KEYWORDS'],
							'post_html'      => json_encode( $html ),
							'post_image'     => json_encode( $image ),
							'post_other'     => json_encode( $other ),
							'post_gallery'   => json_encode( $gallery ),
							'post_text'   => json_encode( $text ),
							'post_link'   => json_encode( $link )
						) );
					}
				} catch ( PDOException $e ) {
					var_dump( $e->getMessage() );
				}*/
				//var_dump($user_id);
				$urls = explode( '/', $row['PUBLISHED_URL'] );
				array_shift( $urls );
				$url = array_pop( $urls );
				//var_dump($url);
				$text = $this->embeds( $objid, $article['LEAD'] ? '<h4>' . $article['LEAD'] . '</h4>' . $article['ATEXT'] : trim( $article['ATEXT'] ) );
				if ($text) {
					$tags = explode( ', ', $row['KEYWORDS'] );
					$post =
						array(
							'content'   => $text,
							'slug'      => $url,
							'title'     => $row['NAME'],
							'status'    => 'publish',
							'type'      => 'post',
							'author'    => 'adriaticmedia',
							'excerpt'   => $article['LEAD'] ? $article['LEAD'] : ' ',
							'date'      => $article['PUBLISH_DATE'],
                            'publicize' => false,
                            'categories' => $urls,
                            'tags' => $tags,
                            'metadata' => array(
                                    'signature' => $row['SIGNATURE'],
                                'old_id' => $row['OBJID']
                            )
						);
					$this->post_to_vip($post);
				}
			}
		}
	}

	/**
	 * Deletes articles.
	 *
	 * ## OPTIONS
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     wp transfer get_articles
	 *
	 * @synopsis
	 */
	function truncate() {
		global $wpdb;
		$wpdb->query('TRUNCATE TABLE wp_posts');
		$wpdb->query('TRUNCATE TABLE wp_postmeta');
		echo "done";
	}

	function articles($args) {
		list( $objid_from, $objid_to ) = $args;
		global $wpdb;
		$type = 6;
		for ($i = intval($objid_from); $i<=intval($objid_to); $i++) {
			$objid = $i . '%';
			$rows = $this->get_objects_by_type( $objid, $type );
			foreach ( $rows as $row ) {
				$objid = $row['OBJID'];
				$article = $this->get_article( $objid );
				$user_id = $wpdb->get_var( 'SELECT new_id FROM authors WHERE old_id=' . $row['USERID'] );
				$urls = explode( '/', $row['PUBLISHED_URL'] );
				array_shift( $urls );
				$url = array_pop( $urls );
				$text = $this->embeds( $objid, $article['LEAD'] ? '<h4>' . $article['LEAD'] . '</h4>' . $article['ATEXT'] : trim( $article['ATEXT'] ) );
				var_dump($text);
				var_dump($row['NAME']);
				/*$id   = wp_insert_post(
					array(
						'post_content'   => $text,
						'post_name'      => $url,
						'post_title'     => $row['NAME'],
						'post_status'    => 'publish',
						'post_type'      => 'post',
						'post_author'    => $user_id,
						'ping_status'    => 'closed',
						'post_excerpt'   => $article['LEAD']?$article['LEAD']:' ',
						'post_date'      => $article['PUBLISH_DATE'],
						'comment_status' => 'closed',
					) );
				wp_set_object_terms( $id, $urls, 'category' );
				$tags = explode(', ', $row['KEYWORDS']);
				wp_set_object_terms( $id, $tags, 'post_tag' );
				update_post_meta( $id, 'old_id', $objid );
				var_dump( $id );*/
			}
		}
		wp_mail('mbanusic@gmail.com', 'Complete '.$objid_from.' - '.$objid_to, 'Done');
	}

	/**
	 * Delete articles.
	 *
	 * ## OPTIONS
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     wp transfer delete_articles
	 *
	 * @synopsis <objid>
	 */
	function delete_articles($args) {
		global $wpdb;
		list($objid) = $args;
		$objid = '"'.$objid.'%"';
		$rows = $wpdb->get_results('SELECT post_id from wp_postmeta WHERE meta_key="old_id" AND meta_value LIKE '.$objid, ARRAY_A);
		foreach ($rows as $one) {
			wp_delete_post($one['post_id'], true);
			var_dump($one['post_id']);
		}
	}

	/**
	 * Delete date.
	 *
	 * ## OPTIONS
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     wp transfer delete_date
	 *
	 * @synopsis <objid>
	 */
	function delete_date($args) {
		list( $from, $to ) = $args;

		$q = new WP_Query( array(
			'post_per_page' => 100,
			'date_query' => array(
				array(
					'after'     => $from,
					'before'    => $to,
					'inclusive' => true,
				),
			),
		) );
		while ($q->have_posts()) {
			$q->the_post();
			var_dump($q->post->ID);
			wp_delete_post($q->post->ID);
		}
	}

	function assign_authors() {
		global $wpdb;
		$rows = $wpdb->get_results('SELECT wp_users.ID, wp_users.user_login, authors.new_username FROM authors LEFT JOIN wp_users ON authors.new_username = wp_users.user_login', ARRAY_A);
		foreach ($rows as $row) {
			$wpdb->update('authors', array('new_id' => $row['ID']), array('new_username' => $row['new_username']));
		}
	}

	function parse_embeds($start, $offset) {
		try {
			$sth   = $this->dbh->prepare( 'SELECT * FROM posts LIMIT :start, :offset' );
			$sth->bindParam('start', $start, PDO::PARAM_INT);
			$sth->bindParam('offset', $offset, PDO::PARAM_INT);
			$sth->execute();
			$row = $sth->fetch( PDO::FETCH_ASSOC );
			while ( $row = $sth->fetch( PDO::FETCH_ASSOC ) ) {
				$out = array();
				$embed = false;
				$html = json_decode($row['post_html'], true);
				if (!empty($html)) {
					foreach ($html as $one) {
						$text = $this->parse_html( $one['OBJID'] );
						$src = $this->parse_embed($text);
						if ($src) {
							$one['link_type'] = $src['type'];
							$one['link'] = $src['link'];
						}
						else {
							$one['TEXT'] = $text;
							$embed = true;
						}
						$out[$one['ORDNUM']] = $one;
					}
				}
				$texts = json_decode($row['post_text'], true);
				if (!empty($texts)) {
					foreach ($texts as $one) {
						$text= $this->parse_text($one['OBJID']);
						$one['TEXT'] = $text;
						$out[$one['ORDNUM']] = $one;
					}
				}
				$images = json_decode($row['post_image'], true);
				if (!empty($images)) {
					foreach ($images as $one) {
						$image = $this->parse_image( $one['OBJID'], intval( $one['SKINID'] ) );
						if ( $image ) {
							$one['TEXT']           = $image;
							$out[ $one['ORDNUM'] ] = $one;
						}
					}
				}

				$links = json_decode($row['post_link'], true);
				if (!empty($links)) {
					foreach ($links as $one) {
						$link = $this->parse_link($one['OBJID']);
						$one['TEXT'] = $link;
						$out[$one['ORDNUM']] = $one;
					}
				}
				$s = $this->dbh->prepare('UPDATE posts SET embeds=:embeds, neznan=:neznan WHERE id=:id');
				$s->execute(array(
					'id' => $row['id'],
					'embeds' => json_encode($out),
					'neznan' => intval($embed)
				));
			}
		}
		catch (PDOException $e) {
			var_dump($e->getMessage());
		}

	}

	function parse_embed($text) {
		if (!$text)
			return;
		$dom = new DOMDocument();

		$dom->loadHTML($text);
		$lst = $dom->getElementsByTagName('iframe');
		if ($lst->length) {
			$iframe = $lst->item( 0 );
			$src    = $iframe->attributes->getNamedItem( 'src' )->value;
			//switch $src
			if ( strpos( $src, 'youtube' ) ) {
				return $src;
			}
			if ( strpos( $src, 'dailymotion' ) ) {
				return $src;
			}
		}



		$lst = $dom->getElementsByTagName('a');
		if ($lst->length) {
			$hrefs = $lst->item(0);
			$href = $hrefs->attributes->getNamedItem('href')->value;

			if (strpos($href,'instagram')) {
				return $href;
			}
			if (strpos($href,'facebook')) {
				return $href;
			}

			//twitter
			$hrefs = $lst->item($lst->length-1);
			$href = $hrefs->attributes->getNamedItem('href')->value;
			if (strpos($href,'twitter')) {
				return  $href;
			}
		}
		return $text;
	}

	function parse_html($objid) {
		$stid = oci_parse( $this->oconn, "SELECT * FROM LO_HTML WHERE OBJID = :objid" );
		oci_bind_by_name( $stid, ':objid', $objid);
		oci_execute( $stid );
		$row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS);
		return $row['TEXT'];
	}

	function parse_link($objid) {
		$stid = oci_parse( $this->oconn, "SELECT * FROM LO_LINKS WHERE OBJID = :objid" );
		oci_bind_by_name( $stid, ':objid', $objid);
		oci_execute( $stid );
		$row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS);
		return $row['URL'];
	}

	function parse_text($objid) {
		$stid = oci_parse( $this->oconn, "SELECT * FROM LO_TEXT WHERE OBJID = :objid" );
		oci_bind_by_name( $stid, ':objid', $objid);
		oci_execute( $stid );
		$row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS);
		return $row['TEXT'];
	}

	function parse_image($objid) {
		$stid = oci_parse( $this->oconn, 'select lo_images.objid, lo_images."SOURCE", photo_instance."PATH", PHOTO_INSTANCE.FILENAME, photo."NAME" from LO_IMAGES
left join photo_instance on photo_instance.LO_IMAGE_ID = lo_images.objid
left join photo on PHOTO.oid = PHOTO_INSTANCE.photo_id
where lo_images.OBJID = :objid and format_id = :format' );
		oci_bind_by_name( $stid, ':objid', $objid);
		$format = 48;
		oci_bind_by_name( $stid, ':format', $format);
		oci_execute( $stid );
		$row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS);
		if ($row) {
			if ( $row['PATH'] ) {
				$p           = substr( $row['PATH'], - 10 );
				return 'http://www1.net.hr/' . $p . '/' . $row['FILENAME'];

				return $row;
			}
			else
				return false;
		}
		else {
			oci_free_statement($stid);
			$stid = oci_parse( $this->oconn, 'select lo_images.objid, lo_images."SOURCE", photo_instance."PATH", PHOTO_INSTANCE.FILENAME, photo."NAME" from LO_IMAGES
left join photo_instance on photo_instance.LO_IMAGE_ID = lo_images.objid
left join photo on PHOTO.oid = PHOTO_INSTANCE.photo_id
where lo_images.OBJID = :objid and format_id = :format' );
			oci_bind_by_name( $stid, ':objid', $objid);
			$format = 63;
			oci_bind_by_name( $stid, ':format', $format);
			oci_execute( $stid );
			$row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS);
			if ($row) {
				if ( $row['PATH'] ) {
					$p           = substr( $row['PATH'], - 10 );
					 'http://www1.net.hr/' . $p . '/' . $row['FILENAME'];

					return $row;
				}
				else
					return false;
			}
		}
	}

	function pull_authors() {
		$stid = oci_parse( $this->oconn,
			"SELECT * FROM users"
		);
		oci_execute( $stid );
		$out = array();
		while ( $row = oci_fetch_array( $stid, OCI_ASSOC + OCI_RETURN_NULLS ) ) {
			$sth = $this->dbh->prepare('
					INSERT INTO authors(name, username, old_id)
					            VALUES (:name, :username, :old_id)
				');
			$sth->execute(array(
				'name' => $row['NAME'],
				'username' => $row['LOGIN'],
				'old_id' => $row['USERID']
			));
		}
	}

	function pull_categories() {
		$sth = $this->dbh->prepare('SELECT * FROM posts');
		$sth->execute();
		while ( $row = $sth->fetch( PDO::FETCH_ASSOC ) ) {
			//var_dump($row);
			$id = $row['id'];
			$items = explode('/', $row['post_name']);
			$link = array_pop($items);
			$cat = array_pop($items);
			$s = $this->dbh->prepare('UPDATE posts SET post_real_category=:cat, post_real_name=:link where id=:id');
			$s->execute(array( 'id' => $id, 'cat' => $cat, 'link' => $link));
		}
	}

	function assign_authors1() {
		$sth = $this->dbh->prepare('SELECT posts.id, authors.new_username FROM posts left join authors on posts.post_author = authors.old_id');
		$sth->execute();
		while ( $row = $sth->fetch( PDO::FETCH_ASSOC ) ) {
			//var_dump($row);
			$id = $row['id'];
			$author = $row['new_username'];
			$s = $this->dbh->prepare('UPDATE posts SET post_real_author=:author where id=:id');
			$s->execute(array( 'id' => $id, 'author' => $author));
		}
	}

	function assing_coauthors() {
		$sth = $this->dbh->prepare('SELECT id, post_coauthor FROM posts');
		$sth->execute();
		while ( $row = $sth->fetch( PDO::FETCH_ASSOC ) ) {
			//var_dump($row);
			$id = $row['id'];
			if (false !== strpos(strtolower($row['post_coauthor']), 'danas')) {
				$author = 'danas-hr';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'hot')) {
				$author = 'hot-hr';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'magazin')) {
				$author = 'magazin-hr';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'netmobil')) {
				$author = 'netmobil-hr';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'sportski')) {
				$author = 'sportski-net';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'tehnoklik')) {
				$author = 'tehnoklik-hr';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'webcafe')) {
				$author = 'webcafe-hr';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'hina')) {
				$author = 'hina';
			}
			else if (false !== strpos(strtolower($row['post_coauthor']), 'net.hr')) {
				$author = 'net-hr';
			}
			else {
				$author = '';
			}
			if ($author) {
				$s = $this->dbh->prepare( 'UPDATE posts SET post_real_coauthor=:author where id=:id' );
				$s->execute( array( 'id' => $id, 'author' => $author ) );
			}
		}
	}

	function replace_placeholders($start, $offset) {
		$s = $this->dbh->prepare('SELECT * FROM posts WHERE post_gallery="[]" LIMIT :start, :offset');
		$s->bindParam('start', $start, PDO::PARAM_INT);
		$s->bindParam('offset', $offset, PDO::PARAM_INT);
		$s->execute();
		while ($row = $s->fetch(PDO::FETCH_ASSOC)) {
			$embeds = json_decode($row['embeds'],true);
			$text = $row['post_content'];
			foreach ($embeds as $key=>$value) {

				switch ($value['TYPEID']) {
					case '35':
						$text = str_replace('<#o'.$key.'>', '[nethr_light_box]'.$value['TEXT'].'[/nethr_light_box]', $text);
						break;
					case '32':
						if (isset($value['link']) && $value['link'] ) {
							$text = str_replace('<#o'.$key.'>', $value['link'], $text);
						}
						else {
							$text = str_replace('<#o'.$key.'>', $value['TEXT'], $text);
						}
						break;
					case '1':
						$text = str_replace('<#o'.$key.'>', '<a href="'.$value['link'].'">Link</a>', $text);
						break;
				}
			}
			$a = $this->dbh->prepare('UPDATE posts SET post_content=:text where id=:id');
			$a->execute(array(
				'id' => $row['id'],
				'text' => $text
			));
		}
	}

	function export() {
		header('Content-Type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>'."\n"; ?><rss version="2.0"
		                                                            xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
		                                                            xmlns:content="http://purl.org/rss/1.0/modules/content/"
		                                                            xmlns:wfw="http://wellformedweb.org/CommentAPI/"
		                                                            xmlns:dc="http://purl.org/dc/elements/1.1/"
		                                                            xmlns:wp="http://wordpress.org/export/1.2/"
			>
		<channel>
			<title>Net.hr</title>
			<link>http://www1.net.hr</link>
			<description>Tu počinje internet</description>
			<pubDate><?php echo date( 'D, d M Y H:i:s +0000' ); ?></pubDate>
			<language>hr-HR</language>
			<wp:wxr_version>1.2</wp:wxr_version>
			<wp:base_site_url>http://www1.net.hr</wp:base_site_url>
			<wp:base_blog_url>http://www1.net.hr</wp:base_blog_url><?php /* foreach ( $cats as $c ) : ?>
				<wp:category><wp:term_id><?php echo $c->term_id ?></wp:term_id><wp:category_nicename><?php echo $c->slug; ?></wp:category_nicename><wp:category_parent><?php echo $c->parent ? $cats[$c->parent]->slug : ''; ?></wp:category_parent><?php wxr_cat_name( $c ); ?><?php wxr_category_description( $c ); ?></wp:category>
			<?php endforeach; ?>
			<?php foreach ( $tags as $t ) : ?>
				<wp:tag><wp:term_id><?php echo $t->term_id ?></wp:term_id><wp:tag_slug><?php echo $t->slug; ?></wp:tag_slug><?php wxr_tag_name( $t ); ?><?php wxr_tag_description( $t ); ?></wp:tag>
			<?php endforeach; ?>
			<?php foreach ( $terms as $t ) : ?>
				<wp:term><wp:term_id><?php echo $t->term_id ?></wp:term_id><wp:term_taxonomy><?php echo $t->taxonomy; ?></wp:term_taxonomy><wp:term_slug><?php echo $t->slug; ?></wp:term_slug><wp:term_parent><?php echo $t->parent ? $terms[$t->parent]->slug : ''; ?></wp:term_parent><?php wxr_term_name( $t ); ?><?php wxr_term_description( $t ); ?></wp:term>
			<?php endforeach; */ ?><?php $s = $this->dbh->prepare('SELECT * FROM posts WHERE post_gallery="[]" AND neznan = 1 AND objid LIKE "20150601%" LIMIT 5');

			$s->execute();
			while ($row = $s->fetch(PDO::FETCH_ASSOC)) {
				$this->export_item($row);
			}
			?></channel></rss><?php
	}

	function mysql2date( $format, $date, $translate = true ) {
		if ( empty( $date ) )
			return false;

		if ( 'G' === $format )
			return strtotime( $date . ' +0000' );

		$i = strtotime( $date );

		if ( 'U' === $format )
			return $i;

		return date( $format, $i );
	}

	function export_item($item) {
		?><item>
		<title><?php echo $item['post_title'] ?></title>
		<link>http://www.net.hr<?php echo $item['post_name']; ?></link>
		<pubDate><?php echo $this->mysql2date( 'D, d M Y H:i:s +0000', $item['post_date'], false ); ?></pubDate>
		<dc:creator><![CDATA[<?php echo $item['post_real_author'] ?>]]></dc:creator>
		<guid isPermaLink="false">http://www.net.hr<?php  echo $item['post_name']; ?></guid>
		<description></description>
		<content:encoded><![CDATA[<?php echo $item['post_content'] ?>]]></content:encoded>
		<excerpt:encoded><![CDATA[]]></excerpt:encoded>
		<wp:post_id><?php echo $item['objid']; ?></wp:post_id>
		<wp:post_date><?php echo $item['post_date']; ?></wp:post_date>
		<wp:post_date_gmt><?php echo $item['post_date']; ?></wp:post_date_gmt>
		<wp:comment_status>closed</wp:comment_status>
		<wp:ping_status>closed</wp:ping_status>
		<wp:post_name><?php echo $item['post_real_name'] ?></wp:post_name>
		<wp:status>draft</wp:status>
		<wp:post_parent>0</wp:post_parent>
		<wp:menu_order>0</wp:menu_order>
		<wp:post_type>post</wp:post_type>
		<wp:post_password></wp:post_password>
		<wp:is_sticky>0</wp:is_sticky><?php /*	if ( $post->post_type == 'attachment' ) : ?>
		<wp:attachment_url><?php echo wp_get_attachment_url( $post->ID ); ?></wp:attachment_url>
<?php 	endif; */ ?><category domain="author" nicename="cap-<?php echo $item['post_real_coauthor'] ?>"><![CDATA[<?php echo $item['post_real_coauthor'] ?>]]></category>
	<category domain="category" nicename="<?php echo $item['post_real_category'] ?>"><![CDATA[<?php echo $item['post_real_category'] ?>]]></category><?php $tags = explode(',',$item['post_tags']);
		foreach ($tags as $tag) {
			$t = str_replace(' ', '-', trim($tag));
			?><category domain="post_tag" nicename="<?php echo $t ?>"><![CDATA[<?php echo $tag ?>]]></category><?php
		}
		?><wp:postmeta>
		<wp:meta_key>extra_titles</wp:meta_key>
		<wp:meta_value><![CDATA[<?php echo serialize(array('over_title'=>$item['post_overtitle'], 'short_title'=>'', 'sticker' => '')) ?>]]></wp:meta_value>
		</wp:postmeta><?php /* <wp:postmeta>
			<wp:meta_key>_thumbnail_id</wp:meta_key>
			<wp:meta_value><![CDATA[13]]></wp:meta_value>
		</wp:postmeta> */ ?></item><?php
	}

	function post_to_vip($post) {
	    $r = wp_remote_post('https://public-api.wordpress.com/rest/v1.1/sites/90557224/posts/new', array(
		    'timeout'     => 45,
	            'headers' => array(
	                    'Authorization' => 'Bearer vjAH5wZKTthQ#)jSZaUu#QiD@GwsOzO9ClAo^)I^gYZ6Re3FsCaIfSg5t3(#!Ck8'
                ),
            'body' => $post
        ));
	    if (is_wp_error($r)) {
	        WP_CLI::error($r);
        }
        else {
	        WP_CLI::line( json_decode($r['body'], true)['ID'] );
        }
    }

}

WP_CLI::add_command( 'transfer', 'Nethr_Command' );

