<?php

add_filter( 'sharing_services', 'nethr_sharing_services' );

function nethr_sharing_services( $services ) {
	$services['messenger'] = 'Nethr_Share_Messenger';
	if ( jetpack_is_mobile() || is_admin() ) {
		$services['whatsapp'] = 'Nethr_Share_Whatsapp';
		$services['viber']    = 'Nethr_Share_Viber';
	}
	return $services;
}

class Nethr_Share_Messenger extends Sharing_Source {
	var $shortname = 'messenger';
	var $genericon = '\f204';

	public function __construct( $id, array $settings ) {
		parent::__construct( $id, $settings );
	}

	public function get_name() {
		return 'Messenger';
	}

	public function get_display( $post ) {
		$share_url = $this->get_share_url( $post->ID );
		return '<div class="fb-send" data-href="' . esc_attr( $share_url ) . '"></div>';
	}
}

class Nethr_Share_Whatsapp extends Sharing_Source {
	var $shortname = 'whatsapp';

	public function __construct( $id, array $settings ) {
		parent::__construct( $id, $settings );
	}

	public function get_name() {
		return 'Whatsapp';
	}

	public function get_display( $post ) {
		$share_url = wp_get_shortlink( $post->ID );
		$share_title = $this->get_share_title( $post->ID );

		return '<a id="whatsapp" class="link" href="whatsapp://send?text=' . rawurlencode($share_title . ' - ' . $share_url ) . '"><i class="fa fa-whatsapp"></i> WhatsApp</a>';

	}
}

class Nethr_Share_Viber extends Sharing_Source {
	var $shortname = 'viber';

	public function __construct( $id, array $settings ) {
		parent::__construct( $id, $settings );
	}

	public function get_name() {
		return 'Viber';
	}

	public function get_display( $post ) {
		$share_url = wp_get_shortlink( $post->ID );
		$title = $this->get_share_title( $post->ID );

		return '<a id="viber" class="link" href="viber://forward?text="' . rawurlencode( $title . ' - ' . $share_url ) . '"><i class="ico ico-viber"></i> Viber</a>';

	}
}