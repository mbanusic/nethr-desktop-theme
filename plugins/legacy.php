<?php

class Nethr_Legacy {
	public function __construct() {
		add_action( 'init', array( $this, 'nethr_legacy_iframe' ) );
	}

	function nethr_legacy_iframe() {
		$request = $_SERVER['REQUEST_URI'];
		if ( '/iframe/header.html' === $request ) {
			header( 'Content-Type: text/html' );
			?>
			<!DOCTYPE HTML>
			<html>
			<head>
				<meta charset="utf-8">
				<link rel="stylesheet" href="<?php echo esc_url( wpcom_vip_theme_url( 'plugins/legacy.css', 'adriaticmedia-nethr' ) ); ?>">
				<base target="_parent" />
			</head>
			<body>

			<header class="home">

				<div class="top-navbar" style="margin-top: 0px;">
					<div class="container cf">

						<div class="top-menu">
							<?php wp_nav_menu( array( 'theme_location' => 'header' ) ); ?>
						</div>

					</div>
				</div>
			</header>

			</body>
			</html>
<?php
			exit();
		}
	}
}

new Nethr_Legacy();