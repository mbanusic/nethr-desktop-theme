<?php

class AdriaticMedia_Embeds {
	public function __construct() {
		wp_embed_register_handler( 'adriaticmedia_jwplayer', '#http:\/\/dashboard\.jwplatform\.com\/videos\/(.*?)[\/]?$#i', array( $this, 'jwplayer' ) );
		wp_embed_register_handler( 'adriaticmedia_jwplayer_playlist', '#http:\/\/dashboard\.jwplatform\.com\/channels\/(.*?)[\/]?$#i', array( $this, 'jwplayer' ) );
		wp_embed_register_handler( 'adriaticmedia_video', '#http:\/\/video\.adriaticmedia\.hr\/videos\/(.*?)[\/]?$#i', array( $this, 'video' ) );
		wp_embed_register_handler( 'adriaticmedia_galleries', '#http:\/\/net\.hr\/(.*)#i', array( $this, 'galleries') );
		wp_embed_register_handler( 'adriaticmedia_vidme', '#https:\/\/vid\.me\/(.*)#i', array( $this, 'vidme' ) );
		wp_embed_register_handler( 'adriaticmedia_streamable', '#https:\/\/streamable.com\/(.*)#i', array( $this, 'streamable' ) );
		wp_embed_register_handler( 'adriaticmedia_vkcom', '#http://vk.com/video(.*)_(.*)?hash=(.*)#i', array( $this, 'vkcom' ) );
		wp_embed_register_handler( 'adriaticmedia_ooyala', '#http://player.ooyala.com/iframe.html(.*)#i', array( $this, 'ooyala' ) );
		wp_embed_register_handler( 'adriaticmedia_playwire', '#http://config.playwire.com/(.*)/videos/v2/(.*)/zeus.json#i', array( $this, 'playwire' ) );
		wp_embed_register_handler( 'adriaticmedia_sporttube', '#https://www.sporttube.com/play/(.*)#i', array( $this, 'sporttube' ) );
		wp_embed_register_handler( 'adriaticmedia_rtl', '#https://(vijesti|zivotistil|tabloid).rtl.hr/video/(.*)#i', array( $this, 'rtl' ) );

	}

	function jwplayer( $matches, $attr, $url, $rawattr ) {
		$video = $matches[1];
		ob_start();
		?>
		<video id="<?php echo esc_attr( 'v-' . $video ) ?>" class="video-js vjs-default-skin" controls preload="auto" width="670" height="377">
			<source src="<?php echo esc_url( 'https://video.adriaticmedia.hr/originals/jw/videos/' . $video . '.mp4' ) ?>" type="video/mp4" />
		</video>
		<?php

		return ob_get_clean();
	}

	function video( $matches, $attr, $url, $rawattr ) {
		$video = $matches[1];
		ob_start();
		?>
		<video id="<?php echo esc_attr( 'v-' . $video ) ?>" class="video-js vjs-default-skin" controls preload="auto" width="670" height="377" poster="https://video.adriaticmedia.hr/thumbnails/<?php echo rawurlencode( $video ) ?>.jpg" data-setup="{}">
			<source src="<?php echo esc_url( 'https://video.adriaticmedia.hr/videos/' . $video . '.mp4') ?>" type="video/mp4" />
		</video>
		<?php

		return ob_get_clean();
	}

	// easier for users to paste only url to the gallery
	function galleries( $matches, $attr, $url, $rawattr ) {
		$post_id = wpcom_vip_url_to_postid( $url );
		$post_format = get_post_format( $post_id );
		if ( 'gallery' === $post_format ) {
			return '[nethr_galerija url="'. esc_url( $url ) .'" id="'. intval( $post_id ) .'"]';
		}
		return esc_url( $url );
	}

	function vidme( $matches, $attr, $url, $rawattr ) {
		return '<iframe src="'. esc_url( 'https://vid.me/e/' . $matches[1] ) . '" width="656" height="365" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen scrolling="no"></iframe>';
	}

	function streamable( $matches, $attr, $url, $rawattr ) {
		return '<div style="width: 100%; height: 0px; position: relative; padding-bottom: 56.250%;"><iframe src="'. esc_url( 'https://streamable.com/e/'. $matches[1] . '?logo=0' ) . '" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen scrolling="no" style="width: 100%; height: 100%; position: absolute;"></iframe></div>';
	}
	function vkcom( $matches, $attr, $url, $rawattr ) {
		$width = Jetpack::get_content_width();
		$height = $width/1.777;

		return '<iframe src="' . esc_url( 'http://vk.com/video_ext.php?oid=' . $matches[1] . '&id=' . $matches[2] . '&hash=' . $matches[3] . '&hd=3' ) . '" width="' . esc_attr( $width ) . '" height="' . esc_attr( $height ) . '" frameborder="0"></iframe>';

	}
	function ooyala( $matches, $attr, $url, $rawattr ) {
		$width = Jetpack::get_content_width();
		$height = $width/1.777;

		return '<script height="' . esc_attr( $height ) . '" width="' . esc_attr( $width ) . '" src="' . esc_url( str_replace('.html', '.js', $matches[0] ) ) . '"></script>';

	}

	function playwire( $matches, $attr, $url, $rawattr ) {

		return '<script data-config="' . esc_url( $url ) . '" data-height="100%" data-width="100%" src="//cdn.playwire.com/bolt/js/zeus/embed.js" type="text/javascript"></script>';

	}

	function sporttube( $matches, $attr, $url, $rawattr ) {
		$width = Jetpack::get_content_width();
		$height = $width/1.777;
		return '<iframe src="'. esc_url( 'https://cc.sporttube.com/embed/' . $matches[1] ) . '" frameborder="0" allowfullscreen width="' . esc_attr( $width ) . '" height="' . esc_attr( $height ) . '"></iframe>';
	}

	function rtl($matches, $attr, $url, $rawattr) {
	    $url = str_replace('/video/', '/embed/video/', $url);
	    return '<iframe frameborder="0" width="660" height="350" src="'. esc_url($url) . '?autoplay=true&src=net_hr" ></iframe>';
    }


}

new AdriaticMedia_Embeds();