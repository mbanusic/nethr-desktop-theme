<?php

class Nethr_Term_Meta {
	public function __construct() {
		add_action( 'init', array( $this, 'register' ) );
		add_action( 'category_add_form_fields', array( $this, 'new_term_color_field' ) );
		add_action( 'category_edit_form_fields', array( $this, 'edit_term_color_field' ) );
		add_action( 'edit_category', array( $this, 'save_term_color' ) );
		add_action( 'create_category', array( $this, 'save_term_color' ) );
	}

	function register() {
		register_meta( 'term', 'color', array( $this, 'sanitize_color' ) );
	}

	function sanitize_color( $color ) {
		return preg_match( '/#([A-Fa-f0-9]{6})$/', $color ) ? $color : '';
	}

	function new_term_color_field() {

		wp_nonce_field( basename( __FILE__ ), 'nethr_term_color_nonce' ); ?>

		<div class="form-field nethr-term-color-wrap">
			<label for="nethr-term-color">Boja:</label>
			<input type="color" name="nethr_term_color" id="nethr-term-color" value="#f95905" class="nethr-color-field" />
		</div>
	<?php
	}

	function edit_term_color_field( $term ) {

		$default = '#f95905';
		$color   = $this->sanitize_color( get_term_meta( $term->term_id, 'color', true ) );

		if ( ! $color )
			$color = $default; ?>

		<tr class="form-field nethr-term-color-wrap">
			<th scope="row"><label for="nethr-term-color">Boja:</label></th>
			<td>
				<?php wp_nonce_field( basename( __FILE__ ), 'nethr_term_color_nonce' ); ?>
				<input type="color" name="nethr_term_color" id="nethr-term-color" value="<?php echo esc_attr( $color ); ?>" class="nethr-color-field" />
			</td>
		</tr>
	<?php
	}

	function save_term_color( $term_id ) {

		if ( !current_user_can( 'manage_categories', $term_id ) ) {
			return;
		}

		if ( ! isset( $_POST['nethr_term_color_nonce'] ) || ! wp_verify_nonce( $_POST['nethr_term_color_nonce'], basename( __FILE__ ) ) )
			return;

		$old_color = $this->sanitize_color( get_term_meta( $term_id, 'color', true ) );
		$new_color = isset( $_POST['nethr_term_color'] ) ? $this->sanitize_color( sanitize_text_field( $_POST['nethr_term_color'] ) ) : '';

		if ( $old_color && '' === $new_color ) {
			delete_term_meta( $term_id, 'color' );
		}
		else if ( $old_color !== $new_color ) {
			update_term_meta( $term_id, 'color', $new_color );
		}
	}
}

new Nethr_Term_Meta();

