<?php

class Nethr_Analytics {
	public function __construct() {
		add_action( 'wp_head', array( $this, 'analytics' ) );
		add_action( 'nethr_body_start', array( $this, 'facebook' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'scroll_depth' ) );
	}

	function switch_category( $cat ) {
		$out = array(
			'gemius' => '15aQAmMQW9iFAGZeG8KJQKei.MhIw4xDi6L09YLL4Nr.q7',
			'dot'    => 208
		);
		switch ( $cat ) {
			case 'danas':
				$out = array(
					'gemius' => 'nXqV7XtpEXOQIx38wEzRsXXpnF8NTsv7Fip7GkGdQvv.z7',
					'dot'    => 318
				);
				break;
			case 'hot':
				$out = array(
					'gemius' => '1vaQcmOQWxi6K7AjH8uNw.UIPxNFVbr77aHq3.3h5ar.k7',
					'dot'    => 322
				);
				break;
			case 'magazin':
				$out = array(
					'gemius' => 'bI3q_JfCd5FaQIKnWaaA0rRI7H8KR_v3_75RfZ7ryGz.f7',
					'dot'    => 321
				);
				break;
			case 'auto':
				$out = array(
					'gemius' => 'AqKapq7mN3E10SxClt0QhZe..M5I04vHm.Yx1Lf8zEL.k7',
					'dot'    => 210
				);
				break;
			case 'sport':
				$out = array(
					'gemius' => 'nLuQdAfdW0_PAguxFERnKqe0.jhIlweLweu_OyYtoMD.l7',
					'dot'    => 320
				);
				break;
			case 'tehnoklik':
				$out = array(
					'gemius' => 'd60wtINmS866bzat56gBD4ZQTGPdiKx67QTuhdsqIyb.97',
					'dot'    => 209
				);
				break;
			case 'webcafe':
				$out = array(
					'gemius' => '15zrO2LMrMC1zMwT84WfXZdSrrJIPObwwUc__i5dB.3.r7',
					'dot'    => 319
				);
				break;
			case 'zena':
				$out = array(
					'gemius' => 'bDtAfZihq7mKVtuvaRUbpYYg.BjdeIypvYkx43OrtHv.l7'
				);
		}

		return $out;
	}

	function switch_category_mobile( $cat ) {
		$out = [
            'gemius' => '1vOa2GO_N_iAmIbtp386d2X17MQNiexVgwkUi9a.oBT.g7',
            'dot' => 208
        ];

        switch ( $cat ) {
            case 'danas':
                $out = array(
                    'gemius' => 'bVeQ1ArdW89LuLBIHSMNrpQJTM46IKyGkuVkXyGw_L3.17',
                    'dot'    => 318
                );
                break;
            case 'hot':
                $out = array(
                    'gemius' => '1vM6FmO_JzAqqB1mp7jFkrQG.h4KkwbPa.RRy8Sbdbj.R7',
                    'dot'    => 322
                );
                break;
            case 'magazin':
                $out = array(
                    'gemius' => '1vM6uGO_J_eKRB12j_1NIbQG7MQKk_xVMny_u3DpCvT.Z7',
                    'dot'    => 321
                );
                break;
            case 'auto':
                $out = array(
                    'gemius' => 'bVeadgrdNzDr4k28fUYNdZQJ7Do6IOw5MiyeUCm6yzT.F7',
                    'dot'    => 210
                );
                break;
            case 'sport':
                $out = array(
                    'gemius' => '11MwtGMuS83g_TatjzeJ7mZVTM4NSayGg5juxtXfI3z.u7',
                    'dot'    => 320
                );
                break;
            case 'tehnoklik':
                $out = array(
                    'gemius' => 'bVeQ0grdW5fly2XFFJZJEZQJXmc6IMcfaGShX8vUpc3.I7',
                    'dot'    => 209
                );
                break;
            case 'webcafe':
                $out = array(
                    'gemius' => '11MwsmMuS5bgmTaPx_IBjmZVXmcNSccfTSEnG7NXJef.b7',
                    'dot'    => 319
                );
                break;
            case 'zena':
                $out = array(
                    'gemius' => 'ctVAe0_pq4K16xTVzgBIbZR7..k6tIyZmB8xBYwmN.X.n7'
                );
        }
		return $out;
	}

	function analytics() {
		$gemius = '15aQAmMQW9iFAGZeG8KJQKei.MhIw4xDi6L09YLL4Nr.q7';
		$dot = 208;
		if ( jetpack_is_mobile() ) {
			$gemius = '1vOa2GO_N_iAmIbtp386d2X17MQNiexVgwkUi9a.oBT.g7';
			if ( is_archive() ) {
				if ( is_post_type_archive('webcafe') ) {
					$gemius = '11MwsmMuS5bgmTaPx_IBjmZVXmcNSccfTSEnG7NXJef.b7';
				}
				else if ( is_post_type_archive( 'zena' ) || is_tax( 'zena-cat' ) ) {
					$gemius = 'ctVAe0_pq4K16xTVzgBIbZR7..k6tIyZmB8xBYwmN.X.n7';
				}
				else if ( is_category() ) {
					$cat    = get_category( get_query_var( 'cat' ) );
					while ( $cat->parent ) {
						$cat = get_category( $cat->parent );
					}
                    $out    = $this->switch_category_mobile( $cat->slug );
                    $gemius = $out['gemius'];
                    $dot    = $out['dot'];
				}
			}
			else if ( is_single() ) {
				if ( 'webcafe' === get_post_type() ) {
					$gemius = '11MwsmMuS5bgmTaPx_IBjmZVXmcNSccfTSEnG7NXJef.b7';
                    $dot    = 319;
				}
				else if ( 'zena' === get_post_type() ) {
					$gemius = 'ctVAe0_pq4K16xTVzgBIbZR7..k6tIyZmB8xBYwmN.X.n7';
                    $dot = 3930;
				}
				else if ( 'igre' === get_post_type() ) {
				    $dot = 3931;
                }
				else {
					$cat    = nethr_get_top_category();
                    $out    = $this->switch_category_mobile( $cat->slug );
                    $gemius = $out['gemius'];
                    $dot    = $out['dot'];
				}
			}
			if ( is_page( 'zena' ) ) {
				$gemius = 'ctVAe0_pq4K16xTVzgBIbZR7..k6tIyZmB8xBYwmN.X.n7';
                $dot = 3930;
			}

			if (is_page('igrice')) {
			    $dot = 3931;
            }
		}
		else {
			if ( is_archive() ) {
				if ( is_post_type_archive( 'webcafe' ) ) {
					$gemius = '15zrO2LMrMC1zMwT84WfXZdSrrJIPObwwUc__i5dB.3.r7';
					$dot    = 319;
				}
				else if ( is_post_type_archive( 'zena' ) || is_tax( 'zena-cat' ) ) {
					$gemius = 'bDtAfZihq7mKVtuvaRUbpYYg.BjdeIypvYkx43OrtHv.l7';
				}
				else if ( is_category() ) {
					$cat    = get_category( get_query_var( 'cat' ) );
					while ( $cat->parent ) {
						$cat = get_category( $cat->parent );
					}
					$out    = $this->switch_category( $cat->slug );
					$gemius = $out['gemius'];
					$dot    = $out['dot'];
				}
			} else if ( is_single() ) {
				if ( 'webcafe' === get_post_type() ) {
					$gemius = '15zrO2LMrMC1zMwT84WfXZdSrrJIPObwwUc__i5dB.3.r7';
					$dot    = 319;
				}
				else if ( 'zena' === get_post_type() ) {
					$gemius = 'bDtAfZihq7mKVtuvaRUbpYYg.BjdeIypvYkx43OrtHv.l7';
					$dot = 3930;
				}
				else {
					$cat    = nethr_get_top_category();

					$out    = $this->switch_category( $cat->slug );
					$gemius = $out['gemius'];
					$dot    = $out['dot'];
				}
				if ( 'gallery' === get_post_format() ) {
					$gemius = 'd2Lr24LIrE.rf8yTtLsfjcWonM91h8xbpjrxdcxr3AP.s7';
				}
			}
			if ( is_page( 'zena' ) ) {
				$gemius = 'bDtAfZihq7mKVtuvaRUbpYYg.BjdeIypvYkx43OrtHv.l7';
                $dot = 3930;
			}
		}
		$show_gemius = true;
		if ( is_single() ) {

			global $post;
			$oglasi = get_post_meta( $post->ID, 'oglasi', true );
			if ( isset( $oglasi['gemius'] ) && 1 === intval( $oglasi['gemius'] ) ) {
			    $show_gemius = false;
			}
		}

		if ($show_gemius) {
		?><!-- (C)2000-2015 Gemius SA - gemiusAudience / iskon.hr  -->
            <script type="text/javascript">
                <!--//--><![CDATA[//><!--
                var pp_gemius_identifier = '<?php echo esc_attr( $gemius ) ?>';
                // lines below shouldn't be edited
                function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
                gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
                (function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
                    gt.setAttribute('defer','defer'); gt.src=l+'://hr.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
                //--><!]]>
            </script>
        <?php } ?>

        <!--    danas   -->
        <script type="text/javascript">
            /* <![CDATA[ */
            (function() {
                window.dm=window.dm||{AjaxData:[]};
                window.dm.AjaxEvent=function(et,d,ssid,ad){
                    dm.AjaxData.push({et:et,d:d,ssid:ssid,ad:ad});
                    window.DotMetricsObj&&DotMetricsObj.onAjaxDataUpdate();
                };
                var d=document,
                    h=d.getElementsByTagName('head')[0],
                    s=d.createElement('script');
                s.type='text/javascript';
                s.async=true;
                s.src=document.location.protocol + '//script.dotmetrics.net/door.js?id=<?php echo intval( $dot ); ?>';
                h.appendChild(s);
            }());
            /* ]]> */
        </script>

		<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('set', 'anonymizeIp', true);
			ga('create', 'UA-56863111-1', 'auto', 'newTracker');
            ga('newTracker.send', 'pageview');

			ga('create', 'UA-30375875-1', 'auto', 'oldTracker');
			ga('oldTracker.send', 'pageview');
		</script>
	<?php

	}

	function facebook() {
		?>
		<div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '103100136402693',
					xfbml      : true,
					version    : 'v3.2'
				});
			};

			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/hr_HR/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '1132408460152629');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id=1132408460152629&ev=PageView
&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
<?php
	}

	function scroll_depth() {
	    if ( is_single() ) {
		    $oglasi = get_post_meta( get_the_ID(), 'oglasi', true );
		    if ( isset( $oglasi['scroll'] ) && 1 === intval( $oglasi['scroll'] ) ) {
		        wp_enqueue_script('nethr-scroll-depth', esc_url( get_template_directory_uri() ) . '/js/jquery.scrolldepth.js', array( 'jquery' ) );
            }
        }
    }

}

new Nethr_Analytics();