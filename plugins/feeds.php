<?php

class Nethr_Feeds {

	public function __construct() {
		add_action( 'init', array( $this, 'add_feeds' ) );
	}

	public function add_feeds() {
		add_feed( 'tes_feed', array( $this, 'tes_feed' ) );
		add_feed( 'midas_feed', array( $this, 'midas_feed' ) );
	}

	function tes_feed() {
	    $q = z_get_zone_query( 'tes-widget');
		header('Content-Type: application/xml');
		echo '<?xml version="1.0" encoding="utf-8"?>';
		?>
		<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
			<channel>
				<title>Net.hr tes feed</title>
				<link>https://net.hr/feed/tes_feed</link>
				<description/>
				<atom:link href="https://net.hr/feed/tes_feed" rel="self"/>
				<language>hr-HR</language>
				<lastBuildDate><?php the_date('U'); ?></lastBuildDate>
				<?php while( $q->have_posts() ) {
				    $q->the_post();
					?>
					<item>
						<title><?php the_title_rss(); ?></title>
						<link><?php the_permalink_rss() ?>?utm_source=Vecernji&amp;utm_medium=Widget&amp;utm_campaign=RazmjenaPrometa</link>
						<description><![CDATA[ <?php the_excerpt_rss() ?> ]]></description>
						<guid><?php the_permalink_rss() ?></guid>
						<enclosure url="<?php echo esc_url( get_the_post_thumbnail_url() ) ?>" length="None" type="None"/>
					</item>
				<?php
				} ?>
			</channel>
		</rss>
	<?php
	}

	function midas_feed() {
		$q = new WP_Query( array(
		        'post_status' => 'publish',
            'post_type' => array( 'post', 'webcafe', 'zena' ),
            'posts_per_page' => 15,
            'ignore_sticky_posts' => true,
            'no_found_rows' => true,
        ) );
		header('Content-Type: ' . feed_content_type('rss2') . '; charset=' . get_option('blog_charset'), true);
		echo '<?xml version="1.0" encoding="'. esc_attr( get_option('blog_charset') ) .'"?'.'>';
		do_action( 'rss_tag_pre', 'rss2' );
		?><rss version="2.0"
             xmlns:content="http://purl.org/rss/1.0/modules/content/"
             xmlns:wfw="http://wellformedweb.org/CommentAPI/"
             xmlns:dc="http://purl.org/dc/elements/1.1/"
             xmlns:atom="http://www.w3.org/2005/Atom"
             xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
             xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
			<?php
			/**
			 * Fires at the end of the RSS root to add namespaces.
			 *
			 * @since 2.0.0
			 */
			do_action( 'rss2_ns' );
			?>
        >
            <channel>
                <title><?php wp_title_rss(); ?></title>
                <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
                <link><?php bloginfo_rss('url') ?></link>
                <description><?php bloginfo_rss("description") ?></description>
                <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
                <language><?php bloginfo_rss( 'language' ); ?></language>
                <sy:updatePeriod><?php
		            $duration = 'hourly';

		            /**
		             * Filter how often to update the RSS feed.
		             *
		             * @since 2.1.0
		             *
		             * @param string $duration The update period. Accepts 'hourly', 'daily', 'weekly', 'monthly',
		             *                         'yearly'. Default 'hourly'.
		             */
		            echo apply_filters( 'rss_update_period', $duration );
		            ?></sy:updatePeriod>
                <sy:updateFrequency><?php
		            $frequency = '1';

		            /**
		             * Filter the RSS update frequency.
		             *
		             * @since 2.1.0
		             *
		             * @param string $frequency An integer passed as a string representing the frequency
		             *                          of RSS updates within the update period. Default '1'.
		             */
		            echo apply_filters( 'rss_update_frequency', $frequency );
		            ?></sy:updateFrequency>
				<?php  while ( $q->have_posts() ) {
				    $q->the_post();
					if ( false === strpos( get_the_permalink(), 'net.hr' ) ) {
						//skip redirected posts
						continue;
					}
						?>
                        <item>
                            <title><?php the_title_rss() ?></title>
                            <link><?php the_permalink_rss() ?></link>
                            <description><![CDATA[<?php the_excerpt_rss() ?>]]></description>
                            <enclosure url="<?php echo esc_url( get_the_post_thumbnail_url() ) ?>" length="None" type="None"/>
                            <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                        </item>
					<?php
				} ?>
            </channel>
        </rss>
		<?php
	}
}

new Nethr_Feeds();