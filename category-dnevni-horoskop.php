<?php
// NET.HR -> Category
get_header();
$cat_id = get_query_var( 'cat' );
?>
	<div class="container category cf">

		<div class="section-header">
			<div class="section-titles">
				<?php
				$category_link = wpcom_vip_get_term_link( $cat_id, 'category' );
				?>
				<a href="<?php echo esc_url( $category_link ); ?>"
				   class="active"><?php echo esc_html( get_cat_name($cat_id) ); ?></a>
			</div>
			<?php get_template_part( 'templates/layout/socials' ); ?>
		</div>

		<div class="page-grid cf">
			<?php
			// Get grid
			nethr_get_template( 'templates/webcafe/horoskop-grid' ); ?>
            <?php
                $branding = intval( get_option( 'nethr_horoscope_branding', 0) );
                if ( 1 === $branding ) {
                    $url = get_option( 'nethr_horoscope_branding_url' );
                    $image = get_option( 'nethr_horoscope_branding_image_large' );
                    ?>
                    <br>
                    <a href="<?php echo esc_url( $url ) ?>" target="_blank">
                        <img src="<?php echo esc_url( $image ) ?>" width="660"/>
                    </a>
                <?php } ?>
		</div>


		<div class="sidebar single-sidebar single-sidebar-1">
			<?php dynamic_sidebar( 'sidebar-webcafe' ) ?>
		</div>

	</div>


<?php
get_footer();
