<?php

class Nethr_Promo_Category_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_promo_category_widget', 'Nethr: Promo na kategoriji', array(
			'description' => 'Prikaz widgeta za promo članke na kategoriji',
		) );
	}

	public function widget( $args, $instance ) {
		$number = $instance['number'];
		$cat = nethr_get_top_category();
		$data = wp_cache_get( 'promo_widget_' . $cat->slug, 'nethr_widgets' );
		if ( !$data ) {
			$data = '';
			$posts = nethr_promos( 'cat-widget', $cat );
			if ( $posts && ! empty( $posts ) ) {
				ob_start();
				?>
				<div class="widget">
					<div class="widget-header cf">
						<h2 class="section-title"><?php echo esc_html( $instance['title'] ); ?></h2>
					</div>
					<div class="widget-body gray cf">
						<?php
						$args     = array(
							'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
							'post_type'      => array( 'promo' ),
							'post__in'       => $posts,
							'orderby'        => 'post__in',
							'no_found_rows'  => true,
							'posts_status' => 'publish'
						);
						$articles = new WP_Query( $args );
						if ( $articles->have_posts() ) {
							while ( $articles->have_posts() ) {
								$articles->the_post(); ?>
								<?php get_template_part( 'templates/articles/article-3' ); ?>
							<?php }
						}
						wp_reset_postdata(); ?>
					</div>
				</div>
				<?php
				$data = ob_get_clean();
				wp_cache_set( 'promo_widget_' . $cat->slug, $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
			}
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );

		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$number  = intval( $instance['number'] );
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>

	<?php
	}

}

register_widget( 'Nethr_Promo_Category_Widget' );
