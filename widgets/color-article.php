<?php

class Nethr_Big_Color_Article_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'nethr_big_color_article_widget', 'Net.hr: Obojani članak',
            array(
                'description' => 'Obojani članak na naslovnici koji se povlači preko 11. zone',
            )
        );
    }

    public function widget( $args, $instance )
    {
        ?>

        <!-- Start widget HTML -->
        <?php
        $articles = z_get_zone_query( $instance['zone'], array( 'posts_per_page' => 1, 'offset' => intval( $instance['offset'] ) ) );
        while ($articles->have_posts()) {
            $articles->the_post();
            get_template_part( 'templates/articles/article-color-big' );

        }
    }

    function update( $new_instance, $instance ) {
        $instance['zone']       = sanitize_text_field( $new_instance['zone'] );
        $instance['offset']     = intval( $new_instance['offset'] );

        return $instance;
    }

    function form( $instance ) {
        $zone       = empty( $instance['zone'] ) ? '' : $instance['zone'];
        $offset       = empty( $instance['offset'] ) ? 10 : intval( $instance['offset'] );
        ?>
        <p><label
                for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
                type="text" value="<?php echo esc_attr( $zone ); ?>"></p>
        <p><label
                    for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Offset</label>
            <input
                    id="<?php echo esc_attr( $this->get_field_id( 'offset' ) ); ?>"
                    class="widefat"
                    name="<?php echo esc_attr( $this->get_field_name( 'offset' ) ); ?>"
                    type="number" value="<?php echo esc_attr( $offset ); ?>"></p>
        <?php
    }

}

register_widget( 'Nethr_Big_Color_Article_Widget' );
