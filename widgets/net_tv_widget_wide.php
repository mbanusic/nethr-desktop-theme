<?php

class Nethr_NetTV_Wide_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_nettv_wide_widget', 'Net TV widget - Naslovnica', array(
            'description' => 'Prikaz Net TV widgeta na naslovnici',
        ) );
    }


    public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nettv_widget', 'nethr_widgets' );
        if ( !$data ) {
		        ob_start();
		        ?>
		        <div class="widget home-widget nettv_widget_wide">
			        <div class="widget-header cf">
				        <a href="https://net.hr/kategorija/net-tv/">
					        <img
						        src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/netTV_widgetLogo.png"
						        width="56" height="16"/>
				        </a>

				        <a href="https://net.hr/kategorija/net-tv/" class="more">
					        Vidi sve <i class="fa fa-chevron-right"></i>
				        </a>
			        </div>
			        <div class="widget-body cf">
				        <?php
				        $q = z_get_zone_query( 'widget-net-tv', array( 'posts_per_page' => 5 ) );
				        while ( $q->have_posts() ) {
					        $q->the_post();
					        ?>
					        <article class="article-2">
						        <div class="inner">

							        <a href="<?php the_permalink(); ?>">
								        <div class="thumb">
									        <?php the_post_thumbnail( 'article-2' ); ?>
								        </div>
								        <div class="article-text">
									        <h2 class="overtitle net-tv">
										        NET TV
									        </h2>

									        <h1 class="title">
										        <?php the_title(); ?>
									        </h1>
								        </div>
							        </a>

						        </div>
					        </article>
				        <?php } ?>
			        </div>
		        </div>
		        <?php
		        $data = ob_get_clean();
		        wp_cache_set( 'nettv_widget', $data, 'nethr_widgets', HOUR_IN_SECONDS );
        }
	    echo $data;
    }

    function update( $new_instance, $instance ) {
        return $instance;
    }

    function form( $instance ) {

    }

}

register_widget( 'Nethr_NetTV_Wide_Widget' );
