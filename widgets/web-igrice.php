<?php

class Nethr_Igrice_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_igrice_widget', 'Igrice -> Kategorije', array(
            'description' => 'Prikaz kategorija Web igrica',
        ) );
    }

    public function widget( $args, $instance ) {
	    $data = wp_cache_get( 'igrice_widget', 'nethr_widgets' );
	    if ( !$data ) {
		    ob_start();

		    ?>
		    <div class="widget web-igrice">
			    <div class="widget-header cf">
				    <h2 class="section-title">Kategorije igrica</h2>
			    </div>
			    <div class="widget-body">
				    <?php
				    $terms = get_terms(
					    'igrice', array(
					    'orderby'    => 'name',
					    'order'      => 'ASC',
				    )
				    );
				    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
					    foreach ( $terms as $term ) {
						    ?>
						    <div class="game-category">
							    <a href="<?php echo esc_url( wpcom_vip_get_term_link( $term, 'igrice' ) ); ?>">
								    <?php echo esc_html( $term->name ); ?>
							    </a>
						    </div>
						    <?php
					    }
				    }
				    ?>

			    </div>
		    </div>
		    <?php
		    $data = ob_get_clean();
		    wp_cache_set( 'igrice_widget',  $data,'nethr_widgets', DAY_IN_SECONDS );
	    }

	    echo $data;
    }

    function update( $new_instance, $instance ) {

        return $instance;
    }

    function form( $instance ) {

    }

}

register_widget( 'Nethr_Igrice_Widget' );
