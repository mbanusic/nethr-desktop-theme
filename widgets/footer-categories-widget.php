<?php

class Nethr_Footer_Categories_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_footer_categories_widgeta', 'Kategorije u footeru',
			array(
				'description' => 'Popis svih first-level kategorija',
			) );
	}

	public function widget( $args, $instance ) {

		$data = wp_cache_get( 'nethr_footer_categories_widget', 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="footer-categories">
				<div class="container cf">
					<?php
					$categories = array(
						'danas',
						'sport',
						'webcafe',
						'hot',
						'magazin',
						'tehnoklik',
						'auto',
					);
					foreach ( $categories as $category_slug ) {
							$category = wpcom_vip_get_category_by_slug( $category_slug );
							if (!$category) {
							    continue;
                            }
							$children = get_categories(
								array(
									'parent'     => $category->term_id,
									'hide_empty' => false,
								)
							);

							?>
							<div class="category-container">
								<a href="<?php echo esc_url( wpcom_vip_get_term_link( $category, 'category' ) ) ?>">
									<h4>
										<?php echo esc_html( $category->name ); ?>
									</h4>
								</a>
								<?php
								if ( ! empty( $children ) ) {
									foreach ( $children as $child ) { ?>
										<a href="<?php echo esc_url( wpcom_vip_get_term_link( $child, 'category' ) ); ?>">
											<?php echo esc_html( $child->name ); ?>
										</a>
										<?php
									}
								}
								if ('sport' === $category_slug) {
									?>
                                    <a href="https://www.rezultati.com/" target="_blank">
                                        Rezultati.com
                                    </a>
									<?php
								}
								?>
							</div>
						<?php
					}?>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_get( 'nethr_footer_categories_widget', $data, 'nethr_widgets' );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {


		return $instance;
	}

	function form( $instance ) {


	}
}

register_widget( 'Nethr_Footer_Categories_Widget' );
