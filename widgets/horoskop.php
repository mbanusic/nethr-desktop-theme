<?php

class Nethr_Horoskop extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_horoskop_widget', 'Horoskop',
			array(
				'classname'   => 'horoskop-widget',
				'description' => 'Gatanje iz graška, voodoo revivacije, lečenje uroka, copranje',
			) );
	}

	function template($sign) {
		$cat = wpcom_vip_get_category_by_slug( $sign );
		?>

		<div class="item <?php echo esc_attr( $sign ); ?> cf">
			<div class="item-sign <?php echo esc_attr( $sign ); ?>"><!-- as bg --></div>
			<div class="item-text">
				<h2 class="title">
					<?php echo esc_html( $cat->name ) ?><span> <?php echo esc_html( $cat->description ) ?></span>

				</h2>
				<div class="content">
					<?php
					echo nethr_excerpt_tagged( 16 );
					?>
				</div>
				<div class="horoskop-buttons">
					<a href="<?php the_permalink(); ?>" class="button button-small">Pročitaj više</a>
					<a href="<?php echo esc_url( wpcom_vip_get_term_link( $cat->parent, 'category' ) ); ?>" class="button button-small dark">Ostali znakovi</a>
				</div>
			</div>
		</div>

	<?php }

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_horoscope_widget', 'nethr_widgets' );

		if ( ! $data ) {

			ob_start();
			?>

			<div class="widget horoskop">

				<a class="horoskop-arrow up"><i
						class="fa fa-chevron-up"></i></a>
				<a class="horoskop-arrow down"><i
						class="fa fa-chevron-down"></i></a>

				<div class="item-container">

					<?php
					$signs = array( 'ovan', 'bik', 'blizanci', 'rak', 'lav', 'djevica', 'vaga', 'skorpion', 'strijelac', 'jarac', 'vodenjak', 'ribe');


					foreach ( $signs as $sign ) {
						$q    = new WP_Query(
							array(
								'posts_per_page' => 1,
								'category_name'  => $sign,
								'post_type'      => 'webcafe',
								'no_found_rows' => true,
								'posts_status' => 'publish'
							)
						);
						while ( $q->have_posts() ) {
							$q->the_post();
							$this->template( $sign );
						}
					}
					wp_reset_postdata();
					?>

				</div>
			</div>

			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_horoscope_widget', $data, 'nethr_widgets', HOUR_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		return $instance;
	}

	function form( $instance ) {

		?>
		<p>Horoskop</p>
	<?php
	}
}

register_widget( 'Nethr_Horoskop' );
