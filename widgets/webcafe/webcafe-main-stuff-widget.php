<?php

class Nethr_Webcafe_Main_Stuff_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_webcafe_main_stuff', 'Webcafe ključni sadržaj',
			array(
				'description' => 'Komnetar, cura, dečko, vic...',
			) );
	}

	function template( $name ) {
		?>
			<article class="article-2 webcafe">
				<div class="inner">
					<a href="<?php the_permalink(); ?>">
						<div class="thumb">
							<?php the_post_thumbnail( 'article-2' ); ?>
						</div>

						<div class="article-text">
							<h2 class="overtitle webcafe">
								<?php echo esc_html( $name ); ?>
							</h2>
						</div>
					</a>
				</div>
			</article>
		<?php
	}

	function template_vic() {
		?>
		<article class="article-num-title cf" data-layout="article-num">
			<div class="inner">

				<div class="number-container">
					<div class="number overtitle">VICEVI</div>
					<div class="skew-left"></div>
					<div class="skew-right"></div>
				</div>
				<div class="number-title title">
					<a href="<?php the_permalink() ?>"><?php the_excerpt(); ?></a>
				</div>

			</div>
		</article>
		<?php
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_webcafe_main_stuff', 'nethr_webcafe_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<section class="webcafe-main-stuff cf">
				<?php
				$cats = array( 'komnetar' => 'Komnetar', 'overkloking' => 'Overkloking', 'cura-dana' => 'Cura dana', 'decko-dana' => 'Dečko dana', 'vic-dana' => 'Vic dana' );
				foreach ( $cats as $cat => $name ) {
					$q = new WP_Query( array(
						'posts_per_page' => 1,
						'category_name' => $cat,
						'post_type' => 'webcafe',
						'no_found_rows' => true,
						'posts_status' => 'publish'
					) );
					while ( $q->have_posts() ) {
						$q->the_post();
						if ( $cat === 'vic-dana' ) {
							$this->template_vic();
						}
						else {
							$this->template( $name );
						}
					}
				}
				wp_reset_postdata();
			?></section><?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_webcafe_main_stuff', $data, 'nethr_webcafe_widgets', HOUR_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );

		return $instance;
	}

	function form( $instance ) {

		$title  = empty( $instance['title'] ) ? '' : $instance['title'];

		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'twentyfourteen' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>"></p>

	<?php
	}
}

register_widget( 'Nethr_Webcafe_Main_Stuff_Widget' );
