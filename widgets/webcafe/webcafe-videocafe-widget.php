<?php

class Nethr_Webcafe_Videocafe_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_webcafe_videocafe', 'Videocafe',
			array(
				'description' => 'Videi iz rubrike Webcafe',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$data = wp_cache_get( 'nethr_webcafe_video_widget', 'nethr_webcafe_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="big-gallery-widget videocafe cf">

				<div class="section-header">
					<div class="section-titles">
						<a href="#" class="active">Videocafe</a>

					</div>
				</div>
					<?php
					// Article 1 -> Featured article
					$args     = array(
						'posts_per_page' => 4,
						'category_name' => 'webcafe',
						'post_type' => 'post',
						'tax_query' => array(
							array(
								'taxonomy' => 'post_format',
								'field'    => 'slug',
								'terms'    => array(
									'post-format-video',
								)
							),
						),
						'no_found_rows'  => true,
						'posts_status' => 'publish'
					);
					$articles = new WP_Query( $args );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							if ( 0 === $articles->current_post ) {
								?><div class="grid-item-1"><?php
								nethr_get_template( 'templates/articles/article-gallery-1', array( 'gallery_view' => true ) );
								?></div><?php
							}
							if ( 1 === $articles->current_post ) {
								?><div class="grid-item-2 cf"><?php
							}
							if ( $articles->current_post > 0 ) {
								nethr_get_template( 'templates/articles/article-3', array( 'gallery_view' => true ) );
							}
							if ( 3 === $articles->current_post ) {
								?></div><?php
							}
						}
					}
					wp_reset_postdata();
					?>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_webcafe_video_widget', $data, 'nethr_webcafe_widgets', 30 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );


		return $instance;
	}

	function form( $instance ) {

		//ovo je samo primjer za formu unutar admina
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];

		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'twentyfourteen' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>"></p>

	<?php
	}
}

register_widget( 'Nethr_Webcafe_Videocafe_Widget' );
