<?php

class Nethr_Small_Category_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_small_category_widgeta', 'Male kategorije',
			array(
				'description' => 'Male kategorije za naslovnicu',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$data = wp_cache_get( 'nethr_small_category_widget_' . $instance['cache_id'], 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="small-categories cf">
				<?php
				$categories = explode( ',', $instance['categories'] );

				if ( ! empty( $categories ) ) {
					foreach ( $categories as $category_id) {
						$category = get_category( $category_id );
						if ( !$category || is_wp_error( $category ) ) {
							break;
						}
						?>

						<div class="small-category-item <?php echo esc_attr( $category->slug ); ?>">
							<div class="section-header">
								<div class="section-titles">
									<a href="<?php echo esc_url( wpcom_vip_get_term_link( $category, 'category' ) ); ?>"
									   class="active"><?php echo esc_html( $category->name ); ?></a>
								</div>
							</div>
						<?php
							$args = array(
								'posts_per_page' => 4,
								'cat' => $category_id,
								'post_type' => array( 'post', 'webcafe' ),
								'no_found_rows'  => true,
								'posts_status' => 'publish'
							);
							$articles = new WP_Query( $args );
							if ( $articles->have_posts() ) {
								while ( $articles->have_posts() ) {
									$articles->the_post();
									if ( 0 === $articles->current_post ) {
										$this->single_large( $category );
									}
									else {
										$this->single_small();
									}
								}
							}
							wp_reset_postdata();
						 ?>
					</div>
					<?php
					}
				} ?>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_small_category_widget_' . $instance['cache_id'], $data, 'nethr_widgets', ( mt_rand( 15 * MINUTE_IN_SECONDS, 30 * MINUTE_IN_SECONDS) ) );
		}
		echo $data;
	}

	function single_small() {
		?><article class="small cf">
		<a href="<?php the_permalink(); ?>">
			<div class="inner">
				<div class="square"></div>
				<div class="text">
					<b class="overtitle">
						<?php
						$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
						if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							echo esc_html( $titles['over_title'] );
						} ?>
					</b>
					<span class="title"><?php the_title(); ?></span>
				</div>
			</div>
		</a>
		</article><?php
	}

	function single_large( $cat ) {
		?><article class="article-4">
		<div class="inner cf">
			<a href="<?php the_permalink(); ?>">
				<div class="thumb">
					<?php
					the_post_thumbnail( 'feed-2' );
					?>
				</div>

				<div class="article-text">
					<h2 class="overtitle <?php echo esc_attr( $cat->slug ) ?>">
						<?php
						$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
						if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							echo esc_html( $titles['over_title'] );
						} else {

							echo esc_html( $cat->name );
						}
						?>
					</h2>

					<h1 class="title"><?php the_title(); ?></h1>
				</div>
			</a>

		</div>
		</article><?php
	}

	function update( $new_instance, $instance ) {
		$instance['categories']  = sanitize_text_field( $new_instance['categories'] );
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['cache_id'] = sanitize_text_field( $new_instance['cache_id'] );
		return $instance;
	}

	function form( $instance ) {
		$categories = $instance['categories'];
		$title = $instance['title'];
		$cache_id = $instance['cache_id'];
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'categories' ) ); ?>"><?php esc_html_e( 'Kategorije', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'categories' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>"
				type="text" value="<?php echo esc_attr( $categories ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'cache_id' ) ); ?>"><?php esc_html_e( 'Cache ID', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'cache_id' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'cache_id' ) ); ?>"
				type="text" value="<?php echo esc_attr( $cache_id ); ?>">
		</p>
<?php }

}

register_widget( 'Nethr_Small_Category_Widget' );
