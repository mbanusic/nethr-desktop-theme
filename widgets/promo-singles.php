<?php

class Nethr_Promo_Singles_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_promo_single_widget', 'Nethr: Promo na single', array(
			'description' => 'Prikaz widgeta za promo članke na single',
		) );
	}

	public function widget( $args, $instance ) {
        $posts = z_get_zone_query( 'promos-single' );
        if ( $posts->have_posts() ) {
            ?>
            <div class="widget">
                <div class="widget-header cf">
                    <h2 class="section-title"><?php echo esc_html( $instance['title'] ); ?></h2>
                </div>
                <div class="widget-body gray cf">
                    <?php
                    if ( $posts->have_posts() ) {
                        while ( $posts->have_posts() ) {
                            $posts->the_post(); ?>
                            <?php get_template_part( 'templates/articles/article-3' ); ?>
                        <?php }
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php
        }
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );

		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
	<?php
	}

}

register_widget( 'Nethr_Promo_Singles_Widget' );
