<?php

class Nethr_EURO_Kolumnisti_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_euro_kolumnisti_widget', 'EURO kolumnisti', array(
            'description' => 'Prikaz EURO kolumnista',
        ) );
    }

	private function article() {
		?><article class="article-3 cf" data-layout="article-3">
		<div class="inner cf">

			<a href="<?php the_permalink(); ?>">

				<div class="thumb">
					<?php $authors = get_coauthors();
						echo wp_kses_post( get_the_post_thumbnail( $authors[0]->ID, 'article-3' ) );
					 ?>
				</div>

				<div class="article-text">
					<?php $category = nethr_get_the_category( get_the_ID() ); ?>
					<h2 class="overtitle <?php echo esc_html( nethr_category_color() ); ?>">
						<?php
						$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
						if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							echo esc_html( $titles['over_title'] );
						} else {
							echo esc_html( $category->name );
						}
						?>
					</h2>
					<h1 class="title"><?php the_title(); ?></h1>
				</div>
			</a>

		</div>
		</article><?php
	}

    public function widget( $args, $instance ) {
        $data = wp_cache_get( 'nethr_euro_kolumnisti', 'nethr_widgets' );
	    $data = false;
        if ( !$data ) {
            ob_start();
            ?>
            <div class="widget sidebar-widget rss-widget">
                <div class="widget-header cf">
	                <h2 class="section-title">
                        <?php echo esc_html( $instance['title'] ); ?>
	                </h2>
                </div>
                <div class="widget-body gray cf">
                    <?php
                    $authors = array( 'aleksandar-holiga', 'mihovil-topic', 'miro-par', 'sasa-ibrulj' );
                    shuffle( $authors );
                    foreach ( $authors as $author ) {
	                    $q = new WP_Query( array(
		                    'post_status' => 'publish',
		                    'post_type' => array( 'post' ),
		                    'posts_per_page' => 1,
		                    'no_found_rows' => true,
		                    'ignore_sticky_posts' => true,
		                    'author_name' => $author
	                    ) );
	                    while ( $q->have_posts() ) {
		                    $q->the_post();
		                    $this->article();
	                    }
                    }
                    ?>
                </div>
            </div>
            <?php
            $data = ob_get_clean();
            wp_cache_set( 'nethr_euro_kolumnisti', $data, 'nethr_widgets', 15 * MINUTE_IN_SECONDS );

        }
        echo $data;
    }

	public function form( $instance ) {
		$title = $instance['title'];
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}

}

register_widget( 'Nethr_EURO_Kolumnisti_Widget' );
