<?php

class Nethr_Top_Graphic_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_top_graphic_widget', 'Gornja grafika',
			array(
				'description' => 'Uredite prikaz gornje grafike',
			)
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'upload_scripts' ) );
	}

	public function upload_scripts()
	{
		wp_enqueue_media();
		wp_enqueue_script( 'nethr_widget_scripts', get_template_directory_uri() . '/admin/widget-scripts.js', array( 'jquery', 'jquery-ui-autocomplete' ) );
	}


	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_top_graphic', 'nethr_widgets' );
		if ( ! $data ) {
			$status = ( isset( $instance['status'] ) && $instance['status'] ) ? $instance['status'] : 'off';
			if ( 'off' === $status ) {
				$data = '';
			}
			else {
				$image_id = intval( $instance['image'] );
				$image2_id = intval( $instance['image_2'] );
				$link     = isset( $instance['link'] ) ? $instance['link'] : '#';
				ob_start();
				$src1         = wp_get_attachment_image_src( $image2_id, 'full' );
				$src2         = wp_get_attachment_image_src( $image_id, 'full' );
				$standard_url = $src1[0];
				$retina_url   = $src2[0];
				?>
				<style>

					<?php
					if( $standard_url != '' ) {
						$graphic = $standard_url;
					} else {
						$graphic = $retina_url; } ?>

					.top-graphic-container {
						background-image: url(<?php echo esc_html( $graphic ); ?>);
					}

					@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and ( min--moz-device-pixel-ratio: 2), only screen and ( -o-min-device-pixel-ratio: 2/1), only screen and ( min-device-pixel-ratio: 2), only screen and ( min-resolution: 192dpi), only screen and ( min-resolution: 2dppx) {

						.top-graphic-container {
							background-image: url(<?php echo esc_html( $retina_url ); ?>);
						}

					}

				</style>
				<a href="<?php echo esc_url( $link ) ?>">
					<div class="top-graphic-container">

					</div>
				</a>
				<?php
				$data = ob_get_clean();

			}
			wp_cache_set( 'nethr_top_graphic', $data, 'nethr_widgets', DAY_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		if ( isset( $new_instance['status'] ) && 'on' === $new_instance['status'] ) {
			$instance['status'] = 'on';
		}
		else {
			$instance['status'] = 'off';
		}
		$instance['link'] = trim( sanitize_text_field( $new_instance['link'] ) );
		$instance['image'] = trim( sanitize_text_field( $new_instance['image'] ) );
		$instance['image_2'] = trim( sanitize_text_field( $new_instance['image_2'] ) );

		wp_cache_delete( 'nethr_top_graphic', 'nethr_widgets' );
		return $instance;
	}

	function form( $instance ) {
		$status = 'off';
		if ( isset( $instance['status'] ) ) {
			$status = $instance['status'];
		}

		$image = '';
		if ( isset( $instance['image'] ) ) {
			$image = $instance['image'];
		}

		$image_2 = '';
		if ( isset( $instance['image_2'] ) ) {
			$image_2 = $instance['image_2'];
		}

		$link = '';
		if ( isset( $instance['link'] ) ) {
			$link = $instance['link'];
		}
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'status' ) ); ?>"><?php esc_html_e( 'Status:' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'status' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'status' ) ); ?>" type="checkbox" value="on" <?php checked( $status, 'on' ); ?> />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>"><?php esc_html_e( 'Slika Retina (1000x180)*:' ); ?></label>
			<input name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" class="widefat image_id <?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" type="text" size="36"  value="<?php echo esc_attr( $image ); ?>" />
			<input class="upload_image_button button button-primary" type="button" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" value="Odaberite sliku" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'image_2' ) ); ?>"><?php esc_html_e( 'Slika Standard (500x90):' ); ?></label>
			<input name="<?php echo esc_attr( $this->get_field_name( 'image_2' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'image_2' ) ); ?>" class="widefat image_id <?php echo esc_attr( $this->get_field_id( 'image_2' ) ); ?>" type="text" size="36"  value="<?php echo esc_attr( $image_2 ); ?>" />
			<input class="upload_image_button button button-primary" type="button" id="<?php echo esc_attr( $this->get_field_id( 'image_2' ) ); ?>" value="Odaberite sliku" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>"><?php esc_html_e( 'ID članka' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
		</p>
	<?php
	}
}

register_widget( 'Nethr_Top_Graphic_Widget' );
