<?php

class Nethr_More_Articles_From_Author_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_more_articles_from_author_widget', 'Još od Autora',
			array(
				'description' => 'Još članaka od autora',
			) );
	}


	function template() { ?>

	<article class="article-feed cf">
		<div class="inner cf">
			<?php
			$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
			?>

			<a href="<?php the_permalink(); ?>">
				<div class="thumb">
					<?php
						if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'feed-1' );
						} else { ?>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed.gif"/>
							<?php
						}

					?>
				</div>
			</a>

			<div class="article-text">
				<a href="<?php the_permalink(); ?>">

					<?php
					$category = nethr_get_the_category();
					if ( $category->term_id !== 90 ) { ?>
					<h2 class="overtitle <?php echo esc_attr( $category->slug ); ?>">
						<?php
						if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							echo esc_html( $titles['over_title'] );
						}  ?>
					</h2>
					<?php } ?>

					<h1 class="title"><?php the_title(); ?></h1>
				</a>

				<p class="undertitle"><?php the_time(); ?>
                    <?php
                        $term_link =  get_term_link( $category, 'category' );
                        if ( !is_wp_error( $term_link ) ) {
                    ?>
                            <a href="<?php echo esc_url( $term_link ); ?>"><?php echo esc_html( $category->cat_name ) ?></a>
                    <?php } ?>
				</p>
			</div>

		</div>
	</article>

	<?php }

	public function widget( $args, $instance ) {
	    $coauthors = get_coauthors();
        $author = intval($instance['author_id']);
	    $author_name = false;
	    foreach ($coauthors as $coauthor) {
	        if (intval($coauthor->ID) === $author) {
	            $author_name = $coauthor->user_login;
	            break;
            }
        }
        if ($author_name) {
            $query = array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'no_found_rows' => true,
                'posts_status' => 'publish',
                'offset' => 1,
                'author_name' => $author_name
            );


            $articles = wp_cache_get('nethr_more_articles_for_author_' . $author, 'nethr_widgets');

            if (!$articles) {
                $articles = new WP_Query($query);
                wp_cache_set('nethr_more_articles_for_author_' . $author, $articles, 'nethr_widgets', 3 * HOUR_IN_SECONDS);
            }
            ?>

            <div class="more-categories-widget widget">

                <h2 class="section-title">Još iz kolumni</h2>
                <div class="category-feed more-from-category cf">
                    <?php
                    if ($articles->have_posts()) {
                        while ($articles->have_posts()) {
                            $articles->the_post();
                            $this->template();
                        }
                    }
                    wp_reset_postdata(); ?>

                </div>
            </div>
            <?php
        }
	}

	function update( $new_instance, $instance ) {
        $instance['author_id'] = intval( $new_instance['author_id'] );
		return $instance;
	}

	function form( $instance ) {
        $author_id = $instance['author_id'];
        ?>
        <p><label
                    for="<?php echo esc_attr( $this->get_field_id( 'author_id' ) ); ?>">Author id</label>
            <input
                    id="<?php echo esc_attr( $this->get_field_id( 'author_id' ) ); ?>"
                    class="widefat"
                    name="<?php echo esc_attr( $this->get_field_name( 'author_id' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $author_id ); ?>"></p>

        <?php
	}
}

register_widget( 'Nethr_More_Articles_From_Author_Widget' );
