<?php

class Nethr_Photo_News_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_photo_news_widget', 'Foto vijesti',
			array(
			'description' => 'Prikaz foto vijesti',
		) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$number = $instance['number'];
		$data   = wp_cache_get( 'nethr_photo_news_widget', 'nethr_widgets' );
		if ( ! $data ) {
			ob_start();
			?>
			<div class="widget foto-news">
				<div class="widget-header cf">
					<h2 class="section-title"><?php echo esc_html( $instance['title'] ); ?></h2>
					<?php if ( $instance['more'] ) { ?>
						<a href="<?php echo esc_url( $instance['url'] ); ?>" class="more">
							<?php echo esc_html( $instance['more'] ); ?>
							<i class="fa fa-chevron-right"></i>
						</a>
					<?php } ?>
				</div>

				<div class="widget-body cf">
					<?php
					// Article 1 -> Featured article
					$args     = array(
						'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
						'tax_query' => array(
							array(
								'taxonomy' => 'post_format',
								'field'    => 'slug',
								'terms'    => array( 'post-format-gallery' )
							),
						),
						'no_found_rows'  => true,
						'posts_status' => 'publish'
					);
					$articles = new WP_Query( $args );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							if ( 0 === $articles->current_post ) {
								?>
								<article class="widget-article big">
									<a href="<?php the_permalink(); ?>">
										<div class="thumb">
											<?php the_post_thumbnail( 'feed-2' ); ?>
											<div class="shaddow"></div>
											<h3 class="title">
												<?php
												$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
												if ( isset( $titles['short_title'] ) && $titles['short_title'] ) {
													echo esc_html( $titles['short_title'] );
												}
												else {
													the_title();
												} ?>
											</h3>
										</div>
									</a>
								</article>
								<?php
							}
							else {
								?><article class="widget-article">
								<a href="<?php the_permalink(); ?>">
									<div class="thumb">
										<?php the_post_thumbnail( 'feed-2' ); ?>
										<div class="shaddow"></div>
										<h3 class="title">
											<?php
											$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
											if ( isset( $titles['short_title'] ) && $titles['short_title'] ) {
												echo esc_html( $titles['short_title'] );
											}
											else  {
												the_title();
											} ?>
										</h3>
									</div>
								</a>
								</article><?php
							}
						}
					}
					wp_reset_postdata();
					?>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_photo_news_widget', $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 5 : absint( $new_instance['number'] );
		$instance['more']  = sanitize_text_field( $new_instance['more'] );
		$instance['url']  = sanitize_text_field( $new_instance['url'] );

		return $instance;
	}

	function form( $instance ) {
		//ovo je samo primjer za formu unutar admina
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$more   = empty( $instance['more'] ) ? '' : $instance['more'];
		$url   = empty( $instance['url'] ) ? '' : $instance['url'];
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] );
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'more' ) ); ?>"><?php esc_html_e( 'Još iz...', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'more' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'more' ) ); ?>"
				type="text" value="<?php echo esc_attr( $more ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"><?php esc_html_e( 'URL', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>"
				type="text" value="<?php echo esc_attr( $url ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj posteva', 'nethr' ); ?></label>
			</br>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>

	<?php
	}

}

register_widget( 'Nethr_Photo_News_Widget' );
