<?php

class Nethr_404_Joke_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_404_joke_widgeta', '404 joke',
			array(
				'description' => 'Vic za 404',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$data = wp_cache_get( 'nethr_404_joke_widget', 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="joke">
				<?php
				$args = array(
					'posts_per_page' => 1,
					'post_type' => 'webcafe',
					'category_name' => 'vic-dana',
					'no_found_rows'  => true,
					'posts_status' => 'publish'
				);
				$articles = new WP_Query( $args );
				if ( $articles->have_posts() ) {
					while ( $articles->have_posts() ) {
						$articles->the_post(); ?>

						<h2 class="title"><?php the_title(); ?></h2>
						<div class="content"><?php the_content(); ?></div>

					<?php
					}
				}
				wp_reset_postdata();
				?>
				<i class="fa fa-smile-o"></i>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_404_joke_widget', $data,  'nethr_widgets', HOUR_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		return $instance;
	}

	function form( $instance ) {
	}
}

register_widget( 'Nethr_404_Joke_Widget' );
