<?php

class Nethr_Facebook_Comments_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_facebook_comments_widget', 'Facebook komentari',
			array(
				'description' => 'facebook komentari',
			) );
	}

	public function widget( $args, $instance ) {
		$oglasi = get_post_meta( get_the_ID(), 'oglasi', true );
		if ( isset( $oglasi['facebook'] ) && 1 === intval( $oglasi['facebook'] ) ) {
			return;
		}
		?>
		<div class="widget widget-fb-comments">
			<h2 class="section-title">Imaš komentar?</h2>
			<div class="fb-comments"
				 data-width="100%"
				 data-numposts="5"
				 data-colorscheme="<?php echo (isset($_COOKIE['dark_mode']) && $_COOKIE['dark_mode'] === 'true')?'dark':'light' ?>"
				 data-href="<?php the_permalink() ?>"></div>
		</div>
<?php
	}

	function update( $new_instance, $instance ) {
		return $instance;
	}

	function form( $instance ) {
	}
}

register_widget( 'Nethr_Facebook_Comments_Widget' );
