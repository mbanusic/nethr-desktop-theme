<?php

class Nethr_Home_Category_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_home_category_widget', 'Kategorija na naslovnici',
			array(
				'classname'   => 'nethr_home_category',
				'description' => 'Pregled glavnih vijesti iz kategorije s bannerom',
			) );
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_home_category_widget'.$instance['category'], 'nethr_widgets' );
		if ( ! $data ) {
			ob_start();
			nethr_get_template( 'templates/layout/home-category', array(
				'category' => $instance['category'],
				'banner' => $instance['banner'],
				'num_data' => array(
					'text' => $instance['num_text'],
					'number' => $instance['num_number'],
					'url' => $instance['num_url'],
					),
				'color' => $instance['color'],
			) );

			$data = ob_get_clean();
			wp_cache_set( 'nethr_home_category_widget'.$instance['category'], $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['category']   = sanitize_text_field( $new_instance['category'] );
		$instance['num_text']   = sanitize_text_field( $new_instance['num_text'] );
		$instance['num_number'] = sanitize_text_field( $new_instance['num_number'] );
		$instance['num_url']    = sanitize_text_field( $new_instance['num_url'] );
		$instance['banner']     = sanitize_text_field( $new_instance['banner'] );
		$instance['color']      = intval( $new_instance['color'] );
		wp_cache_delete( 'nethr_home_category_widget'.$instance['category'], 'nethr_widgets' );
		return $instance;
	}

	function form( $instance ) {

		$category   = empty( $instance['category'] ) ? '' : $instance['category'];
		$num_text   = empty( $instance['num_text'] ) ? '' : $instance['num_text'];
		$num_number = empty( $instance['num_number'] ) ? '' : $instance['num_number'];
		$num_url    = empty( $instance['num_url'] ) ? '' : $instance['num_url'];
		$banner     = empty( $instance['banner'] ) ? '' : $instance['banner'];
		$color      = intval( $instance['color'] );
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>">Kategorija</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>"
				type="text" value="<?php echo esc_attr( $category ); ?>"></p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'num_text' ) ); ?>">Tekst</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'num_text' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'num_text' ) ); ?>"
				type="text" value="<?php echo esc_attr( $num_text ); ?>"></p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'num_number' ) ); ?>">Brojka</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'num_number' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'num_number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $num_number ); ?>"></p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'num_url' ) ); ?>">Link</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'num_url' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'num_url' ) ); ?>"
				type="text" value="<?php echo esc_attr( $num_url ); ?>"></p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>">Obojani članak</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'color' ) ); ?>"
				type="text" value="<?php echo esc_attr( $color ); ?>"></p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'banner' ) ); ?>">Banner zona</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'banner' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'banner' ) ); ?>"
				type="text" value="<?php echo esc_attr( $banner ); ?>"></p>


	<?php
	}
}

register_widget( 'Nethr_Home_Category_Widget' );
