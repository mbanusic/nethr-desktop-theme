<?php

class Nethr_Home_Fold1_Arafat_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_home_fold1_arafat', 'Arafat',
			array(
				'description' => 'Arafat članak',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		if ( isset( $instance['on'] ) && 'on' === $instance['on'] ) {
			$data = wp_cache_get( 'nethr_home_fold1_arafat', 'nethr_widgets' );
			if ( ! $data ) {

				ob_start();
				?>
				<div class="grid-item-1">
					<?php
					$q = new WP_Query( array(
							'post_type' => array( 'post', 'webcafe' ),
							'posts_per_page' => 1,
							'p'  => intval( $instance['arafat'] ),
							'no_found_rows'  => true,
							'posts_status' => 'publish'
						)
					);
					while ( $q->have_posts() ) {
						$q->the_post();
						global $post;
						nethr_get_template( 'templates/articles/article-1', array( 'short' => $instance['arafat_short'] ) );
					}
					wp_reset_postdata();
					nethr_get_template( 'templates/articles/article-num-title', array( 'data' => array( 'number' => $instance['numbered_number'], 'text' => $instance['numbered_text'], 'url' => $instance['url'] ) ) );
					?>
				</div>
				<?php
				$data = ob_get_clean();
				wp_cache_set( 'nethr_home_fold1_arafat', $data, 'nethr_widgets', 5 * MINUTE_IN_SECONDS );
			}

			echo $data;
		}
	}

	function update( $new_instance, $instance ) {
		$instance['arafat']  = strip_tags( $new_instance['arafat'] );
		$instance['numbered_number']  = strip_tags( $new_instance['numbered_number'] );
		$instance['numbered_text']  = strip_tags( $new_instance['numbered_text'] );
		$instance['on'] = isset( $new_instance['on'] ) ? $new_instance['on'] : 'off';
		$instance['arafat_short'] = isset( $new_instance['arafat_short'] ) ? $new_instance['arafat_short'] : 'no';
		$instance['url']  = strip_tags( $new_instance['url'] );
		set_transient( 'nethr_arafat', $instance );
		wp_cache_delete( 'nethr_home_fold1_arafat', 'nethr_widgets' );
		return $instance;
	}

	function form( $instance ) {

		$arafat  = empty( $instance['arafat'] ) ? '' : $instance['arafat'];

		$on = ( 'on' === $instance['on'] )? 'checked' : '';
		$arafat_short = ( $instance['arafat_short'] === 'yes' ) ? 'checked' : '';
		$numbered_number = empty( $instance['numbered_number'] ) ? '' : $instance['numbered_number'];
		$numbered_text = empty( $instance['numbered_text'] ) ? '' : $instance['numbered_text'];
		$url  = empty( $instance['url'] ) ? '' : $instance['url'];
		?>
		<p>
			<input type="checkbox"
			       id="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>"
			       name="<?php echo esc_attr( $this->get_field_name( 'on' ) ) ?>"
			       value="on" <?php echo esc_html( $on ) ?>>
			<label for="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>">Uključi</label>
		</p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'arafat' ) ); ?>">ID članka</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'arafat' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'arafat' ) ); ?>"
				type="text" value="<?php echo esc_attr( $arafat ); ?>"></p>
		<p>
			<input type="checkbox"
			       id="<?php echo esc_attr( $this->get_field_id( 'arafat_short' ) ) ?>"
			       name="<?php echo esc_attr( $this->get_field_name( 'arafat_short' ) ) ?>"
			       value="yes" <?php echo esc_html( $arafat_short ) ?>>
			<label for="<?php echo esc_attr( $this->get_field_id( 'arafat_short' ) ) ?>">Kraći naslov</label>
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'numbered_number' ) ); ?>">Brojčana vrijednost</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'numbered_number' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'numbered_number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $numbered_number ); ?>"></p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'numbered_text' ) ); ?>">Tekst uz broj</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'numbered_text' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'numbered_text' ) ); ?>"
				type="text" value="<?php echo esc_attr( $numbered_text ); ?>"></p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">Poveznica na članak</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>"
				type="text" value="<?php echo esc_attr( $url ); ?>"></p>


	<?php
	}
}

register_widget( 'Nethr_Home_Fold1_Arafat_Widget' );
