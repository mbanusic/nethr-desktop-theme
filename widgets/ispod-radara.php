<?php

class Nethr_Ispod_Radara_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_ispod_radara_widget', 'Ispod radara', array(
			'description' => 'Prikaz widgeta Ispod radara',
		) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$number = $instance['number'];
		$data   = wp_cache_get( 'nethr_ispod_radara_widget', 'nethr_widgets' );
		if ( ! $data ) {
			ob_start();
			?>
			<div class="widget">
				<div class="widget-header cf">
					<h2 class="section-title"><?php echo esc_html( $instance['title'] ); ?></h2>
				</div>
				<div class="widget-body gray cf">
					<?php
					$args = array(
						'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
						'tax_query' => array(
							array(
								'taxonomy' => 'positions',
								'field' => 'slug',
								'terms' => 'ispod-radara',
							),
						),
						'no_found_rows'  => true,
						'posts_status' => 'publish'
					);
					$articles        = new WP_Query( $args );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post(); ?>
							<?php get_template_part( 'templates/articles/article-3' ); ?>
						<?php }
					}
					wp_reset_postdata(); ?>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_ispod_radara_widget', $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );

		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$number  = intval( $instance['number'] );
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>

	<?php
	}

}

register_widget( 'Nethr_Ispod_Radara_Widget' );
