<?php

class Nethr_Category_Five_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_category_five_widget', 'Category Five widget',
			array(
				'description' => '5 članaka u kategoriji',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$location = isset( $instance['location'] ) ? $instance['location']: 'category';
		switch ( $location ) {
			case 'home':
				$location = 'home-' . sanitize_key( $instance['category'] );
				break;
			default:
				$location = get_category( get_query_var( 'cat' ) );
				$location = 'cat-'.$location->slug;
				break;
		}
		$data = wp_cache_get( 'nethr_category_five_widget_'.$location, 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();

			$args = array(
				'post_type' => array( 'post', 'webcafe' ),
				'posts_per_page' => 5,
				'tax_query' => array(
					array(
						'taxonomy'  => 'positions',
						'field'     => 'slug',
						'terms'     => array( $location ),
					),
				),
				'no_found_rows'  => true,
				'posts_status' => 'publish'
			);
			$articles = new WP_Query( $args );
			if ( $articles->have_posts() ) {
				while ( $articles->have_posts() ) {
					$articles->the_post();
					if ( 0 === $articles->current_post ) {
						?>
						<div class="grid-1 cf">
							<div class="grid-item-1">
						<?php
								get_template_part( 'templates/articles/article-1' );
						?></div>
						<?php
					}
					if ( 1 === $articles->current_post ) {
					?>
					<div class="grid-item-2 cf"><?php
					}
					if ( $articles->current_post > 0 && $articles->current_post < 5 ) {
						get_template_part( 'templates/articles/article-3-alt' );
					}

					if ( 4 === $articles->current_post ) {
					?></div>
					</div>
					<?php
					}
				}
			}
			wp_reset_postdata(); ?>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_category_five_widget_'.$location, $data, 'nethr_widgets', 5 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {

		return $instance;
	}

	function form( $instance ) {


	}
}

register_widget( 'Nethr_Category_Five_Widget' );
