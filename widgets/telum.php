<?php

class Nethr_Telum_Widget extends WP_Widget {


	public function __construct() {
		parent::__construct( 'nethr_telum', 'Midas widget',
			array(
				'description' => 'Midas widget',
			) );
	}

	public function widget( $args, $instance ) {
	    if ( is_single() ) {
	        $oglasi = get_post_meta( get_the_ID(), 'oglasi', true);
	        if ( isset( $oglasi['midas'] ) && 1 === intval( $oglasi['midas'] ) ) {
	            return false;
            }
        }
		?><div id="midasWidget__<?php echo intval( $instance['type'] ) ?>"></div><?php
		if ( isset( $instance['script'] ) && 'on' === $instance['script'] ) {
			$id = 1;
			if ( ! is_front_page() ) {
				$cat = nethr_get_top_category();
				switch ( $cat->slug ) {
					case 'danas';
						$id = 2;
						break;
					case 'sport':
						$id = 3;
						break;
					case 'webcafe':
						$id = 4;
						break;
					case 'hot':
						$id = 4;
						break;
					case 'magazin':
						$id = 6;
						break;
					case 'tehnoklik':
						$id = 7;
						break;
					case 'auto':
						$id = 8;
						break;
					case 'zena':
						$id = 10;
						break;
				}
			}
			$url = 'https://cdn.midas-network.com/Widget/IndexAsync/' . intval( $id ) . '?';
			$widgets = explode( ',', $instance['widgets'] );
			if ( jetpack_is_mobile() && is_front_page() ) {
			    //load only first widget request code
				$widgets = array( $widgets[0] );
            }
			for ( $i = 0; $i < count( $widgets ); $i++ ) {
				$url .= 'portalWidgetId=' . intval( trim( $widgets[$i] ) );
				if ( $i < count( $widgets ) - 1 ) {
					$url .= '&';
				}
			}
			?>
			<script async src="<?php echo esc_url( $url ) ?>" type="text/javascript"></script><?php
		}
	}

	function form( $instance ) {
		$script = isset( $instance['script'] ) ? $instance['script'] : 'off';
		$type = intval( $instance['type'] );
		$widgets = isset( $instance['widgets'] ) ? $instance['widgets'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>"><?php esc_html_e( 'ID tipa' ); ?></label>
			<input name="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" class="widefat <?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" type="text" size="3"  value="<?php echo esc_attr( $type ); ?>" />
		</p>
		<p>
		            <label for="<?php echo esc_attr( $this->get_field_name( 'script' ) ); ?>"><?php esc_html_e( 'Status:' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'script' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'script' ) ); ?>" type="checkbox" value="on" <?php checked( $script, 'on' ) ?> />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'widgets' ) ); ?>"><?php esc_html_e( 'Widgets' ); ?></label>
			<input name="<?php echo esc_attr( $this->get_field_name( 'widgets' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'widgets' ) ); ?>" class="widefat <?php echo esc_attr( $this->get_field_id( 'widgets' ) ); ?>" type="text" size="3"  value="<?php echo esc_attr( $widgets ); ?>" />
		</p>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$old_instance['script'] = sanitize_text_field( $new_instance['script'] );
		$old_instance['widgets'] = sanitize_text_field( $new_instance['widgets'] );
		$old_instance['type'] = intval( $new_instance['type'] );
		return $old_instance;
	}

}

register_widget( 'Nethr_Telum_Widget' );
