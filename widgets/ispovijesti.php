<?php

class Nethr_Ispovijesti_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'nethr_ispovijesti_widget', 'Net.hr: Ispovijesti',
            array(
                'description' => 'Ispovijesti',
            )
        );
    }

    public function widget( $args, $instance ) {
	    $data = wp_cache_get( 'nethr_ispovijesti_' . $instance['cache_name'], 'nethr_widgets' );
	    if ( !$data ) {
		    $posts = get_transient( $instance['category'] );
		    if ( empty( $posts ) ) {
			    return;
		    }
		    ob_start();
		    ?>

		    <div class="widget ispovijest-widget">

			    <div class="widget-header cf">
				    <h2 class="section-title"><?php echo esc_html( $instance['title'] ); ?></h2>
			    </div>

			    <div class="widget-body">
				    <?php
				    $args = array(
					    'posts_per_page' => nethr_sanitize_posts_per_page( $instance['number'] ),
					    'post__in' => $posts,
					    'orderby' => 'post__in',
					    'no_found_rows'  => true,
					    'posts_status' => 'publish',
					    'post_type' => array( 'post', 'ispovijesti' ),
					    'ignore_sticky_posts' => true
				    );
				    $q = new WP_Query( $args );
				    while ( $q->have_posts() ) {
					    $q->the_post();
				    ?>
				    <article>
					    <h2 class="title">
						    <a href="<?php the_permalink(); ?>">
							    <?php the_title(); ?>
						    </a>
					    </h2>
					    <div class="article-meta">
						    <i class="fa fa-comment"></i>
						    <block class="fb-comments-count"
						           data-href="<?php the_permalink(); ?>"></block>
						    Komentara
					    </div>
				    </article>
				    <?php } ?>
			    </div>

		    </div>

		    <?php
		    $data = ob_get_clean();
		    wp_cache_set( 'nethr_ispovijesti_' . $instance['cache_name'], $data, 'nethr_widgets', HOUR_IN_SECONDS );
	    }
	    echo $data;
    }

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['category']  = sanitize_text_field( $new_instance['category'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
		$instance['cache_name'] = sanitize_text_field( $new_instance['cache_name'] );
		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$category  = empty( $instance['category'] ) ? '' : $instance['category'];
		$cache_name  = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
		$number  = intval( $instance['number'] );
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php esc_html_e( 'Tip:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>"
				type="text" value="<?php echo esc_attr( $category ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>

		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"><?php esc_html_e( 'Cache name:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
				type="text" value="<?php echo esc_attr( $cache_name ); ?>">
		</p>

		<?php
	}
}

register_widget( 'Nethr_Ispovijesti_Widget' );
