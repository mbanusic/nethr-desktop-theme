<?php

class Nethr_Four_Large_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_four_large_widget', 'Four large',
			array(
				'description' => '4 veća članka',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$data = wp_cache_get( 'nethr_four_large_widget', 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			$args = array(
				'post_type' => array( 'post', 'webcafe' ),
				'posts_per_page' => 4,
				'post__in' => array(
					intval( $instance['post1'] ),
					intval( $instance['post2'] ),
					intval( $instance['post3'] ),
					intval( $instance['post4'] ),
				),
				'orderby' => 'post__in',
				'no_found_rows'  => true,
				'posts_status' => 'publish'
			);
			$q = new WP_Query( $args );
			?>
			<div class="grid-item-3">
				<?php
				while ( $q->have_posts() ) {
					$q->the_post();
					nethr_get_template( 'templates/articles/article-2', array( 'short' => $instance[ 'post'.( $q->current_post + 1 ).'_short' ] ) );
				}

				?>
			</div>
			<?php
			wp_reset_postdata();
			$data = ob_get_clean();
			wp_cache_set( 'nethr_four_large_widget', $data, 'nethr_widgets', 5 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title'] = '4 glava';
		$instance['post1'] = intval( $new_instance['post1'] );
		$instance['post2'] = intval( $new_instance['post2'] );
		$instance['post3'] = intval( $new_instance['post3'] );
		$instance['post4'] = intval( $new_instance['post4'] );
		$instance['post1_short'] = ( isset( $new_instance['post1_short'] ) && 'yes' === $new_instance['post1_short'] ) ? 'yes' : 'no';
		$instance['post2_short'] = ( isset( $new_instance['post2_short'] ) && 'yes' === $new_instance['post2_short'] ) ? 'yes' : 'no';
		$instance['post3_short'] = ( isset( $new_instance['post3_short'] ) && 'yes' === $new_instance['post3_short'] ) ? 'yes' : 'no';
		$instance['post4_short'] = ( isset( $new_instance['post4_short'] ) && 'yes' === $new_instance['post4_short'] ) ? 'yes' : 'no';

		wp_cache_delete( 'nethr_four_large_widget', 'nethr_widgets' );
		return $instance;
	}

	function form( $instance ) {

		$post1  = empty( $instance['post1'] ) ? '' : intval( $instance['post1'] );
		$post2  = empty( $instance['post2'] ) ? '' : intval( $instance['post2'] );
		$post3  = empty( $instance['post3'] ) ? '' : intval( $instance['post3'] );
		$post4  = empty( $instance['post4'] ) ? '' : intval( $instance['post4'] );
		$post1_short = ( 'yes' === $instance['post1_short'] ) ? 'checked' : '';
		$post2_short = ( 'yes' === $instance['post2_short'] ) ? 'checked' : '';
		$post3_short = ( 'yes' === $instance['post3_short'] ) ? 'checked' : '';
		$post4_short = ( 'yes' === $instance['post4_short'] ) ? 'checked' : '';
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post1' ) ); ?>">Prvi post</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post1' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post1' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post1 ); ?>"></p>
		<p>
			<input type="checkbox" value="yes" <?php echo esc_html( $post1_short ) ?>
			       name="<?php echo esc_attr( $this->get_field_name( 'post1_short' ) ) ?>"
			       id="<?php echo esc_attr( $this->get_field_id( 'post1_short' ) ) ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'post1_short' ) ); ?>">Kraći naslov</label>
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post2' ) ); ?>">Drugi post</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post2' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post2' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post2 ); ?>"></p>
		<p>
			<input type="checkbox" value="yes" <?php echo esc_html( $post2_short ) ?>
			       name="<?php echo esc_attr( $this->get_field_name( 'post2_short' ) ) ?>"
			       id="<?php echo esc_attr( $this->get_field_id( 'post2_short' ) ) ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'post2_short' ) ); ?>">Kraći naslov</label>
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post3' ) ); ?>">Treći post</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post3' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post3' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post3 ); ?>"></p>
		<p>
			<input type="checkbox" value="yes" <?php echo esc_html( $post3_short ) ?>
			       name="<?php echo esc_attr( $this->get_field_name( 'post3_short' ) ) ?>"
			       id="<?php echo esc_attr( $this->get_field_id( 'post3_short' ) ) ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'post3_short' ) ); ?>">Kraći naslov</label>
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post4' ) ); ?>">Četvrti post</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post4' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post4' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post4 ); ?>"></p>
		<p>
			<input type="checkbox" value="yes" <?php echo esc_html( $post4_short ) ?>
			       name="<?php echo esc_attr( $this->get_field_name( 'post4_short' ) ) ?>"
			       id="<?php echo esc_attr( $this->get_field_id( 'post4_short' ) ) ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'post4_short' ) ); ?>">Kraći naslov</label>
		</p>




	<?php
	}
}

register_widget( 'Nethr_Four_Large_Widget' );
