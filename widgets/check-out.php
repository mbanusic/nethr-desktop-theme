<?php

class Nethr_Check_Out_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_check_out_widget', 'Nethr: Ne propustite', array(
			'description' => 'Prikaz Ne propustite vijesti',
		) );
	}

	public function widget( $args, $instance ) {
		$cat = nethr_get_top_category();
		$name = $cat->name;
		$slug = $cat->slug;
		$data = wp_cache_get( 'nethr_check_out_widget_' . $slug, 'nethr_widgets' );
		if ( ! $data ) {
			$posts = get_transient( 'nethr_most_read_' . $slug );
			if ( empty( $posts ) ) {
				$posts = get_transient( 'nethr_most_read_danas' ); //fallback
				$name = 'Danas';
				if ( empty( $posts ) ) {
					return;
				}
			}
			ob_start();
			?>
			<div class="widget ne-propustite">
				<div class="widget-header cf">
					<h2 class="section-title"><?php echo esc_html( trim( $name . ' ' . $instance['title'] ) ); ?></h2>
				</div>
				<div class="widget-body cf">
					<?php
					$number = $instance['number'];
					$args = array(
						'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
						'post__in' => $posts,
						'orderby' => 'post__in',
						'no_found_rows'  => true,
						'posts_status' => 'publish',
						'post_type' => array( 'post', 'promo' ),
						'ignore_sticky_posts' => true
					);
					$articles = new WP_Query( $args );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							get_template_part( 'templates/articles/article-4' );
						}
						wp_reset_postdata();
					}
					?>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_check_out_widget_' . $slug, $data, 'nethr_widgets', HOUR_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['days'] = empty( $new_instance['days'] ) ? 2 : absint( $new_instance['days'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$days = intval( $instance['days'] );
		$number = intval( $instance['number'] );
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'days' ) ); ?>">Broj dana</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'days' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'days' ) ); ?>"
				type="text" value="<?php echo esc_attr( $days ); ?>" size="3">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">Broj članaka</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>
	<?php
	}

}

register_widget( 'Nethr_Check_Out_Widget' );
