<?php

class Nethr_Category_Four_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_category_four_widget', 'Category Four widget',
			array(
				'description' => '4 članka u kategoriji',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$location = isset( $instance['location'] ) ? $instance['location']: 'category';
		$data = wp_cache_get( 'nethr_category_four_widget_'.$location, 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();

			$args = array(
				'posts_per_page' => 4,
				'tax_query' => array(
					array(
						'taxonomy'  => 'positions',
						'field'     => 'slug',
						'terms'     => $location,
					),
				),
				'no_found_rows'  => true,
				'posts_status' => 'publish'
			);
			$articles = new WP_Query( $args );
			if ( $articles->have_posts() ) {
				while ( $articles->have_posts() ) {
					$articles->the_post();
					get_template_part( 'templates/articles/article-2' );
				}
			}
			wp_reset_postdata(); ?>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_category_four_widget_'.$location, $data, 'nethr_widgets', 5 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {

		return $instance;
	}

	function form( $instance ) {


	}
}

register_widget( 'Nethr_Category_Four_Widget' );
