<?php

class Nethr_Freemail_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_freemail_widget', 'Freemail',
			array(
				'classname'   => 'ovo_je_neka_klasa',
				'description' => 'Freemail widget',
			) );
	}

	public function widget( $args, $instance ) {
			?>

			<div class="widget freemail">
				<div class="widget-header cf">
					<h2 class="section-title"><i class="fa fa-envelope"></i> Freemail</h2>
					<a class="more" href="https://prijava.net.hr/registracija/" target="_blank">
						Otvori besplatan E-mail
					</a>
				</div>
				<div class="widget-body">
					<form id="freemail-widget-form" method="post" action="https://user.net.hr/">
						<input type="hidden" name="ego_domain" value="net.hr" class="hidden">
						<input type="hidden" id="url" name="url" value="https://freemail.net.hr" class="hidden">
						<input type="text" name="ego_user" placeholder="Korisničko ime..."/>
						<input type="password" name="ego_secret" placeholder="Lozinka..."/>
						<div class="form-bottom cf">
							<div class="half left">
								<a href="https://prijava.net.hr/ZaboravljenaLozinka/" target="_blank">Zaboravljena lozinka?</a>
							</div>
							<div class="half left">
								<input type="submit" value="Prijavi se" class="button send-button"/>
							</div>
						</div>
					</form>
				</div>
			</div>

			<?php
	}

	function update( $new_instance, $instance ) {
		return $instance;
	}

	function form( $instance ) {

		?>
		<p>Pozicija Freemail widgeta na naslovnici</p>
	<?php
	}
}

register_widget( 'Nethr_Freemail_Widget' );
