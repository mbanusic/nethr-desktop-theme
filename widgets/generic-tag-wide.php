<?php

class Nethr_Generic_Tag_Wide_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_generic_tag_wide_widget', 'Naslovnica -> Članci po tagu', array(
            'description' => 'Prikaz widgeta za tag',
        ) );
    }

    public function widget( $args, $instance ) {
        $number = $instance['number'];
        $tag = $instance['tag'];
        $yellow_title = $instance['yellow_title'];
        $data   = wp_cache_get( 'nethr_generic_tag_wide_widget_'.$tag, 'nethr_widgets' );
        if ( ! $data ) {
            ob_start();
            ?>
            <div class="widget">

                <?php
                $section_title = 'section-title';
                if( $yellow_title ) {
                    $section_title = 'yellow-title';
                } ?>

                <div class="widget home-widget generic widget-wide">
                    <div class="widget-header cf">
                        <h2 class="<?php echo esc_attr( $section_title ); ?>">
                            <a href="<?php echo esc_url( site_url( '/tema/'. $instance['tag'] ) ); ?>">
                                <?php echo esc_html( $instance['title'] ); ?>
                            </a>
                        </h2>
                    </div>
                    <div class="widget-body cf">
                    <?php
                    $args = array(
                        'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
                        'post_type' => array( 'post', 'webcafe' ),
                        'tag' => $tag,
                        'no_found_rows'  => true,
                        'posts_status' => 'publish'
                        );
                    $articles = new WP_Query( $args );
                        if ( $articles->have_posts() ) {
	                        while ( $articles->have_posts() ) {
		                        $articles->the_post(); ?>
		                        <article class="article-2">
			                        <div class="inner">

				                        <a href="<?php the_permalink(); ?>">
					                        <div class="thumb">
						                        <?php the_post_thumbnail( 'article-2' ); ?>
					                        </div>
					                        <div class="article-text">
						                        <h2 class="overtitle danas">
							                        <?php
							                        $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
							                        if ( $tag === 'lijepenase' ) {
								                        $title = explode( ' ', $titles['short_title'] );
								                        $last  = array_pop( $title );
								                        echo esc_html( $last );
							                        }
							                        else {
								                        echo esc_html( $titles['over_title'] );
							                        } ?>
						                        </h2>
						                        <?php
						                        if ( $tag !== 'lijepenase' ) {
							                        ?>
							                        <h1 class="title">
								                        <?php the_title() ?>
							                        </h1>
						                        <?php } ?>
					                        </div>
				                        </a>

			                        </div>
		                        </article>
		                        <?php
	                        }
	                        wp_reset_postdata();
                        }
                    ?>
                    </div>
                </div>
            </div>
            <?php
            $data = ob_get_clean();
            wp_cache_set( 'nethr_generic_tag_wide_widget_'.$tag, $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
        }
        echo $data;
    }

    function update( $new_instance, $instance ) {
        $instance['title']  = sanitize_text_field( $new_instance['title'] );
        $instance['tag']  = sanitize_text_field( $new_instance['tag'] );
        $instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
        $instance['yellow_title']  = intval( $new_instance['yellow_title'] );

        return $instance;
    }

    function form( $instance ) {
        $title  = empty( $instance['title'] ) ? '' : $instance['title'];
        $tag  = empty( $instance['tag'] ) ? '' : $instance['tag'];
        $number  = intval( $instance['number'] );
        $yellow_title  = intval( $instance['yellow_title'] );
        ?>
        <p>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'tag' ) ); ?>"><?php esc_html_e( 'Tag:', 'nethr' ); ?></label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'tag' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'tag' ) ); ?>"
                type="text" value="<?php echo esc_attr( $tag ); ?>">
        </p>
        <p>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
                type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
        </p>

        <p>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'yellow_title' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'yellow_title' ) ); ?>"
                type="checkbox" value="1" <?php if ( $yellow_title ) echo 'checked'; ?>>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'yellow_title' ) ); ?>">&nbsp;Žuti sticker?</label>
        </p>

        <?php
    }

}

register_widget( 'Nethr_Generic_Tag_Wide_Widget' );