<?php

class Nethr_Banner_Widget extends WP_Widget {


	public function __construct() {
		parent::__construct( 'nethr_banner', 'Banner',
			array(
				'description' => 'Banner widget',
			) );
	}

	public function widget( $args, $instance ) {
	    $size = $instance['size'];
			switch ( $size ) {
				case 'nethr_desktop_homepage_billboard_v1':
				case 'nethr_desktop_homepage_billboard_v2':
				case 'nethr_desktop_billboard_v1':
					?>
					<div class="billboard">
						<div class="banner">
							<?php $this->banner_slave( $size ); ?>
						</div>
					</div>
					<?php
					break;
				case 'nethr_desktop_wallpaper_left':
				case 'nethr_desktop_homepage_wallpaper_left':
					?>
					<div class="wallpaper fixed wallpaper-left">
						<div class="banner">
							<?php $this->banner_slave( $size ) ?>
						</div>
					</div>
					<?php
					break;
				case 'nethr_desktop_wallpaper_right':
				case 'nethr_desktop_homepage_wallpaper_right':
					?>
					<div class="wallpaper fixed wallpaper-right">
						<div class="banner">
							<?php $this->banner_slave( $size ) ?>
						</div>
					</div>
					<?php
					break;
				default:
					?>
					<div class="banner banner-size3">
						<?php $this->banner_slave( $size ); ?>
					</div>
					<?php
					break;
			}

	}

	function banner_slave($id) {
	    //special case for World Championship

	    if ( is_category( 'nogom-u-metu' ) ) {
	        if ( !jetpack_is_mobile() ) {
		        if ( 'nethr_desktop_billboard_v1' === $id ) {
			        ?><img
                    src="<?php echo esc_url( get_template_directory_uri() . '/img/kaufland/billboard.png' ) ?>"
                    width="970" height="250"><?php
		        } else if ( 'nethr_desktop_wallpaper_left' === $id ) {
			        ?><img
                    src="<?php echo esc_url( get_template_directory_uri() . '/img/kaufland/wallL.png' ) ?>"
                    width="300" height="900"><?php
		        } else if ( 'nethr_desktop_wallpaper_right' === $id ) {
			        ?><img
                    src="<?php echo esc_url( get_template_directory_uri() . '/img/kaufland/wallR.png' ) ?>"
                    width="300" height="900"><?php
		        } else {
			        return '';
		        }
	        }
	        else {
		        if ( 'nethr_mobile_header' === $id ) {
			        ?><img
                    src="<?php echo esc_url( get_template_directory_uri() . '/img/kaufland/kaufland_nogomUmetu_mobile_noCTA.jpg' ) ?>"
                    width="300" height="250"><?php
		        }
		        else {
			        return '';
		        }
            }
        }
        else {
	        ?>
            <!-- /1092744/telegram -->
            <div id='<?php echo esc_attr( $id ) ?>'>
                <script>
                    googletag.cmd.push(function () {
                        googletag.display('<?php echo esc_attr( $id ) ?>');
                    });
                </script>
            </div>
	        <?php
        }
	}

	function update( $new_instance, $instance ) {
		$instance['size'] = sanitize_text_field( $new_instance['size'] );
		return $instance;
	}

	function form( $instance ) {
		$size = $instance['size'];
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>">Veličina</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'size' ) ); ?>"
				type="text" value="<?php echo esc_attr( $size ); ?>"></p>

	<?php
	}
}

register_widget( 'Nethr_Banner_Widget' );
