<?php

class Nethr_Gallery_Video_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_gallery_video_widget', 'Net.hr: Gallerije and Video',
			array(
				'description' => 'Galerije i Video za kategorije',
			) );
	}

	public function widget( $args, $instance ) {
		$slug = 'danas';
		$cat = nethr_get_top_category();
		if ( $cat && !is_wp_error( $cat ) ) {
			$slug = $cat->slug;
		}
		$data = wp_cache_get( 'gallery_video_widget_' . $slug, 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="big-gallery-widget cf">

				<div class="section-header">
					<div class="section-titles">
						<a href="#" class="active">Sve galerije</a>
						<a href="<?php echo esc_url( get_post_format_link('gallery') ); ?>">Foto</a>
						<a href="<?php echo esc_url( get_post_format_link('video') ); ?>">Video</a>
					</div>
				</div>
				<?php
				if ( 'auto' === $slug ) {
					$articles = z_get_zone_query( 'kategorija-auto-galerije', array( 'posts_per_page' => 4 ) );
				}
				else {
					$args = array(
						'posts_per_page' => 4,
						'tax_query'      => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'post_format',
								'field'    => 'slug',
								'terms'    => array(
									'post-format-gallery',
									'post-format-video',
								),
							),
							array(
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => $slug
							)
						),
						'no_found_rows'  => true,
						'posts_status'   => 'publish'
					);

					$articles = new WP_Query( $args );
				}
				if ( $articles->have_posts() ) {
					$opened = false;
					while ( $articles->have_posts() ) {
						$articles->the_post();
						if ( 0 === $articles->current_post )  {
							?>
							<div class="grid-item-1"><?php
								nethr_get_template( 'templates/articles/article-gallery-1', array( 'gallery_view' => true ) );
								?>
							</div>
							<?php
						}
						if ( 1 === $articles->current_post ) {
							?><div class="grid-item-2 cf"><?php
							$opened = true;
						}
						if ( $articles->current_post > 0 ) {
							nethr_get_template( 'templates/articles/article-3', array( 'gallery_view' => true, 'short' => 'yes' ) );
						}
						if ( 3 === $articles->current_post ) {
							?></div><?php
							$opened = false;
						}
					}
					if ( $opened ) {
						?></div><?php
					}
				}
				wp_reset_postdata();
				?>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'gallery_video_widget_'.$slug, $data, 'nethr_widgets', 20 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];

		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'twentyfourteen' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>"></p>

		<?php
	}
}

register_widget( 'Nethr_Gallery_Video_Widget' );
