<?php

class Nethr_Clickattack_Banner extends WP_Widget {
	public function __construct() {
		parent::__construct( 'nethr_clickattack_banner', 'Nethr: CA Banner',
			array(
				'description' => 'CA Banner widget',
			) );
	}

	public function widget( $args, $instance ) {
		$id = 'apthj3rdawlwop6j5d8eg6';
		$category = false;
		if ( is_single() ) {
			if ( 'zena' === get_post_type() ) {
				$category = 'zena';
			}
			else {
				$cat = nethr_get_top_category();
				if ( $cat && ! is_wp_error( $cat ) ) {
					$category = $cat->slug;
				}
			}
		}
		else if ( is_category() ) {
			$cat = get_category( get_query_var( 'cat' ) );
			while ( $cat->parent ) {
				$cat = get_category( $cat->parent );
			}
			$category = $cat->slug;
		}
		if ( $category ) {
			switch ( $category ) {
				case 'sport':
					$id = 'spyca7nx7u1lda26qserm1';
					break;
				case 'magazin':
					$id = 'px1tlczv2lh23o7nnicd65';
					break;
				case 'auto':
					$id = 'cnjpotkfvth39o71al6mj0';
					break;
				case 'tehnoklik':
					$id = 'nkjs537w2lh2fo7z00sc79';
					break;
				case 'zena':
					$id = '7u3wddxtdqh2dl8t40dnlg';
					break;
				default:
					$id = 'apthj3rdawlwop6j5d8eg6';
					break;
			}
		}
		?>
		<script id ="apid_<?php echo esc_attr( $id ); ?>" type="text/javascript">
			var script= document.createElement("script");
			script.setAttribute("src", "//mas.nth.ch/mas/mas.jsp?apid=<?php echo esc_attr( $id ) ?>");
			script.type = "text/javascript";
			var head = document.getElementsByTagName("head")[0];
			head.appendChild(script);
		</script>
		<?php
	}
}

register_widget( 'Nethr_Clickattack_Banner' );