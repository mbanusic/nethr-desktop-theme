<?php

class Nethr_Telegram_Home_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_telegram_home_widget', 'Telegram widget - Naslovnica', array(
            'description' => 'Prikaz Telegram widgeta na naslovnici',
        ) );
    }


    public function widget( $args, $instance ) {
	    $feed = nethr_get_telegram_posts();
	    if ( $feed && !empty( $feed ) ) {
		    ?>
		    <div class="widget home-widget telegram widget-wide">
			    <div class="widget-header cf">
				    <h2 class="section-title">
					   <a href="https://www.telegram.hr?utm_source=NetNaslovnica&utm_medium=Widget&utm_campaign=NetWidget" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/tg_widget_logo.gif" width="124" height="24"/></a>
				    </h2>
			    </div>
			    <div class="widget-body gray cf">
				    <?php foreach ( $feed as $item ) {
					    ?>
					    <article class="article-2">
						    <div class="inner">

							    <a href="<?php echo esc_url( $item['link'] . '?utm_source=NetNaslovnica&utm_medium=Widget&utm_campaign=NetWidget' ) ?>"
							       target="_blank">
								    <div class="thumb">
									    <img
										    src="<?php echo esc_url( $item['image'] ) ?>"
										    width="186" height="119">
								    </div>
								    <div class="article-text">
									    <h2 class="overtitle danas">
										    Telegram
									    </h2>

									    <h1 class="title">
										    <?php echo esc_html( $item['title'] ) ?>
									    </h1>
								    </div>
							    </a>

						    </div>
					    </article>
				    <?php } ?>
			    </div>
		    </div>
		    <?php
	    }
    }

    function update( $new_instance, $instance ) {
        return $instance;
    }

    function form( $instance ) {

    }

}

register_widget( 'Nethr_Telegram_Home_Widget' );
