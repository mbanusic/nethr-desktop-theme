<?php

class Nethr_Telegram_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_telegram_widget', 'Telegram widget', array(
            'description' => 'Prikaz Telegram widgeta',
        ) );
    }

    public function widget( $args, $instance ) {
        $feed = nethr_get_telegram_posts();
        if ( $feed && !empty( $feed ) ) {
            ?>
            <div class="widget telegram-article">
                <div class="widget-header cf">
                    <h2 class="section-title">
                        <a href="https://www.telegram.hr?utm_source=NetClanak&utm_medium=Widget&utm_campaign=NetWidget" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/tg_widget_logo.gif" width="124" height="24"/></a>
                    </h2>
                </div>
                <div class="widget-body gray cf">
                    <?php
                        foreach( $feed as $item ) {
	                        ?>
	                        <article class="article-3 cf">
		                        <div class="inner cf">

			                        <a href="<?php echo esc_url( $item['link'] . '?utm_source=NetClanak&utm_medium=Widget&utm_campaign=NetWidget' ); ?>" target="_blank">

				                        <div class="thumb">
					                        <img src="<?php echo esc_url( $item['image'] ); ?>" width="100" height="60">
				                        </div>
				                        <div class="article-text">
					                        <h2 class="overtitle danas">Telegram</h2>
					                        <h1 class="title"><?php echo esc_html( $item['title'] ); ?></h1>
				                        </div>
			                        </a>
		                        </div>
	                        </article>
	                        <?php
                        }
                    ?>
                </div>
            </div>
            <?php
        }
    }

    function update( $new_instance, $instance ) {
        return $instance;
    }

    function form( $instance ) {

    }

}

register_widget( 'Nethr_Telegram_Widget' );
