<?php

class Nethr_Fossil_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_fossil_widget', 'Nethr: Fossil',
            array(
                'description' => 'Widget Fossil',
            ) );
    }

    public function widget( $args, $instance ) {
       ?><style>
            .widget-satovi article {
                width: 19.99%;
                text-align: center;
                float: left;
            }

            .widget-satovi article .inner {
                padding: 20px 10px;
            }

            .widget-satovi .btn1 {
                background-color: #f4511e;
                color: white;
                padding: 5px 20px;
                text-align: center;
                text-transform: uppercase;
                font-size: 10px;
                font-family: "Oswald";
                line-height: 45px;

            }

            .widget-satovi a:hover {
                background-color: rgb(194, 0, 0);
            }

            .widget-satovi .article-text {
                font-family: "Oswald";
                text-align: center;
                font-size: 21px;
                line-height: 25px;
                color: #333 !important;
            }
        </style>
        <div class="widget home-widget widget-satovi" style="max-width:960px">
            <div class="widget-header cf" style="text-align: center">
                <h2 class="section-title">
                    Proljetni trendovi: satovi
                </h2>
            </div>
            <div class="widget-body cf">
                <article>
                    <div class="inner">
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank">
                            <div class="thumb" style="height:auto;">
                                <img src="https://adriaticmedianethr.files.wordpress.com/2019/03/1-19.jpg?quality=100&strip=all&amp"
                                     style="width:100%" alt=" Muški stil u smeđoj i plavoj" />
                            </div>
                            <div class="article-text">
                                <h1>
                                    Muški stil u smeđoj i plavoj
                                </h1>
                            </div>
                        </a>
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank" class="btn1">Kupi ovdje</a>
                    </div>
                </article>
                <article>
                    <div class="inner">
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank">
                            <div class="thumb" style="height:auto">
                                <img src="https://adriaticmedianethr.files.wordpress.com/2019/03/2-13.jpg?quality=100&strip=all&amp"
                                     style="width:100%" alt="Srebrni sjaj na ženskoj ruci">
                            </div>
                            <div class="article-text">
                                <h1>
                                    Srebrni sjaj na ženskoj ruci
                                </h1>
                            </div>
                        </a>
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank" class="btn1">Kupi ovdje</a>
                    </div>
                </article>
                <article>
                    <div class="inner">
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank">
                            <div class="thumb" style="height:auto">
                                <img src="https://adriaticmedianethr.files.wordpress.com/2019/03/3-10.jpg?quality=100&strip=all&amp "
                                     style="width:100%" alt="Decentna bijela elegancija">
                            </div>
                            <div class="article-text">
                                <h1>
                                    Decentna bijela elegancija
                                </h1>
                            </div>
                        </a>
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank" class="btn1">Kupi ovdje</a>
                    </div>
                </article>
                <article>
                    <div class="inner">
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank">
                            <div class="thumb" style="height:auto">
                                <img src="https://adriaticmedianethr.files.wordpress.com/2019/03/4-9.jpg?quality=100&strip=alll"
                                     style="width:100%" alt="Muževan modni dodatak">
                            </div>
                            <div class="article-text">
                                <h1>
                                    Muževan modni &nbsp; dodatak&nbsp;
                                </h1>
                            </div>
                        </a>
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank" class="btn1">Kupi ovdje</a>
                    </div>
                </article>
                <article>
                    <div class="inner">
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank">
                            <div class="thumb" style="height:auto">
                                <img src="https://adriaticmedianethr.files.wordpress.com/2019/03/5-6.jpg?quality=100&strip=all&amp"
                                     style="width:100%" alt="Vrijeme je za Fossil Smart">
                            </div>
                            <div class="article-text">
                                <h1>
                                    Vrijeme je za Fossil Smart
                                </h1>
                            </div>
                        </a>
                        <a href="https://www.satovi.com/satovi/fossil" target="_blank" class="btn1">Kupi ovdje</a>
                    </div>
                </article>

            </div>
        </div><?php
    }
}

register_widget( 'Nethr_Fossil_Widget' );
