<?php

class Nethr_Instar_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_instar_widget', 'Net Instar widget', array(
			'description' => 'Prikaz Instar na naslovnici',
		) );
	}

	public function widget( $args, $instance ) {
        $data = array (
            1203 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-samsung-galaxy-s10-g973f-61-1440x3040px-octa-core-273ghz-8gb-ram-128gb-memorija-dual-sim-4glte-android-90-crni/INS-51517/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-samsung-galaxy-s10-g973f-61-1440-ins-51517_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Samsung Galaxy S10',
                            'old_price' => '7.156,84 Kn',
                            'new_price' => '6.799,00 Kn',
                            'description' => '6.1", 1440x3040px, Octa-core 2.73GHz, 8GB RAM, 128GB Memorija, Dual SIM, 4G/LTE, Android 9.0, Crni',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-dell-inspiron-3576-156-fhd-intel-core-i5-8250u-up-to-340ghz-8gb-ddr4-256gb-ssd-amd-radeon-520-2gb-dvd-linux-2-god-akcija/272958703-N0533/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/272958703-n0533.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Dell Inspiron 3576',
                            'old_price' => '5.599,00 Kn',
                            'new_price' => '4.599,00 Kn',
                            'description' => '15.6" FHD, Intel Core i5-8250U up to 3.40GHz, 8GB DDR4, 256GB SSD, AMD Radeon 520 2GB, DVD, Linux, 2 god - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-hp-15-da0012nm-4pp73ea-156-fhd-intel-core-i3-7020u-230ghz-4gb-ddr4-1tb-hdd-128gb-ssd-nvidia-geforce-mx110-2gb-dvd-dos-3-god-best-buy/0381924/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0381924.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook HP 15-da0012nm',
                            'old_price' => '',
                            'new_price' => '3.749,00 Kn',
                            'description' => ' 4PP73EA, 15.6" FHD, Intel Core i3 7020U 2.30GHz, 4GB DDR4, 1TB HDD + 128GB SSD, NVIDIA GeForce MX110 2GB, DVD, DOS, 3 god  - BEST BUY',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/fifa-19-ps4/3202050314/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/fifa-19-ps4-3202050314_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'FIFA 19 PS4',
                            'old_price' => '577,89 Kn',
                            'new_price' => '299,00 Kn',
                            'description' => '',
                        ),
                ),
            1303 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-asus-vivobook-x540ub-dm031-156-fhd-intel-core-i5-7200u-up-to-31ghz-8gb-ddr-256gb-ssd-nvidia-geforce-mx110-2gb-no-odd-linux-2-god-akcija/asus-x540ub-dm031/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-asus-vivobook-x540ub-dm031-156--asus-x540ub-dm031_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Asus VivoBook X540UB-DM031',
                            'old_price' => '5.299,00 Kn',
                            'new_price' => '4.299,00 Kn',
                            'description' => ' 15.6" FHD, Intel Core i5-7200U up to 3.1GHz, 8GB DDR, 256GB SSD, NVIDIA GeForce MX110 2GB, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-samsung-a600f-galaxy-a6-2018-56-super-amoled-720x1480px-octa-core-16-ghz-3gb-ram-32gb-memorija-dual-sim-crni/INS-0541/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/samsung-a600f-galaxy-a6-2018-ds-32gb-bla-02411446_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Samsung A600F Galaxy A6 2018',
                            'old_price' => '',
                            'new_price' => '1.548,99 Kn',
                            'description' => ' 5.6" Super AMOLED 720x1480px, Octa-core 1.6 GHz, 3GB RAM, 32GB Memorija, Dual SIM, Crni',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/pametni-sat-samsung-r760-gear-s3-frontier-akcija/INS-0440/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/pametni-sat-samsung-r760-gear-s3-frontie-ins-0440_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Pametni sat SAMSUNG R760 Gear S3 Frontier - AKCIJA',
                            'old_price' => '2.099,00 Kn',
                            'new_price' => '1.599,00 Kn',
                            'description' => '',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/tv-tesla-49-49s317bf-led-full-hd-akcija/49S317BF/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/tesla-led-tv-full-hd-rezolucija-49s317bf-49s317bf_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'TV Tesla 49"',
                            'old_price' => '2.990,00 Kn',
                            'new_price' => '2.499,00 Kn',
                            'description' => ' 49S317BF,  LED, Full HD   - AKCIJA',
                        ),
                ),
            1403 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-acer-aspire-1-nxshxex048-14-hd-intel-celeron-n3350-240ghz-4gb-ddr4-64gb-ssd-intel-hd-graphics-500-no-odd-win-10-2-god-akcija/0852200/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0852200.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Acer Aspire 1',
                            'old_price' => '2.399,00 Kn',
                            'new_price' => '1.899,00 Kn',
                            'description' => ' NX.SHXEX.048, 14" HD, Intel Celeron N3350 2.40GHz, 4GB DDR4, 64GB SSD, Intel HD Graphics 500, no ODD, Win 10, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-hp-15-db0041nm-5sy85ea-156-fhd-amd-ryzen-5-2500u-up-to-360ghz-4gb-ddr4-256gb-nvme-ssd-amd-radeon-rx-vega-8-no-odd-dos-3-god-akcija/0385828/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-hp-15-db0041nm-5sy85ea-156-fhd--0385828_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook HP 15-db0041nm',
                            'old_price' => '4.199,00 Kn',
                            'new_price' => '3.399,00 Kn',
                            'description' => ' 5SY85EA, 15.6" FHD, AMD Ryzen 5 2500U up to 3.60GHz, 4GB DDR4, 256GB NVMe SSD, AMD Radeon RX Vega 8, no ODD, DOS, 3 god - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-samsung-a605f-galaxy-a6-2018-60-super-amoled-1080x2220px-octa-core-18-ghz-3gb-ram-32gb-memorija-dual-sim-crni-best-buy/INS-0544/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/samsung-a605f-galaxy-a6-2018-ds-32gb-bla-02411449_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Samsung A605F Galaxy A6+ 2018',
                            'old_price' => '',
                            'new_price' => '1.899,00 Kn',
                            'description' => ' 6.0" Super AMOLED 1080x2220px, Octa- Core 1.8 GHz, 3GB RAM, 32GB Memorija, Dual SIM, Crni - BEST BUY',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/pametni-sat-samsung-r800-galaxy-watch-46mm-silver-best-buy/INS-1000/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/pametni-sat-samsung-r800-galaxy-watch-46-ins-1000_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Pametni sat SAMSUNG R800 Galaxy Watch 46mm',
                            'old_price' => '',
                            'new_price' => '2.099,00 Kn',
                            'description' => ' Silver - BEST BUY',
                        ),
                ),
            1503 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/ultrabook-acer-swift-3-nxgxzex034-14-fhd-ips-intel-core-i3-8130u-up-to-340ghz-4gb-ddr4-128gb-ssd-intel-uhd-graphics-620-no-odd-linux-2-god-akcija/ACR-2086/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/acr-2086.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Ultrabook Acer Swift 3',
                            'old_price' => '4.699,00 Kn',
                            'new_price' => '3.799,00 Kn',
                            'description' => ' NX.GXZEX.034, 14" FHD IPS, Intel Core i3-8130U up to 3.40GHz, 4GB DDR4, 128GB SSD, Intel UHD Graphics 620, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/racunalo-instar-gamer-hydra-ryzen-3-2200g-up-to-37ghz-8gb-ddr4-120gb-ssd-1tb-hdd-nvidia-geforce-gtx1050ti-4gb-no-odd-5-god-jamstvo-akcija/2200G-HYD-HDD-SSD-1050TI/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/racunalo-instar-gamer-hydra-ryzen-3-2200-2200g-hyd-hdd-ssd-1050ti_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Računalo INSTAR Gamer HYDRA',
                            'old_price' => '5.599,00 Kn',
                            'new_price' => '4.599,00 Kn',
                            'description' => ' Ryzen 3 2200G up to 3.7GHz, 8GB DDR4, 120GB SSD + 1TB HDD, Nvidia GeForce GTX1050Ti 4GB, no ODD, 5 god jamstvo - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/monitor-samsung-235-ls24f356fhuxen-gaming-amd-freesync-vga-hdmi-full-hd-best-buy/0222689/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0222689.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Monitor Samsung 23.5" LS24F356FHUXEN',
                            'old_price' => '',
                            'new_price' => '919,00 Kn',
                            'description' => ' Gaming, AMD FreeSync, VGA, HDMI, Full HD - BEST BUY',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/gaming-combo-set-redragon-combo-4u1-mis-tipkovnica-podloga-slusalice/6950376750341/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/gaming-combo-set-redragon-combo-4u1-mis--6950376750341_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Gaming Combo set',
                            'old_price' => '',
                            'new_price' => '299,00 Kn',
                            'description' => ' REDRAGON COMBO 4u1, miš + tipkovnica + podloga + slušalice',
                        ),
                ),
            1803 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/ultrabook-asus-zenbook-ux410ua-gv572-14-fhd-ips-intel-core-i3-8130u-up-to-34ghz-4gb-ddr4-256gb-ssd-intel-uhd-graphics-620-no-odd-linux-2-god-akcija/UX410UA-GV572/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/ultrabook-asus-zenbook-ux410ua-gv572-14--ux410ua-gv572_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Ultrabook Asus ZenBook UX410UA-GV572',
                            'old_price' => '5.299,00 Kn',
                            'new_price' => '4.299,00 Kn',
                            'description' => ' 14" FHD IPS, Intel Core i3-8130U up to 3.4GHz, 4GB DDR4, 256GB SSD, Intel UHD Graphics 620, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-asus-vivobook-x570zd-e4166-156-fhd-amd-ryzen-7-2700u-up-to-380ghz-8gb-ddr4-256gb-m2-ssd-nvidia-geforce-gtx1050-4gb-no-odd-linux-2-god-akcija-/X570ZD-E4166/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-asus-vivobook-x570zd-e4166-156--x570zd-e4166_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook ASUS VivoBook X570ZD-E4166',
                            'old_price' => '7.499,00 Kn',
                            'new_price' => '6.099,00 Kn',
                            'description' => ' 15.6" FHD, AMD Ryzen 7 2700U up to 3.80GHz, 8GB DDR4, 256GB M2 SSD, NVIDIA GeForce GTX1050 4GB, no ODD, Linux, 2 god - AKCIJA ',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-8-64-gb-space-gray/INS-0419/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-8-64-gb-space-gray-ins-0419_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone 8 64 GB',
                            'old_price' => '',
                            'new_price' => '4.799,00 Kn',
                            'description' => ' Space Gray',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/tv-samsung-40-40nu7192-4k-smart-tv/02411456/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/tv-40-samsung-40nu7192-ultra-hd-smart-02411456_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'TV Samsung 40"',
                            'old_price' => '',
                            'new_price' => '2.530,80 Kn',
                            'description' => ' 40NU7192, 4K, SMART TV',
                        ),
                ),
            1903 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/e-book-kindle-paperwhite-2018-sp-6-8gb-wifi-300dpi-black-akcija/EREAMA014/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/ereader-kindle-paperwhite-2018-sp-6-8gb--ereama014_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'E-Book Kindle Paperwhite 2018 SP',
                            'old_price' => '1.499,00 Kn',
                            'new_price' => '1.199,00 Kn',
                            'description' => ' 6" 8GB WiFi, 300dpi, black - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-8-plus-64-gb-space-gray/INS-0439/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-8-plus-64-gb-space--ins-0439_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone 8 Plus 64 GB',
                            'old_price' => '',
                            'new_price' => '5.699,00 Kn',
                            'description' => ' Space Gray',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-x-64-gb-space-gray/INS-0444/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-x-64-gb-space-gray--ins-0444_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone X 64 GB',
                            'old_price' => '',
                            'new_price' => '6.899,00 Kn',
                            'description' => ' Space Gray',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-xs-64-gb-space-grey/INS-0796/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-xs-64-gb-space-grey-ins-0796_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone XS',
                            'old_price' => '',
                            'new_price' => '8.499,00 Kn',
                            'description' => ' 64 GB, Space Grey',
                        ),
                ),
            2003 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-dell-inspiron-3573-156-hd-intel-pentium-silver-n5000-up-to-27ghz-4gb-ddr4-1tb-hdd-intel-uhd-graphics-605-dvd-linux-2-god-akcija-/273050336-N0598/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-dell-inspiron-3573-156-hd-intel-273050336-n0598_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Dell Inspiron 3573',
                            'old_price' => '2.599,00 Kn',
                            'new_price' => '2.149,00 Kn',
                            'description' => ' 15.6" HD, Intel Pentium Silver N5000 up to 2.7GHz, 4GB DDR4, 1TB HDD, Intel UHD Graphics 605, DVD, Linux, 2 god - AKCIJA ',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-lenovo-ideapad-330-81dc00fjsc-156-fhd-intel-core-i3-6006u-20ghz-4gb-ddr4-1tb-hdd-intel-hd-graphics-520-no-odd-dos-2-god-akcija/LEN-4779/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-lenovo-ideapad-330-156-fhd-tn-i-len-4779_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Lenovo IdeaPad 330',
                            'old_price' => '3.099,00 Kn',
                            'new_price' => '2.499,00 Kn',
                            'description' => ' 81DC00FJSC, 15.6" FHD, Intel Core i3-6006U 2.0GHz, 4GB DDR4, 1TB HDD, Intel HD Graphics 520, no ODD, DOS, 2 god - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-samsung-galaxy-s9-g960f-58-super-amoled-1440x2960px-octa-core-27-ghz-4gb-ram-64gb-memorija-crni-akcija/INS-0514/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-samsung-galaxy-s9-g960f-58-super-ins-0514_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Samsung Galaxy S9 G960F',
                            'old_price' => '4.898,25 Kn',
                            'new_price' => '4.299,00 Kn',
                            'description' => ' 5.8" Super AMOLED 1440x2960px, Octa-Core 2.7 GHz, 4GB RAM, 64GB Memorija, Crni - AKCIJA',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/pametni-sat-samsung-r810-galaxy-watch-42mm-rose-gold-/INS-1002/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/pametni-sat-samsung-r810-galaxy-watch-42-ins-1002_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Pametni sat SAMSUNG R810 Galaxy Watch 42mm',
                            'old_price' => '2.209,47 Kn',
                            'new_price' => '1.899,00 Kn',
                            'description' => ' Rose gold ',
                        ),
                ),
            2103 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/racunalo-instar-gamer-dominator-intel-core-i5-9400f-up-to-41ghz-8gb-ddr4-240gb-ssd-nvidia-geforce-gtx1050-2gb-no-dvd-5-god-jamstvo-best-buy/9400F-DOM-SSD/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/racunalo-instar-gamer-dominator-intel-co-9400f-dom-ssd_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Računalo INSTAR Gamer Dominator',
                            'old_price' => '',
                            'new_price' => '5.199,00 Kn',
                            'description' => ' Intel Core i5 9400F up to 4.1GHz, 8GB DDR4, 240GB SSD, NVIDIA GeForce GTX1050 2GB, no DVD, 5 god jamstvo - BEST BUY',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/monitor-aoc-245-g2590vxq-gaming-75-hz-freesync-1ms-vga-dp-hdmi-full-hd-akcija/aoc-g2590vqx/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/monitor-aoc-245-g2590vxq-gaming-75-hz-fr-aoc-g259vxq_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Monitor AOC 24.5" G2590VXQ',
                            'old_price' => '1.499,00 Kn',
                            'new_price' => '1.199,00 Kn',
                            'description' => ' Gaming, 75 Hz, FreeSync, 1ms, VGA, DP, HDMI, Full HD - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/gaming-combo-set-genesis-fury-thunderstreak-4in1-mis-tipkovnica-podloga-slusalice-best-buy/590196941059/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/gaming-combo-set-genesis-fury-thunderstr-590196941059_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Gaming Combo set',
                            'old_price' => '',
                            'new_price' => '249,00 Kn',
                            'description' => ' Genesis FURY Thunderstreak, 4in1, miš + tipkovnica + podloga + slušalice - BEST BUY',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/gaming-stolica-fury-avenger-m/NFF-1354/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/gaming-stolica-fury-avenger-m-nff-1354_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Gaming stolica Fury Avenger M',
                            'old_price' => '',
                            'new_price' => '969,00 Kn',
                            'description' => '',
                        ),
                ),
            2203 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-lenovo-ideapad-330-81dc00nysc-156-fhd-intel-core-i3-6006u-200ghz-8gb-ddr4-1tb-hdd-intel-hd-graphics-520-dvd-dos-2-god-akcija/06408395/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/06408395.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Lenovo IdeaPad 330',
                            'old_price' => '3.599,00 Kn',
                            'new_price' => '2.899,00 Kn',
                            'description' => ' 81DC00NYSC, 15.6" FHD, Intel Core i3 6006U 2.00GHz, 8GB DDR4, 1TB HDD, Intel HD Graphics 520, DVD, DOS, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-dell-inspiron-3576-156-fhd-intel-core-i5-8250u-up-to-340ghz-8gb-ddr4-256gb-ssd-amd-radeon-520-2gb-dvd-linux-2-god-akcija/272958703-N0533/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/272958703-n0533.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Dell Inspiron 3576',
                            'old_price' => '5.599,00 Kn',
                            'new_price' => '4.599,00 Kn',
                            'description' => ' 15.6" FHD, Intel Core i5-8250U up to 3.40GHz, 8GB DDR4, 256GB SSD, AMD Radeon 520 2GB, DVD, Linux, 2 god - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-huawei-p-smart-2019-621-1080x2340px-octa-core-22ghz-3gb-ram-64gb-memorija-dual-sim-4glte-android-90-crni/INS-152120/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-huawei-p-smart-2019-621-1080x234-ins-152120_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Huawei P Smart (2019)',
                            'old_price' => '',
                            'new_price' => '1.709,05 Kn',
                            'description' => ' 6.21" 1080x2340px, Octa-core 2.2GHz, 3GB RAM, 64GB Memorija, Dual SIM, 4G/LTE, Android 9.0, Crni',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/eksterni-disk-wd-my-passport-blue-1tb-wdbynn0010bbl-wesn-maxi-ponuda/0130694/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0130694.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Eksterni disk WD My Passport Blue 1TB',
                            'old_price' => '',
                            'new_price' => '445,00 Kn',
                            'description' => ' WDBYNN0010BBL-WESN - MAXI PONUDA',
                        ),
                ),
            2503 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-asus-vivobook-x570ud-dm371-156-fhd-intel-core-i5-8250u-up-to-340ghz-8gb-ddr4-256gb-m2-ssd-nvidia-geforce-gtx1050-4gb-no-odd-linux-2-god-akcija/X570UD-DM371/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-asus-vivobook-x570ud-dm371--x570ud-dm371_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook ASUS VivoBook X570UD-DM371',
                            'old_price' => '7.299,00 Kn',
                            'new_price' => '5.899,00 Kn',
                            'description' => ' 15.6" FHD, Intel Core i5 8250U up to 3.40GHz, 8GB DDR4, 256GB M2 SSD, NVIDIA GeForce GTX1050 4GB, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-xr-64gb-white/INS-1004/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-xr-64gb-white-ins-1004_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone XR',
                            'old_price' => '',
                            'new_price' => '6.299,00 Kn',
                            'description' => ' 64GB, White',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/racunalo-instar-gamer-anubis-pro-intel-core-i7-9700k-up-to-49ghz-vodeno-hladenje-16gb-ddr4-256gb-nvme-ssd-1tb-hdd-nvidia-geforce-rtx2080-8gb-no-odd-5-god-jamstvo/9700K-ANU-SSD256-2080/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/racunalo-instar-gamer-anubis-pro-intel-c-9700k-anu-ssd256-2080_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Računalo INSTAR Gamer Anubis Pro',
                            'old_price' => '',
                            'new_price' => '16.099,00 Kn',
                            'description' => ' Intel Core i7 9700K up to 4.9GHz, Vodeno hlađenje, 16GB DDR4, 256GB NVMe SSD + 1TB HDD, NVIDIA GeForce RTX2080 8GB, no ODD, 5 god jamstvo',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-alienware-17-r5-173-fhd-ips-intel-core-i7-8750h-up-to-41ghz-16gb-ddr4-256gb-pci-e-ssd-1tb-hdd-nvidia-geforce-gtx1070-8gb-no-odd-win-10-pro-3-god-best-buy/27295792-N0546/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-alienware-17-r5-173-qhd-ips-120-27995793-n0545_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Alienware 17 R5',
                            'old_price' => '',
                            'new_price' => '19.599,00 Kn',
                            'description' => ' 17.3" FHD IPS, Intel Core i7-8750H up to 4.1GHz, 16GB DDR4, 256GB PCI-e SSD + 1TB HDD, NVIDIA GeForce GTX1070 8GB, no ODD, Win 10 Pro, 3 god - BEST BUY',
                        ),
                ),
            2603 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-lenovo-ideapad-330-81dc00fusc-156-fhd-intel-core-i3-7100u-24ghz-8gb-ddr4-2tb-hdd-nvidia-geforce-mx130-2gb-dvd-dos-2-god-akcija/LEN-4740/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-lenovo-ideapad-330-81dc00fusc-1-len-4740_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Lenovo IdeaPad 330',
                            'old_price' => '4.199,00 Kn',
                            'new_price' => '3.499,00 Kn',
                            'description' => ' 81DC00FUSC, 15.6" FHD, Intel Core i3-7100U 2.4GHz, 8GB DDR4, 2TB HDD, NVIDIA GeForce MX130 2GB, DVD, DOS, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/ultrabook-acer-swift-3-nxgxzex035-14-fhd-ips-intel-core-i5-8250u-up-to-340ghz-8gb-ddr4-256gb-ssd-intel-uhd-graphics-620-no-odd-linux-2-god-akcija/NX.GXZEX.035-8GB/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/acr-2087.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Ultrabook Acer Swift 3',
                            'old_price' => '6.099,00 Kn',
                            'new_price' => '4749,05 kn',
                            'description' => ' NX.GXZEX.035, 14" FHD IPS, Intel Core i5-8250U up to 3.40GHz, 8GB DDR4, 256GB SSD, Intel UHD Graphics 620, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-xs-max-64-gb-space-grey-akcija/INS-0797/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-xs-max-64-gb-space--ins-0797_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone XS Max',
                            'old_price' => '10.119,25 Kn',
                            'new_price' => '9.399,01 Kn',
                            'description' => ' 64 GB, Space Grey - AKCIJA',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/google-chromecast-3-media-player-streamer/chrom-cast-eu3/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/media-player-google-chromecast-3-media-p-chrom-cast-eu3_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'GOOGLE CHROMECAST 3',
                            'old_price' => '',
                            'new_price' => '419,00 Kn',
                            'description' => ' Media player - streamer',
                        ),
                ),
            2703 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-samsung-galaxy-j6-2018-j600f-56-super-amoled-1480x720-px-octa-core-16-ghz-3gb-ram-32gb-memorija-crni-akcija/INS-0548/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-samsung-galaxy-j6-2018-j600f-56--ins-0548_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Samsung Galaxy J6 (2018) J600F',
                            'old_price' => '1.577,89 Kn',
                            'new_price' => '1.299,00 Kn',
                            'description' => ' 5,6" Super AMOLED 1480x720 px, Octa-core 1,6 GHz, 3GB RAM, 32GB Memorija, Crni - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/tv-samsung-55-55nu7172-4k-smart-tv/02411432/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/02411432.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'TV Samsung 55"',
                            'old_price' => '',
                            'new_price' => '3.624,85 Kn',
                            'description' => ' 55NU7172, 4K, SMART TV',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/monitor-samsung-27-gaming-ls27e330hzxen-hdmi-vga-full-hd-best-buy/0225052/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0225052.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Monitor Samsung 27" Gaming',
                            'old_price' => '1.569,74 Kn',
                            'new_price' => '1.399,00 Kn',
                            'description' => ' LS27E330HZX/EN, HDMI, VGA, Full HD - BEST BUY',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/tablet-samsung-galaxy-tab-e-t560-96-wxga-1280x800-spreadtrum-sc8830a-quad-core-13ghz-15gb-ram-8gb-rom-5mp2mp-wifi-80211-bgn-bluetooth-v40-crni-android-akcija/0410910/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0410910.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Tablet Samsung Galaxy Tab E T560',
                            'old_price' => '1.299,00 Kn',
                            'new_price' => '999,00 Kn',
                            'description' => ' 9.6" WXGA 1280x800, Spreadtrum SC8830A Quad Core 1.3GHz, 1.5GB RAM, 8GB ROM, 5MP/2MP, WiFi 802.11 b/g/n, Bluetooth v4.0, crni, Android - AKCIJA',
                        ),
                ),
            2803 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-asus-vivobook-x540ub-dm544-156-fhd-intel-core-i3-7020u-23ghz-8gb-ddr4-256gb-ssd-nvidia-geforce-mx110-2gb-no-odd-linux-2-god-akcija/asus-x540ub-dm544/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-asus-x540ub-i3-8gb-256gb-mx110--asus-x540ub-dm544_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Asus VivoBook X540UB-DM544',
                            'old_price' => '4.299,00 Kn',
                            'new_price' => '3.499,00 Kn',
                            'description' => ' 15.6" FHD, Intel Core i3-7020U 2.3GHz, 8GB DDR4, 256GB SSD, NVIDIA GeForce MX110 2GB, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-hp-17-ca0012nm-5sv75ea-173-hd-amd-ryzen-3-2200u-up-to-340ghz-8gb-ddr4-256gb-nvme-ssd-amd-radeon-vega-3-dvd-dos-3-god-best-buy/0385837/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-hp-17-ca0012nm-5sv75ea-173-hd-a-0385837_2.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook HP 17-ca0012nm',
                            'old_price' => '',
                            'new_price' => '3.419,05 Kn',
                            'description' => ' 5SV75EA, 17.3" HD+, AMD Ryzen 3 2200U up to 3.40GHz, 8GB DDR4, 256GB NVMe SSD, AMD Radeon Vega 3, DVD, DOS, 3 god - BEST BUY',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-huawei-mate-20-lite-63-1080x2340px-octa-core-22ghz-4gb-ram-64gb-memorija-android-81-crna-akcija/INS-0855/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-huawei-mate-20-lite-63-1080x2340-ins-0855_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Huawei Mate 20 Lite',
                            'old_price' => '2.735,79 Kn',
                            'new_price' => '2.499,00 Kn',
                            'description' => ' 6.3" 1080x2340px, Octa-core 2.2GHz, 4GB RAM, 64GB Memorija, Android 8.1, Crna - AKCIJA',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-xiaomi-pocophone-f1-618-1080x2246px-octa-core-28ghz-6gb-ram-64-gb-memorija-dualsim-android-81-crni-best-buy/INS-151895/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-xiaomi-pocophone-f1-618-1080x224-ins-151895_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Xiaomi Pocophone F1',
                            'old_price' => '',
                            'new_price' => '2.599,00 Kn',
                            'description' => ' 6.18" 1080x2246px, Octa-core 2.8GHz, 6GB RAM, 64 GB Memorija, DualSIM, Android 8.1, Crni - BEST BUY',
                        ),
                ),
            2903 =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-lenovo-ideapad-330-81d1006vsc-156-hd-intel-pentium-n5000-up-to-27ghz-4gb-ddr4-256gb-ssd-intel-uhd-graphics-605-dvd-dos-2-god-akcija/LEN-4604/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/notebook-lenovo-ideapad-330-81d1006vsc-1-len-4604_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Lenovo IdeaPad 330',
                            'old_price' => '3.099,00 Kn',
                            'new_price' => '2.499,00 Kn',
                            'description' => ' 81D1006VSC, 15.6" HD, Intel Pentium N5000 up to 2.7GHz, 4GB DDR4, 256GB SSD, Intel UHD Graphics 605, DVD, DOS, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-dell-gaming-inspiron-3779-g3-173-fhd-ips-intel-core-i7-8750h-up-to-41ghz-16gb-ddr4-2tb-hdd-256gb-pci-e-ssd-nvidia-geforce-gtx1060-6gb-no-odd-linux-3-god-akcija/273050425-N0561-1/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/273050425-n0561-1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Dell Gaming Inspiron 3779 G3',
                            'old_price' => '12.999,00 Kn',
                            'new_price' => '10.599,00 Kn',
                            'description' => ' 17.3" FHD IPS, Intel Core i7-8750H up to 4.1GHz, 16GB DDR4, 2TB HDD + 256GB PCI-e SSD, NVIDIA GeForce GTX1060 6GB, no ODD, Linux, 3 god - AKCIJA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-huawei-honor-7a-57-3gb-ram-32gb-memorija-dual-sim-android-80-crni/INS-0573/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-huawei-honor-7a-32gb-dual-sim-cr-ins-0573_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Huawei Honor 7A',
                            'old_price' => '',
                            'new_price' => '1.167,55 Kn',
                            'description' => ' 5.7", 3GB RAM , 32GB Memorija, Dual SIM, Android 8.0, Crni',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-xiaomi-mi-a2-lite-584-1080x2280px-octa-core-20ghz-3gb-ram-32gb-memorija-dual-sim-microsd-android-81-crni/INS-0566/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-xiaomi-mi-a2-lite-584-1080x2280p-ins-0566_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Xiaomi Mi A2 Lite',
                            'old_price' => '',
                            'new_price' => '1.349,00 Kn',
                            'description' => ' 5.84" 1080x2280px, Octa-core 2.0GHz, 3GB RAM, 32GB Memorija, Dual SIM, microSD, Android 8.1, Crni',
                        ),
                ),
            '0104' =>
                array (
                    0 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/notebook-acer-gaming-nitro-5-nhq3rex022-156-fhd-ips-amd-ryzen-5-2500u-up-to-360ghz-8gb-ddr4-256gb-ssd-amd-radeon-rx560x-4gb-no-odd-linux-2-god-akcija/0852207/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/0852207.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Notebook Acer Gaming Nitro 5',
                            'old_price' => '6.499,00 Kn',
                            'new_price' => '5.349,00 Kn',
                            'description' => ' NH.Q3REX.022, 15.6" FHD IPS, AMD Ryzen 5 2500U up to 3.60GHz, 8GB DDR4, 256GB SSD, AMD Radeon RX560X 4GB, no ODD, Linux, 2 god - AKCIJA',
                        ),
                    1 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-apple-iphone-7-32-gb-black-maxi-ponuda/INS-0051/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-apple-iphone-7-32-gb-black-ins-0051_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Apple iPhone 7 32 GB',
                            'old_price' => '',
                            'new_price' => '3.699,00 Kn',
                            'description' => ' Black - MAXI PONUDA',
                        ),
                    2 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/mobitel-huawei-p20-pro-61-oled-2240x1080px-octa-core-236ghz-6gb-ram-128gb-memorija-dual-sim-android-81-crni-akcija/INS-0522/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/mobitel-huawei-p20-pro-61-oled-fhd-2240x-ins-0522_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Mobitel Huawei P20 PRO',
                            'old_price' => '5.998,95 Kn',
                            'new_price' => '5.099,00 Kn',
                            'description' => ' 6.1" OLED 2240x1080px, Octa-core 2.36GHz, 6GB RAM, 128GB Memorija, Dual SIM,  Android 8.1, Crni - AKCIJA',
                        ),
                    3 =>
                        array (
                            'link' => 'https://www.instar-informatika.hr/pametni-sat-huawei-watch-3-gt-akcija/INS-1007/product/',
                            'image' => 'https://www.instar-informatika.hr/slike/velike/pametni-sat-huawei-watch-3-gt-ins-1007_1.jpg',
                            'overtitle' => 'AKCIJA',
                            'title' => 'Pametni sat Huawei Watch 3 GT - AKCIJA',
                            'old_price' => '1.999,00 Kn',
                            'new_price' => '1.599,00 Kn',
                            'description' => '',
                        ),
                ),
        );
        $today = date( 'dm' );
		if ( key_exists( $today, $data ) ) {
			?>
            <div class="widget home-widget instar-widget webshop-widget widget-wide cf">
                <div class="widget-header cf">
                    <h2 class="section-title">
                        <a href="https://www.instar-informatika.hr"
                           target="_blank"><img
                                    src="https://adriaticmedianethr.files.wordpress.com/2017/12/logo1.png"
                                    height="30"/></a>
                    </h2>
                </div>
                <div class="widget-body cf">
					<?php
					foreach ( $data[ $today ] as $item ) {
						?>
                        <article class="article-2">
                            <div class="inner">

                                <a href="<?php echo isset( $item['link'] ) ? esc_url( $item['link'] ) : '#' ?>"
                                   target="_blank">
                                    <div class="thumb">
                                        <img
                                                src="<?php echo isset( $item['image'] ) ? esc_url( $item['image'] ) : '' ?>"
                                                width="186" height="119">
                                    </div>
                                </a>
                                <div class="article-text">
                                    <h2 class="overtitle danas">
										<?php echo isset( $item['overtitle'] ) ? esc_html( $item['overtitle'] ) : ''; ?>
                                    </h2>
                                    <a href="<?php echo isset( $item['link'] ) ? esc_url( $item['link'] ) : '#' ?>"
                                       target="_blank">
                                        <h1 class="title">
											<?php echo esc_html( $item['title'] ) ?>
                                        </h1>
                                    </a>
                                    <div class="prices cf">
										<?php if ( isset( $item['new_price'] ) && $item['new_price'] ) { ?>
                                            <div class="left price"><span
                                                        class="red"><?php echo isset( $item['new_price'] ) ? esc_html( $item['new_price'] ) : '' ?></span>
                                                /
                                                <span><?php echo isset( $item['old_price'] ) ? esc_html( $item['old_price'] ) : '' ?></span>
                                            </div>
										<?php } else {
											?>
                                            <div class="left price"><span
                                                    class="red"><?php echo isset( $item['old_price'] ) ? esc_html( $item['old_price'] ) : '' ?></span>
                                            </div><?php
										} ?>
                                    </div>
                                    <p class="description">
										<?php
										echo isset( $item['description'] ) ? esc_html( $item['description'] ) : '' ?>
                                    </p>
                                    <a href="<?php echo isset( $item['link'] ) ? esc_url( $item['link'] ) : '#' ?>"
                                       class="btn" target="_blank">Kupi
                                        ovdje</a>
                                </div>

                            </div>
                        </article>
						<?php
					}
					?>
                </div>
            </div>
			<?php
		}
	}

}

register_widget( 'Nethr_Instar_Widget' );
