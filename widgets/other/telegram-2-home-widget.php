<?php

class Nethr_Telegram_2_Home_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_telegram_2_home_widget', 'Telegram widget 2 - Naslovnica', array(
            'description' => 'Prikaz Telegram widgeta na naslovnici',
        ) );
    }


    public function widget( $args, $instance ) {
        $feed = nethr_get_telegram_posts();
        if ( $feed && !empty( $feed ) ) {
            ?>
            <div class="widget home-widget telegram-widget widget-wide">
                <div class="widget-header cf">
                    <a href="https://www.telegram.hr?utm_source=NetNaslovnica&utm_medium=Widget&utm_campaign=NetWidget" target="_blank">
                        <h2 class="section-title">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/tg_widget_logo.png" width="81" height="10"/>
                        </h2>
                    </a>

                </div>
                <div class="widget-body cf">
                    <?php for ( $i = 0; $i < 4; $i++ ) {
                        $item = $feed[$i];
                       ?>
                        <article class="tg-article <?php if( $i === 0 ) { echo 'first'; } ?>">
                            <div class="inner">

                                <a href="<?php echo esc_url( $item['link'] . '?utm_source=NetNaslovnica&utm_medium=Widget&utm_campaign=NetWidget' ) ?>"
                                   target="_blank">
                                    <div class="thumb" style="background-image: url(<?php echo esc_url( $item['image'] ) ?>);">
                                        <div class="article-text">
                                            <div class="decail"></div>
                                            <h1 class="title">
                                                <?php echo esc_html( $item['title'] ) ?>
                                            </h1>
                                        </div>
                                    </div>

                                </a>

                            </div>
                        </article>
                    <?php } ?>
                </div>
            </div>
            <?php
        }
    }

    function update( $new_instance, $instance ) {
        return $instance;
    }

    function form( $instance ) {

    }

}

register_widget( 'Nethr_Telegram_2_Home_Widget' );
