<?php

class Nethr_Telegram_Thin_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_thin_most_read', 'Najčitanije tanji', array(
			'description' => 'Najčitanije tanki',
		) );
	}

	public function widget( $args, $instance ) {
		$feed = nethr_get_telegram_posts();
		if ( $feed && !empty( $feed ) ) {
			?>
			<div class="widget telegram-thin standard-widget">
				<h2 class="section-title">
					<a href="https://www.telegram.hr?utm_source=NetClanak&utm_medium=Widget&utm_campaign=NetWidget" target="_blank">
						<img
							src="https://s0.wp.com/wp-content/themes/vip/adriaticmedia-nethr/img/tg_widget_logo.gif"
							width="124" height="24">
					</a>
				</h2>

				<div class="widget-body">
			<?php foreach ( $feed as $item ) {
				?>
				<article class="widget-article">
					<a href="<?php echo esc_url( $item['link'] . '?utm_source=NetClanak&utm_medium=Widget&utm_campaign=NetWidget' ) ?>">
						<div class="thumb">
							<img src="<?php echo esc_url( $item['image'] ) ?>" width="165" height="100">
						</div>
						<div class="article-text">
							<h3 class="overtitle">
								Telegram
							</h3>

							<h2 class="title"><?php echo esc_html( $item['title'] ) ?></h2>
						</div>
					</a>
				</article>
				<?php } ?>
				</div>
			</div>
			<?php
		}
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );

		return $instance;
	}

	function form( $instance ) {
		//ovo je samo primjer za formu unutar admina
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] ); ?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>
	<?php
	}
}

register_widget( 'Nethr_Telegram_Thin_Widget' );
