<?php

class Nethr_RSS_Home_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_rss_home_widget', 'RSS widget - Naslovnica', array(
            'description' => 'Prikaz RSS widgeta na naslovnici',
        ) );
    }

	function get_rss_posts( $name, $feed ) {
		$articles = get_transient( 'nethr_rss_posts_' . $name );
		if ( !$articles ) {
			$rss = fetch_feed( $feed );
			if ( ! is_wp_error( $rss ) ) {
				$maxitems  = $rss->get_item_quantity( 10 );
				$articles  = array();
				$rss_items = $rss->get_items( 0, $maxitems );
				$i         = 0;
				foreach ( $rss_items as $item ) {
					/** @var SimplePie_Item $item */
					$e = $item->get_enclosure();
					if ( $e->get_link() ) {
						$articles[] = array(
							'title' => $item->get_title(),
							'link'  => $item->get_permalink(),
							'image' => $e->get_link()
						);
						$i ++;
					}
					else { //image is in the content
						$content = $item->get_content();
						$matches = array();
						preg_match('/<img.*src="(.*?)"/', $content, $matches);
						if ( isset( $matches[1] ) && $matches[1] ) {
							$articles[] = array(
								'title' => $item->get_title(),
								'link'  => $item->get_permalink(),
								'image' => $matches[1]
							);
							$i ++;
						}
					}
					if ( $i === 5 ) {
						break;
					}
				}

				set_transient( 'nethr_rss_posts_' . $name, $articles, 15 * MINUTE_IN_SECONDS );
			}

		}
		return $articles;
	}

    public function widget( $args, $instance ) {
	    $cache_name = $instance['cache_name'];
	    $feed_url   = $instance['feed_url'];
	    $url        = $instance['url'];
	    $logo       = $instance['logo'];
	    $title      = $instance['title'];
	    $data = wp_cache_get( 'nethr_widget_rss_' . $cache_name, 'nethr_widgets' );
	    if ( !$data ) {
		    $feed = $this->get_rss_posts( $cache_name, $feed_url );
		    if ( $feed && ! empty( $feed ) ) {
			    ob_start();
			    ?>
			    <div class="widget home-widget telegram widget-wide <?php echo esc_attr( $instance['class'] ); ?>">
				    <div class="widget-header cf">
					    <h2 class="section-title">
						    <a href="<?php echo esc_url( $url ); ?>"
						       target="_blank"><img
								    src="<?php echo esc_url( $logo ); ?>"
								    width="124" height="24"/></a>
					    </h2>
				    </div>
				    <div class="widget-body gray cf">
					    <?php foreach ( $feed as $item ) {
						    ?>
						    <article class="article-2">
							    <div class="inner">

								    <a href="<?php echo esc_url( $item['link'] ) ?>"
								       target="_blank">
									    <div class="thumb">
										    <img
											    src="<?php echo esc_url( $item['image'] ) ?>"
											    width="186" height="119">
									    </div>
									    <div class="article-text">
										    <h2 class="overtitle danas">
											    <?php echo esc_html( $title ); ?>
										    </h2>

										    <h1 class="title">
											    <?php echo esc_html( $item['title'] ) ?>
										    </h1>
									    </div>
								    </a>

							    </div>
						    </article>
					    <?php } ?>
				    </div>
			    </div>
			    <?php
			    $data = ob_get_clean();
			    wp_cache_set( 'nethr_widget_rss_' . $cache_name, $data, 'nethr_widgets', 15 * MINUTE_IN_SECONDS );
		    }
	    }
	    echo $data;
    }

    function update( $new_instance, $instance ) {
	    $instance['title'] = sanitize_text_field( $new_instance['title'] );
	    $instance['cache_name'] = sanitize_text_field( $new_instance['cache_name'] );
	    $instance['feed_url'] = sanitize_text_field( $new_instance['feed_url'] );
	    $instance['url'] = sanitize_text_field( $new_instance['url'] );
	    $instance['logo'] = sanitize_text_field( $new_instance['logo'] );
	    $instance['class'] = sanitize_text_field( $new_instance['class'] );
        return $instance;
    }

    function form( $instance ) {
	    $title      = empty( $instance['title'] ) ? '' : $instance['title'];
	    $cache_name = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
	    $feed_url   = empty( $instance['feed_url'] ) ? '' : $instance['feed_url'];
	    $url        = empty( $instance['url'] ) ? '' : $instance['url'];
	    $logo       = empty( $instance['logo'] ) ? '' : $instance['logo'];
	    $class       = empty( $instance['class'] ) ? '' : $instance['class'];
	    ?>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov:</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $title ); ?>">
	    </p>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>">Cache name</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $cache_name ); ?>">
	    </p>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'feed_url' ) ); ?>">Feed url</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'feed_url' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'feed_url' ) ); ?>"
			    type="text" value="<?php echo esc_url( $feed_url ); ?>">
	    </p>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">Url</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>"
			    type="text" value="<?php echo esc_url( $url ); ?>">
	    </p>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'logo' ) ); ?>">Logo</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'logo' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'logo' ) ); ?>"
			    type="text" value="<?php echo esc_url( $logo ); ?>">
	    </p>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'class' ) ); ?>">Klasa</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'class' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'class' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $class ); ?>">
	    </p>
	    <?php
    }

}

register_widget( 'Nethr_RSS_Home_Widget' );
