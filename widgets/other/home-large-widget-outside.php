<?php

class Nethr_Home_Large_Outside_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'nethr_home_large_outside_widget', 'Net.hr: Veliki 10 widget - vanjski',
			array(
				'description' => 'Glavni veliki widget za naslovnicu',
			)
		);
	}

	function get_super($zone) {
	    return wpcom_vip_file_get_contents( 'https://super1.net.hr/wp-json/zoninator/v1/zones/' . intval( $zone ) . '/posts' );
	}

	private function article1($article) {
	    ?><article class="article-1 breaking-format breaking-4 cf">

        <div class="shadow"></div>
        <a href="<?php echo esc_url( $article->permalink ) ?>" target="_blank">
            <div class="thumb" style="background-image: url('<?php echo esc_url( $article->image ); ?>');">
            </div>

            <div class="article-text">
                <h2 class="overtitle"><?php echo esc_html( $article->category ); ?></h2>

                <h1 class="title"><?php echo esc_html( $article->post_title ) ?></h1>

                <p class="subtitle"><?php echo esc_html( nethr_get_excerpt( 50, $article->post_content ) ); ?></p>

            </div>

            <div class="icon"><?php // as background ?></div>
        </a>

        </article><?php
    }

	private function article2($article) {
	    ?>
        <article class="article-1">
            <div class="inner">
                <a href="<?php echo esc_url( $article->permalink ) ?>" target="_blank" id="super1-2">
                    <div class="thumb">
						<img src="<?php echo esc_url( $article->image ) ?>">
                    </div>

                    <div class="article-text">
                        <h2 class="overtitle"><?php echo esc_html( $article->category ) ?></h2>
                        <h1 class="title"><?php echo esc_html( $article->post_title ) ?></h1>
                    </div>

                    <div class="icon"><?php // as background ?></div>
                </a>

            </div>
        </article>
        <?php
    }

    private function article3($article, $index) {
        ?><article class="article-3 cf">
        <div class="inner cf">

            <a href="<?php echo esc_url( $article->permalink ); ?>" target="_blank" id="super-<?php echo esc_attr($index) ?>">

                <div class="thumb">
				    <img src="<?php echo esc_url( $article->image ) ?>">
                </div>

                <div class="article-text">
                    <h2 class="overtitle"><?php echo esc_html( $article->category ) ?></h2>

                    <h1 class="title"><?php echo esc_html( $article->post_title ) ?></h1>
                </div>
            </a>

        </div>
        </article><?php
    }

    private function article4($article, $index) {
	    ?><article class="article-2">
        <div class="inner">

            <a href="<?php echo esc_url( $article->permalink ) ?>" target="_blank" id="super1-<?php echo esc_attr($index) ?>">
                <div class="thumb">
				    <img src="<?php echo esc_url( $article->image ); ?>">
                    <div class="icon"></div>
                </div>

                <div class="article-text">
                    <h2 class="overtitle"><?php echo esc_html( $article->category ) ?></h2>

                    <h1 class="title"><?php echo esc_html( $article->post_title ) ?></h1>
                </div>
            </a>

        </div>
        </article><?php
    }

	public function widget( $args, $instance ) {
		$articles = json_decode( $this->get_super( $instance['zone'] ) );
		if ($articles && is_array($articles)) {
			foreach ( $articles as $index => $article ) {

				// G1 - First Big article
				if ( 0 === $index ) {
					$this->article1( $article );
					?>
                    <section class="fold-1 cf super1">
                    <div class="grid-1 cf">
					<?php
				} // G2 - Second article
				else if ( 1 === $index ) { ?>
                    <div class="grid-item-1 cf">
						<?php $this->article2( $article ); ?>
                    </div>
                    <div class="grid-item-2 cf">
					<?php
				} // TOCLOSE: grid-1, fold-1, grid-item-2
				// Four articles -> Article-3
				else if ( $index > 1 && $index < 6 ) {
					$this->article3( $article, $index + 1 );
				} // TOCLOSE: grid-1, fold-1
				else if ( 6 === $index ) { ?>
                    </div> <!-- Close grid-item-2 -->
                    <div class="grid-item-3 cf">
					<?php
					$this->article4( $article, $index + 1 );
				} // Four articles -> Article-2
				else if ( $index > 6 && $index < 9 ) {
					$this->article4( $article, $index + 1 );
				} // Close HTML
				else if ( 9 === $index ) { ?>

					<?php $this->article4( $article, $index + 1 ); ?>
                    </div> <!-- Close grid-item-3 -->

					<?php
				}
			}
			?>
            </div> <!-- Close grid-1 -->
            </section> <!-- Close fold-1 -->
			<?php
		}
	}

	function update( $new_instance, $instance ) {
		$instance['zone']       = sanitize_text_field( $new_instance['zone'] );

		return $instance;
	}

	function form( $instance ) {
		$zone       = empty( $instance['zone'] ) ? '' : $instance['zone'];
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
				type="text" value="<?php echo esc_attr( $zone ); ?>"></p>
		<?php
	}
}

register_widget( 'Nethr_Home_Large_Outside_Widget' );
