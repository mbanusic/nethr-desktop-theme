<?php

class Nethr_Vecernji_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_vecernji_widget', 'Nethr: Vecernji',
            array(
                'description' => 'Widget Vecernjeg lista ispod članka',
            ) );
    }

	function get_article() {
		$article = get_transient( 'nethr_vecernji_feed' );
		if ( !$article ) {
			$rss = fetch_feed( 'https://www.vecernji.hr/feeds/placeholder-head/bigger/vl_partnerski' );
			if ( ! is_wp_error( $rss ) ) {
				$maxitems  = $rss->get_item_quantity( 10 );

				$rss_items = $rss->get_items( 0, $maxitems );
				$item = $rss_items[0];
                /** @var SimplePie_Item $item */
                $e = $item->get_enclosure();
                $article = array(
                    'title' => $item->get_title(),
                    'link'  => $item->get_permalink(),
                    'image' => $e->get_link(),
                    'description' => $item->get_description()
                );
				set_transient( 'nethr_vecernji_feed', $article, 15 * MINUTE_IN_SECONDS );
			}

		}
		return $article;
	}

    public function widget( $args, $instance ) {
        $article = $this->get_article();
        ?>
        <div class="vecernji-widget single-widget widget">
            <div class="widget-header cf">
                <h2 class="section-title">
                    <a href="https://vecernji.hr" target="_blank"><img src="https://adriaticmedianethr.files.wordpress.com/2018/06/nethr_vecernji_widget.png" width="107" height="27" scale="0"></a>
                </h2>
            </div>

                <article class="article-3 cf">
                    <div class="inner cf">

                        <a href="<?php echo esc_url( $article['link'] ) ?>" target="_blank">

                            <div class="thumb">
                                <img src="<?php echo esc_url( $article['image'] ) ?>" />
                            </div>

                            <div class="article-text">
                                <h2 class="overtitle vecernji">
                                    Večernji.hr
                                </h2>

                                <h1 class="title">
                                    <?php echo esc_html( $article['title'] ) ?>
                                </h1>
                            </div>
                        </a>

                    </div>
                </article>

        </div>
        <?php
    }
}

register_widget( 'Nethr_Vecernji_Widget' );
