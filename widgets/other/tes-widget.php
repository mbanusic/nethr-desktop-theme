<?php

class Nethr_Tes_Banner extends WP_Widget {
	public function __construct() {
		parent::__construct( 'nethr_test_banner', 'Nethr: TES',
			array(
				'description' => 'TES widget',
			) );
	}

	public function widget( $args, $instance ) {
	    if ( jetpack_is_mobile() ) {
	        $width = 320;
	        $height= 355;
        }
        else {
	        $width = 669;
	        $height= 200;
        }
		?>
        <script src="//traffic.styria.hr/scripts/widget.js"></script>
        <script>
            _tes.display({
                code: 'net_article_vl',
                widgetWidth: <?php echo wp_json_encode( $width ) ?>,
                widgetHeight: <?php echo wp_json_encode( $height ) ?>,
                params: {

                }
            });
        </script>
        <?php
	}
}

register_widget( 'Nethr_Tes_Banner' );