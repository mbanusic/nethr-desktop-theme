<?php

class Nethr_Super1_Mobile_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct( 'nethr_super1_mobile_banner', 'Nethr: Super1 mobile',
			array(
				'description' => 'Super1 mobile',
			) );
	}

	function get_super($zone) {
	    return wpcom_vip_file_get_contents( 'https://super1.net.hr/wp-json/zoninator/v1/zones/' . intval( $zone ) . '/posts' );
	}

	private function article1($post) {
	    ?><a href="<?php echo esc_url($post->permalink) ?>" target="_blank" id="super1-1">
        <article class="article-1">

            <div class="article-text">
                <h2 class="overtitle"><?php echo esc_html( $post->category ) ?></h2>

                <h1 class="title"><?php echo esc_html( $post->post_title ) ?></h1>
            </div>
            <div class="bg-shadow"></div>
			<img src="<?php echo esc_url($post->image_mobile) ?>">
        </article>
        </a><?php
    }

    private function article2($post, $index) {
	    ?><article class="article-2 cf">
        <a href="<?php echo esc_url($post->permalink) ?>" target="_blank" id="super1-<?php echo intval($index+1); ?>">
            <div class="article-text">
                <div class="thumb">
				    <img src="<?php echo esc_url($post->image_mobile) ?>">
                </div>
                <h2 class="overtitle"><?php echo esc_html($post->category) ?></h2>

                <h1 class="title"><?php echo esc_html($post->post_title) ?></h1>
            </div>
        </a>
        </article><?php
    }

    private function article3($post, $index) {
	    ?><article class="article-3 cf">
        <a href="<?php echo esc_url($post->permalink) ?>" target="_blank" id="super1-<?php echo intval($index+1) ?>">
            <div class="article-text">
                <div class="thumb"><img src="<?php echo esc_url($post->image_mobile) ?>"</div>
                <h2 class="overtitle">
				    <?php echo esc_html($post->category) ?>
                </h2>

                <h1 class="title"><?php echo esc_html($post->post_title) ?></h1>
            </div>
        </a>
        </article><?php
    }

	public function widget( $args, $instance ) {
	    $posts = json_decode( $this->get_super($instance['zone']) );
		?><div class="egida super1"><a href="https://super1.net.hr">Super1</a></div>
        <div class="big-block super1"><?php
			foreach ( $posts as $index => $post ) {
			    if ( 6 === $index ) {
			        break;
                }
				if ( 0 === $index ) {
					$this->article1($post);
				}
				else if ( 1 == $index || 2 == $index ) {
					$this->article2($post, $index);
				}
				else {
					$this->article3($post, $index);
				}

			}
			?></div>
        <a class="load-more" href="https://super1.net.hr">
            <i class="fa fa-align-justify"></i> Učitaj više
        </a><?php
	}

	function update( $new_instance, $instance ) {
		$instance['zone']       = sanitize_text_field( $new_instance['zone'] );

		return $instance;
	}

	function form( $instance ) {
		$zone       = empty( $instance['zone'] ) ? '' : $instance['zone'];
		?>
        <p><label
                    for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona</label>
            <input
                    id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
                    class="widefat"
                    name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $zone ); ?>"></p>
		<?php
	}
}

register_widget( 'Nethr_Super1_Mobile_Widget' );