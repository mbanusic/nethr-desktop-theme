<?php

class Nethr_Igre_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_igre_widget', 'Net.hr igre widget - Stupac', array(
            'description' => 'Prikaz RSS igara na naslovnici',
        ) );
    }

    function get_rss_posts( $name, $feed, $number ) {
        $articles = get_transient( 'nethr_igre_posts_' . $name );
        if ( !$articles ) {
            $rss = fetch_feed( $feed );
            if ( ! is_wp_error( $rss ) ) {
                $maxitems  = $rss->get_item_quantity( 10 );
                $articles  = array();
                $rss_items = $rss->get_items( 0, $maxitems );
                $i         = 0;
                foreach ( $rss_items as $item ) {
                    /** @var SimplePie_Item $item */
                    $image = '';
                    $e = $item->get_enclosure();
                    if ( $e->get_link() ) {
                        $image = $e->get_link();
                    }
                    else { //image is in the content
                        $content = $item->get_content();
                        $matches = array();
                        preg_match('/<img.*src="(.*?)"/', $content, $matches);
                        if ( isset( $matches[1] ) && $matches[1] ) {
                            $image = $matches[1];
                        }

                    }
                    if ( strpos( $image, 'igre234.com' ) !== false ) {
                        $image = str_replace( 'http://igre234.com', 'https://igre.net.hr', $image );
                        $image = str_replace( 'http://www.igre234.com', 'https://igre.net.hr', $image );
                        $articles[] = array(
                            'title' => $item->get_title(),
                            'link' => $item->get_permalink(),
                            'image' => $image
                        );
                        $i++;
                    }
                    if ( $i === $number ) {
                        break;
                    }
                }
                set_transient( 'nethr_rss_posts_' . $name, $articles, 15 * MINUTE_IN_SECONDS );
            }

        }
        return $articles;
    }

    public function widget( $args, $instance ) {
        $cache_name = $instance['cache_name'];
        $feed_url   = $instance['feed_url'];
        $url        = $instance['url'];
        $logo       = $instance['logo'];
        $title      = $instance['title'];
        $number      = intval( $instance['number'] );
        $data = wp_cache_get( 'nethr_widget_rss_' . $cache_name, 'nethr_widgets' );
        if ( !$data ) {
            $feed = $this->get_rss_posts( $cache_name, $feed_url, $number );
            if ( $feed && ! empty( $feed ) ) {
                ob_start();
                ?>
                <div class="widget sidebar-widget rss-widget widget-igre">
                    <div class="widget-header cf">
                        <a href="<?php echo esc_url( $url ); ?>"
                           target="_blank"><img
                                src="<?php echo esc_url( $logo ); ?>"
                                width="124" height="24"/></a>

                        <span>Više na <a href="https://igre.net.hr/" target="_blank">igre.net.hr</a></span>
                    </div>
                    <div class="widget-body gray cf">
                        <?php foreach ( $feed as $item ) {
                            ?>
                            <article class="article-3 cf" data-layout="article-3">
                                <div class="inner cf">

                                    <a href="<?php echo esc_url( $item['link'] ) ?>" target="_blank">

                                        <div class="thumb">
                                            <img
                                                src="<?php echo esc_url( $item['image'] ) ?>"
                                                width="100">
                                        </div>

                                        <div class="article-text">
                                            <h2 class="overtitle">
                                                <?php echo esc_html( $title ); ?>
                                            </h2>

                                            <h1 class="title">
                                                <?php echo esc_html( $item['title'] ) ?>
                                            </h1>
                                        </div>
                                    </a>

                                </div>
                            </article>
                        <?php } ?>
                    </div>
                </div>

                <style>
                    .widget-igre .widget-header img {
                        float: left;
                    }
                    .widget-igre .widget-header span {
                        text-align: right;
                        font-size: 12px;
                        text-transform: uppercase;
                        float: right;
                        position: relative;
                        bottom: -8px;
                    }
                </style>
                <?php
                $data = ob_get_clean();
                wp_cache_set( 'nethr_widget_rss_' . $cache_name, $data, 'nethr_widgets', 15 * MINUTE_IN_SECONDS );
            }
        }
        echo $data;
    }

    function update( $new_instance, $instance ) {
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        $instance['cache_name'] = sanitize_text_field( $new_instance['cache_name'] );
        $instance['feed_url'] = sanitize_text_field( $new_instance['feed_url'] );
        $instance['url'] = sanitize_text_field( $new_instance['url'] );
        $instance['logo'] = sanitize_text_field( $new_instance['logo'] );
        $instance['number'] = intval( $new_instance['number'] );
        return $instance;
    }

    function form( $instance ) {
        $title      = empty( $instance['title'] ) ? '' : $instance['title'];
        $cache_name = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
        $feed_url   = empty( $instance['feed_url'] ) ? '' : $instance['feed_url'];
        $url        = empty( $instance['url'] ) ? '' : $instance['url'];
        $logo       = empty( $instance['logo'] ) ? '' : $instance['logo'];
        $number       = intval( $instance['number'] );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov:</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>">Cache name</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
                type="text" value="<?php echo esc_attr( $cache_name ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'feed_url' ) ); ?>">Feed url</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'feed_url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'feed_url' ) ); ?>"
                type="text" value="<?php echo esc_url( $feed_url ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">Url</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>"
                type="text" value="<?php echo esc_url( $url ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'logo' ) ); ?>">Logo</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'logo' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'logo' ) ); ?>"
                type="text" value="<?php echo esc_url( $logo ); ?>">
        </p>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">Broj igara</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $number ); ?>">
	    </p>
        <?php
    }

}

register_widget( 'Nethr_Igre_Widget' );
