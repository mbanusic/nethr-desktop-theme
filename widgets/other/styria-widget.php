<?php

class Nethr_Styria_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_styria_widgeta', 'Nethr: Styria',
			array(
				'description' => 'Traffice Eco system widget',
			) );
	}

	public function widget( $args, $instance ) {

			?>
		<div class="tes-widget">
			<script src="https://traffic.styria.hr/static/scripts/widget.js"></script>
			<script>
				_tes.display({
					code: <?php echo wp_json_encode( $instance['code'] ) ?>,
					widgetWidth: <?php echo wp_json_encode( $instance['width'] ) ?>,
					widgetHeight: <?php echo wp_json_encode( $instance['height'] ) ?>,
					params: {
						number_of_articles: <?php echo wp_json_encode( $instance['num_articles'] ) ?>
					}
				});
			</script>
		</div>
			<?php
	}

	function update( $new_instance, $instance ) {
		$instance['code'] = sanitize_text_field( $new_instance['code'] );
		$instance['width'] = sanitize_text_field( $new_instance['width'] );
		$instance['height'] = sanitize_text_field( $new_instance['height'] );
		$instance['num_articles'] = sanitize_text_field( $new_instance['num_articles'] );
		return $instance;
	}

	function form( $instance ) {
		$code  = empty( $instance['code'] ) ? '' : $instance['code'];
		$width  = empty( $instance['width'] ) ? '' : $instance['width'];
		$height  = empty( $instance['height'] ) ? '' : $instance['height'];
		$num_articles  = empty( $instance['num_articles'] ) ? '' : $instance['num_articles'];
		?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'code' ) ); ?>">Code:</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'code' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'code' ) ); ?>"
				type="text" value="<?php echo esc_attr( $code ); ?>" class="widefat">
		</p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>">Width:</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'width' ) ); ?>"
				type="text" value="<?php echo esc_attr( $width ); ?>" class="widefat">
		</p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>">Height:</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'height' ) ); ?>"
				type="text" value="<?php echo esc_attr( $height ); ?>" class="widefat">
		</p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'num_articles' ) ); ?>">No. of articles:</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'num_articles' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'num_articles' ) ); ?>"
				type="text" value="<?php echo esc_attr( $num_articles ); ?>" class="widefat">
		</p>

		<?php
	}
}

register_widget( 'Nethr_Styria_Widget' );
