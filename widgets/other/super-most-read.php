<?php

class Nethr_Super_Most_Read_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_super_most_read', 'Net.hr: Najčitanije za Super', array(
            'description' => 'Prikaz najčitanijih članaka za Super1',
        ) );
    }

	function get_super() {
		return wpcom_vip_file_get_contents( 'https://super1.net.hr/wp-json/zoninator/v1/zones/474/posts' );
	}

	private function article3($article, $index) {
		?><article class="article-3 cf">
        <div class="inner cf">

            <a href="<?php echo esc_url( $article->permalink ); ?>" target="_blank" id="super-read-<?php echo esc_attr($index) ?>">

                <div class="thumb">
                    <img src="<?php echo esc_url( $article->image ) ?>">
                </div>

                <div class="article-text">
                    <h2 class="overtitle"><?php echo esc_html( $article->category ) ?></h2>

                    <h1 class="title"><?php echo esc_html( $article->post_title ) ?></h1>
                </div>
            </a>

        </div>
        </article><?php
	}

    public function widget( $args, $instance ) {
	        $posts = json_decode( $this->get_super() );
	        if ( empty( $posts ) ) {
		        return;
	        }
            ?>
            <div class="widget najcitanije-widget">
                <div class="widget-header cf">
                    <h2 class="section-title">Najčitanije</h2>
                </div>
                <div class="widget-body gray cf">
	                <?php
                    $i = 0;
	                foreach ($posts as $index => $post) {

	                    if (is_object($post)) {
		                    $this->article3( $post, $index );
	                    }
	                    $i++;
	                    if ($i>6) {
	                        break;
                        }
                    }
                    ?>
                </div>

            </div>
            <?php
    }

}

register_widget( 'Nethr_Super_Most_Read_Widget' );
