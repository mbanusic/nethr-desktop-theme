<?php

class Nethr_Webshop_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_webshop_widget', 'Net Webshop widget', array(
            'description' => 'Prikaz Net.hr Webshop feeda na naslovnici',
        ) );
    }

    function get_rss_posts( $name, $feed ) {
        $articles = get_transient( 'nethr_webshop_widget' . $name );
        if ( !$articles ) {
            $rss = fetch_feed( $feed );
            if ( ! is_wp_error( $rss ) ) {
                $maxitems  = $rss->get_item_quantity( 10 );
                $articles  = array();
                $rss_items = $rss->get_items( 0, $maxitems );
                $i         = 0;
                foreach ( $rss_items as $item ) {
                    /** @var SimplePie_Item $item */
                    $e = $item->get_enclosure();

	                $content = $item->get_content();
	                $old_price = $item->get_item_tags(SIMPLEPIE_NAMESPACE_RSS_20, 'description')[0]['child'][""]['oldprice'][0]['data'];
	                $new_price = $item->get_item_tags(SIMPLEPIE_NAMESPACE_RSS_20, 'description')[0]['child'][""]['newprice'][0]['data'];
	                $overtitle = $item->get_item_tags(SIMPLEPIE_NAMESPACE_RSS_20, 'overtitle')[0]['data'];
                    $matches = array();
                    preg_match('/<img.*src="(.*?)"/', $content, $matches);
                    if ( isset( $matches[1] ) && $matches[1] ) {
                        $articles[] = array(
                            'title' => $item->get_title(),
                            'link'  => $item->get_permalink(),
	                        'description' => $content,
                            'image' => $matches[1],
                            'old_price' => $old_price,
                            'new_price' => $new_price,
	                        'overtitle' => $overtitle
                        );
                        $i ++;
                    }

                    if ( $i === 5 ) {
                        break;
                    }

                }
	            set_transient( 'nethr_webshop_widget_' . $name, $articles, 15 * MINUTE_IN_SECONDS );
            }
        }
        return $articles;
    }

    public function widget( $args, $instance ) {
        $cache_name = $instance['cache_name'];
        $feed_url   = $instance['feed_url'];
        $url        = $instance['url'];
        $logo       = $instance['logo'];
        $data = wp_cache_get( 'nethr_widget_rss_' . $cache_name, 'nethr_widgets' );
        if ( !$data ) {
            $feed = $this->get_rss_posts( $cache_name, $feed_url );
            if ( $feed && ! empty( $feed ) ) {
                ob_start();
                ?>
                <div class="widget home-widget webshop-widget widget-wide cf">
                    <div class="widget-header cf">
                        <h2 class="section-title">
                            <a href="<?php echo esc_url( $url ); ?>"
                               target="_blank"><img
                                    src="<?php echo esc_url( $logo ); ?>"
                                    height="30"/></a>
                        </h2>
                    </div>
                    <div class="widget-body cf">
                        <?php foreach ( $feed as $item ) {
                            ?>
                            <article class="article-2">
                                <div class="inner">

                                    <a href="<?php echo esc_url( $item['link'] ) ?>"
                                       target="_blank">
                                        <div class="thumb">
                                            <img
                                                src="<?php echo esc_url( $item['image'] ) ?>"
                                                width="186" height="119">
                                        </div>
                                    </a>
                                    <div class="article-text">
                                        <h2 class="overtitle danas">
                                            <?php echo esc_html( $item['overtitle'] ); ?>
                                        </h2>
                                        <a href="<?php echo esc_url( $item['link'] ) ?>" target="_blank">
                                            <h1 class="title">
                                                <?php echo esc_html( $item['title'] ) ?>
                                            </h1>
                                        </a>
                                        <div class="prices cf">
	                                        <?php if ( $item['new_price'] ) { ?>
                                            <div class="left price"><span class="red"><?php echo esc_html( $item['new_price'] ) ?></span> / <span><?php echo esc_html( $item['old_price'] ) ?></span></div>
			                                <?php } else {
		                                        ?><div class="left price"><span class="red"><?php echo esc_html( $item['old_price'] ) ?></span> </div><?php
	                                        } ?>
                                        </div>
                                        <p class="description">
                                            <?php

                                            echo esc_html( strip_tags( $item['description'] ) ) ?>
                                        </p>
                                        <a href="<?php echo esc_url( $item['link'] ) ?>" class="btn">Kupi ovdje</a>
                                    </div>

                                </div>
                            </article>
                        <?php } ?>
                    </div>
                </div>
                <?php
                $data = ob_get_clean();
                wp_cache_set( 'nethr_webshop_widget_' . $cache_name, $data, 'nethr_widgets', 15 * MINUTE_IN_SECONDS );
            }
        }
        echo $data;
    }

    function update( $new_instance, $instance ) {
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        $instance['cache_name'] = sanitize_text_field( $new_instance['cache_name'] );
        $instance['feed_url'] = sanitize_text_field( $new_instance['feed_url'] );
        $instance['url'] = sanitize_text_field( $new_instance['url'] );
        $instance['logo'] = sanitize_text_field( $new_instance['logo'] );
        return $instance;
    }

    function form( $instance ) {
        $title      = empty( $instance['title'] ) ? '' : $instance['title'];
        $cache_name = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
        $feed_url   = empty( $instance['feed_url'] ) ? '' : $instance['feed_url'];
        $url        = empty( $instance['url'] ) ? '' : $instance['url'];
        $logo       = empty( $instance['logo'] ) ? '' : $instance['logo'];
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov:</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>">Cache name</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
                type="text" value="<?php echo esc_attr( $cache_name ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'feed_url' ) ); ?>">Feed url</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'feed_url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'feed_url' ) ); ?>"
                type="text" value="<?php echo esc_url( $feed_url ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">Url</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>"
                type="text" value="<?php echo esc_url( $url ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'logo' ) ); ?>">Logo</label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'logo' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'logo' ) ); ?>"
                type="text" value="<?php echo esc_url( $logo ); ?>">
        </p>
        <?php
    }

}

register_widget( 'Nethr_Webshop_Widget' );
