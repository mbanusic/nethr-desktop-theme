<?php

class Nethr_More_Articles_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_more_articles_widget', 'Članak - Još iz rubrike',
			array(
				'description' => 'Još članaka iz rubrike',
			) );
	}


	function template() { ?>

	<article class="article-feed <?php echo esc_attr( get_post_format() ); ?> cf"
         data-id="<?php the_ID() ?>" data-layout="feed-1">
		<div class="inner cf">
			<?php
			$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
			if ( isset( $titles['sticker'] ) && $titles['sticker'] ) { ?>
				<a href="#" class="sticker">
					<?php
					echo esc_html( $titles['sticker'] );
					?>
				</a>
			<?php } ?>

			<a href="<?php the_permalink(); ?>">
				<div class="thumb">
					<?php
					if ( 'video' === get_post_format() ) {
						if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'feed-2' );
						} else { ?>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed-2.gif"/>
							<?php
						}
						?><div class="icon"></div><?php

					}
					else {
						if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'feed-1' );
						} else { ?>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed.gif"/>
							<?php
						}
					}
					?>
					<?php
					if ( 'gallery' === get_post_format() ) {
						?><div class="article-icon"><i class="fa fa-camera"></i> Foto
						</div><?php
					} ?>
				</div>
			</a>

			<div class="article-text">
				<a href="<?php the_permalink(); ?>">

					<?php
					$category = nethr_get_the_category();
					if ( $category->term_id != 90 ) { ?>
					<h2 class="overtitle <?php echo esc_attr( $category->slug ); ?>">
						<?php
						if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							echo esc_html( $titles['over_title'] );
						} else {
							echo esc_html( $category->name );
						} ?>
					</h2>
					<?php } ?>

					<h1 class="title"><?php the_title(); ?></h1>
				</a>

				<p class="undertitle"><?php the_time(); ?>
					<a href="<?php echo esc_url( wpcom_vip_get_term_link( $category, 'category' ) ); ?>"><?php echo esc_html( $category->cat_name ) ?></a>
				</p>
			</div>

		</div>
	</article>

	<?php }

	function template_webcafe() { ?>

		<article class="more-articles cf">
			<a href="<?php the_permalink(); ?>">
			 	<div class="thumb">
			 		<?php the_post_thumbnail( 'feed-1' ); ?>
			 	</div>
			 	<div class="text">
			 		<div class="date"><?php the_date(); ?></div>
			 		<h2 class="title"><?php the_title(); ?></h2>
			 		<div class="votes">
			 			<a href="#"><i class="fa fa-star"></i></a>
			 			<a href="#"><i class="fa fa-star"></i></a>
			 			<a href="#"><i class="fa fa-star"></i></a>
			 			<a href="#"><i class="fa fa-star"></i></a>
			 			<a href="#"><i class="fa fa-star"></i></a>
			 		</div>
			 	</div>
			 </a>
		</article>

	<?php }

	public function widget( $args, $instance ) {
		global $post;
		$query     = array(
			'post_type' => $post->post_type,
			'posts_per_page' => 5,
			'no_found_rows'  => true,
			'posts_status' => 'publish'
		);
		if ( 'webcafe' === $post->post_type ) {
			$cat = get_the_category();
			$cat = $cat[0];
			$query['category_name'] = $cat->slug;
			if ( 90 === $cat->term_id ) {
				$query['posts_per_page'] = 12;
			}
		}
		else if ( 'zena' === $post->post_type ) {
			$cat = get_the_terms( $post, 'zena-cat' );
			$cat = $cat[0];
			$query['tax_query'] = array(
				array(
					'taxonomy' => 'zena-cat',
					'field' => 'slug',
					'terms' => $cat->slug
				)
			);
		}
		else {
			$cat = nethr_get_top_category();
			$query['category_name'] = $cat->slug;
		}


		$data = wp_cache_get( 'nethr_more_articles_widget' . $cat->slug, 'nethr_widgets' );

		if ( ! $data ) {

			ob_start();
			?>

			<div class="more-categories-widget widget">

				<h2 class="section-title">Još iz rubrike</h2>
				<div class="category-feed more-from-category cf">
					<?php

					$articles = new WP_Query( $query );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							if ( 'webcafe' === $articles->post->post_type ) {
								$this->template_webcafe();
							}
							else if ( 'zena' === $articles->post->post_type ) {
								get_template_part('templates/articles/article-feed-zena');
							}
							else {
								$this->template();
							}
							if ( 0 === $articles->current_post ) {
								$promos = nethr_promos( 'home-feed' );
								if ( ! empty( $promos ) ) {
									$post = get_post( intval( $promos[ rand( 0, ( count( $promos ) - 1 ) ) ] ) );
									if ( $post ) {
										if ( 'zena' === $articles->post->post_type ) {
											get_template_part('templates/articles/article-feed-zena');
										}
										else {
											$this->template();
										}
									}
								}
							}
						}
					}
					wp_reset_postdata(); ?>

				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_more_articles_widget'.$cat->slug, $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		return $instance;
	}

	function form( $instance ) {
	}
}

register_widget( 'Nethr_More_Articles_Widget' );
