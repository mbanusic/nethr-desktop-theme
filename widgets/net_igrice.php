<?php

class Nethr_Net_Igrice_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_net_igrice_widget', 'Net Igrice', array(
            'description' => 'Prikaz dviju Net Igrica',
        ) );
    }


    public function widget( $args, $instance ) {
        $data = wp_cache_get( 'nethr_net_igrice_widget', 'nethr_widgets' );
        if ( !$data ) {
            ob_start();
            ?>
            <div class="widget home-widget net-igrice-widget igrice">
                <div class="widget-header cf">
                    <a href="https://net.hr/igrice/">
                        <img
                            src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/igrice_widget.png"
                            width="300" height="63"/>
                        <h1>Net Igrice</h1>
                    </a>
                    <a href="https://net.hr/igrice/" class="button">Vidi sve</a>
                </div>
                <div class="widget-body cf">
                    <?php
                    $q = z_get_zone_query( 'igrice-2', array( 'posts_per_page' => 2 ) );
                    while ( $q->have_posts() ) {
                        $q->the_post();
                        ?>
                        <article class="article-3 cf <?php echo esc_attr( get_post_format() ) ?>"
                                 data-id="<?php the_ID() ?>" data-layout="article-3">
                            <div class="inner cf">

                                <a href="<?php the_permalink(); ?>" target="<?php echo esc_html( nethr_permalink_target() ) ?>">

                                    <div class="thumb">
                                        <?php
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail( 'article-3' );
                                        }
                                        else {
                                            ?><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-3.gif"/>
                                        <?php } ?>
                                    </div>

                                    <div class="article-text">
                                        <?php
                                        $category = get_the_terms( $post, 'igrice' );
                                        $category = $category[0];
                                        ?>
                                        <h2 class="overtitle <?php echo esc_html( nethr_category_color() ); ?>">
                                            <?php echo esc_html( $category->name ); ?>
                                        </h2>

                                        <h1 class="title">
                                            <?php the_title(); ?>
                                        </h1>
                                    </div>
                                </a>

                            </div>
                        </article>
                    <?php } ?>
                </div>
            </div>
            <?php
            $data = ob_get_clean();
            wp_cache_set( 'nethr_net_igrice_widget', $data, 'nethr_widgets', HOUR_IN_SECONDS );
        }
        echo $data;
    }

    function update( $new_instance, $instance ) {
        return $instance;
    }

    function form( $instance ) {

    }

}

register_widget( 'Nethr_Net_Igrice_Widget' );
