<?php

class Break_Icons_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'break_icons_widget', 'Izbornik s ikonicama', array(
            'description' => 'kvizovi, ljubimci, #lijepenase, sprdex, FB like ',
        ) );
    }

    public function widget( $args, $instance ) {
	    $cache_name = esc_html( $instance['cache_name'] );
        $data   = wp_cache_get( 'break_icons_widget_' . $cache_name, 'nethr_widgets' );
        if ( !$data ) {
	        $first_img_icon = $instance['first_img_icon'];
	        $first_title = $instance['first_title'];
	        $first_url = $instance['first_url'];
	        $first_blank = intval( $instance['first_blank'] );

	        $second_img_icon = $instance['second_img_icon'];
	        $second_title = $instance['second_title'];
	        $second_url = $instance['second_url'];
	        $second_blank = intval( $instance['second_blank'] );

	        $third_img_icon = $instance['third_img_icon'];
	        $third_title = $instance['third_title'];
	        $third_url = $instance['third_url'];
	        $third_blank = intval( $instance['third_blank'] );

	        $fourth_img_icon = $instance['fourth_img_icon'];
	        $fourth_title = $instance['fourth_title'];
	        $fourth_url = $instance['fourth_url'];
	        $fourth_blank = intval( $instance['fourth_blank'] );

            $fifth_img_icon = $instance['fifth_img_icon'];
            $fifth_title = $instance['fifth_title'];
            $fifth_url = $instance['fifth_url'];
            $fifth_blank = intval( $instance['fifth_blank'] );

	        $fb_button = intval( $instance['fb_button'] );

            ob_start();
            ?>

            <div class="icons-widget
            <?php if( $fifth_img_icon ) { echo 'five-icons'; } ?>
            cf">

                <div class="first-icon <?php echo esc_attr( $second_title ); ?>">
                    <div class="icon-sticker"></div>
                    <a href="<?php echo esc_url( $first_url ); ?>" <?php if ( $first_blank ) echo 'target="_blank"' ?>>
                        <?php
                            if( $first_img_icon ) {
                            echo '<img src="' . esc_url( $first_img_icon ) . '"/>';
                        } ?>
                        <span><?php echo esc_html( $first_title ); ?></span>
                    </a>
                </div>

                <div class="second-icon <?php echo esc_attr( $second_title ); ?>">
                    <div class="icon-sticker"></div>
                    <a href="<?php echo esc_url( $second_url ); ?>" <?php if ( $second_blank ) echo 'target="_blank"' ?>>
                        <?php
                            if( $second_img_icon ) {
                            echo '<img src="' . esc_url( $second_img_icon ) . '"/>';
                        } ?>
                        <span><?php echo esc_html( $second_title ); ?></span>
                    </a>
                </div>

                <div class="third-icon <?php echo esc_attr( $third_title ); ?>">
                    <div class="icon-sticker"></div>
                    <a href="<?php echo esc_url( $third_url ); ?>" <?php if ( $third_blank ) echo 'target="_blank"' ?>>
                        <?php
                            if( $third_img_icon ) {
                            echo '<img src="' . esc_url( $third_img_icon ) . '"/>';
                        } ?>
                        <span><?php echo esc_html( $third_title ); ?></span>
                    </a>
                </div>

                <div class="fourth-icon <?php echo esc_attr( $fourth_title ); ?>">
                    <div class="icon-sticker"></div>
                    <a href="<?php echo esc_url( $fourth_url ); ?>" <?php if ( $fourth_blank ) echo 'target="_blank"' ?>>
                        <?php
                            if( $fourth_img_icon ) {
                            echo '<img src="' . esc_url( $fourth_img_icon ) . '"/>';
                        } ?>
                        <span><?php echo esc_html( $fourth_title ); ?></span>
                    </a>
                </div>

                <?php if( $fifth_img_icon ) { ?>

                <div class="fifth-icon <?php echo esc_attr( $fifth_title ); ?>">
                    <div class="icon-sticker"></div>
                    <a href="<?php echo esc_url( $fifth_url ); ?>" <?php if ( $fifth_blank ) echo 'target="_blank"' ?>>
                        <?php
                        if( $fifth_img_icon ) {
                            echo '<img src="' . esc_url( $fifth_img_icon ) . '"/>';
                        } ?>
                        <span><?php echo esc_html( $fifth_title ); ?></span>
                    </a>
                </div>

                <?php } ?>

                <?php if( $fb_button ) { ?>
                    <div class="fb-like" data-href="https://www.facebook.com/Net.hr"
                    data-layout="standard"
                    data-action="like"
                    data-show-faces="false"
                    data-width="360"
                    data-share="false"></div>
                <?php } ?>

            </div>
            <?php
            $data = ob_get_clean();
            wp_cache_set( 'break_icons_widget_' . $cache_name, $data, 'nethr_widgets', HOUR_IN_SECONDS );
        }
        echo $data;
    }

    function update( $new_instance, $instance ) {
        $instance['first_img_icon']  = sanitize_text_field( $new_instance['first_img_icon'] );
        $instance['first_title']  = sanitize_text_field( $new_instance['first_title'] );
        $instance['first_url']  = sanitize_text_field( $new_instance['first_url'] );
        $instance['first_blank']  = intval( $new_instance['first_blank'] );

        $instance['second_img_icon']  = sanitize_text_field( $new_instance['second_img_icon'] );
        $instance['second_title']  = sanitize_text_field( $new_instance['second_title'] );
        $instance['second_url']  = sanitize_text_field( $new_instance['second_url'] );
	    $instance['second_blank']  = intval( $new_instance['second_blank'] );

        $instance['third_img_icon']  = sanitize_text_field( $new_instance['third_img_icon'] );
        $instance['third_title']  = sanitize_text_field( $new_instance['third_title'] );
        $instance['third_url']  = sanitize_text_field( $new_instance['third_url'] );
	    $instance['third_blank']  = intval( $new_instance['third_blank'] );

        $instance['fourth_img_icon']  = sanitize_text_field( $new_instance['fourth_img_icon'] );
        $instance['fourth_title']  = sanitize_text_field( $new_instance['fourth_title'] );
        $instance['fourth_url']  = sanitize_text_field( $new_instance['fourth_url'] );
	    $instance['fourth_blank']  = intval( $new_instance['fourth_blank'] );

        $instance['fifth_img_icon']  = sanitize_text_field( $new_instance['fifth_img_icon'] );
        $instance['fifth_title']  = sanitize_text_field( $new_instance['fifth_title'] );
        $instance['fifth_url']  = sanitize_text_field( $new_instance['fifth_url'] );
        $instance['fifth_blank']  = intval( $new_instance['fifth_blank'] );

	    $instance['fb_button']  = intval( $new_instance['fb_button'] );
	    $instance['cache_name']  = sanitize_text_field( $new_instance['cache_name'] );

        return $instance;
    }

    function form( $instance ) {
        //ovo je samo primjer za formu unutar admina
        $first_img_icon  = empty( $instance['first_img_icon'] ) ? '' : $instance['first_img_icon'];
        $first_title  = empty( $instance['first_title'] ) ? '' : $instance['first_title'];
        $first_url  = empty( $instance['first_url'] ) ? '' : $instance['first_url'];
        $first_blank  = intval( $instance['first_blank'] );

        $second_img_icon  = empty( $instance['second_img_icon'] ) ? '' : $instance['second_img_icon'];
        $second_title  = empty( $instance['second_title'] ) ? '' : $instance['second_title'];
        $second_url  = empty( $instance['second_url'] ) ? '' : $instance['second_url'];
	    $second_blank  = intval( $instance['second_blank'] );

        $third_img_icon  = empty( $instance['third_img_icon'] ) ? '' : $instance['third_img_icon'];
        $third_title  = empty( $instance['third_title'] ) ? '' : $instance['third_title'];
        $third_url  = empty( $instance['third_url'] ) ? '' : $instance['third_url'];
	    $third_blank  = intval( $instance['third_blank'] );

        $fourth_img_icon  = empty( $instance['fourth_img_icon'] ) ? '' : $instance['fourth_img_icon'];
        $fourth_title  = empty( $instance['fourth_title'] ) ? '' : $instance['fourth_title'];
        $fourth_url  = empty( $instance['fourth_url'] ) ? '' : $instance['fourth_url'];
	    $fourth_blank  = intval( $instance['fourth_blank'] );

        $fifth_img_icon  = empty( $instance['fifth_img_icon'] ) ? '' : $instance['fifth_img_icon'];
        $fifth_title  = empty( $instance['fifth_title'] ) ? '' : $instance['fifth_title'];
        $fifth_url  = empty( $instance['fifth_url'] ) ? '' : $instance['fifth_url'];
        $fifth_blank  = intval( $instance['fifth_blank'] );

	    $fb_button  = intval( $instance['fb_button'] );
	    $cache_name  = sanitize_text_field( $instance['cache_name'] );
        ?>

        <style>
            .half {
                width: 50%;
                display: inline-block;
                float: left;
            }
            .half input {
                width: 95%;;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                -ms-box-sizing: border-box;
                -o-box-sizing: border-box;
                box-sizing: border-box;
            }
            p { clear: both; padding-bottom: 1px; }
        </style>

        <p>
            <!-- Prva ikona -->
            <h4 style="border-bottom: 1px solid #ddd; margin-bottom: 10px;">PRVA IKONA</h4>


            <div>
                <label
                for="<?php echo esc_attr( $this->get_field_id( 'first_img_icon' ) ); ?>">URL Ikone</label>
                <br>
                <input
                    id="<?php echo esc_attr( $this->get_field_id( 'first_img_icon' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'first_img_icon' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $first_img_icon ); ?>">
            </div>

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'first_title' ) ); ?>">Naslov</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'first_title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'first_title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $first_title ); ?>">

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'first_url' ) ); ?>">URL</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'first_url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'first_url' ) ); ?>"
                type="text" value="<?php echo esc_attr( $first_url ); ?>">

            <br><br>

            <input
                id="<?php echo esc_attr( $this->get_field_id( 'first_blank' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'first_blank' ) ); ?>"
                type="checkbox" value="1" <?php if ( $first_blank ) echo 'checked'; ?>>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'first_blank' ) ); ?>">&nbsp;Novi prozor</label>
        </p>

        <p>
            <!-- Druga ikona -->
            <h4 style="border-bottom: 1px solid #ddd; margin-bottom: 10px;">DRUGA IKONA</h4>

            <div>
                <label
                for="<?php echo esc_attr( $this->get_field_id( 'second_img_icon' ) ); ?>">URL Ikone</label>
                <br>
                <input
                    id="<?php echo esc_attr( $this->get_field_id( 'second_img_icon' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'second_img_icon' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $second_img_icon ); ?>">
            </div>

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'second_title' ) ); ?>">Naslov</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'second_title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'second_title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $second_title ); ?>">

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'second_url' ) ); ?>">URL</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'second_url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'second_url' ) ); ?>"
                type="text" value="<?php echo esc_attr( $second_url ); ?>">

            <br><br>

            <input
                id="<?php echo esc_attr( $this->get_field_id( 'second_blank' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'second_blank' ) ); ?>"
                type="checkbox" value="1" <?php if ( $second_blank ) echo 'checked'; ?>>

            <label
                for="<?php echo esc_attr( $this->get_field_id( 'second_blank' ) ); ?>">&nbsp;Novi prozor</label>
        </p>

        <p>
            <!-- Treca ikona -->
            <h4 style="border-bottom: 1px solid #ddd; margin-bottom: 10px;">TREĆA IKONA</h4>

            <div>
                <label
                for="<?php echo esc_attr( $this->get_field_id( 'third_img_icon' ) ); ?>">URL Ikone</label>
                <br>
                <input
                    id="<?php echo esc_attr( $this->get_field_id( 'third_img_icon' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'third_img_icon' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $third_img_icon ); ?>">
            </div>

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'third_title' ) ); ?>">Naslov</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'third_title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'third_title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $third_title ); ?>">

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'third_url' ) ); ?>">URL</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'third_url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'third_url' ) ); ?>"
                type="text" value="<?php echo esc_attr( $third_url ); ?>">

            <br><br>

            <input
                id="<?php echo esc_attr( $this->get_field_id( 'third_blank' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'third_blank' ) ); ?>"
                type="checkbox" value="1" <?php if ( $third_blank ) echo 'checked'; ?>>

            <label
		        for="<?php echo esc_attr( $this->get_field_id( 'third_blank' ) ); ?>">&nbsp;Novi prozor</label>
        </p>

        <p>
            <!-- Cetvrta ikona -->
            <h4 style="border-bottom: 1px solid #ddd; margin-bottom: 10px;">ČETVRTA IKONA</h4>

            <div>
                <label
                for="<?php echo esc_attr( $this->get_field_id( 'fourth_img_icon' ) ); ?>">URL Ikone</label>
                <br>
                <input
                    id="<?php echo esc_attr( $this->get_field_id( 'fourth_img_icon' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'fourth_img_icon' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $fourth_img_icon ); ?>">
            </div>

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'fourth_title' ) ); ?>">Naslov</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'fourth_title' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'fourth_title' ) ); ?>"
                type="text" value="<?php echo esc_attr( $fourth_title ); ?>">

            <label
            for="<?php echo esc_attr( $this->get_field_id( 'fourth_url' ) ); ?>">URL</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'fourth_url' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'fourth_url' ) ); ?>"
                type="text" value="<?php echo esc_attr( $fourth_url ); ?>">

            <br><br>

            <input
                id="<?php echo esc_attr( $this->get_field_id( 'fourth_blank' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'fourth_blank' ) ); ?>"
                type="checkbox" value="1" <?php if ( $fourth_blank ) echo 'checked'; ?>>

            <label
		        for="<?php echo esc_attr( $this->get_field_id( 'fourth_blank' ) ); ?>">&nbsp;Novi prozor</label>
        </p>


        <p>
            <!-- Peta ikona -->
        <h4 style="border-bottom: 1px solid #ddd; margin-bottom: 10px;">PETA IKONA</h4>

        <div>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'fifth_img_icon' ) ); ?>">URL Ikone</label>
            <br>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'fifth_img_icon' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'fifth_img_icon' ) ); ?>"
                type="text" value="<?php echo esc_attr( $fifth_img_icon ); ?>">
        </div>

        <label
            for="<?php echo esc_attr( $this->get_field_id( 'fifth_title' ) ); ?>">Naslov</label>
        <br>
        <input
            id="<?php echo esc_attr( $this->get_field_id( 'fifth_title' ) ); ?>"
            class="widefat"
            name="<?php echo esc_attr( $this->get_field_name( 'fifth_title' ) ); ?>"
            type="text" value="<?php echo esc_attr( $fifth_title ); ?>">

        <label
            for="<?php echo esc_attr( $this->get_field_id( 'fifth_url' ) ); ?>">URL</label>
        <br>
        <input
            id="<?php echo esc_attr( $this->get_field_id( 'fifth_url' ) ); ?>"
            class="widefat"
            name="<?php echo esc_attr( $this->get_field_name( 'fifth_url' ) ); ?>"
            type="text" value="<?php echo esc_attr( $fifth_url ); ?>">

        <br><br>

        <input
            id="<?php echo esc_attr( $this->get_field_id( 'fifth_blank' ) ); ?>"
            name="<?php echo esc_attr( $this->get_field_name( 'fifth_blank' ) ); ?>"
            type="checkbox" value="1" <?php if ( $fifth_blank ) echo 'checked'; ?>>

        <label
            for="<?php echo esc_attr( $this->get_field_id( 'fifth_blank' ) ); ?>">&nbsp;Novi prozor</label>
        </p>


        <p>
            <h4 style="border-bottom: 1px solid #ddd; margin-bottom: 10px;">PRIKAŽI FACEBOOK LIKE?</h4>

            <input
                id="<?php echo esc_attr( $this->get_field_id( 'fb_button' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'fb_button' ) ); ?>"
                type="checkbox" value="1" <?php if ( $fb_button ) echo 'checked'; ?>>
            <label
		    for="<?php echo esc_attr( $this->get_field_id( 'fb_button' ) ); ?>">&nbsp;Prikaži</label>
        </p>
	    <p>
		    <label
			    for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>">Cache name</label>
		    <br>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $cache_name ); ?>">
	    </p>
        <?php
    }
}

register_widget( 'Break_Icons_Widget' );
