<?php

class Nethr_Horoskop_Big extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_horoskop_widget_big', 'Horoskop veliki',
            array(
                'classname'   => 'horoskop-widget',
                'description' => 'Gatanje iz graška, voodoo revivacije, lečenje uroka, copranje',
            ) );
    }

    function template($sign) {
        $cat = wpcom_vip_get_category_by_slug( $sign );
        ?>

        <div class="zodiac-sign <?php echo esc_html( $cat->slug ); ?>">
            <a href="<?php the_permalink(); ?>">
                <?php echo esc_html( $cat->name ); ?>
            </a>
        </div>

    <?php }

    public function widget( $args, $instance ) {
        $data = wp_cache_get( 'nethr_horoscope_widget_big', 'nethr_widgets' );

        if ( ! $data ) {

            ob_start();
            $branding = intval( get_option( 'nethr_horoscope_branding', 0) );
            if ( 1 === $branding ) {
            ?>
            <style>
                #aurora {
                    padding: 5px 7px;
                    display: inline-block;
                    font-size: 14px;
                    color: #111;
                    font-weight: 600;
                }

                #aurora a {
                    color: #550b6c;
                }
                #aurora a:hover {
                    text-decoration: underline;
                }

                .aurora-mini {
                    position: absolute;
                    bottom: 0;
                    right: 0;
                    text-align: right;
                }
                .aurora-mini img,
                .aurora-mini .text {
                    display: inline-block;
                    color: #111;
                    font-weight: 600;
                    font-size: 14px;
                }
                .aurora-mini .text h3 {
                    color: #742069;
                    font-size: 16px;
                    text-transform: uppercase;
                }
                .aurora-mini .text p {
                    padding-bottom: 7px;
                }
                .aurora-mini img {
                    position: relative;
                    bottom: -10px;
                }
            </style>
            <?php } ?>
            <div class="widget horoskop-big cf">

                <div class="widget-header cf">
                    <h2>
                        <a href="https://net.hr/dnevni-horoskop">
                            Horoskop
                        </a>
                    </h2>
                    <?php if ( 1 == $branding ) {
	                    $url = get_option( 'nethr_horoscope_branding_url' );
	                    $image = get_option( 'nethr_horoscope_branding_head_image' );
	                    $title = get_option( 'nethr_horoscope_branding_head_title' );
	                    $subtitle = get_option( 'nethr_horoscope_branding_head_subtitle' );
                        ?>
                    <span id="aurora">
                        powered by
                        <a href="<?php echo esc_url( $url ) ?>" target="_blank">Aurora</a>
                    </span>

                    <div class="aurora-mini">
                        <a href="<?php echo esc_url( $url ) ?>" target="_blank">Aurora
                            <div class="text">
                                <h3><?php echo esc_html( $title ); ?></h3>
                                <p><?php echo esc_html( $subtitle ) ?></p>
                            </div>
                            <img src="<?php echo esc_url( $image ) ?>" width="54"/>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <div class="widget-body">
                    <?php
                    $signs = array( 'ovan', 'bik', 'blizanci', 'rak', 'lav', 'djevica', 'vaga', 'skorpion', 'strijelac', 'jarac', 'vodenjak', 'ribe');


                    foreach ( $signs as $sign ) {
                        $q    = new WP_Query(
                            array(
                                'posts_per_page' => 1,
                                'category_name'  => $sign,
                                'post_type'      => 'webcafe',
                                'no_found_rows' => true,
                                'posts_status' => 'publish',
	                            'ignore_sticky_posts' => true
                            )
                        );
                        while ( $q->have_posts() ) {
                            $q->the_post();
                            $this->template( $sign );
                        }
                    }
                    wp_reset_postdata();
                    ?>

                </div>

            </div>

            <?php
            $data = ob_get_clean();
            wp_cache_set( 'nethr_horoscope_widget_big', $data, 'nethr_widgets', HOUR_IN_SECONDS );
        }
        echo $data;
    }

}

register_widget( 'Nethr_Horoskop_Big' );
