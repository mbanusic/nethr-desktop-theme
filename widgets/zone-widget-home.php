<?php

class Nethr_Zone_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_zone_widget', 'Nethr: Zone Widget', array(
			'description' => 'Zone widget za naslovnicu',
		) );
	}

	public function widget( $args, $instance ) {
		$cache_name = $instance['cache_name'];
		$data = wp_cache_get( 'nethr_zone_widget_' . $cache_name, 'nethr_widgets' );
		if ( !$data ) {
			ob_start();
			$number = $instance['number'];
			$zone = $instance['zone'];
			$image = $instance['image'];
			if ( $zone && z_get_zone( $zone ) ) {
				?>
				<div class="widget promo-widget standard-widget">
					<h2 class="section-title" style="border-bottom: none; padding-bottom: 5px;">
                        <img src="<?php echo esc_url( $image ) ?>" width="300" />
                    </h2>

					<div class="widget-body">
						<?php
						$articles = z_get_zone_query( $zone, array( 'posts_per_page' => nethr_sanitize_posts_per_page( $number ) ) );

						if ( $articles->have_posts() ) {
							while ( $articles->have_posts() ) {
								$articles->the_post();
								get_template_part( 'templates/articles/article-3' );
							}
						}
						wp_reset_postdata(); ?>
					</div>
				</div>
				<?php
			}
			$data = ob_get_clean();
			wp_cache_set( 'nethr_zone_widget_' . $cache_name, $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['zone'] = sanitize_text_field( $new_instance['zone'] );
		$instance['cache_name'] = sanitize_text_field( $new_instance['cache_name'] );
		$instance['image'] = sanitize_text_field( $new_instance['image'] );
		return $instance;
	}

	function form( $instance ) {
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] );
		$title = $instance['title'] ? $instance['title'] : '';
		$cache_name = $instance['cache_name'] ? $instance['cache_name'] : '';
		$zone = $instance['zone'] ? $instance['zone'] : '';
		$image = $instance['image'] ? esc_url( $instance['image'] ) : '';
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"><?php esc_html_e( 'Zona', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
				type="text" value="<?php echo esc_attr( $zone ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_html_e( 'Slika', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>"
				type="text" value="<?php echo esc_attr( $image ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"><?php esc_html_e( 'Cache name', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
				type="text" value="<?php echo esc_attr( $cache_name ); ?>">
		</p>
	<?php
	}
}

register_widget( 'Nethr_Zone_Widget' );

