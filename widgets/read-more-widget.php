<?php

class Nethr_Read_More_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_read_more_widget', 'Read More',
			array(
				'description' => 'Za ispod članka',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		$data = wp_cache_get( 'nethr_read_more_widget', 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="standard-widget widget more-news inline six cf">
				<h2 class="section-title">Pročitajte i ovo</h2>

				<div class="widget-body cf">
					<?php
					$args     = array( 'posts_per_page' => 6, 'no_found_rows'  => true, 'posts_status' => 'publish' );
					$articles = new WP_Query( $args );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							$titles = get_post_meta( get_the_ID(), 'extra_titles', true );  ?>

							<article class="widget-article">
								<div class="inner">
									<a href="<?php the_permalink(); ?>">
										<div class="thumb">
											<?php the_post_thumbnail( 'feed-1' ); ?>
										</div>
										<div class="article-text">
											<h3 class="overtitle"><?php
											if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
												echo esc_html( $titles['over_title'] );
											} ?></h3>
											<h2 class="title"><?php the_title() ?></h2>
										</div>
									</a>
								</div>
							</article>
						<?php
						}
						wp_reset_postdata();
					}
					 ?>
				</div>
			</div>

			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_read_more_widget', $data, 'nethr_widgets', 20 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		//ovo je samo primjer za formu unutar admina
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'twentyfourteen' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>"></p>
	<?php
	}
}

register_widget( 'Nethr_Read_More_Widget' );
