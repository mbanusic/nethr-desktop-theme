<?php

class Nethr_Satovi_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_satovi_vidget', 'Satovi widget',
            array(
                'description' => '5 satova',
            ) );
    }

    public function widget( $args, $instance ) {
         ?>
            <script type="text/javascript">
                jQuery(document).ready( function() {
                    jQuery('.widget-satovi .widget-container').slick({
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        autoplay: true,
                        arrows: false,
                        dots: false,
                        responsive: [
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 2.5
                                }
                            }
                        ]
                    });
                });
            </script>

            <style type="text/css">

                .widget-satovi .article-item {
                    width: 25%;
                    margin: 10px;
                }

                .widget-satovi .article-item .thumb {
                    border: 1px solid #ddd;
                }

                .widget-satovi .article-item .thumb img {
                    width: 100%;
                    height: auto;
                }

                .widget-satovi .article-item .text {
                    text-align: center;
                    padding: 5px 0;
                }

                .widget-satovi .article-item .title {
                    color: #111;
                    font-family: "Oswald", sans-serif;
                    font-size: 20px;
                    line-height: 1.2em;
                    margin-bottom: 3px;
                }

                .widget-satovi .btn-more {
                    font-size: 13px;
                }
            </style>

            <div class="widget widget-wide widget-satovi">
                <div class="widget-header cf">
                    <h2 class="section-title">
                        Jesenski hitovi: satovi i nakit
                    </h2>
                </div>
                <div class="widget-body">
                    <div class="widget-container">
                        <!-- Ovo možemo doslovno kao input feildovi
                        za url, naslov, sliku. Ili neka pametnija logika ako misliš
                        da je potrebno. Vidi što je najpametnije -->
                        <div class="article-item">
                            <div class="thumb">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <img src="http://staging.net.hr/wp-content/uploads/2018/09/ES4427.jpg?quality=100&strip=all"/>
                                </a>
                            </div>
                            <div class="text">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <h1 class="title">
                                        Elegancija u plavim tonovima
                                    </h1>
                                </a>
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil" class="btn-more">Pogledaj više</a>
                            </div>
                        </div>

                        <div class="article-item">
                            <div class="thumb">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <img src="http://staging.net.hr/wp-content/uploads/2018/09/FS5468.jpg?quality=100&strip=all"/>
                                </a>
                            </div>
                            <div class="text">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <h1 class="title">
                                        Najljepši ukras muške ruke
                                    </h1>
                                </a>
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil" class="btn-more">Pogledaj više</a>
                            </div>
                        </div>

                        <div class="article-item">
                            <div class="thumb">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <img src="http://staging.net.hr/wp-content/uploads/2018/09/ES4419-1.jpg?quality=100&strip=all"/>
                                </a>
                            </div>
                            <div class="text">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <h1 class="title">
                                        Ružičasti sat pristaje baš svima
                                    </h1>
                                </a>
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil" class="btn-more">Pogledaj više</a>
                            </div>
                        </div>

                        <div class="article-item">
                            <div class="thumb">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <img src="http://staging.net.hr/wp-content/uploads/2018/09/JF02961791.jpg?quality=100&strip=all"/>
                                </a>
                            </div>
                            <div class="text">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <h1 class="title">
                                        Ženstvene ogrlice visokog sjaja
                                    </h1>
                                </a>
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil" class="btn-more">Pogledaj više</a>
                            </div>
                        </div>

                        <div class="article-item">
                            <div class="thumb">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <img src="http://staging.net.hr/wp-content/uploads/2018/09/ES4431.jpg?quality=100&strip=all"/>
                                </a>
                            </div>
                            <div class="text">
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil">
                                    <h1 class="title">
                                        Kad sat umisli da je narukvica
                                    </h1>
                                </a>
                                <a target="_blank" href="https://www.satovi.com/satovi/fossil" class="btn-more">Pogledaj više</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
    }

    function update( $new_instance, $instance ) {

        return $instance;
    }

    function form( $instance ) {


    }
}

register_widget( 'Nethr_Satovi_Widget' );
