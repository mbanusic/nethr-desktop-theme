<?php

class Nethr_Promo_Home_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_promo_home_widget', 'Net.hr - Promo widget - wide', array(
            'description' => 'Prikaz promo widgeta na naslovnici 5 vijesti u redu',
        ) );
    }

	function article_template() {
		?>
		<article class="article-2 promo">
			<div class="inner">

				<a href="<?php the_permalink(); ?>" target="<?php echo esc_html( nethr_permalink_target() ) ?>">
					<div class="thumb">
						<?php
						if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'article-1', array( 'class' => 'promo_img' ) );
						} else { ?>
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed-1.gif"/>
							<?php
						}
						?><div class="icon"></div><?php
						?>
					</div>

					<div class="article-text">
						<?php $category = nethr_get_the_category( get_the_ID() ); ?>
						<h2 class="overtitle <?php echo esc_html( nethr_category_color() ); ?>">
							<?php
							$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
							if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
								echo esc_html( $titles['over_title'] );
							}
							else {
								echo esc_html( $category->name );
							}
							?>
						</h2>

						<h1 class="title">
							<?php
								the_title();
							?></h1>
					</div>
				</a>

			</div>
		</article>
		<?php
	}

    public function widget( $args, $instance ) {
	    $data = wp_cache_get( 'nethr_widget_promo_home', 'nethr_widgets' );
	    if ( !$data ) {
		    $promos = nethr_promos( 'home-feed', false, 5 );
		    ob_start();
		    ?>
		    <div class="widget home-widget telegram widget-wide">
			    <div class="widget-header cf">
				    <h2 class="section-title">
					    Promo
				    </h2>
			    </div>
			    <div class="widget-body gray cf">
				    <?php
				    if ( count( $promos ) < 5 ) {
					    $backups = get_option( 'nethr_promos' );
					    $backups = $backups['backup'];
					    shuffle( $backups );
					    foreach( $backups as $one ) {
						    if ( !in_array( $one, $promos ) ) {
							    $promos[] = $one;
						    }
						    if ( 5 === count( $promos ) ) {
							    break;
						    }
					    }
				    }
				    shuffle( $promos );
				        $q = new WP_Query( array(
					        'posts_per_page' => 5,
					        'post_status' => 'publish',
					        'post_type' => 'any',
					        'no_found_rows' => true,
					        'post__in' => $promos,
					        'orderby' => 'post__in',
					        'ignore_sticky_posts' => true
				        ) );
				    while ( $q->have_posts() ) {
					    $q->the_post();

						$this->article_template();

				    }
				    ?>
			    </div>
		    </div>
		    <?php
		    $data = ob_get_clean();
		    wp_cache_set( 'nethr_widget_promo_home', $data, 'nethr_widgets', 5 * MINUTE_IN_SECONDS );

	    }
	    echo $data;
    }

    function update( $new_instance, $instance ) {
	    $instance['title'] = sanitize_text_field( $new_instance['title'] );
        return $instance;
    }

    function form( $instance ) {
	    $title      = empty( $instance['title'] ) ? '' : $instance['title'];
	    ?>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov:</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $title ); ?>">
	    </p>

	    <?php
    }

}

register_widget( 'Nethr_Promo_Home_Widget' );
