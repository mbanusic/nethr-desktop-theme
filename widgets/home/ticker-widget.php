<?php

class Nethr_Ticker_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_ticker_widget', 'Ticker',
			array(
				'description' => 'Ticker',
			) );
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_ticker_widget', 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			$q = new WP_Query( array(
				'posts_per_page' => 5,
				'post_type' => 'post',
				'orderby' => 'date',
				'order' => 'DESC',
				'no_found_rows'  => true,
				'posts_status' => 'publish'
			) );
			?>
			<div class="ticker">
				<ul>
					<?php
					while ( $q->have_posts() ) {
						$q->the_post();
						?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<strong><?php the_time( 'H:i' ) ?></strong> <?php the_title() ?>
							</a>
						</li>
					<?php
					}
					?>
				</ul>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_ticker_widget', $data, 'nethr_widgets', 15 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		return $instance;
	}

	function form( $instance ) {}
}

register_widget( 'Nethr_Ticker_Widget' );
