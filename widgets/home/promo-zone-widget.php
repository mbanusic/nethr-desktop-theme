<?php

class Nethr_Promo_Zone_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_promo_zone_widget', 'Net.hr - Promo widget zone', array(
            'description' => 'Prikaz promo zone widgeta na naslovnici 5 vijesti u redu',
        ) );
    }

	function article_template() {
		?>
		<article class="article-2 promo">
			<div class="inner">

				<a href="<?php the_permalink(); ?>">
					<div class="thumb" style="height: 250px;">
						<?php
							the_post_thumbnail( 'article-promo', array( 'class' => 'promo_img' ) );
						?><div class="icon"></div>
					</div>

					<div class="article-text">
						<h2 class="overtitle">
							<?php
							$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
							if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
								echo esc_html( $titles['over_title'] );
							}
							?>
						</h2>

						<h1 class="title">
							<?php
								the_title();
							?></h1>
					</div>
				</a>

			</div>
		</article>
		<?php
	}

    public function widget( $args, $instance ) {
		    ?>
		    <div class="widget home-widget telegram widget-wide">
			    <div class="widget-header cf">
				    <h2 class="section-title">
					    <?php echo esc_html($instance['title']) ?>
				    </h2>
			    </div>
			    <div class="widget-body gray cf">
				    <?php
				    $q = z_get_zone_query( $instance['zone'], ['posts_per_page' => 5] );
				    while ( $q->have_posts() ) {
					    $q->the_post();
						$this->article_template();
				    }
				    ?>
			    </div>
		    </div>
		    <?php
    }

    function update( $new_instance, $instance ) {
	    $instance['title'] = sanitize_text_field( $new_instance['title'] );
	    $instance['zone'] = sanitize_text_field( $new_instance['zone'] );
        return $instance;
    }

    function form( $instance ) {
	    $title      = empty( $instance['title'] ) ? '' : $instance['title'];
	    $zone     = empty( $instance['zone'] ) ? '' : $instance['zone'];
	    ?>
	    <p>
		    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov:</label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $title ); ?>">
	    </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona:</label>
            <input
                    id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
                    class="widefat"
                    name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
                    type="text" value="<?php echo esc_attr( $zone ); ?>">
        </p>

	    <?php
    }

}

register_widget( 'Nethr_Promo_Zone_Widget' );
