<?php

class Nethr_Breaking_News3_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_breaking_news3_widget', 'Breaking News 3',
			array(
				'description' => 'Breaking News 3',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		if ( isset( $instance['on'] ) && 'on' === $instance['on'] ) {
			if ( jetpack_is_mobile() || ( isset( $_COOKIE['akm_mobile'] ) && 'true' === $_COOKIE['akm_mobile'] ) ) {
				$this->mobile( $instance );
			}
			else {
				$this->desktop( $instance );
			}
		}
	}

	function mobile($instance) {
        $q = new WP_Query( array(
		        'posts_status' => 'publish',
	            'posts_per_page' => 5,
				'post__in' => array(
					intval( $instance['post_id_1'] ),
					intval( $instance['post_id_2'] ),
					intval( $instance['post_id_3'] ),
					intval( $instance['post_id_4'] ),
					intval( $instance['post_id_5'] ),
				),
				'orderby' => 'post__in',
	            'no_found_rows'  => true
            )
        );
        while ( $q->have_posts() ) {
            $q->the_post();
            if ( 0 === $q->current_post ) {
                get_template_part('templates/articles/article-1');
            } else { ?>
                <article class="breaking-small">
                    <a href="<?php the_permalink(); ?>">
                        <i class="fa  fa-caret-right"></i> <?php the_title(); ?>
                    </a>
                </article>
                <?php
            }
        }
	}

	function desktop($instance) {
		?><div class="breaking-container cf">
		<?php
		$q = new WP_Query( array(
			'posts_status' => 'publish',
			'posts_per_page' => 5,
			'post__in' => array(
				intval( $instance['post_id_1'] ),
				intval( $instance['post_id_2'] ),
				intval( $instance['post_id_3'] ),
				intval( $instance['post_id_4'] ),
				intval( $instance['post_id_5'] ),
			),
			'orderby' => 'post__in',
			'no_found_rows'  => true
		) );
	while ( $q->have_posts() ) {
		$q->the_post();
	if ( 0 === $q->current_post ) {
		?>
		<article class="article-1 breaking-format breaking-3">
			<div class="inner">
				<div class="thumb">
					<?php
					the_post_thumbnail( 'breaking-3' );
					?>
				</div>

				<div class="article-text">
					<?php $cat = nethr_get_the_category(); ?>
					<a href="<?php the_permalink() ?>">
						<h2 class="overtitle <?php echo esc_attr( $cat->slug ); ?>">
							<?php
							$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
							if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
								echo esc_html( $titles['over_title'] );
							} else {
								echo esc_html( $cat->name );
							}
							?>
						</h2>

						<h1 class="title"><?php the_title(); ?></h1>

						<p class="subtitle"><?php echo esc_html( nethr_get_excerpt( 50 ) ); ?></p>
					</a>

					<a class="button more"
					   href="<?php the_permalink() ?>">Saznaj
						više</a>

				</div>

			</div>
		</article>
		<div class="breaking-right">
		<?php
		}
		else if ( 1 === $q->current_post ) {
		$cat = nethr_get_the_category(); ?>
		<article
			class="article-2 colored breaking breaking-more <?php echo esc_attr( $cat->slug ); ?>">
			<div class="inner cf">

				<a href="<?php the_permalink() ?>">
					<div
						class="thumb"><?php the_post_thumbnail( 'feed-2' ) ?></div>

					<div class="article-text">
						<h2 class="overtitle <?php echo esc_attr( $cat->slug ); ?>">
							<?php
							$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
							if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
								echo esc_html( $titles['over_title'] );
							} else {
								echo esc_html( $cat->name );
							}
							?>
						</h2>

						<h1 class="title"><?php the_title(); ?></h1>
					</div>
				</a>

			</div>
		</article>
		<div class="widget latest-posts breaking">
		<div class="widget-body">
		<?php
	}
	else {
		?>
		<article class="widget-article">
			<div class="white-square"></div>
			<h3 class="title">
				<a href="<?php the_permalink(); ?>">
					<?php the_title() ?>
				</a>
			</h3>
		</article>
		<?php
	}
	}
		wp_reset_postdata();
		?>
		</div>
		</div>

		</div>
		</div><?php
	}

	function update( $new_instance, $instance ) {
		$instance['on'] = isset( $new_instance['on'] ) ? $new_instance['on'] : 'off';
		$instance['post_id_1'] = intval( $new_instance['post_id_1'] );
		$instance['post_id_2'] = intval( $new_instance['post_id_2'] );
		$instance['post_id_3'] = intval( $new_instance['post_id_3'] );
		$instance['post_id_4'] = intval( $new_instance['post_id_4'] );
		$instance['post_id_5'] = intval( $new_instance['post_id_5'] );

		return $instance;
	}

	function form( $instance ) {
		$on = ( $instance['on'] === 'on' )? 'checked' : '';
		$post_id_1 = intval( $instance['post_id_1'] );
		$post_id_2 = intval( $instance['post_id_2'] );
		$post_id_3 = intval( $instance['post_id_3'] );
		$post_id_4 = intval( $instance['post_id_4'] );
		$post_id_5 = intval( $instance['post_id_5'] );
		?>
		<p>
			<input type="checkbox"
			       name="<?php echo esc_attr( $this->get_field_name( 'on' ) ) ?>"
			       value="on" <?php echo esc_html( $on ) ?>>
			<label for="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>">Uključi</label>
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post_id_1' ) ); ?>">Arafat</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post_id_1' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post_id_1' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post_id_1 ); ?>">
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post_id_2' ) ); ?>">Manji</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post_id_2' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post_id_2' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post_id_2 ); ?>">
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post_id_3' ) ); ?>">Ostalo 1</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post_id_3' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post_id_3' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post_id_3 ); ?>">
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post_id_4' ) ); ?>">Ostalo 2</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post_id_4' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post_id_4' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post_id_4 ); ?>">
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post_id_5' ) ); ?>">Ostalo 3</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post_id_5' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post_id_5' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post_id_5 ); ?>">
		</p>
		<?php

	}
}

register_widget( 'Nethr_Breaking_News3_Widget' );
