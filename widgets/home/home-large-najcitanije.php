<?php

class Nethr_Home_Large_Najcitanije_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'nethr_home_large_najcitanije', 'Net.hr: Najčitanije u kategoriji', array(
            'description' => 'Prikaz najčitanijih članaka u kategorijskom bloku na naslovnici',
        ) );
    }

    public function widget( $args, $instance ) {
        $category = $instance['category'] ? $instance['category'] : false;
        $number = $instance['number'];
        $data   = wp_cache_get( 'nethr_home_large_read_' . $instance['cache_name'], 'nethr_widgets' );
        if ( ! $data ) {
	        $posts = get_transient( $category );
	        if ( empty( $posts ) ) {
		        return;
	        }
            ob_start();
            ?>
            <div class="widget najcitanije-widget">
                <div class="widget-header cf">
                    <h2 class="section-title active"><?php echo esc_html( $instance['title'] ); ?></h2>
                    <h2 class="section-title latest">Najnovije</h2>
                </div>
	            <div class="tab tab_1">
                <div class="widget-body gray cf">
	                <?php
	                $args = array(
		                'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
		                'post__in' => $posts,
		                'orderby' => 'post__in',
		                'no_found_rows'  => true,
		                'posts_status' => 'publish',
		                'post_type' => array( 'post', 'promo', 'zena' )
	                );
	                $articles        = new WP_Query( $args );
	                if ( $articles->have_posts() ) {
		                while ( $articles->have_posts() ) {
			                $articles->the_post();
			                get_template_part( 'templates/articles/article-3' );
		                }
	                }
	                wp_reset_postdata(); ?>
                </div>
	            </div>
	            <div class="tab tab_2">

		            <div class="widget-body gray cf">
			            <?php
			            $args = array(
				            'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
				            'no_found_rows'  => true,
				            'posts_status' => 'publish',
				            'post_type' => array( 'post', 'promo', 'zena' )
			            );
			            if ( isset($instance['category2'] ) && $instance['category2'] ) {
			            	$args['category_name'] = $instance['category2'];
			            }
			            $articles        = new WP_Query( $args );
			            if ( $articles->have_posts() ) {
				            while ( $articles->have_posts() ) {
					            $articles->the_post();
					            get_template_part( 'templates/articles/article-3' );
				            }
			            }
			            wp_reset_postdata(); ?>
		            </div>
	            </div>
            </div>
            <?php
            $data = ob_get_clean();
            wp_cache_set( 'nethr_home_large_read_' . $instance['cache_name'], $data, 'nethr_widgets', 30 * MINUTE_IN_SECONDS );
        }
        echo $data;
    }

    function update( $new_instance, $instance ) {
        $instance['title']  = sanitize_text_field( $new_instance['title'] );
        $instance['category']  = sanitize_text_field( $new_instance['category'] );
        $instance['category2']  = sanitize_text_field( $new_instance['category2'] );
        $instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
		$instance['cache_name'] = sanitize_text_field( $new_instance['cache_name'] );
        return $instance;
    }

    function form( $instance ) {
        $title  = empty( $instance['title'] ) ? 'Najčitanije' : $instance['title'];
        $category  = empty( $instance['category'] ) ? '' : $instance['category'];
        $category2  = empty( $instance['category2'] ) ? '' : $instance['category2'];
        $cache_name  = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
        $number  = intval( $instance['number'] );
        ?>
	    <p>
		    <label
			    for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $title ); ?>">
	    </p>
        <p>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php esc_html_e( 'Ime transienta:', 'nethr' ); ?></label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"
                class="widefat"
                name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>"
                type="text" value="<?php echo esc_attr( $category ); ?>">
        </p>

	    <p>
		    <label
			    for="<?php echo esc_attr( $this->get_field_id( 'category2' ) ); ?>"><?php esc_html_e( 'Ime kategorije:', 'nethr' ); ?></label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'category2' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'category2' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $category2 ); ?>">
	    </p>
        <p>
            <label
                for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
            <input
                id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
                name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
                type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
        </p>

	    <p>
		    <label
			    for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"><?php esc_html_e( 'Cache name:', 'nethr' ); ?></label>
		    <input
			    id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
			    class="widefat"
			    name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
			    type="text" value="<?php echo esc_attr( $cache_name ); ?>">
	    </p>

        <?php
    }

}

register_widget( 'Nethr_Home_Large_Najcitanije_Widget' );
