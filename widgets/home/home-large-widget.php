<?php

class Nethr_Home_Large_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'nethr_home_large_widget', 'Net.hr: Veliki 10 widget',
			array(
				'description' => 'Glavni veliki widget za naslovnicu',
			)
		);
	}

	public function widget( $args, $instance ) {
		$articles = z_get_zone_query( $instance['zone'], array( 'posts_per_page' => 10 ) );
		while ( $articles->have_posts() ) {
			$articles->the_post();

			// G1 - First Big article
			if ( 0 === $articles->current_post ) {
				nethr_get_template( 'templates/articles/article-large', array('zone' => $instance['zone'], 'position' => $articles->current_post+1) );
				?>
				<section class="fold-1 cf">
				<div class="grid-1 cf">
				<?php
			}
			// G2 - Second article
			else if ( 1 === $articles->current_post ) { ?>

				<div class="grid-item-1 cf">
					<?php get_template_part( 'templates/articles/article-1', array('zone' => $instance['zone'], 'position' => $articles->current_post+1) ); ?>
				</div>

				<div class="grid-item-2 cf">

				<?php
			} // TOCLOSE: grid-1, fold-1, grid-item-2
			// Four articles -> Article-3
			else if ( $articles->current_post > 1 && $articles->current_post < 6 ) {
				nethr_get_template( 'templates/articles/article-3', array('zone' => $instance['zone'], 'position' => $articles->current_post+1) );
			} // TOCLOSE: grid-1, fold-1
			else if ( 6 === $articles->current_post ) { ?>
				</div> <!-- Close grid-item-2 -->
				<div class="grid-item-3 cf">
				<?php
				nethr_get_template( 'templates/articles/article-2', array('zone' => $instance['zone'], 'position' => $articles->current_post+1) );
			}

			// Four articles -> Article-2
			else if ( $articles->current_post > 6 && $articles->current_post < 9 ) {
				nethr_get_template( 'templates/articles/article-2', array('zone' => $instance['zone'], 'position' => $articles->current_post+1) );
			}

			// Close HTML
			else if ( 9 === $articles->current_post ) { ?>

				<?php nethr_get_template( 'templates/articles/article-2', array('zone' => $instance['zone'], 'position' => $articles->current_post+1) ); ?>
				</div> <!-- Close grid-item-3 -->

				<?php
			}
		}
		?>
		</div> <!-- Close grid-1 -->
		</section> <!-- Close fold-1 -->
		<?php
	}

	function update( $new_instance, $instance ) {
		$instance['zone']       = sanitize_text_field( $new_instance['zone'] );

		return $instance;
	}

	function form( $instance ) {
		$zone       = empty( $instance['zone'] ) ? '' : $instance['zone'];
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
				type="text" value="<?php echo esc_attr( $zone ); ?>"></p>
		<?php
	}
}

register_widget( 'Nethr_Home_Large_Widget' );
