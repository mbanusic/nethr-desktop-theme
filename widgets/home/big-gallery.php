<?php

class Nethr_Home_Big_Gallery_Widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct('nethr_big_gallery_widget', 'Net.hr: Velika galerija',
            array(
                'description' => 'Galerije i Video ',
            ));
    }

    function activeGallery()
    {
        ?>
        <article id="activeGallery" class="article-1 breaking-format breaking-4 gallery cf">

            <div class="arrow arrow-next"><i class="fa  fa-angle-right"></i></div>
            <div class="arrow arrow-prev"><i class="fa  fa-angle-left"></i></div>

            <div class="shadow"></div>
            <a href="#">
                <div class="thumb">
                </div>
                <div class="article-text">
                    <h2 class="overtitle">

                    </h2>
                    <h1 class="title">
                    </h1>
                </div>
            </a>
        </article>
        <?php
    }

    function videoArticle() {
	    $args = array(
		    'posts_per_page' => 1,
		    'tax_query' => array(
			    array(
				    'taxonomy' => 'post_format',
				    'field' => 'slug',
				    'terms' => array(
					    'post-format-video'
				    )
			    ),
		    ),
		    'no_found_rows' => true,
		    'posts_status' => 'publish',
		    'ignore_sticky_posts' => true
	    );
	    $articles = new WP_Query( $args );
	    while ( $articles->have_posts() ) {
		    $articles->the_post();
		    ?>
		    <div class="shadow"></div>
		    <a href="<?php the_permalink(); ?>">
			    <?php
			    $img_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'breaking-3' );
			    ?>
			    <div class="thumb"
			         style="background-image: url('<?php echo esc_url( $img_url[0] ); ?>');"></div>

			    <div class="icon"></div>

			    <div class="article-text">
				    <?php $category = nethr_get_the_category( get_the_ID() ); ?>
				    <h2 class="overtitle <?php echo esc_html( nethr_category_color() ); ?>">
					    <?php
					    $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
					    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
						    echo esc_html( $titles['over_title'] );
					    }
					    else {
						    echo esc_html( $category->name );
					    } ?>
				    </h2>
				    <h1 class="title">
					    <?php the_title(); ?>
				    </h1>
			    </div>
		    </a>

		    <?php
	    }
    }


    public function widget($args, $instance) {
	    $data = wp_cache_get( 'nethr_big_gallery_widget', 'nethr_widgets' );
	    if ( ! $data ) {
		    ob_start();
		    ?>
		    <section class="fold-2 cf">
			    <div class="big-gallery-widget home cf">
				    <div class="section-header">
					    <div class="section-titles">
						    <a href="#" class="active">Sve galerije</a>
						    <a href="<?php echo esc_url( get_post_format_link( 'gallery' ) ); ?>">Foto</a>
						    <a href="<?php echo esc_url( get_post_format_link( 'video' ) ); ?>">Video</a>
					    </div>
				    </div>

				    <div class="left-content active-gallery">
					    <?php $this->activeGallery(); ?>
				    </div>
				    <article class="gallery-article-2 video">
					    <?php $this->videoArticle(); ?>
				    </article>

				    <div class="clear"></div>

				    <div class="small-articles cf">
					    <?php
					    $args     = array(
						    'posts_per_page'      => 5,
						    'tax_query'           => array(
							    array(
								    'taxonomy' => 'post_format',
								    'field'    => 'slug',
								    'terms'    => array(
									    'post-format-gallery'
								    )
							    ),
						    ),
						    'no_found_rows'       => true,
						    'posts_status'        => 'publish',
						    'ignore_sticky_posts' => true
					    );
					    $articles = new WP_Query( $args );
					    $out      = array();
					    if ( $articles->have_posts() ) {
						    while ( $articles->have_posts() ) {
							    $articles->the_post();
							    $post_id = get_the_ID();
							    $titles  = get_post_meta( get_the_ID(), 'extra_titles', true );
							    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
								    $overtitle = $titles['over_title'];
							    }
							    else {
								    $category  = nethr_get_the_category( get_the_ID() );
								    $overtitle = $category->name;
							    }
							    $out[] = array(
								    'url'       => get_the_permalink(),
								    'title'     => get_the_title(),
								    'image'     => esc_url( wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'breaking-3' )[0] ),
								    'overtitle' => $overtitle
							    );
							    ?>
							    <article class="gallery-article-small">
								    <div class="inner">
									    <a href="<?php the_permalink(); ?>">
										    <div class="thumb">
											    <?php the_post_thumbnail( 'article-3_2x' ); ?>
										    </div>
										    <div class="shadow"></div>

										    <div class="article-text">
											    <div class="article-icon"><i
													    class="fa fa-camera"></i>
												    Foto
											    </div>
											    <h1 class="title">
												    <?php
												    the_title(); ?>
											    </h1>
										    </div>
									    </a>
								    </div>
							    </article>
							    <?php
						    }
					    }
					    wp_reset_postdata();
					    ?>
				    </div>
				    <script>

					    (function ($) {
						    var photos = <?php echo wp_json_encode( $out ); ?>,
							    current = 0;

						    function loadImage(index) {
							    $('#activeGallery a').attr('href', photos[index]['url']);
							    $('#activeGallery .thumb').css('background-image', 'url("' + photos[index]['image'] + '")');
							    $('#activeGallery .overtitle').html(photos[index]['overtitle']);
							    $('#activeGallery .title').html( photos[index]['title'] );
							    $('.gallery-article-small').removeClass('active');
							    $($('.gallery-article-small').get(index)).addClass('active');

						    };
						    $('.arrow-next').on('click', function () {
							    current++;
							    if (current > 4) {
								    current = 0;
							    }
							    loadImage(current);
						    });
						    $('.arrow-prev').on('click', function () {
							    current--;
							    if (current < 0) {
								    current = 5;
							    }
							    loadImage(current);
						    });
						    loadImage(current);
					    })(jQuery);

				    </script>
		    </section>
		    <?php
		    $data = ob_get_clean();
		    wp_cache_set( 'nethr_big_gallery_widget', $data, 'nethr_widgets', 20 * MINUTE_IN_SECONDS );
	    }
	    echo $data;
    }

}

register_widget( 'Nethr_Home_Big_Gallery_Widget' );
