<?php

class Nethr_Color_Article_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_color_article_widget', 'Color article',
			array(
				'description' => 'Color article widget',
			) );
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_color_article_widget' . $instance['location'], 'nethr_widgets' );
		if ( ! $data ) {

			// Color article - Big
			$args     = array(
				'posts_status' => 'publish',
				'posts_per_page' => 1,
				'post_type' => array( 'post', 'webcafe' ),
				'p' => intval( $instance['post'] ),
				'no_found_rows'  => true
			);
			$articles = new WP_Query( $args );
			ob_start();
			if ( $articles->have_posts() ) {
				while ( $articles->have_posts() ) {
					$articles->the_post();
					get_template_part( 'templates/articles/article-color' );
				}
			}
			$data = ob_get_clean();
			wp_reset_postdata();
			wp_cache_set( 'nethr_color_article_widget' . $instance['location'], $data, 'nethr_widgets', 5 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['location'] = sanitize_text_field( $new_instance['location'] );
		$instance['post'] = sanitize_text_field( $new_instance['post'] );
		wp_cache_delete( 'nethr_color_article_widget' . $instance['location'], 'nethr_widgets' );

		return $instance;
	}

	function form( $instance ) {

		$location = empty( $instance['location'] ) ? '' : intval( $instance['location'] );
		$post = empty( $instance['post'] ) ? '' : intval( $instance['post'] );

		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>">Lokacija</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'location' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'location' ) ); ?>"
				type="text" value="<?php echo esc_attr( $location ); ?>"></p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post' ) ); ?>">ID posta</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post ); ?>"></p>
	<?php
	}
}

register_widget( 'Nethr_Color_Article_Widget' );
