<?php

class Nethr_Home_Promo_Small_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_home_promo_small_widget', 'Nethr: Promo mali', array(
			'description' => 'Promo mali widget za naslovnicu',
		) );
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'promo_widget_home_small', 'nethr_widgets' );
		if ( !$data ) {
			ob_start();
			$number = $instance['number'];
			$posts  = nethr_promos( 'home-widget' );
			if ( $posts && ! empty( $posts ) ) {
				?>
				<div class="widget promo-widget standard-widget">
					<h2 class="section-title">Promo</h2>

					<div class="widget-body">
						<?php
						$args     = array(
							'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
							'post_type'      => array( 'promo' ),
							'post__in'       => $posts,
							'no_found_rows'  => true,
							'posts_status' => 'publish',
							'ignore_sticky_posts' => true
						);
						$articles = new WP_Query( $args );
						if ( $articles->have_posts() ) {
							while ( $articles->have_posts() ) {
								$articles->the_post();
								get_template_part( 'templates/articles/article-3' );
							}
						}
						wp_reset_postdata(); ?>
					</div>
				</div>
				<?php
			}
			$data = ob_get_clean();
			wp_cache_set( 'promo_widget_home_small', $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );

		return $instance;
	}

	function form( $instance ) {
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] ); ?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>
	<?php
	}
}

register_widget( 'Nethr_Home_Promo_Small_Widget' );

