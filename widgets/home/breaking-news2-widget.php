<?php
//TODO
class Nethr_Breaking_News2_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_breaking_news2_widget', 'Breaking News 2',
			array(
				'description' => 'Breaking News 2',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		if ( isset( $instance['on'] ) && 'on' === $instance['on'] ) {

				$q = new WP_Query( array(
					'posts_status' => 'publish',
					'posts_per_page' => 1,
					'post__in' => array( intval( $instance['post_id'] ) ),
					'no_found_rows'  => true,
				) );
				while ( $q->have_posts() ) {
					$q->the_post();
					if ( jetpack_is_mobile() || ( isset( $_COOKIE['akm_mobile'] ) && 'true' === $_COOKIE['akm_mobile'] ) ) {
						$this->mobile();
					}
					else {
						$this->desktop();
					}
				}
		}
	}

	function desktop() {
		?><article class="article-1 breaking-format breaking-2">
		<div class="inner">
			<div class="thumb">
				<?php
				the_post_thumbnail( 'breaking-2' );
				?>
			</div>

			<div class="article-text">
				<?php $cat = nethr_get_the_category(); ?>
				<a href="<?php the_permalink() ?>">
					<h2 class="overtitle <?php echo esc_attr( $cat->slug ); ?>">
						<?php
						$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
						if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							echo esc_html( $titles['over_title'] );
						} else {
							echo esc_html( $cat->name );
						}
						?>
					</h2>

					<h1 class="title"><?php the_title() ?></h1>

					<p class="subtitle"><?php echo esc_html( nethr_get_excerpt( 40 ) ); ?></p>
				</a>

				<a class="button more"
				   href="<?php the_permalink(); ?>">Saznaj
					više</a>

			</div>

		</div>
		</article><?php
	}

	function mobile() {
        get_template_part('templates/articles/article-1');
        echo '<br>';
	}

	function update( $new_instance, $instance ) {
		$instance['post_id']  = intval( $new_instance['post_id'] );
		$instance['on'] = isset( $new_instance['on'] ) ? $new_instance['on'] : 'off';


		return $instance;
	}

	function form( $instance ) {

		$post_id  = empty( $instance['post_id'] ) ? '' : $instance['post_id'];

		$on = ( $instance['on'] === 'on' )? 'checked' : '';
		?>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>"
			       type="checkbox"
			       name="<?php echo esc_attr( $this->get_field_name( 'on' ) ) ?>"
			       value="on" <?php echo esc_html( $on ) ?>>
			<label for="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>">Uključi</label>
		</p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'post_id' ) ); ?>">ID posta</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'post_id' ) ); ?>"
				class="widefat autocomplete"
				name="<?php echo esc_attr( $this->get_field_name( 'post_id' ) ); ?>"
				type="text" value="<?php echo esc_attr( $post_id ); ?>"></p>



	<?php
	}
}

register_widget( 'Nethr_Breaking_News2_Widget' );
