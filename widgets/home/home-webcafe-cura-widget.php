<?php
class Nethr_Home_Webcafe_Cura_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_home_webcafe', 'Naslovnica - Webcafe široki',
			array(
				'description' => 'Široki narančasti Webcafe widget na naslovnici',
			) );
	}


	function template( $post, $name ) { ?>
		<div class="webcafe-item">
			<h3 class="overtitle">
				<?php echo esc_html( $name ); ?>
			</h3>
			<div class="thumb">
				<a href="<?php echo esc_url( get_the_permalink( $post->ID ) ); ?>">
					<?php echo wp_kses_post( get_the_post_thumbnail( $post->ID, 'article-2' ) ); ?>
				</a>
			</div>
		</div>
	<?php }

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_home_webcafe_wide_widget', 'nethr_widgets' );
		if ( ! $data ) {
			$cats = array( 'cura-dana' => 'Cura dana', 'overkloking' => 'Overkloking', 'fora-dana' => 'Fora dana' );
			ob_start();
			?>
			<div class="grid-3 cf">
				<div class="webcafe-widget">

					<div class="section-header">
						<div class="section-titles">
							<a href="#" class="active">Webcafe</a>
						</div>
					</div>

					<div class="webcafe-body cf">

						<?php
						foreach ( $cats as $slug => $name ) {
							$posts = get_posts(
								array(
									'posts_per_page' => 1,
									'category_name'  => $slug,
									'post_type'      => array('webcafe', 'post'),
									'supress_filters' => false,
									'posts_status' => 'publish'
								)
							);
							$this->template( $posts[0], $name );
						}

						wp_reset_postdata();

						?>
						<div class="webcafe-item vic-dana">
							<h3>Vic dana</h3>

							<?php
							// Vic dana
							$q = new WP_Query( array(
								'posts_per_page' => 1,
								'category_name'  => 'vic-dana',
								'post_type'      => array('webcafe', 'post'),
								'no_found_rows'  => true,
								'posts_status' => 'publish'
							) );
							while ( $q->have_posts() ) {
								$q->the_post(); ?>
								<a href="<?php the_permalink(); ?>">
									<h1 class="title"><?php the_title(); ?></h1>
                                    <?php the_excerpt() ?>
								</a>
							<?php }
							wp_reset_postdata(); ?>
						</div>

						<div class="webcafe-item komnetar">
							<h3 class="overtitle">Komnetar</h3>
							<?php
							// Komnetar
							$q = new WP_Query( array(
								'posts_per_page' => 1,
								'category_name'  => 'komnetar',
								'post_type'      => 'webcafe',
								'no_found_rows'  => true,
								'posts_status' => 'publish'
							) );
							while ( $q->have_posts() ) {
								$q->the_post(); ?>

								<a href="<?php the_permalink(); ?>">
									<div class="thumb"><?php the_post_thumbnail( 'article-2' ); ?></div>
								</a>
							<?php }
							wp_reset_postdata(); ?>
						</div>

					</div>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_home_webcafe_wide_widget', $data, 'nethr_widgets', 30 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );


		return $instance;
	}

	function form( $instance ) {

	}
}

register_widget( 'Nethr_Home_Webcafe_Cura_Widget' );
