<?php

class Nethr_Breaking_News1_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_breaking_news1_widget', 'Breaking News 1',
			array(
				'description' => 'Breaking News 1',
			) );
	}

	public function widget( $args, $instance ) {
		//ovdje ide sadržaj widgeta, cachirano
		if ( isset( $instance['on'] ) && 'on' === $instance['on'] ) {
			?>
				<div class="breaking-1 cf">

					<div class="breaking-sticker">
						<h3>Prijelomna vijest</h3>
					</div>
					<div class="arrow-right"></div>

					<div class="breaking-text">
						<h1 class="title">
							<a href="<?php echo esc_url( $instance['link'] ) ?>"><?php echo esc_html( $instance['title'] ) ?></a>
						</h1>

						<h2 class="subtitle">
							<a href="<?php echo esc_url( $instance['link'] ) ?>"><?php echo esc_html( $instance['description'] ) ?></a>
						</h2>
					</div>

				</div>
				<?php

		}
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['description'] = sanitize_text_field( $new_instance['description'] );
		$instance['link'] = sanitize_text_field( $new_instance['link'] );
		$instance['on'] = isset( $new_instance['on'] ) ? $new_instance['on'] : 'off';

		return $instance;
	}

	function form( $instance ) {

		//ovo je samo primjer za formu unutar admina
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$description = empty( $instance['description'] ) ? '' : $instance['description'];
		$link = empty( $instance['link'] ) ? '' : $instance['link'];
		$on = ($instance['on'] === 'on')? 'checked' : '';
		?>
		<p>
			<input type="checkbox"
			       id="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>"
			       name="<?php echo esc_attr( $this->get_field_name( 'on' ) ) ?>"
			       value="on" <?php echo esc_html( $on ) ?>>
			<label for="<?php echo esc_attr( $this->get_field_id( 'on' ) ) ?>">Uključi</label>
		</p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>"></p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>">Sažetak</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>"
				type="text" value="<?php echo esc_attr( $description ); ?>">
		</p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>">Link</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>"
				type="text" value="<?php echo esc_attr( $link ); ?>">
		</p>


	<?php
	}
}

register_widget( 'Nethr_Breaking_News1_Widget' );
