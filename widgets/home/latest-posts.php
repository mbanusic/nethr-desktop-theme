<?php


class Nethr_Latest_Posts extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_latest_posts', 'Most read posts',
			array(
				'description' => 'Show read posts on homepage',
			) );
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_latest_posts', 'nethr_widgets' );
		if ( ! $data ) {

			ob_start();
			?>
			<div class="widget latest-posts">
				<h2 class="section-title"><?php echo esc_html( $instance['title'] ) ?></h2>
				<div class="widget-body">
					<?php
					$posts = wpcom_vip_top_posts_array( 2, 100 );
					$i = 1;
					foreach ( $posts as $one ) {
						if ( $one['post_id'] ) {
							$post_publish = get_the_time( 'U', $one['post_id']);
							$time = current_time( 'timestamp' );
							if ( $post_publish > ( $time - 2*24*3600 ) ) { // we only want posts published in last two days
							?>
							<article class="widget-article">
								<div class="white-square"></div>
								<h3 class="title">
									<a href="<?php echo esc_url( $one['post_permalink'] ) ?>">
										<?php echo esc_html( $one['post_title'] ) ?>
									</a>
								</h3>
							</article>
						<?php
								$i++;
								if ( $i > intval($instance['number'] ) ) {
									break; // we took 100 posts, but we need less, since we cannot separate older posts in stats.
								}
							}
						}
					} ?>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_latest_posts', $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );

		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] ); ?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Naslov:', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Broj članaka', 'nethr' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>
	<?php
	}
}

register_widget( 'Nethr_Latest_Posts' );
