<?php

class Nethr_Home_Large_Category_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_home_large_category_widget', 'Net.hr: Kategorija na naslovnici - velika',
			array(
				'description' => 'Pregled glavnih vijesti iz kategorije s bannerom',
			) );
	}

	public function widget( $args, $instance ) {
		$data = wp_cache_get( 'nethr_home_category_widget'.$instance['cache_name'], 'nethr_widgets' );
		if ( ! $data ) {
			ob_start();

			$category = $instance['category'];
			$cat = wpcom_vip_get_category_by_slug( $category );
			$permalink = wpcom_vip_get_term_link( $category, 'category' );
			$cat_title = $cat->name;
			$banner = $instance['banner'];
			$num = get_option( 'nethr_short_texts' );
?>
			<section class="home-category <?php echo esc_attr( $category ); ?> cf">
				<div class="section-header">
					<div class="section-titles">
						<a href="<?php echo esc_url( $permalink ); ?>"
						   class="active"><?php echo esc_html( $cat_title ); ?></a>
					</div>
				</div>
				<?php
				$articles = z_get_zone_query( $instance['zone'], array( 'posts_per_page' => 10 ) );
				while ( $articles->have_posts() ) {
					$articles->the_post();
					if ( 1 === $articles->current_post ) {
						?>
						<div class="grid-item-2 cf">
						<?php
					}
					if ( 0 === $articles->current_post ) {
						?>
						<div class="grid-1 cf">
						<div class="grid-item-1">
							<?php
							get_template_part( 'templates/articles/article-1' );
							?>
						</div>
						<?php
					}
					else if ( $articles->current_post > 0 && $articles->current_post < 5 ) {
						get_template_part( 'templates/articles/article-3-alt' );
					}
					else if ( $articles->current_post !== 9 ) {
						get_template_part( 'templates/articles/article-2' );
					}
					if ( 4 === $articles->current_post ) {
						?></div><!-- .grid-item-2 -->
						</div><!-- .grid-1 -->
						<?php
						the_widget( 'Nethr_Banner_Widget', array( 'size' => $banner ) );
						?>
						<div class="grid-2 cf">
							<div class="article-num-text cf">
								<article class="article-num-title cf" data-layout="article-num">
									<div class="inner">

										<div class="number-container">
											<div class="number overtitle"><?php echo esc_html( $num[$category]['number'] ); ?></div>
											<div class="skew-left"></div>
											<div class="skew-right"></div>
										</div>
										<div class="number-title title">
											<a href="<?php echo esc_url( $num[$category]['url'] ); ?>">
												<span><?php echo esc_html( $num[$category]['text'] ); ?></span>
											</a>
										</div>

									</div>
								</article>

							</div>
						</div>
						<div class="grid-item-3">
						<?php
					}

					if ( 9 === $articles->current_post ) {
						?></div><?php
						get_template_part( 'templates/articles/article-color' );
					}
				}
				?>
			</section>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_home_category_widget'.$instance['cache_name'], $data, 'nethr_widgets', 10 * MINUTE_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['category']   = sanitize_text_field( $new_instance['category'] );
		$instance['banner']     = sanitize_text_field( $new_instance['banner'] );
		$instance['zone']     = sanitize_text_field( $new_instance['zone'] );
		$instance['cache_name']     = sanitize_text_field( $new_instance['cache_name'] );
		return $instance;
	}

	function form( $instance ) {

		$category   = empty( $instance['category'] ) ? '' : $instance['category'];
		$banner     = empty( $instance['banner'] ) ? '' : $instance['banner'];
		$zone     = empty( $instance['zone'] ) ? '' : $instance['zone'];
		$cache_name     = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
				type="text" value="<?php echo esc_attr( $zone ); ?>"></p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>">Kategorija</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>"
				type="text" value="<?php echo esc_attr( $category ); ?>"></p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'banner' ) ); ?>">Banner zona</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'banner' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'banner' ) ); ?>"
				type="text" value="<?php echo esc_attr( $banner ); ?>"></p>

		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>">Cache name</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
				type="text" value="<?php echo esc_attr( $cache_name ); ?>"></p>


	<?php
	}
}

register_widget( 'Nethr_Home_Large_Category_Widget' );
