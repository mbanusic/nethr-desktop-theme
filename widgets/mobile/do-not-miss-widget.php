<?php

class Nethr_Do_Not_Miss_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_do_not_miss_widgeta', 'Nethr: Mobile - Ne propustite',
			array(
				'description' => 'Ne propustite widget za mobilnu temu',
			) );
	}

	public function widget( $args, $instance ) {
		if ( is_front_page() ) {
			$slug = 'home';
		}
		else {
			$cat = nethr_get_top_category();
			$slug = $cat->slug;
		}
		$data = wp_cache_get( 'nethr_mobile_do_not_miss_' . $slug, 'nethr-mobile-widgets' );
		if ( ! $data ) {
			$posts = get_transient( 'nethr_most_read_' . $slug );
			if ( empty( $posts ) ) {
				$posts = get_transient( 'nethr_most_read_danas' ); //fallback
				if ( empty( $posts ) ) {
					return;
				}
			}
			ob_start();
			?>
			<div class="ne-propustite">
				<h2 class="section-title"><?php echo esc_html( $instance['title'] ) ?></h2>

				<div class="underline"></div>
				<?php
				$number = $instance['number'];
				$args = array(
					'posts_per_page' => nethr_sanitize_posts_per_page( $number ),
					'post__in' => $posts,
					'orderby' => 'post__in',
					'no_found_rows'  => true,
					'posts_status' => 'publish',
					'post_type' => array( 'post', 'promo' ),
					'ignore_sticky_posts' => true
				);
				$articles = new WP_Query( $args );
				if ( $articles->have_posts() ) {
					while ( $articles->have_posts() ) {
						$articles->the_post();
						get_template_part( 'templates/article-2' );
					}
					wp_reset_postdata();
				}
				 ?>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_mobile_do_not_miss_' . $slug,  $data, 'nethr-mobile-widgets', HOUR_IN_SECONDS );
		}

		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
		$instance['days'] = empty( $new_instance['days'] ) ? 2 : absint( $new_instance['days'] );

		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$days = intval( $instance['days'] );
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] );
		?>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'twentyfourteen' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>"></p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'days' ) ); ?>">Broj dana</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'days' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'days' ) ); ?>"
				type="text" value="<?php echo esc_attr( $days ); ?>" size="3">
		</p>
		<p><label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'twentyfourteen' ); ?></label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>

	<?php
	}
}

register_widget( 'Nethr_Do_Not_Miss_Widget' );
