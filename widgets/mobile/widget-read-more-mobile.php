<?php

class Nethr_Read_More_Mobile_Widget extends WP_Widget {

	/**
	 * @param string $id_base
	 * @param string $name
	 * @param array  $widget_options
	 * @param array  $control_options
	 */
	public function __construct() {
		parent::__construct( 'nethr_read_more_mobile_widgeta', 'Nethr: Mobile - Pročitaj Više',
			array(
				'description' => 'Pročitaj više widget za mobilnu temu',
			)
		);
	}

	/**
	 *
	 * @param  array $args
	 * @param  array $instance
	 * @return void
	 */
	public function widget( $args, $instance ) {
		$post_id = get_the_ID();
		$read_more = wp_cache_get( 'read_more_' . $post_id, 'nethr_mobile' );
		if ( ! $read_more ) {
			$args = array(
				'size' => 3,
			);
			$read_more = Jetpack_RelatedPosts::init_raw()->get_for_post_id( $post_id, $args );
			wp_cache_set( 'read_more_' . $post_id, $read_more, 'nethr_mobile', DAY_IN_SECONDS );
		}
		?>
		<h2 class="section-title head auto">Pročitaj i ovo</h2>
		<div class="read-more">
			<?php
			foreach ( $read_more as $post ) {
				$id = $post['id'];
				?><a href="<?php the_permalink( $id ) ?>"><i class="fa fa-caret-right"></i> <?php echo esc_html( get_the_title( $id ) ) ?></a><?php
			}
			?>
		</div>
		<?php
	}
}

register_widget( 'Nethr_Read_More_Mobile_Widget' );
