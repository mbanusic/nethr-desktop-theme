<?php

class Nethr_Google_Search extends WP_Widget
{

    public function __construct()
    {
        parent::__construct('google_search_widget', 'Google pretraživanje', array(
            'description' => 'Widget za google pretragu u navbaru',
        ));
    }

    public function widget($args, $instance)
    {
        ?>
        <form id="google-search" action="<?php echo esc_url( site_url( 'pretrazivanje' ) ); ?>">
            <input type="hidden" name="cx" value="partner-pub-2317149376955370:wn57ucrd4ll"/>
            <input type="hidden" name="cof" value="FORID:10"/>
            <input type="hidden" name="ie" value="UTF-8"/>
            <input type="text" name="q" size="31" placeholder="Google pretraživanje"/>
        </form>
        <button type="submit" form="google-search" value="Submit"><i class="fa fa-search"></i></button>
        <?php
    }

}
register_widget( 'Nethr_Google_Search' );