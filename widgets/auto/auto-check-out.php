<?php

class Nethr_Auto_Check_Out_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'nethr_auto_check_out_widget', 'Nethr: Ne propustite - Auto', array(
			'description' => 'Prikaz Ne propustite vijesti - auto',
		) );
	}

	public function widget( $args, $instance ) {
		$slug = $instance['cache_name'];
		$data = wp_cache_get( 'nethr_check_out_widget_' . $slug, 'nethr_widgets' );
		if ( ! $data ) {
			ob_start();
			?>
			<div class="widget ne-propustite">
				<div class="widget-header cf">
					<h2 class="section-title"><?php echo esc_html( trim( $instance['title'] ) ); ?></h2>
				</div>
				<div class="widget-body cf">
					<?php
					$number = $instance['number'];
					$articles = z_get_zone_query( $instance['zone'], array( 'posts_per_page' => nethr_sanitize_posts_per_page( $number ) ) );
					if ( $articles->have_posts() ) {
						while ( $articles->have_posts() ) {
							$articles->the_post();
							get_template_part( 'templates/articles/article-4' );
						}
						wp_reset_postdata();
					}
					?>
				</div>
			</div>
			<?php
			$data = ob_get_clean();
			wp_cache_set( 'nethr_check_out_widget_' . $slug, $data, 'nethr_widgets', HOUR_IN_SECONDS );
		}
		echo $data;
	}

	function update( $new_instance, $instance ) {
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['cache_name']  = sanitize_text_field( $new_instance['cache_name'] );
		$instance['zone']  = sanitize_text_field( $new_instance['zone'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );
		return $instance;
	}

	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : $instance['title'];
		$zone  = empty( $instance['zone'] ) ? '' : $instance['zone'];
		$cache_name  = empty( $instance['cache_name'] ) ? '' : $instance['cache_name'];
		$number = intval( $instance['number'] );
		?>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Naslov</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>">Zona</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'zone' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'zone' ) ); ?>"
				type="text" value="<?php echo esc_attr( $zone ); ?>">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">Broj članaka</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
				type="text" value="<?php echo esc_attr( $number ); ?>" size="3">
		</p>
		<p>
			<label
				for="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>">Cache name</label>
			<input
				id="<?php echo esc_attr( $this->get_field_id( 'cache_name' ) ); ?>"
				class="widefat"
				name="<?php echo esc_attr( $this->get_field_name( 'cache_name' ) ); ?>"
				type="text" value="<?php echo esc_attr( $cache_name ); ?>">
		</p>
	<?php
	}

}

register_widget( 'Nethr_Auto_Check_Out_Widget' );
