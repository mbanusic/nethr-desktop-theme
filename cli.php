<?php
/**
 * Implements nethr cli command
 */
class Nethr_Commands extends WPCOM_VIP_CLI_Command {

	/**
	 * Move posts from category to post type.
	 *
	 * ## OPTIONS
	 *
	 * <category>
	 * : category ID of the category
	 *
	 * <from_post_type>
	 * : post_type to move posts from
	 *
	 * <to_post_type>
	 * : post_type to move posts to
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     wp nethr move_posts --category=17437 --from_post_type=post --to_post_type=promo
	 *
	 * @synopsis --category=<category> --from_post_type=<from_post_type> --to_post_type=<to_post_type> [--live-run]
	 */
	function move_posts( $args, $assoc_args ) {
		$dry_mode = empty ( $assoc_args['live-run'] );

		if ( $dry_mode ) {
		   WP_CLI::line( "Entering dry-run mode" );
		} else {
		   WP_CLI::line( "Entering live-run mode" );
		}

		$cat = intval( $assoc_args['category'] );

		WP_CLI::line( "Category is $cat");

		$from_post_type = sanitize_text_field( $assoc_args['from_post_type'] );
		$to_post_type = sanitize_text_field( $assoc_args['to_post_type'] );

		if ( !post_type_exists( $from_post_type ) ) {
			WP_CLI::error( "Post type from doesn't exists" );
		}
		if ( !post_type_exists( $to_post_type ) ) {
			WP_CLI::error( "Post type to doesn't exists" );
		}

		WP_CLI::line("Transfering from $from_post_type to $to_post_type");

		global $wpdb;
		$posts_per_page = 100;
		$loop = 1;
		do {
			WP_CLI::line("Start of loop $loop");
			$posts_array = array(
				'posts_per_page' => $posts_per_page,
				'category' => $cat,
				'post_type' => $from_post_type,
				'paged' => 1
			);
			if ( $dry_mode ) {
				// in dry-run we increase the pages parameter loop over all posts in cat and avoid infinite loop
				// not needed in live-run since posts are removed from future queries
				$posts_array['paged'] = $loop;
			}
			$posts = get_posts( $posts_array );
			foreach ( $posts as $post ) {
				if ( ! $dry_mode ) {
					wp_update_post( array( 'ID' => $post->ID, 'post_type' => $to_post_type ) );
					WP_CLI::line( "Update post {$post->ID} to post type {$to_post_type}" );
				}
				else {
					WP_CLI::line( "Dry_run: Update post {$post->ID} to post type {$to_post_type}" );
				}
			}
			$loop++;

			// Free up memory
			$this->stop_the_insanity();
		} while ( count( $posts ) );

		WP_CLI::success( 'Posts transfered' );
	}
}

WP_CLI::add_command( 'nethr', 'Nethr_Commands' );
