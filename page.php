<?php
// NET.HR -> Page
get_header();
?>
	<div class="container page cf">

		<div class="section-header">
			<div class="section-titles">
				<h2 class="section-title"><?php the_title(); ?></h2>
			</div>
			<?php get_template_part( 'templates/layout/socials' ); ?>
		</div>


		<div class="page-grid single-article">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="featured-img">
					<?php the_post_thumbnail( 'single' ); ?>
				</div>

				<div class="article-content">
					<?php the_content(); ?>
				</div>

			<?php endwhile;  endif; ?>
		<div id="__xclaimwords_wrapper"></div>
		</div>

		<div class="sidebar single-sidebar single-sidebar-1">
			<?php dynamic_sidebar( 'sidebar-single' ) ?>
		</div>

	</div>

<?php
get_footer();
