<?php
// NET.HR -> Single article
get_header();
?>
	<div class="container single cf">
    <div class="single-inner cf">
		<div class="section-header">
			<div class="section-titles">
				<?php
				$category      = nethr_get_the_category();
				$category_link = get_term_link( $category, 'category' );
				if( !is_wp_error( $category_link ) ) {
				?>
				<a href="<?php echo esc_url( $category_link ); ?>"
				   class="active"><?php echo esc_html( $category->name ); ?></a>
                <?php } ?>
			</div>
			<?php get_template_part( 'templates/layout/socials' ); ?>
		</div>

		<div class="page-grid single-article">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>




                <?php $titles = get_post_meta( get_the_ID(), 'extra_titles', true );  ?>
				<h3 class="overtitle"><?php
				if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
					echo esc_html( $titles['over_title'] );
				}
					?></h3>
                <?php
                if ( isset( $titles['sponsor'] ) && $titles['sponsor'] ) {
                    ?><div class="native-tag"><?php echo esc_html( $titles['sponsor'] ) ?></div><?php
                }
                ?>
				<h1 class="title"><?php the_title(); ?></h1>

				<div class="article-meta cf">
					<?php get_template_part( 'templates/layout/socials-count' ) ?>

					<div class="metabox">
					<span><?php echo esc_html( nethr_get_photographer() ) ?></span>
						<span>Autor: <?php coauthors(); ?> </span>
						<span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
					</div>
				</div>

				<?php if( has_post_thumbnail() ) { ?>
					<div class="featured-img thumb">
						<?php
						// Custom sticker: Lokalni izbori 2017
						if( has_term('lokalni-izbori-2017', 'post_tag') ) {
							?><img class="custom-sticker" src="<?php echo esc_url ( get_template_directory_uri() . '/img/lok_izb_2017.svg' ) ?>" width="90" height="90" /><?php
						}
						// The post thumbnail
						the_post_thumbnail( 'single' ); ?>
					</div>
				<?php } ?>

				<div class="article-content" id="__xclaimwords_wrapper">
					<?php the_content(); ?>
					<div class="tags">
						<?php the_tags( '', ' ', '' ); ?>
					</div>
				</div>

				<div class="article-meta cf">
					<?php get_template_part( 'templates/layout/socials-count' ) ?>

					<div class="metabox">
					<span><?php echo esc_html( nethr_get_photographer() ) ?></span>
						<span>Autor: <?php coauthors() ?> </span>
						<span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
					</div>
				</div>
			<?php endwhile;  endif; ?>

		</div>
		<div class="sidebar single-sidebar single-sidebar-1">
			<?php
            dynamic_sidebar( 'sidebar-single' );
            ?>
		</div>
        <script>
            jQuery(document).ready(function() {
            jQuery('.single-sidebar').stick_in_parent({
                offset_top: 100,
                spacer: false
            });
            })
        </script>



    </div>
        <div class="single-inner cf">
            <div class="page-grid single-article">
	        <?php dynamic_sidebar( 'single-under' ) ?>
            </div>
        </div>
		<?php dynamic_sidebar( 'single-footer' ) ?>
	</div>

<?php
get_footer();
