<?php
// NET.HR -> Category
get_header();
$cat_id = get_query_var( 'cat' );
?>
	<div class="container category cf">

		<div class="section-header">
			<?php
			$category = get_category( $cat_id );
			$category_link = wpcom_vip_get_term_link( $category, 'category' );
			if ( $category_link && !is_wp_error( $category_link ) ) {
				if ( 'euro-2016' === $category->slug ) { //special header for EURO2016
					$image = get_option( 'nethr_euro_image' );
					if ( $image ) {
						?><img
						src="<?php echo esc_url( $image ); ?>"
						width="996"><?php
					}
				}
				else {
					?>
					<div class="section-titles">
						<a href="<?php echo esc_url( $category_link ); ?>"
						   class="active"><?php echo esc_html( $category->name ); ?></a>
					</div>
					<?php get_template_part( 'templates/layout/socials' ); ?>
				<?php }
			}
			?>
		</div>

		<div class="page-grid">
			<?php
			$zone = false;
			$slug = $category->slug;
			if ( 'auto' === $slug ) {
				$zone = 'kategorija-auto';
			}
			else if ( 'net-tv' === $slug ) {
				$zone = 'kategorija-net-tv';
			}
			else if ( z_get_zoninator()->zone_exists( 'naslovnica-' . $slug ) ) {
				$zone = 'naslovnica-' . $slug;
			}
			if ( $zone ) {
				?>
				<div class="large-home-widget cf <?php echo esc_attr( $slug ); if ( 'euro-2016' === $slug ) { echo ' sport'; } ?>">
					<?php
					the_widget(
						'Nethr_Home_Large_Widget', array(
							'zone'       => $zone,
							'cache_name' => $slug
						)
					); ?>
				</div>
				<?php
			}

			if ( 'webcafe' === $slug ) {
				the_widget( 'Nethr_Webcafe_Main_Stuff_Widget' );
			}
			?>
			<section class="feed cf">
				<?php
				//main feed
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						get_template_part( 'templates/articles/article-feed' );
					}
					wp_reset_postdata();
				}

				?>
			</section>

			<div class="article-navigation">
				<?php next_posts_link( 'JOŠ VIJESTI <i class="fa fa-angle-right"></i>' ); ?>
		    </div>

		</div>


		<div class="sidebar single-sidebar single-sidebar-1">
			<?php dynamic_sidebar( 'sidebar-category' ) ?>
		</div>

		<?php dynamic_sidebar( 'under-category' ) ?>
	</div>



<?php
get_footer();
