<?php
get_header('ispovijesti');
?>
    <div class="container single ispovijesti-single cf">

	    <div class="ispovijesti-count">
		    <?php
		    $cat = false; $post_count = 0; $url = false;
		    if ( is_single() ) {
			    $cat = get_the_category();
			    if ( $cat && !is_wp_error( $cat ) && is_array( $cat ) ) {
				    $cat = $cat[0]; //Ispovijesti will only have one category
			    }
		    }
		    if ( $cat && !is_wp_error( $cat ) ) {
			    if ( 'ispovijesti' === $cat->slug ) {
				    $post_count = wp_count_posts( 'ispovijesti' );
				    $post_count = $post_count->publish;
			    }
			    else {
				    $post_count = $cat->category_count;
			    }
			    $url = wpcom_vip_get_term_link( $cat, 'category' );
		    }
		    if ( $post_count && $url ) {
			    ?>
			    Trenutno imamo <a href="<?php echo esc_url( $url ); ?>"
				    class="highlight"><?php echo esc_html( $post_count ); ?>
			    ispovijesti </a><?php

			    if ( $cat && ! is_wp_error( $cat ) && 'ispovijesti' != $cat->slug  ) {
				    echo esc_html( 'u kategoriji ' . $cat->name );
			    }
			    ?>
		    <?php }
		    ?>
	    </div>

        <div class="page-grid single-article">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <?php the_category(); ?>

                <h1 class="title">
                    <?php the_title(); ?>
                </h1>

                <div class="ispovijesti-meta">
                    <span>
                        Korisnik: <?php echo esc_html( get_post_meta( get_the_ID(), 'autor', true ) ) ?>
                    </span>
                    <span>
                        Vrijeme: <?php the_time(); ?>
                    </span>
                    <span>
                        <i class="fa fa-comment"></i>
                        <block class="fb-comments-count" data-href="<?php the_permalink(); ?>"></block>
                        Komentara
                    </span>
                </div>

                <div class="article-content">
                    <?php the_content(); ?>
                </div>
	            <div  id="__xclaimwords_wrapper"></div>

                <div class="fb-like" data-href="<?php the_permalink(); ?>"
                     data-layout="standard"
                     data-action="like"
                     data-show-faces="false"
                     data-share="true">

                </div>

                <?php
	            the_widget( 'Nethr_Facebook_Comments_Widget' );
	            ?>

            <?php endwhile;  endif; ?>

        </div>

        <div class="sidebar single-sidebar single-sidebar-1">
            <?php dynamic_sidebar( 'ispovijesti_sidebar' ) ?>
        </div>

    </div>

<?php
get_footer();
