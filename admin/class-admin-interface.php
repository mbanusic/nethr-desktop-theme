<?php

/**
 * Class Nethr_Admin_Interface
 *
 * All additional stuff for admin interface, like admin pages and settings
 */
class Nethr_Admin_Interface {
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'nethr_admin_theme_style' ) );

		add_action( 'admin_menu', array( $this, 'menus' ) );
		add_action( 'admin_init', array( $this, 'register' ) );
	}

	// External Admin CSS - we like our colors
	function nethr_admin_theme_style() {
		wp_enqueue_style( 'my-admin-style', get_template_directory_uri() . '/admin/style-admin.css' );
	}

	function menus() {
		add_menu_page( 'Postavke', 'Net.hr', 'manage_options', 'nethr_options', array( $this, 'options' ) );
		add_posts_page('Crosslinkovi', 'Crosslinkovi', 'edit_posts', 'nethr_crosslink', array( $this, 'crosslinks' ));
	}

	function register() {
		register_setting( 'nethr_options', 'nethr_parse_app_id', array( $this, 'text' ) );
		register_setting( 'nethr_options', 'nethr_parse_app_rest', array( $this, 'text' ) );
		register_setting( 'nethr_options', 'nethr_most_read_days', array( $this, 'integer' ) );
		register_setting( 'nethr_options', 'nethr_ispovijesti_header', array( $this, 'url' ) );
		register_setting( 'nethr_options', 'nethr_wallpaper_top_special_class', array( $this, 'text' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding', array( $this, 'integer' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding_url', array( $this, 'url' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding_image_small', array( $this, 'url' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding_image_large', array( $this, 'url' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding_head_image', array( $this, 'url' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding_head_title', array( $this, 'text' ) );
		register_setting( 'nethr_options', 'nethr_horoscope_branding_head_subtitle', array( $this, 'text' ) );

		add_settings_section( 'most_read',  'Najčitanije', '', 'nethr_options' );
		add_settings_field( 'nethr_most_read_days', 'Broj dana', array( $this, 'input' ), 'nethr_options', 'most_read', array( 'id' => 'nethr_most_read_days' ) );

		add_settings_section( 'parse', 'Parse data', '', 'nethr_options' );
		add_settings_field( 'nethr_parse_app_id', 'App ID', array( $this, 'input' ), 'nethr_options', 'parse', array( 'id' => 'nethr_parse_app_id' ) );
		add_settings_field( 'nethr_parse_app_rest', 'REST key', array( $this, 'input' ), 'nethr_options', 'parse', array( 'id' => 'nethr_parse_app_rest' ) );

		add_settings_section( 'ispovijesti', 'Ispovijesti', '', 'nethr_options' );
		add_settings_field( 'nethr_ispovijesti_header', 'Header slika', array( $this, 'input' ), 'nethr_options', 'ispovijesti', array( 'id' => 'nethr_ispovijesti_header' ) );

		add_settings_section( 'banneri', 'Banneri', '', 'nethr_options' );
		add_settings_field( 'nethr_wallpaper_top_special_class', 'Wallpaper top class', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_wallpaper_top_special_class' ) );
		add_settings_field( 'nethr_horoscope_branding', 'Horoscope banners', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding' ) );
		add_settings_field( 'nethr_horoscope_branding_url', 'Horoscope url', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding_url' ) );
		add_settings_field( 'nethr_horoscope_branding_image_small', 'Horoscope small image', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding_image_small' ) );
		add_settings_field( 'nethr_horoscope_branding_image_large', 'Horoscope large image', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding_image_large' ) );
		add_settings_field( 'nethr_horoscope_branding_head_image', 'Horoscope head image', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding_head_image' ) );
		add_settings_field( 'nethr_horoscope_branding_head_title', 'Horoscope head title', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding_head_title' ) );
		add_settings_field( 'nethr_horoscope_branding_head_subtitle', 'Horoscope head subtitle', array( $this, 'input' ), 'nethr_options', 'banneri', array( 'id' => 'nethr_horoscope_branding_head_subtitle' ) );

		//zena options
		register_setting( 'nethr_zena_options', 'nethr_zena_widget_post', array( $this, 'integer' ) );
		register_setting( 'nethr_zena_options', 'nethr_zena_frajer', array( $this, 'text' ) );
		add_settings_section( 'zena_widget', 'Widget', '', 'nethr_zena_options' );
		add_settings_field( 'nethr_zena_widget_post', 'Članak u widgetu', array( $this, 'autocomplete' ), 'nethr_zena_options', 'zena_widget', array( 'id' => 'nethr_zena_widget_post' ) );
		add_settings_section( 'zena_frajer', 'Frajer', '', 'nethr_zena_options' );
		add_settings_field( 'nethr_zena_frajer', 'Frajerov mail', array( $this, 'input' ), 'nethr_zena_options', 'zena_frajer', array( 'id' => 'nethr_zena_frajer' ) );
	}

	function integer( $value ) {
		return intval( $value );
	}

	function text( $value ) {
		return sanitize_text_field( $value );
	}

	function url( $value ) {
		return esc_url_raw( sanitize_text_field(  $value ) );
	}

	function options() {
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		?>
		<div class="wrap">
			<h1>Postavke</h1>
			<?php
			if ( isset( $_POST['option_page'] ) && 'nethr_options' === $_POST['option_page'] && check_admin_referer( 'nethr_options-options' ) ) {
				/*
				 * Following POST variables are sanitized through register_setting callback
				 * L28-L33
				 */
				update_option( 'nethr_parse_app_id', sanitize_text_field( $_POST['nethr_parse_app_id'] ) );
				update_option( 'nethr_parse_app_rest', sanitize_text_field( $_POST['nethr_parse_app_rest'] ) );
				update_option( 'nethr_most_read_days', intval( $_POST['nethr_most_read_days'] ) );
				update_option( 'nethr_ispovijesti_header', esc_url_raw( $_POST['nethr_ispovijesti_header'] ) );
				update_option( 'nethr_wallpaper_top_special_class', sanitize_text_field( $_POST['nethr_wallpaper_top_special_class'] ) );
				update_option( 'nethr_horoscope_branding', intval( $_POST['nethr_horoscope_branding'] ) );
				update_option( 'nethr_horoscope_branding_url', esc_url_raw( $_POST['nethr_horoscope_branding_url'] ) );
				update_option( 'nethr_horoscope_branding_image_small', esc_url_raw( $_POST['nethr_horoscope_branding_image_small'] ) );
				update_option( 'nethr_horoscope_branding_image_large', esc_url_raw( $_POST['nethr_horoscope_branding_image_large'] ) );
				update_option( 'nethr_horoscope_branding_head_image', esc_url_raw( $_POST['nethr_horoscope_branding_head_image'] ) );
				update_option( 'nethr_horoscope_branding_head_title', sanitize_text_field( $_POST['nethr_horoscope_branding_head_title'] ) );
				update_option( 'nethr_horoscope_branding_head_subtitle', sanitize_text_field( $_POST['nethr_horoscope_branding_head_subtitle'] ) );
				?>
				<div class="updated"><p><strong>Spremljeno</strong></p></div>
				<?php
			}
			?>
			<form method="POST"><?php
				settings_fields( 'nethr_options' );

				do_settings_sections( 'nethr_options' );

				submit_button();
				?></form>
		</div>
		<?php
	}

	function input( $args ) {
		$id = $args['id'];
		if ( !$id ) {
			return;
		}
		$option = get_option( $id );
		?><input type="text" name="<?php echo esc_attr( $id ) ?>" value="<?php echo esc_attr( $option ) ?>" id="<?php echo esc_attr( $id ) ?>" ><?php
	}

	function autocomplete( $args ) {
		$id = $args['id'];
		if ( !$id ) {
			return;
		}
		$option = get_option( $id );
		?><input type="text" class="autocomplete" name="<?php echo esc_attr( $id ) ?>" value="<?php echo esc_attr( $option ) ?>" id="<?php echo esc_attr( $id ) ?>" ><?php
	}

    function crosslinks() {
	    $page = 0;
	    if (isset($_GET['paged'])) {
		    $page = intval( $_GET['paged'] );
	    }
	    $q = new WP_Query([
            'posts_per_page' => 40,
            'post_type' => 'post',
            'meta_query' => [
                [
                    'key' => '_external_permalink',
                    'compare' => 'EXISTS'
                ]
            ],
            'paged' => $page,
            'no_found_rows' => true,
            'ignore_sticky_posts' => true
        ])
	    ?>
        <div class="wrap">
            <table class="widefat">
                <thead>
                <tr>
                    <th class="manage-column">Link</th>
                    <th class="manage-column">Naslov</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    while ($q->have_posts()) {
                        $q->the_post();
                        ?>
                        <tr>
                            <td><?php echo esc_html( get_post_meta($q->post->ID, '_external_permalink', true) ); ?></td>
                            <td><a href="<?php echo esc_url( get_edit_post_link() ) ?>"><?php the_title(); ?></a></td>
                        </tr>
                        <?php
                    }?>
                </tbody>
                <tfoot>
                <tr>
                    <th class="manage-column">Link</th>
                    <th class="manage-column">Naslov</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <a href="<?php echo esc_url( add_query_arg(['page'=>'nethr_crosslink', 'paged' => ++$page], admin_url('edit.php')) ); ?>">Sljedeća stranica</a>
        <?php
    }
}

new Nethr_Admin_Interface();