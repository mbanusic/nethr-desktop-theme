(function() {

    tinymce.PluginManager.add('nethr_shortcodes', function( editor )
    {
        var shortcodeValues = [];

        shortcodeValues.push({text: 'Tamni okvir', value:'nethr_dark_box'});
        shortcodeValues.push({text: 'Svijetli okvir', value:'nethr_light_box'});
        shortcodeValues.push({text: 'Citat', value:'nethr_quote_box'});


        editor.addButton('nethr_shortcodes', {
            type: 'listbox',
            text: 'Shortcodes',
            onselect: function(e) {
                var v = e.target.settings.value;
                tinyMCE.activeEditor.selection.setContent( '[' + v + ']'+tinyMCE.activeEditor.selection.getContent()+'[/' + v + ']' );
            },
            values: shortcodeValues
        });
    });
})();