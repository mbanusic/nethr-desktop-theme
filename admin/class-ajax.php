<?php


class Nethr_Ajax {
	public function __construct() {
		//this is for gallery
		add_action( 'wp_ajax_nethr_get_gallery_image', array( $this, 'get_gallery_image' ) );
		add_action( 'wp_ajax_nopriv_nethr_get_gallery_image', array( $this, 'get_gallery_image' ) );
		add_action( 'wp_ajax_nethr_widget_get_posts', array( $this, 'widget_get_posts' ) );
	}

	function get_gallery_image() {
		$data       = array( 'status' => 'fail', 'image' => array() );
		$gallery_id = intval( $_POST['gallery'] );
		$image_num  = intval( $_POST['image'] );
		$image_num --;

		$gallery = get_post( $gallery_id );
		if ( ! is_null( $gallery ) && has_post_format( 'gallery', $gallery->ID ) ) {
			$content = $gallery->post_content;
			preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );
			$attr = shortcode_parse_atts( $matches[0][3] );
			$ids  = explode( ',', $attr['ids'] );

			if ( isset( $ids[ $image_num ] ) ) {
				$image_id       = $ids[ $image_num ];
				$attachment     = get_post( $image_id );
				$debug          = $attachment->ID;
				$description    = $attachment->post_excerpt;
				$title          = $attachment->post_title;
				$src            = wp_get_attachment_image_src( $image_id, 'large' );
				$data['image']  = array(
					'src'          => $src[0],
					'description'  => esc_html( $description ),
					'title'        => esc_html( $title ),
					'photographer' => esc_html( nethr_get_photographer( $image_id ) ),
					'debug'        => esc_html( $debug ),
				);
				$data['status'] = 'ok';
			}
		}

		$json = json_encode( $data );
		header( 'Content: application/json' );
		echo $json;
		die();
	}

	function widget_get_posts() {
		$q = new WP_Query( array(
			'post_type' => array( 'post', 'webcafe', 'zena', 'ispovijesti', 'igre' ),
			'posts_per_page' => 10,
			's' => sanitize_text_field( $_POST['term'] ),
			'no_found_rows'  => true,
			'post_status'    => 'publish',
		) );
		$posts = array();
		while ( $q->have_posts() ) {
			$q->the_post();
			$posts[] = array( 'label' => get_the_title(), 'value' => get_the_ID() );
		}
		header( 'Content-Type: text/json' );
		echo json_encode( $posts );
		die();
	}
}

new Nethr_Ajax();
