<?php

class Nethr_Meta_Boxes {
	function __construct() {
		add_action( 'init', array( $this, 'boxes' ) );
		add_filter( 'attachment_fields_to_edit', array( $this, 'photographer_credit' ), 10, 2 );
		add_filter( 'attachment_fields_to_save', array( $this, 'photographer_save' ), 10, 2 );
	}

	function boxes() {
		$fm = new Fieldmanager_Group( array(
			'name'     => 'extra_titles',
			'children' => array(
				'over_title'  => new Fieldmanager_TextField( 'Nadnaslov' ),
				'short_title' => new Fieldmanager_TextField( 'Skraćeni naslov' ),
				'sticker' => new Fieldmanager_TextField( 'Sticker' ),
                'sponsor' => new Fieldmanager_TextField('Omogućava')
			),

		) );
		$fm->add_meta_box( 'Naslovi', array( 'post', 'webcafe', 'promo', 'zena' ) );

		$fm = new Fieldmanager_TextField( 'Naziv boje', array( 'name' => 'nethr_color' ) );
		$fm->add_meta_box( 'Boja', array( 'post', 'webcafe' ) );

		$fm = new Fieldmanager_Group( array(
			'name'     => 'oglasi',
			'children' => array(
				'adsense'  => new Fieldmanager_Checkbox( 'Isključi Adsense' ),
				'xclaim' => new Fieldmanager_Checkbox( 'Isključi Xclaim' ),
				'adocean' => new Fieldmanager_Checkbox( 'Isključi Adocean' ),
				'facebook' => new Fieldmanager_Checkbox( 'Isključi komentare' ),
				'video' => new Fieldmanager_Checkbox( 'Isključi video preroll' ),
				'autoplay' => new Fieldmanager_Checkbox( 'Isključi video autoplay' ),
				'video_move' => new Fieldmanager_Checkbox( 'Uključi pomicanje videa' ),
				'video_muted' => new Fieldmanager_Checkbox( 'Mute video' ),
				'adex' => new Fieldmanager_Checkbox( 'Isključi AdEx' ),
				'midas' => new Fieldmanager_Checkbox( 'Isključi Midas' ),
				'scroll' => new Fieldmanager_Checkbox( 'Uključi GA heatmap' ),
				'gemius' => new Fieldmanager_Checkbox( 'Isključi Gemius' )
			),

		) );
		$fm->add_meta_box( 'Oglasi', array( 'post', 'promo', 'zena' ) );

			$fm = new Fieldmanager_Group( array(
				'name'     => 'promos',
				'tabbed' => 'vertical',
				'serialize_data' => false,
				'add_to_prefix'  => false,
				'children' => array(
					'home-feed' => new Fieldmanager_Group( 'Naslovnica glavni widget', array(
						'serialize_data' => false,
						'add_to_prefix'  => false,
						'children' => array(
							'home-feed-on'  => new Fieldmanager_Checkbox( 'Uključi na naslovnici na glavnom widgetu' ),
							'home-feed-from' => new Fieldmanager_Datepicker( 'Datum od', array(
								'name' => 'home-feed-from',
								'use_time' => true,
								'use_am_pm' => false,
								'date_format' => 'd.m.Y.',
								'js_opts' => array(
									'dateFormat' => 'dd.mm.yy.',
								),
							) ),
							'home-feed-to' => new Fieldmanager_Datepicker( 'Datum do', array(
								'name' => 'home-feed-to',
								'use_time' => true,
								'use_am_pm' => false,
								'date_format' => 'd.m.Y.',
								'js_opts' => array(
									'dateFormat' => 'dd.mm.yy.',
								),
							) ), )
						)),
					'zena-feed' => new Fieldmanager_Group( 'Žena glavni widget', array(
						'serialize_data' => false,
						'add_to_prefix'  => false,
						'children' => array(
							'zena-feed-on'  => new Fieldmanager_Checkbox( 'Uključi na naslovnici na glavnom widgetu' ),
							'zena-feed-from' => new Fieldmanager_Datepicker( 'Datum od', array(
								'name' => 'zena-feed-from',
								'use_time' => true,
								'use_am_pm' => false,
								'date_format' => 'd.m.Y.',
								'js_opts' => array( 'dateFormat' => 'dd.mm.yy.' ),
							) ),

							'zena-feed-to' => new Fieldmanager_Datepicker( 'Datum do',array(
								'name' => 'zena-feed-to',
								'use_time' => true,
								'use_am_pm' => false,
								'date_format' => 'd.m.Y.',
								'js_opts' => array(
									'dateFormat' => 'dd.mm.yy.',
								),
							) ),
						) ) ),
					'home-feed-backup' => new Fieldmanager_Group( 'Naslovnica glavni widget backup', array(

						'serialize_data' => false,
						'add_to_prefix'  => false,
						'children' => array(
							'home-feed-backup-on'  => new Fieldmanager_Checkbox( 'Uključi na naslovnici kao backup' ),
						) ) ),
					'home-widget' => new Fieldmanager_Group( 'Naslovnica widget', array(
						'serialize_data' => false,
						'add_to_prefix'  => false,
							'children' => array(
								'home-widget-on'  => new Fieldmanager_Checkbox( 'Uključi na naslovnici na malom widgetu' ),
								'home-widget-from' => new Fieldmanager_Datepicker( 'Datum od', array(
									'name' => 'home-widget-from',
									'use_time' => true,
									'use_am_pm' => false,
									'date_format' => 'd.m.Y.',
									'js_opts' => array(
										'dateFormat' => 'dd.mm.yy.',
									),
								) ),

								'home-widget-to' => new Fieldmanager_Datepicker( 'Datum do', array(
									'name' => 'home-widget-to',
									'use_time' => true,
									'use_am_pm' => false,
									'date_format' => 'd.m.Y.',
									'js_opts' => array(
										'dateFormat' => 'dd.mm.yy.',
									),
								) ),

							) ) ),
					'cat-widget' => new Fieldmanager_Group( 'Kategorija widget', array(
						'serialize_data' => false,
						'add_to_prefix'  => false,
							'children' => array(
								'cat-widget-on'  => new Fieldmanager_Checkbox( 'Uključi na kategoriji' ),
								'cat-widget-from' => new Fieldmanager_Datepicker( 'Datum od', array(
									'name' => 'cat-widget-from',
									'use_time' => true,
									'use_am_pm' => false,
									'date_format' => 'd.m.Y.',
									'js_opts' => array(
										'dateFormat' => 'dd.mm.yy.',
									),
								) ),
								'cat-widget-to' => new Fieldmanager_Datepicker( 'Datum do', array(
									'name' => 'cat-widget-to',
									'use_time' => true,
									'use_am_pm' => false,
									'date_format' => 'd.m.Y.',
									'js_opts' => array(
										'dateFormat' => 'dd.mm.yy.',
									),
								) ),
							) ) ),
					'mobile-feed' => new Fieldmanager_Group( 'Mobile feed', array(
						'serialize_data' => false,
						'add_to_prefix'  => false,
						'children' => array(
							'mobile-feed-on'  => new Fieldmanager_Checkbox( 'Uključi na mobilnoj' ),
							'mobile-feed-from' => new Fieldmanager_Datepicker( 'Datum od', array(
								'name' => 'mobile-feed-from',
								'use_time' => true,
								'use_am_pm' => false,
								'date_format' => 'd.m.Y.',
								'js_opts' => array(
									'dateFormat' => 'dd.mm.yy.',
								),
							) ),
							'mobile-feed-to' => new Fieldmanager_Datepicker( 'Datum do', array(
								'name' => 'mobile-feed-to',
								'use_time' => true,
								'use_am_pm' => false,
								'date_format' => 'd.m.Y.',
								'js_opts' => array(
									'dateFormat' => 'dd.mm.yy.',
								),
							) ),
						) ) ),
				),

			) );
			$fm->add_meta_box( 'Promo pozicije', array( 'promo', 'igre' ) );
		$cats = new Fieldmanager_Datasource_Term(array('taxonomy' => 'category'));
		$fm = new Fieldmanager_Select( array( 'name' => 'main-category', 'first_empty' => true, 'options' => $cats->get_items() ));
		$fm->add_meta_box( 'Glavna kategorija', array( 'post', 'webcafe', 'promo', 'zena' ), 'side', 'default' );

		$fm = new Fieldmanager_Group( array(
			'name' => 'ispovijesti_data',
				'serialize_data' => false,
				'add_to_prefix' => false,
				'children' => array(
					'autor' => new Fieldmanager_TextField( 'Autor' ),
					'autor_email' => new Fieldmanager_TextField( 'Email autora' ),
					'editor_choice' => new Fieldmanager_Checkbox( 'Urednikov izbor' )
				)
			)
		);
		$fm->add_meta_box( 'Ispovijesti podaci', array( 'ispovijesti' ) );

		$fm = new Fieldmanager_Group( array(
			'name' => 'meta_tags',
			'add_to_prefix' => false,
			'children' => array(
				'title' => new Fieldmanager_TextField( 'Title' ),
				'description' => new Fieldmanager_TextField( 'Description' ),
				'image' => new Fieldmanager_Media( 'Image' )
			)
		) );
		$fm->add_meta_box( 'Meta tags', array( 'post', 'zena', 'webcafe', 'promo', 'ispovijestisv' ) );
	}

	function photographer_credit( $form_fields, $post ) {
		$form_fields['image-url'] = array(
			'label' => 'Link',
			'input' => 'text',
			'value' => get_post_meta( $post->ID, 'image_link', true ),
		);

		$form_fields['photographer-name'] = array(
			'label' => 'Fotograf',
			'input' => 'text',
			'value' => get_post_meta( $post->ID, 'photographer_name', true ),
		);

		$form_fields['photographer-agency'] = array(
			'label' => 'Agencija',
			'input' => 'text',
			'value' => get_post_meta( $post->ID, 'photographer_agency', true ),
		);

		return $form_fields;
	}

	function photographer_save( $post, $attachment ) {
		if ( isset( $attachment['image-url'] ) ) {
			update_post_meta( $post['ID'], 'image_link', $attachment['image-url'] );
		}

		if ( isset( $attachment['photographer-name'] ) ) {
			update_post_meta( $post['ID'], 'photographer_name', $attachment['photographer-name'] );
		}

		if ( isset( $attachment['photographer-agency'] ) ) {
			update_post_meta( $post['ID'], 'photographer_agency', $attachment['photographer-agency'] );
		}

		return $post;
	}

}

new Nethr_Meta_Boxes();