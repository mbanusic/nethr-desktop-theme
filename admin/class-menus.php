<?php

class Nethr_Menus {
	public function __construct() {
		add_action( 'init', array( $this, 'register' ) );
	}

	function register() {
		register_nav_menus( array(
			'home-top-left' => 'Home top left',
			'services' => 'Servisi',
			'header' => 'Gornji izbornik',
			'header-ispovijesti' => 'Gornji izbornik - ispovijesti',
			'main-home'   => 'Glavni - home',
			'main-danas'   => 'Glavni - vijesti',
			'main-hot' => 'Glavni - hot',
			'main-magazin' => 'Glavni - magazin',
			'main-auto' => 'Glavni - netmobil',
			'main-sport' => 'Glavni - sport',
			'main-webcafe' => 'Glavni - webcafe',
			'main-tehnoklik' => 'Glavni - tehnoklik',
			'mobilni' => 'Mobilni',
			'igrice-menu' => 'Igrice',
			'zena-net' => 'Žena Net'
		) );
	}
}

new Nethr_Menus();

class Nethr_Link_Menu extends Walker {

	var $db_fields = array(
		'parent' => 'menu_item_parent',
		'id'     => 'db_id'
	);

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', $classes );
		$output .= sprintf( "<a href='%s' class='%s'>%s</a>",
			$item->url,
			$class_names,
			$item->title
		);
	}

}