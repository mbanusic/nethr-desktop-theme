<?php

class Nethr_Post_Types {
	public function __construct() {
		add_action( 'init', array( $this, 'register' ) );
		add_filter( 'post_type_link', array( $this, 'filter_post_type_link' ), 10, 2 );
	}

	function register() {
		register_post_type( 'promo', array(
			'label'              => 'Promo',
			'description'        => 'Promotivni članci',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'promo/%category%' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 6,
			'menu_icon'          => 'dashicons-portfolio',
			'supports'           => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'post-formats',
				'zoninator_zones'
			),
			'taxonomies' => array(
				'category',
				'post_tag',
			)
		) );
		register_post_type( 'webcafe', array(
			'label'              => 'Webcafe',
			'description'        => 'Webcafe članci',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array(
				'slug' => 'cafe/%category%'
			),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-analytics',
			'supports'           => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'post-formats',
				'zoninator_zones'
			),
			'taxonomies' => array(
				'category',
				'post_tag',
			)
		) );

		register_post_type( 'ispovijesti', array(
			'label'              => 'Ispovijesti',
			'description'        => 'Ispovijesti korisnika',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array(
				'slug' => 'ispovijesti/%category%'
			),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-analytics',
			'supports'           => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'post-formats',
				'zoninator_zones'
			),
			'taxonomies' => array(
				'category',
				'post_tag',
			)
		) );

		register_post_type( 'igre', array(
			'label'              => 'Igrice',
			'description'        => 'Igrice',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var' => 'igre',
			'capability_type'    => 'post',
			'hierarchical'       => false,
			'menu_position'      => 6,
			'menu_icon'          => 'dashicons-album',
			'supports'           => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'zoninator_zones'
			),
			'taxonomies' => array(
				'category',
				'post_tag',
			),
			'rewrite'            => array(
				'slug' => 'igrice/%category%'
			),
		) );
		register_taxonomy(
			'positions',
			array('post', 'webcafe', 'promo', 'zena', 'ispovijesti', 'igre'),
			array(
				'label' => 'Pozicije',
				'rewrite' => array( 'slug' => 'pozicija' ),
				'hierarchical' => true,
			)
		);

		register_taxonomy('igrice', 'igre', array(
			'label' => 'Web igre',
			'public' => true,
			'hierarchical' => true,
			'rewrite' => array(
				'hierarchical' => true,
				'with_front' => true
			),
			'show_admin_column' => true
		));
		register_taxonomy_for_object_type( 'igrice', 'igre' );

		register_post_type( 'zena', array(
			'label'              => 'Žena',
			'description'        => 'Žena.net.hr',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array(
				'slug' => 'zena/%zena-cat%'
			),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-carrot',
			'supports'           => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'post-formats',
				'zoninator_zones'
			),
			'taxonomies' => array(
				'post_tag',
			)
		) );

		register_taxonomy('zena-cat', 'zena', array(
			'label' => 'Kategorije',
			'public' => true,
			'hierarchical' => true,
			'rewrite' => array(
				'hierarchical' => true,
				'with_front' => true,
				'slug' => 'zena-net'
			),
			'show_admin_column' => true
		));
		register_taxonomy_for_object_type( 'zena-cat', 'zena' );

		// register post format taxonomy on CPTs
		register_taxonomy_for_object_type( 'post_format', 'zena' );
		register_taxonomy_for_object_type( 'post_format', 'webcafe' );
		register_taxonomy_for_object_type( 'post_format', 'igre' );
		register_taxonomy_for_object_type( 'post_format', 'promo' );
	}

	function filter_post_type_link( $link, $post ) {
		if ( 'webcafe' === $post->post_type || 'ispovijesti' === $post->post_type ) {
			$cat = nethr_get_the_category( $post->ID );
			if ( !$cat || is_wp_error( $cat ) ) {
				$cat = 'webcafe';
			}
			else {
				$cat = $cat->slug;
			}
			$link = str_replace( '%category%', $cat, $link );
		}
		else if (  'igre' === $post->post_type ) {
			$cat = 'arkada';
			$cats = get_the_terms( $post->ID, 'igrice' );
			if ( !empty( $cats ) ) {
				$cat = $cats[0]->slug;
			}
			$link = str_replace( '%category%', $cat, $link );
		}
		else if ( 'promo' === $post->post_type  ) {
			$cat = nethr_get_the_category( $post->ID );
			$slugs = $cat->slug;
			while ( $cat->parent ) {
				$cat = get_category( $cat->parent );
				if ( !is_wp_error( $cat ) || 'zena' != $cat->slug ) {
					$slugs = $cat->slug . '/' . $slugs;
				}
			}
			$link = str_replace( '%category%', $slugs, $link );
		}
		else if ( 'zena' === $post->post_type  ) {
			$terms = get_the_terms( $post, 'zena-cat' );
			$link = str_replace( '%zena-cat%', $terms[0]->slug, $link );

		}
 		return $link;
	}

}

new Nethr_Post_Types();