jQuery(document).ready( function(){
    function media_upload() {
        jQuery('body').on('click','.upload_image_button', function(e) {
            var button_id ='#'+jQuery(this).attr('id');
            var buttonc ='.'+jQuery(this).attr('id');
            var button = jQuery(button_id);
            wp.media.editor.send.attachment = function(props, attachment){
                jQuery(buttonc).val(attachment.id);
            };
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload();

    jQuery('.autocomplete').autocomplete({
        source: function(request, response) {
            var data = {
                action: 'nethr_widget_get_posts',
                term: request.term
            };
            jQuery.post(ajaxurl, data, function( data ) { response(data); });
        },
        minLength: 3
    });
});

