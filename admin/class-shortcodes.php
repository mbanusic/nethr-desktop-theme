<?php

class Nethr_Shortcodes {
	public function __construct() {
		add_action( 'admin_init', array( $this, 'action_admin_init' ) );
		add_shortcode( 'nethr_light_box', array( $this, 'light_box' ) );
		add_shortcode( 'nethr_dark_box', array( $this, 'dark_box' ) );
		add_shortcode( 'nethr_quote_box', array( $this, 'quote_box' ) );
		add_shortcode( 'nethr_banner', array( $this, 'banner' ) );
		add_shortcode( 'nethr_galerija', array( $this, 'galerija' ) );
		add_shortcode( 'nethr_related', array( $this, 'related' ) );
		add_shortcode( 'nethr_video', array( $this, 'video' ) );
		add_shortcode( 'nethr_playbuzz', array( $this, 'playbuzz' ) );

	}

	function action_admin_init() {
		if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
			add_filter( 'mce_buttons', array( $this, 'buttons' ) );
			add_filter( 'mce_external_plugins', array( $this, 'plugins' ) );
		}
	}

	function buttons( $buttons ) {
		array_push( $buttons, 'separator', 'nethr_shortcodes' );

		return $buttons;
	}

	function plugins( $plugins ) {
		$plugins['nethr_shortcodes'] = plugin_dir_url( __FILE__ ) . 'mce-shortcodes.js';
		return $plugins;
	}

	function light_box( $atts, $content ) {
		return '<div class="article-box light-box cf">' . do_shortcode( wp_kses_post( $content ) ). '</div>';
	}

	function dark_box( $atts, $content ) {
		return '<div class="article-box dark-box cf">' . do_shortcode( wp_kses_post( $content ) ) . '</div>';
	}

	function quote_box( $atts, $content ) {
		return '<div class="quote cf">' . do_shortcode( wp_kses_post( $content ) ) . '</div>';
	}

	function banner( $atts, $content ) {
		ob_start();
		the_widget( 'Nethr_Banner_Widget', array( 'size' => $atts['size'] ) );
		return ob_get_clean();
	}

	function galerija( $atts, $content ) {
		$atts = shortcode_atts( array(
			'id' => 0,
			'url' => ''
		), $atts );
		if ( !$atts['id'] && !$atts['url'] ) {
			return '';
		}
		$url = $atts['url'];
		$id = $atts['id'];
		if ( !$url ) {
			$url = get_permalink( $id );
		}
		if ( !$id ) {
			$id = wpcom_vip_url_to_postid( $url );
		}
		ob_start();
		?>
		<a href="<?php echo esc_url( $url ) ?>#fotogalerija" class="gallery-link uppercase">
            <div class="triangle"></div>
			<div class="inner">
				<div class="gallery-link-text">
					<i class="fa fa-camera"></i>&nbsp;<span>Pogledaj fotogaleriju</span>
				</div>
			</div>
			<?php echo wp_kses_post( get_the_post_thumbnail( $id, 'single' ) ); ?>
		</a>
		<?php
		return ob_get_clean();
	}

	function related( $atts, $content ) {
		$atts = shortcode_atts( array(
			'id' => 0,
		), $atts );
		if ( !$atts['id'] ) {
			return '';
		}
		global $post;
		$post = get_post( $atts['id'] );
		setup_postdata( $post );
		ob_start();
		echo '<div class="related-article">';
		get_template_part( 'templates/articles/article-color' );
		echo '</div>';
		$out = ob_get_clean();
		wp_reset_postdata();

		return $out;
	}

	function video( $atts, $content ) {
		$player_id = 'QThcaxdM';
		$video_id = $atts['id'];
		ob_start();
		?>
		<script type="application/javascript" src="https://video.telegram.hr/players/<?php echo esc_attr( $video_id ) ?>-<?php echo esc_attr( $player_id ) ?>.js"></script>
		<?php
		return ob_get_clean();
	}

	function playbuzz( $atts ) {
		$atts = shortcode_atts(
			array(
				'url'        => '',
			), $atts );

		// Playbuzz Embed Code
		$code = '
		<script type="text/javascript" src="//cdn.playbuzz.com/widget/feed.js"></script>
		<div class="pb_feed" data-provider="WordPress" data-key="net.hr"  data-game="' . esc_url( $atts['url'] ) . '" data-game-info="false" data-comments="false" data-shares="true" data-recommend="false" data-width="auto" data-height="auto" data-margin-top="75"></div>
	';

		return $code;

	}
}

new Nethr_Shortcodes();


