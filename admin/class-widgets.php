<?php

class Nethr_Widgets {
	public function __construct() {
		add_action( 'widgets_init', array( $this, 'register_sidebars' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init' ) );
	}

	function widgets_init() {
		// each widget has it own file and calls register_widget
		foreach ( glob( AMNET_DIR . '/widgets/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/home/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/category/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/webcafe/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/mobile/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/other/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/auto/*.php' ) as $filename ) {
			require( $filename );
		}
		foreach ( glob( AMNET_DIR . '/widgets/zena/*.php' ) as $filename ) {
			require( $filename );
		}

        foreach ( glob( AMNET_DIR . '../adriaticmedia-nethr-mobile-2/widgets/*.php' ) as $filename ) {
            require( $filename );
        }
	}

	function register_sidebars() {

		register_sidebar(
			array(
				'name'          => 'Naslovnica gore',
				'description'   => 'Naslovnica -> Gornja grafika',
				'id'            => 'home-top',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Rubrika',
				'id'            => 'sidebar-category',
				'description'   => 'Bočna traka na rubrici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Rubrika auto',
				'id'            => 'sidebar-category-auto',
				'description'   => 'Bočna traka na rubrici auto',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Rubrika tanki',
				'id'            => 'sidebar-category-thin',
				'description'   => 'Srednja tanja traka na rubrici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Rubrika ispod',
				'id'            => 'under-category',
				'description'   => 'Sidebar ispod feeda',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Članak',
				'id'            => 'sidebar-single',
				'description'   => 'Bočna traka na članku',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Članak auto',
				'id'            => 'sidebar-single-auto',
				'description'   => 'Bočna traka na članku auto',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Članak - ispod',
				'id'            => 'single-under',
				'description'   => 'Ispod članka',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Članak - footer',
				'id'            => 'single-footer',
				'description'   => 'Footer članka',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Galerija',
				'id'            => 'sidebar-gallery',
				'description'   => 'Bočna traka na galerji',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Webcafe',
				'id'            => 'sidebar-webcafe',
				'description'   => 'Bočna traka na webcafeu',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
        register_sidebar(
            array(
                'name'          => 'Web Igrice',
                'id'            => 'sidebar-web-igrice',
                'description'   => 'Bočna traka na Igricama',
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '',
                'after_title'   => '',
            )
        );

		//new homepage sidebars
		register_sidebar(
			array(
				'name'          => 'Mobile footer',
				'id'            => 'sidebar-mobile-footer',
				'description'   => 'Mobile footer',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Mobile single',
				'id'            => 'sidebar-mobile',
				'description'   => 'Mobile single',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Blok glavni',
				'id'            => 'home_block_sidebar_main',
				'description'   => 'Widgeti uz prvi blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Blok danas',
				'id'            => 'home_block_sidebar_danas',
				'description'   => 'Widgeti uz danas blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Blok sport',
				'id'            => 'home_block_sidebar_sport',
				'description'   => 'Widgeti uz sport blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Blok žena',
				'id'            => 'home_block_sidebar_zena',
				'description'   => 'Widgeti uz magazin blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
        register_sidebar(
            array(
                'name'          => 'Naslovnica: Blok magazin',
                'id'            => 'home_block_sidebar_magazin',
                'description'   => 'Widgeti uz magazin blok vijesti na naslovnici',
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '',
                'after_title'   => '',
            )
        );
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Blok hot',
				'id'            => 'home_block_sidebar_hot',
				'description'   => 'Widgeti uz hot blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Naslovnica: Blok webcafe',
				'id'            => 'home_block_sidebar_webcafe',
				'description'   => 'Widgeti uz hot blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod glavnog',
				'id'            => 'home_under_main',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod danas',
				'id'            => 'home_under_danas',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod sport',
				'id'            => 'home_under_sport',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod žena',
				'id'            => 'home_under_zena',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod magazin',
				'id'            => 'home_under_magazin',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod hot',
				'id'            => 'home_under_hot',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Naslovnica: Ispod webcafe',
				'id'            => 'home_under_webcafe',
				'description'   => 'Widgeti ispod blok vijesti na naslovnici',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Glavni navbar',
				'id'            => 'navbar_sidebar',
				'description'   => 'Widgeti na glavnom navbaru -> Search i ikonice',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Mobilna tema',
				'id'            => 'mobile_sidebar',
				'description'   => 'Za mobilnu temu između Glavnog i Vijesti bloka',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Ispovijesti',
				'id'            => 'ispovijesti_sidebar',
				'description'   => '',
				'before_widget' => '<div class="widget ispovijest-form">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<div class="widget-header cf"><h2 class="section-title">',
				'after_title'   => '</h2></div><div class="widget-body gray">',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Blok na Naslovnici',
				'id'            => 'zena_net_sidebar_block',
				'description'   => 'Widgeti na naslovnici Net-a na bloku Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Header',
				'id'            => 'zena_net_header',
				'description'   => 'Za gornju grafiku',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Blok 1',
				'id'            => 'zena_net_category_blok_1',
				'description'   => 'Widgeti prvog bloka na rubrici Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Blok 1: Ispod',
				'id'            => 'zena_net_category_blok_1_bottom',
				'description'   => 'Widgeti ispod prvog bloka na Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Feed 1',
				'id'            => 'zena_net_category_feed_1',
				'description'   => 'Widgeti u prvom feedu na rubrici Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Feed 1: Ispod',
				'id'            => 'zena_net_category_feed_1_bottom',
				'description'   => 'Widgeti nispod prvog Feeda u rubrici Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Blok 2',
				'id'            => 'zena_net_category_blok_2',
				'description'   => 'Widgeti drugog bloka na rubrici Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Blok 2: Ispod',
				'id'            => 'zena_net_category_blok_2_bottom',
				'description'   => 'Widgeti ispod drugog bloka na Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Rubrika - Feed 2',
				'id'            => 'zena_net_category_feed_2',
				'description'   => 'Widgeti u drugom feedu na rubrici Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => 'Žena: Članak',
				'id'            => 'zena_net_sidebar_single',
				'description'   => 'Widgeti na članku Žena Net',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

	}
}

new Nethr_Widgets();