<?php
get_header('ispovijesti');
?>
    <div class="container single ispovijesti ispovijesti-single cf">

	    <div class="ispovijesti-count">
		    <?php
		    $post_count = wp_count_posts( 'ispovijesti' );
		    $post_count = $post_count->publish;
		    if ( $post_count ) {
			    ?>
			    Trenutno imamo <a href="https://net.hr/kategorija/ispovijesti/"
				    class="highlight"><?php echo esc_html( $post_count ); ?>
			    ispovijesti </a>
		    <?php }
		    ?>
	    </div>

        <div class="page-grid single-article">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <div class="post-categories">
                    <?php the_title(); ?>
                </div>

                <div class="article-content" id="__xclaimwords_wrapper">
                    <?php the_content(); ?>
                </div>

            <?php endwhile;  endif; ?>

        </div>

        <div class="sidebar single-sidebar single-sidebar-1">
            <?php dynamic_sidebar( 'ispovijesti_sidebar' ) ?>
        </div>

    </div>

<?php
get_footer();
