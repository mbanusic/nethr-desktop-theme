<?php
get_header();

?>
	<div class="container archive cf">

		<div class="section-header">
			<div class="yellow-title">
				<a href="https://net.hr"
				   class="active">Net.hr - Bez nogometa</a>
			</div>

			<?php get_template_part( 'templates/layout/socials' ); ?>

		</div>

		<div class="page-grid">

			<section class="feed cf">
				<?php
				$data = wp_cache_get( 'nethr_bez_nogometa', 'nethr' );
				if ( !$data ) {
					ob_start();
					$q = new WP_Query(
						array(
							'posts_per_page'   => 30,
							'post_status'      => 'publish',
							'post_type'        => array( 'post', 'promos' ),
							'category__not_in' => array( 226063, 439601120 ), //nogomet category
							'no_found_rows'    => true,
							'ignore_sticky_posts' => true,
						)
					);
					if ( $q->have_posts() ) {
						while ( $q->have_posts() ) {
							$q->the_post();
							get_template_part( 'templates/articles/article-feed' );
						}
					}
					wp_reset_postdata();
					$data = ob_get_clean();
					wp_cache_set( 'nethr_bez_nogometa', $data, 'nethr', 10 * MINUTE_IN_SECONDS );
				}
				echo $data;
				?>
			</section>

		</div>


		<div class="sidebar single-sidebar single-sidebar-1">

			<?php dynamic_sidebar( 'sidebar-category' ) ?>

		</div>

	</div>


<?php
get_footer();
