<?php
// NET.HR -> Single article
get_header();
?>
	<div class="container single cf">

		<div class="section-header">
            <div class="section-titles">
                <a href="https://net.hr/igrice/" class="active">Igrice</a>
            </div>
			<?php get_template_part( 'templates/layout/socials' ); ?>
		</div>

		<div class="single-game">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="article-content">
					<?php the_content(); ?>
				</div>

			<?php endwhile;  endif; ?>

		</div>
		<?php dynamic_sidebar( 'single-footer' ) ?>
	</div>

<?php
get_footer();
