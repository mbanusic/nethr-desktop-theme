<?php

get_header( 'embed' );

if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?>
			<?php
			if ( 'gallery' === get_post_type() ) {
				?>
				<a href="<?php the_permalink() ?>#fotogalerija" class="gallery-link uppercase" target="_top">
					<div class="triangle"></div>
					<div class="inner">
						<div class="gallery-link-text">
							<i class="fa fa-camera"></i>&nbsp;<span>Pogledaj fotogaleriju</span>
						</div>
					</div>
					<?php the_post_thumbnail( $id, 'single' ); ?>
				</a>
				<?php
			}
			else {
				get_template_part( 'templates/articles/article-color' );
			}
			?>
		<?php
	endwhile;
endif;

get_footer( 'embed' );
