<?php // Page pretrazivanje
get_header(); ?>

<div class="search-head">
	<div class="search-container cf">

		<div class="logo"></div>
		<form class="searchbar" action="<?php echo esc_url( site_url( 'pretrazivanje' ) ); ?>" id="cse-search-box">

				<input type="hidden" name="cx" value="partner-pub-2317149376955370:wn57ucrd4ll" />
				<input type="hidden" name="cof" value="FORID:10" />
				<input type="hidden" name="ie" value="UTF-8" />
				<input type="text" name="q" size="31" />

			<div class="search-button">
				<input type="submit" name="sa" value="" />
				<i class="fa fa-search"></i>
			</div>
		</form>

		<script type="text/javascript" src="//www.google.hr/coop/cse/brand?form=cse-search-box&amp;lang=hr"></script>


	</div>
</div>

<div class="search-body">
	<div class="search-container cf">

	<div id="cse-search-results"></div>
	<script type="text/javascript">
		var googleSearchIframeName = "cse-search-results";
		var googleSearchFormName = "cse-search-box";
		var googleSearchFrameWidth = 1000;
		var googleSearchDomain = "www.google.hr";
		var googleSearchPath = "/cse";
	</script>
	<script type="text/javascript" src="//www.google.com/afsonline/show_afs_search.js"></script>

	</div>
</div>
	<div id="__xclaimwords_wrapper"></div>

<?php get_footer();