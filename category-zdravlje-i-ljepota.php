<?php
// NET.HR -> Category
get_header();
?>
    <div class="container category category-new cf">

        <div class="section-header">
            <div class="section-titles">
                <a href="https://net.hr/kategorija/magazin/zdravlje-i-ljepota/"
                   class="active">Zdravlje i ljepota</a>
            </div>

            <?php get_template_part( 'templates/layout/socials' ); ?>

        </div>

        <div class="page-grid">

            <section class="category-head cf">

                <?php
                $zone_query = z_get_zone_query( 'promo-ljepota-i-zdravlje', array( 'posts_per_page' => 6 ) );
                if ( $zone_query->have_posts() ) :
                    while ( $zone_query->have_posts() ) : $zone_query->the_post();
                        if ( 0 === $zone_query->current_post ) {
                            get_template_part( 'templates/articles/article-lead' );
                        }
                        else if ( 4 === $zone_query->current_post ) {
                            get_template_part( 'templates/articles/article-1');
                        }
                        else {
                            get_template_part( 'templates/articles/article-2-special' );
                        }
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>

            </section>

            <section class="feed cf">

                <div class="section-header">
                    <div class="section-titles">
                        <a class="active">Najnovije</a>
                    </div>
                </div>

                <div class="feed-cf">
                    <?php
                    //main feed
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post();
                            get_template_part( 'templates/articles/article-feed' );
                        }
	                    wp_reset_postdata();
                    }
                    ?>
                </div>

            </section>

            <div class="article-navigation">
	            <?php next_posts_link( 'JOŠ VIJESTI <i class="fa fa-angle-right"></i>' ); ?>
            </div>

        </div>


        <div class="sidebar single-sidebar single-sidebar-1">
            <?php
            the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_300x250_v1' ) );

            the_widget( 'Nethr_Promo_Check_Out_Widget', array( 'title' => 'Zdravlje i ljepota', 'number' => 10, 'cache_name' => 'zdravlje_ljepota_widget' ) )
            ?>
        </div>

        <?php dynamic_sidebar( 'under-category' ) ?>
    </div>



<?php
get_footer();
