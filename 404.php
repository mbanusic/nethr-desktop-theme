<?php // 404 - Oh noes!

	get_header(); ?>

	<div class="page-404">

		<h1 class="big">404</h1>
		<h2 class="text">Čini se da stranica nije pronađena. </br> Može vic kao zamjena?</h2>

		<?php the_widget( 'Nethr_404_Joke_Widget' ) ?>

	</div>

	<?php
	get_footer();
