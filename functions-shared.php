<?php
/**
 * Shared functions with desktop and mobile theme
 *
 * Definitions of post types, custom fields, shortcodes, etc.
 */

//require all the files
// Set a theme constant for later use
define( 'AMNET_DIR', __DIR__ );

/**
 * Standard VIP configuration
 */
require_once( WP_CONTENT_DIR . '/themes/vip/plugins/vip-init.php' );


//define constants
define( 'COAUTHORS_DEFAULT_BETWEEN_LAST', ' i ' );

//Load REST API
wpcom_vip_load_wp_rest_api();

//load plugins and plugin configuration
wpcom_vip_load_plugin( 'fieldmanager', 'plugins', '1.1' );
wpcom_vip_load_plugin( 'wpcom-thumbnail-editor' );
wpcom_vip_load_plugin( 'co-authors-plus', 'plugins', '3.2' );
wpcom_vip_load_plugin( 'external-permalinks-redux' );
wpcom_vip_load_plugin( 'msm-sitemap', 'plugins', '1.3' );
wpcom_vip_load_plugin( 'zoninator', 'plugins', '0.8' );
wpcom_vip_load_plugin( 'facebook-instant-articles', 'plugins', '4.0' );
wpcom_vip_load_plugin( 'playbuzz', 'plugins', '1.0' );

wpcom_vip_enable_opengraph();

wpcom_vip_disable_postpost();

wpcom_vip_load_permastruct( '/%category%/%postname%/' );

wpcom_vip_load_category_base( 'kategorija' );

wpcom_vip_load_tag_base( 'tema' );

wpcom_vip_set_image_quality( 100, 'all' );

wpcom_vip_enabled_cap_in_oembed();

// Post Types
require_once( AMNET_DIR . '/admin/class-post-types.php' );

// Shortcodes
require_once( AMNET_DIR . '/admin/class-shortcodes.php' );

// Custom meta fields and boxes
require_once( AMNET_DIR . '/admin/class-meta-boxes.php' );

// Widgets
require_once( AMNET_DIR . '/admin/class-widgets.php' );

//Menus
require_once( AMNET_DIR . '/admin/class-menus.php' );

// Admin interface
require_once( AMNET_DIR . '/admin/class-admin-interface.php' );

require_once( AMNET_DIR . '/admin/class-ajax.php' );

//plugins
require_once( AMNET_DIR . '/plugins/banners.php' );
require_once( AMNET_DIR . '/plugins/embeds.php' );
require_once( AMNET_DIR . '/plugins/analytics.php' );
require_once( AMNET_DIR . '/plugins/feeds.php' );
require_once( AMNET_DIR . '/plugins/legacy.php' );
require_once( AMNET_DIR . '/plugins/term-meta.php' );
require_once( AMNET_DIR . '/plugins/cron.php' );
require_once( AMNET_DIR . '/plugins/dfp.php' );

//require CLI commands
if ( defined('WP_CLI') && WP_CLI ) {
	include __DIR__ . '/cli.php';
}

//setup theme
function nethr_setup() {

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	// Enable support for Post Thumbnails, and declare sizes.
	add_theme_support( 'post-thumbnails' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'video',
		'gallery',
		'aside',
		'status'
	) );


	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );

	//image sizes
	add_image_size( 'thumb', 80, 60, true );
	add_image_size( 'article-1', 480, 330, true );
	add_image_size( 'article-2', 178, 160, true );
	add_image_size( 'article-3', 100, 60, true );
	add_image_size( 'article-3_2x', 200, 120, true );
	add_image_size( 'feed-1', 165, 100, true );
	add_image_size( 'feed-1-long', 165, 273, true );
	add_image_size( 'feed-2', 300, 170, true );
	add_image_size( 'top-graphic', 500, 90, true );
	add_image_size( 'single', 670, 400, true );
	add_image_size( 'single-raw', 670, 9999 );
	add_image_size( 'breaking-2', 740, 365, true );
	add_image_size( 'breaking-3', 740, 500, true );
	add_image_size( 'article-2_special', 218, 170, true );
	add_image_size( 'longform-single', 996, 560, true );
	add_image_size( 'article-promo', 340, 500, true );
}

add_action( 'after_setup_theme', 'nethr_setup' );

// load css and js
function nethr_scripts() {
	// register fonts
	wp_register_style( 'nethr-font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array() );
	wp_register_style( 'nethr-font-oswald', '//fonts.googleapis.com/css?family=Oswald:400,300,700&subset=latin,latin-ext', array() );
	wp_register_style( 'nethr-font-source', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900,400italic&subset=latin,latin-ext', array() );
	wp_register_style( 'nethr-font-pt-sans', 'https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,latin-ext' );
	wp_register_style( 'nethr-font-pt-serif', 'https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic&subset=latin,latin-ext' );
	wp_register_style( 'nethr-videojs', 'https://vjs.zencdn.net/7.1.0/video-js.css' );

	// register scripts
	wp_register_script( 'nethr-ticker', get_template_directory_uri() . '/js/ticker.js', array( 'jquery' ), '20141106', true );
	wp_register_script( 'html5-shiv', '//mocdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js' );
	wp_style_add_data( 'html5-shiv', 'conditional', 'lt IE 9' );

	wp_register_script( 'nethr-videojs-js', 'https://vjs.zencdn.net/7.1.0/video.js' );
	wp_register_script( 'nethr-ima', '//imasdk.googleapis.com/js/sdkloader/ima3.js') ;
	wp_register_script( 'nethr-videojs-ads-js', get_template_directory_uri().'/js/videojs.ads.js', null, null, true );

	wp_enqueue_style( 'nethr-slick-style', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );
	wp_enqueue_style( 'nethr-slick-theme-style', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css' );

	/* https://github.com/kenwheeler/slick */
	wp_register_script( 'nethr-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js' );

	wp_enqueue_script( 'nethr-script', get_template_directory_uri() . '/js/functions.js', array(
		'jquery',
		'nethr-slick',
		'nethr-ticker',
		'nethr-videojs-js',
	), '20160509', true );
	wp_localize_script( 'nethr-script', 'nethrVars', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	wp_enqueue_script('nethr-vingd', 'https://subs.vingd.com/client/subs.js?pubid=net_hr', array(), '20170927', true );
}

add_action( 'wp_enqueue_scripts', 'nethr_scripts' );


function nethr_body_class( $classes ) {
	if ( is_archive() || is_single() ) {
		if ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$classes[] = 'format-gallery';
		}
		$cat = get_the_category();
		if ( is_array( $cat ) ) {
			$cat = $cat[0];
		}
		if ( 'crna-kronika' != $cat->slug ) {
			while ( $cat->parent ) {
				$cat = get_category( $cat->parent );
			}
		}
		$classes[] = $cat->slug;
	}

	$classes[] = 'tmg-no-cookie';

	return $classes;
}

add_filter( 'body_class', 'nethr_body_class' );

//force post format template
function nethr_use_post_format( $template ) {
	if ( is_single() && has_post_format() && 'zena' != get_post_type() ) {
		$post_format_template = locate_template( 'single-' . get_post_format() . '.php' );
		if ( $post_format_template ) {
			$template = $post_format_template;
		}
	}

	return $template;
}

add_filter( 'template_include', 'nethr_use_post_format' );

function nethr_og_graph_tags( $tags ) {
	$tags['fb:app_id'] = '103100136402693';
	$tags['fb:pages'] = '86874647886';
	if ( is_single() ) {
		$tags['article:publisher'] = 'https://www.facebook.com/Net.hr';
		if ( 'ispovijesti' === get_post_type() ) {
			$tags['og:image'] = 'https://adriaticmedianethr.files.wordpress.com/2016/06/ispovijesti_og.png';
		}
		if ( !isset( $tags['article:author'] ) || !$tags['article:author'] ) {
			$coauthors = get_coauthors();
			if ( is_array( $coauthors ) ) {
				foreach ( $coauthors as $one ) {
					if ( isset( $one->facebook ) && $one->facebook ) {
						$tags['article:author'] = esc_url( $one->facebook );
						break;
					}
			}
			}
		}
		$custom_meta_tags = get_post_meta(get_the_ID(), 'meta_tags', true);
		if ( $custom_meta_tags ) {
			if ( isset( $custom_meta_tags['title'] ) && $custom_meta_tags['title'] ) {
				$tags['og:title'] = wp_kses( $custom_meta_tags['title'], array() );
			}
			if ( isset( $custom_meta_tags['description'] ) && $custom_meta_tags['description'] ) {
				$tags['og:description'] = wp_kses( $custom_meta_tags['description'], array() );
			}
			if ( isset( $custom_meta_tags['image'] ) && $custom_meta_tags['image'] ) {
				$image = wp_get_attachment_image_src( $custom_meta_tags['image'], array( 1200, 1200 ) );
				$tags['og:image'] = esc_url( $image[0] );
				$tags['og:image:width'] = intval( $image[1] );
				$tags['og:image:height'] = intval( $image[2] );
			}
		}
	}

	return $tags;
}

add_filter( 'jetpack_open_graph_tags', 'nethr_og_graph_tags', 10, 1 );

add_filter( 'oembed_response_data', 'nethr_oembed_data', 10, 4 );

function nethr_oembed_data($data, $post, $width, $height) {
    $coauthors = get_coauthors( $post->ID );
    if ( is_array( $coauthors ) ) {
	    $data['author_name'] = $coauthors[0]->display_name;
        if ( isset( $coauthors[0]->facebook ) && $coauthors[0]->facebook ) {
            $data['author_url']  =  esc_url( $coauthors[0]->facebook );
        }
    }
    return $data;
}

add_filter('wp_head', 'nethr_meta_tags');

function nethr_meta_tags() {
	if (is_front_page() || is_single()) {
	    global $post;
		?><meta name="description" content="<?php echo esc_attr( nethr_get_excerpt( 200, $post->post_content ) ) ?>"><?php
	}
	if (is_category()) {
		?><meta name="description" content="<?php echo esc_attr( nethr_get_excerpt( 200, category_description() ) ) ?>"><?php
	}
}


////////smart functions

//load some extra data with template files
function nethr_get_template( $template, $options = array() ) {
	include( locate_template( $template . '.php' ) );
}

//better excerpt
function nethr_get_excerpt( $limit, $excerpt = null ) {
    if (!$excerpt) {
	    $excerpt = get_the_content();
    }
	$stripped = strip_tags( $excerpt );
	$stripped = strip_shortcodes( $stripped );
	$stripped = str_replace("\n", '', $stripped);

	if ( mb_strlen( $stripped ) > $limit ) {
		$trimmed           = rtrim( mb_substr( $stripped, 0, $limit ) );
		$new_excerpt_array = explode( ' ', $trimmed );
		$last_word         = array_pop( $new_excerpt_array );
		$forbidden         = array( '.', ',', '?', '!', ':', ';' );
		if ( in_array( mb_substr( $last_word, - 1 ), $forbidden ) ) {
			$last_word = mb_substr( $last_word, 0, mb_strlen( $last_word ) - 1 );
		}
		array_push( $new_excerpt_array, $last_word );
		$new_excerpt = implode( ' ', $new_excerpt_array ) . '...';
		return $new_excerpt;
	} else {
		return $stripped;
	}
}

function nethr_intro_excerpt() {
	global $post;
	$content = $post->post_content;
	$matches = array();
	preg_match('/<h4>(.*)<\/h4>/', $content, $matches);
	$intro = strip_tags( $matches[1] );
	echo esc_html( $intro );
}

/**
 * Function returns top category slug for use with templates.
 * We cache it in post_meta to avoid multiple get_category request
 * for nested categories
 *
 * @param int $post_id
 *
 * @return string
 */
function nethr_category_color( $post_id = 0 ) {
	if ( !$post_id ) {
		$post_id = get_the_ID();
	}
	$color = get_post_meta( $post_id, 'nethr_color', true );
	if ( !$color ) {
		$cat = nethr_get_top_category( $post_id );
		if ( true === is_object( $cat ) && false === is_wp_error( $cat ) ) {
			$color = $cat->slug;
		}
		else {
			$color = 'webcafe';
		}
		update_post_meta( $post_id, 'nethr_color', $color );
	}
	return $color;
}

/**
 * Function to return top parent category for posts.
 *
 * @param int $post_id
 *
 * @return bool|object|WP_Error
 */
function nethr_get_top_category( $post_id = 0 ) {
    $cat = nethr_get_the_category( $post_id );
	if ( !$cat )
		return false;
	if ( 'crna-kronika' === $cat->slug || 'zena' === $cat->slug ) {
		return $cat;
	}
	if ( $cat->parent ) {
		$cat = get_category( $cat->parent );
		while ( $cat->parent ) {
			$cat = get_category( $cat->parent );
		}
	}
	if ( !in_array( $cat->slug, array( 'danas', 'sport', 'hot', 'magazin', 'auto', 'tehnoklik', 'webcafe', 'ispovijesti' ) ) ) {
		$cat =  wpcom_vip_get_category_by_slug( 'danas' );
	}
    return $cat;
}

/**
 * Function to get default post category.
 * Some post will have multiple categories, user can choose the default
 * category.
 *
 * @param bool|false $post_id
 *
 * @return bool|object|WP_Error
 *
 * TODO: refactor this function to simplify logic and enable expansion for future use.
 */
function nethr_get_the_category( $post_id = false ) {
	$cat = false;
	if ( is_singular( 'igre' ) || is_tax( 'igrice' ) || is_page( 'igrice' ) ) {
		return wpcom_vip_get_category_by_slug( 'webcafe' );
	}
	if ( is_singular( 'zena' ) || is_tax( 'zena-cat' ) || is_page( 'zena' ) ) {
		/*
		 * Special case: Since zena CPT doesn't use categories, we build category
		 * object necessary for all the uses.
		 */
		$cat = new stdClass();
		$cat->slug = 'zena';
		$cat->name = 'Žena';
		return $cat;
	}
	if ( is_single() || $post_id ) {
		if ( !$post_id ) { // we are in the loop
			$post_id = get_the_ID();
		}
		$cats = get_the_category( $post_id );

		$default_cat = get_post_meta( $post_id, 'main-category', true );
		if ( $default_cat ) {
			foreach ( $cats as $one ) {
				if ( $one->term_id === $default_cat ) {
					$cat = $one;
					break;
				}
			}
		}
		else { // no default category, use first one, usually only one (editorial guidelines)
			$cat  = $cats[0];
		}
	}
	else if ( is_category() ) {
		$cat = get_category( get_query_var('cat') );
	}
	// if we didn't match any category return "default" category
	if ( !$cat || is_wp_error( $cat ) ) {
		$cat =  wpcom_vip_get_category_by_slug( 'danas' );
	}

	return $cat;
}

/**
 * Return formatted photo information
 *
 * @param int $id
 *
 * @return string
 */
function nethr_get_photographer( $id = 0 ) {
	if ( !$id ) {
		$id = get_post_thumbnail_id();
	}
	else {
		$id = intval( $id );
	}
	$name = get_post_meta( $id, 'photographer_name', true );
	$agency = get_post_meta( $id, 'photographer_agency', true );
	$photo = '';
	if ( $name ) {
		$photo .= $name;
	}
	if ( $agency ) {
		if ( $photo ) {
			$photo .= '/';
		}
		$photo .= $agency;
	}
	if ($photo) {
		$photo = 'Foto: '.$photo;
	}
	return esc_html( $photo );
}

function nethr_get_time() {
	$date = DateTime::createFromFormat( 'U', get_post_time() );
	$yesterday = new DateTime( 'yesterday' );
	if ( $date < $yesterday ){
		return $date->format( 'd.m.Y' );
	}
	$now = new DateTime();
	$now = $now->setTimezone( new DateTimeZone( 'Europe/Zagreb' ) );
	$diff = $now->diff( $date );
	return 'prije ' . $diff->format( ' %h sati %i minuta' );
}

function nethr_excerpt_tagged( $limit ) {
	$excerpt = get_the_content();
	$excerpt = explode( ' ', $excerpt, $limit );
	array_pop( $excerpt );
	$excerpt = implode( ' ', $excerpt );
	$excerpt .= '...';
	$excerpt = balanceTags( $excerpt );
	remove_filter( 'the_content', 'nethr_the_content', 1 );
	remove_filter( 'the_content', 'nethr_mobile_the_content' );
	$excerpt = apply_filters( 'the_content', $excerpt );
	add_filter( 'the_content', 'nethr_the_content', 1 );
	add_filter( 'the_content', 'nethr_mobile_the_content' );
	return $excerpt;
}

function nethr_change_paste_as_text($mceInit, $editor_id){
	//turn off paste_text_use_dialog and turn on sticky (and default)
	$mceInit['paste_as_text'] = true;
	return $mceInit;
}
add_filter('tiny_mce_before_init', 'nethr_change_paste_as_text', 1, 2);

add_filter( "shortcode_atts_caption", 'nethr_img_caption_atts', 10, 3 );

function nethr_img_caption_atts($out, $pairs, $atts ) {
	$id = str_replace( 'attachment_', '', $out['id'] );
	$out['caption'] .= '<span class="right">' . esc_html( nethr_get_photographer( $id ) ) . '</span>';
	return $out;
}

function nethr_promos( $what, $cat = false, $required = 0 ) {
	switch ( $what ) {
		case 'home-feed':
			$transient = 'nethr_promos';
			break;
		case 'home-widget':
			$transient = 'nethr_widget_promos';
			break;
		case 'zena-feed':
			$transient = 'nethr_zena_promos';
			break;
		case 'cat-widget':
			if ( !$cat ) {
				return false;
			}
			$transient = 'nethr_category_promos_' . $cat->slug;
			break;
		case 'mobile-feed':
			$transient = 'nethr_mobile_promos';
			break;
		default:
			return false;
	}
	$posts = get_transient( $transient );
	if ( false === $posts ) {
		$posts = array();
		$args = array(
			'posts_per_page' => 30,
			'post_type'      => array( 'promo', 'igre' ),
			'post_status'    => 'publish',
			'meta_query' => array(
				array(
					'key' => $what . '-on',
					'value' => 1,
					'type' => 'BINARY'
				)
			),
			'no_found_rows'  => true,
			'fields'         => 'ids',
			'ignore_sticky_posts' => true
		);
		if ( 'cat-widget' === $what && $cat ) {
			//per category widget data
			$args['cat'] = $cat->term_id;
		}
		$q   = new WP_Query( $args );
		if ( $q->have_posts() ) {
			foreach( $q->posts as $id ){
				$promos = get_post_meta( $id );
				$from   = intval( $promos[$what . '-from'][0] );
				$to     = intval( $promos[$what . '-to'][0] );
				$time = current_time( 'timestamp' );
				if ( $from < $time && $to > $time ) {
					// post should be visible
					$posts[] = $id;
				}
				else if ( $to < $time ) {
					//post expired, turn off
					update_post_meta( $id, $what . '-on', 0 );
				}

			}
		}
		// shuffle and save
		shuffle( $posts );
		set_transient( $transient, $posts, 15 * MINUTE_IN_SECONDS );
	}
	return $posts;
}

function nethr_permalink_target( $id = false ) {
	if ( !$id ) {
		$id = get_the_ID(); //inside of loop
	}
	$ext = get_post_meta( $id, '_external_permalink', true );
	$target = '_top';
	if ( $ext ) {
		$target = '_blank';
	}
	return $target;
}

add_filter( 'epr_post_types', 'nethr_epr_types' );

function nethr_epr_types( $post_types ) {
	$post_types[] = 'promo';
	$post_types[] = 'webcafe';
	$post_types[] = 'igre';
	$post_types[] = 'ispovijesti';
	$post_types[] = 'zena';
	return $post_types;
}

add_filter( 'wp_feed_cache_transient_lifetime', 'nethr_feed_cache', 10, 2 );

function nethr_feed_cache( $time, $url ) {
	if ( strpos( $url, 'telegram' ) || strpos( $url, 'net.hr' || strpos( $url, 'vecernji.hr' ) )  ) { // we only want lower time for our feeds
		return 300;
	}

	return $time;
}

function nethr_get_telegram_posts() {
	$articles = get_transient( 'nethr_telegram_posts' );

	return $articles;
}

add_filter( 'post_link_category', 'nethr_post_link_category', 10, 3 );

function nethr_post_link_category( $cat, $cats, $post ) {
	$main = get_post_meta( $post->ID, 'main-category', true );
	if ( $main ) {
		foreach ( $cats as $one ) {
			if ( $one->term_id === $main ) {
				return $one;
			}
		}
	}
	return $cat;
}

add_filter( 'post_date_column_time', 'nethr_column_time', 10, 4 );

function nethr_column_time( $time, $post, $name, $mode) {
	if ( 'list' === $mode && 'future' === $post->post_status ) {
		return mysql2date( 'd.m.Y. H:i', $post->post_date );
	}
	return $time;
}

add_filter( 'msm_sitemap_entry_post_type', 'nethr_sitemap_post_types' );

function nethr_sitemap_post_types( $post_types ) {
	return array( 'post', 'promo', 'webcafe', 'ispovijesti', 'zena' );
}

function nethr_allow_my_post_types( $allowed_post_types ) {
	$allowed_post_types[] = 'webcafe';
	$allowed_post_types[] = 'promo';
	$allowed_post_types[] = 'attachment';
	$allowed_post_types[] = 'igre';
	$allowed_post_types[] = 'ispovijesti';
	$allowed_post_types[] = 'zena';
	return $allowed_post_types;
}

add_filter( 'rest_api_allowed_post_types', 'nethr_allow_my_post_types');

function nethr_rest_api_allowed_public_metadata( $allowed_meta_keys )
{
	$allowed_meta_keys[] = 'photographer_name';
	$allowed_meta_keys[] = 'photographer_agency';
	$allowed_meta_keys[] = 'gallery-items';
	$allowed_meta_keys[] = 'nethr_color';
	$allowed_meta_keys[] = 'extra_titles';
	$allowed_meta_keys[] = 'main-category';
	$allowed_meta_keys[] = '_external_permalink';

	return $allowed_meta_keys;
}

add_filter( 'rest_api_allowed_public_metadata', 'nethr_rest_api_allowed_public_metadata' );

/**
 * Save attachment ids in post meta for easier retrieval in REST API
 */
add_action( 'save_post', 'nethr_save_post', 10, 2 );

function nethr_save_post( $post_id, $post ) {
	if ( !current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	if ( 'gallery' != get_post_format( $post ) ) {
		return;
	}

	$content = $post->post_content;

	global $shortcode_tags; //get all registered shortcodes
	$old_shortcode_tags = $shortcode_tags; //save them
	$shortcode_tags = array( 'gallery' => $old_shortcode_tags['gallery'] ); //replace them by gallery shortcode only

	preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );
	$attr = shortcode_parse_atts( $matches[0][3] );
	if ( isset( $attr['ids'] ) ) {
		$ids = explode( ',', $attr['ids'] );

		$shortcode_tags = $old_shortcode_tags; //restore original shortcodes
		update_post_meta( $post_id, 'gallery-items', $ids );
	}
}

add_action('init', function() {
	require_once( AMNET_DIR . '/plugins/sharing.php' );
});

add_filter( 'amp_is_enabled', '__return_false', 100 );

// Trim empty first paragraph
add_filter('the_content', 'nethr_trim', 1,1);

function nethr_trim($content) {
	if (strpos($content, "&nbsp;")==0) {
		$count = 1;
		$content = str_replace("&nbsp;", '', $content, $count );
		return trim($content);
	}
	return $content;
}

/**
 * Helper functions to help ensure that we always have a small positive integer
 * in WP_Query
 *
 * @param int $number
 *
 * @return int
 */
function nethr_sanitize_posts_per_page( $number = 1 ) {
	$number = absint( $number );
	if ( !$number ) {
		$number = 1;
	}
	return absint( min( $number, 50 ) );
}

add_filter( 'zoninator_edit_zone_cap', 'nethr_zoninator_edit_zone_cap', 10, 1 );
/**
 * Raise capability for editing and deleting zones
 */
function nethr_zoninator_edit_zone_cap( $cap ) {
	return 'manage_options';
}

add_filter( 'category_template', 'nethr_child_category_template' );

/**
 * Set category template for specific categories, in this case for children of
 * Ispovijesti category. We hardcoded the category IDs to avoid unncessary query
 * to check category parent.
 *
 * @param $template
 *
 * @return string
 */
function nethr_child_category_template( $template ) {
	$cat = intval( get_query_var( 'cat' ) );
	if ( in_array( $cat, array( 444206542, 444206541, 444206540, 444206539, 444206537 ) ) && file_exists(TEMPLATEPATH . '/category-ispovijesti.php') ) {
		$template = TEMPLATEPATH . '/category-ispovijesti.php';
	}

	return $template;
}

add_filter( 'coauthors_guest_author_fields', 'nethr_filter_guest_author_fields', 10, 2 );
function nethr_filter_guest_author_fields( $fields_to_return, $groups ) {
	if ( in_array( 'all', $groups ) || in_array( 'contact-info', $groups ) ) {
		$fields_to_return[] = array(
			'key'      => 'facebook',
			'label'    => 'Facebook',
			'group'    => 'contact-info',
		);
		$fields_to_return[] = array(
			'key'      => 'twitter',
			'label'    => 'Twitter',
			'group'    => 'contact-info',
		);
	}
	return $fields_to_return;
}

add_filter( 'instant_articles_post_types', 'nethr_instant_articles_post_types' );

function nethr_instant_articles_post_types( $post_types ) {
	$post_types = array( 'post', 'webcafe', 'promo', 'zena', 'ispovijesti' );
	return $post_types;
}

add_filter( 'instant_articles_transformed_element', 'nethr_instant', 10, 1 );

function nethr_instant( $article ) {
	if ( 'zena' === get_post_type() ) {
		$article->withStyle( 'zena' );
	}
	return $article;
}

add_filter( 'msm_sitemap_skip_post', 'nethr_sitemap_skip_post' );

function nethr_sitemap_skip_post( $skip ) {
	if ( false === strpos( get_the_permalink(), 'net.hr' ) ) {
		//skip redirected posts
		return true;
	}
	return $skip;
}

function return_cache_time( $seconds ){
	// change the default feed cache recreation period to 60 seconds
    return (int) 10;
}
//set feed cache duration
add_filter( 'wp_feed_cache_transient_lifetime', 'return_cache_time');

/**
 * Register the rewrite rule for /ads.txt request.
 */
function nethr_adstxt_rewrite() {
	add_rewrite_rule( '^ads\.txt$', 'index.php?nethr_adstxt=true', 'top' );
}
add_action( 'init', 'nethr_adstxt_rewrite', 10 );

/**
 * Filter the list of public query vars in order to allow the WP::parse_request
 * to register the query variable.
 *
 * @param array $public_query_vars The array of whitelisted query variables.
 *
 * @return array
 */
function nethr_adstxt_query_var( $public_query_vars ) {
	$public_query_vars[] = 'nethr_adstxt';
	return $public_query_vars;
}
add_filter( 'query_vars', 'nethr_adstxt_query_var', 10, 1 );

/**
 * Hook the parse_request action and serve the ads.txt when custom query variable is set to 'true'.
 *
 * @param WP $wp Current WordPress environment instance
 */
function nethr_adstxt_request( $wp ) {
	if ( isset( $wp->query_vars['nethr_adstxt'] ) && 'true' === $wp->query_vars['nethr_adstxt'] ) {
		/*
		 * Set proper content-type per specification in
		 * https://iabtechlab.com/wp-content/uploads/2017/09/IABOpenRTB_Ads.txt_Public_Spec_V1-0-1.pdf :
		 *
		 * The HTTP Content-type should be ‘text/plain’, and all other Content-types should be treated
		 * as an error and the content ignored.
		 */
		header( 'Content-Type: text/plain' );

		// The code expects an existing ads.txt file in the root of your active theme.
		echo esc_html( file_get_contents( get_stylesheet_directory() . '/ads.txt' ) );
		exit;
	}
}
add_action( 'parse_request', 'nethr_adstxt_request', 10, 1 );

add_filter( 'zoninator_json_feed_results', 'nethr_zoninator_json', 10, 2 );

function nethr_zoninator_json($results, $zone_id) {
    foreach ( $results as $index => $result ) {
        $titles = get_post_meta( $result->ID, 'extra_titles', true );
        $result->overtitle = $titles['over_title'];
        $result->permalink = get_permalink($result->ID);
    }

    return  $results;
}