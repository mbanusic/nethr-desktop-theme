window.Vue = require('vue');
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-WP-Nonce'] = wpApiSettings.nonce;

Vue.component('adunits-component', require('./components/adunits'));

const app = new Vue({
    el: '#dfp-app'
});