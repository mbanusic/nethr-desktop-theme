
// Net.hr -> Scripts

jQuery(document).ready(function () {
    effects();
    ticker();
    gallery();
    horoscope();
    most_read_tabs();
    track_outbound_link();
});

function effects() {


    // Scroll top
    jQuery('.top').click(function(){
        jQuery('html, body').animate({scrollTop : 0}, 200);
        return false;
    });

    // sticky navbar
    jQuery(document).scroll(function() {
        var navbar = jQuery('.navbar-menu'), wallpaper = jQuery('.wallpaper');
        var y = jQuery(this).scrollTop();
        if (y > 108) {
            navbar.addClass('sticky');
            wallpaper.addClass('sticky');
        }
        else {
            navbar.removeClass('sticky');
            wallpaper.removeClass('sticky');
        }
    });


    // Home gallery/video banner size
    jQuery(document).scroll( function()  {
        var gallery = jQuery('.gallery-video-more');
        var bannerSize = jQuery('.grid-2 .banner ').height();
        if ( bannerSize > 255 ) {
            gallery.show();
        } else {
            gallery.hide();
        }
    });



    // Back to Net.hr
    var lastScrollTop = 0;
    jQuery(window).scroll(function(event){
       var st = jQuery(this).scrollTop();
       if (st > lastScrollTop){
            // dolje
            jQuery('#back_home').css('bottom', '-25px');
       } else {
            // gore
            jQuery('#back_home').css('bottom', '0');
       }
       lastScrollTop = st;
    });


    // Drop menu
    jQuery('#hamburger').click( function() {
        jQuery('.drop-menu').slideToggle(150);
        jQuery('.navbar-menu').toggleClass('active');
    });

}

function horoscope() {
    //Horoskop
    var item = jQuery('.horoskop .item:first-child'),
        up_arrow = jQuery('.horoskop-arrow.up'),
        down_arrow = jQuery('.horoskop-arrow.down');
    var margin = item.css('margin-top');
    if ( margin === '0px' ) {
        up_arrow.hide();
    }
    var click = 0;
    up_arrow.click( function(){
        click++;
        if (click == 1) {
            click = -12;
        }
        item.css('margin-top', click*200 + 'px');
        margin = item.css('margin-top');
        if ( click == 0 ) {
            jQuery(this).hide();
        }
        if ( click == -10 ) {
            down_arrow.show();
        }
    });
    down_arrow.click( function(){
        click--;
        if (click == -12) {
            click = 0;
        }
        item.css('margin-top', click*200+'px');
        if ( click == -11 ) {
            jQuery(this).hide();
        }
        if ( click == -1 ) {
            up_arrow.show();
        }
    });
}

function ticker() {
    jQuery(".ticker ul").ticker({effect: "slideUp"});
}

function gallery() {
    if (jQuery('#gallery').length > 0) {
        var current = 1;
        var hash = window.location.hash;
        var gallery = jQuery('#gallery').data('gallery');
        if (gallery !== '' || gallery !== undefined) {
            current = parseInt(hash.substring(1));
            if (isNaN(current)) {
                current = 1;
            }
            getGalleryImage(gallery, current);
        }
        else {
            window.location.href = '';
        }
        jQuery('#gallery .arw').click(function(event) {
            event.preventDefault();
            if (jQuery(this).hasClass('arw-left')) {
                processNext('prev');
            }
            else {
                processNext('next');
            }
        });
        jQuery(document).keydown(function(e) {
            if (e.which == 39)  {
                processNext('next');
            }
            if (e.which == 37) {
                processNext('prev');
            }
        });

    }
}

function processNext(action) {
    var next = 0;
    var hash = window.location.hash;
    var current = parseInt( hash.substring(1) );
    var gallery = jQuery('#gallery').data('gallery');

    if ( !isNaN( current ) ) {
        if (action == 'next')  {
            next = current + 1;
        }
        if (action == 'prev') {
            next = current - 1;
        }
    }

    if (!next) {
        return;
    }
    if ( next == 5 || next == 10 || next == 15 || next == 20 ) {
       getGalleryAd(gallery, next);
    }
    else {
        getGalleryImage(gallery, next);
    }
    ga('newTracker.send', 'pageview', location.pathname);
    ga('oldTracker.send', 'pageview', location.pathname);
    pp_gemius_hit('15aQAmMQW9iFAGZeG8KJQKei.MhIw4xDi6L09YLL4Nr.q7');
}

function getGalleryAd(gallery, image) {
    var id = image/5;
    jQuery('#the-image .image-src').html('<div data-glade data-ad-unit-path="/88449691/net.hr_300x250v'+id+'" height="250" width="300" style="position:relative; margin-left: -125px; left: 50%;"></div><script async="async" src="https://securepubads.g.doubleclick.net/static/glade.js"></script>');
    window.location.hash = image;
}

function getGalleryImage(gallery, image){
    var speed = 200;
    var height = jQuery('.slide-img').height();
    var deduct =1;
    if (image < 6) {
        deduct = 1;
    }
    else if (image < 11) {
        deduct = 2;
    }
    else if (image < 16) {
        deduct = 3;
    }
    else if (image < 21) {
        deduct = 4;
    }
    else {
        deduct = 5;
    }
    var response = photos[image-deduct];
    if (typeof(response)  === "undefined") {
        window.location.href = next_post;
    }
    else {
        jQuery('.slide-img').fadeOut(speed);
        jQuery('.slide-img').promise().done(function () {
            if ( response.link.length > 8 ) {
                jQuery('#the-image .image-src').html('<a href="' + response.link + '" target="_blank"><img src="' + response.image + '" title="' + response.title + '" /></a>');
            }
            else {
                jQuery('#the-image .image-src').html('<img src="' + response.image + '" title="' + response.title + '" />');
            }

            jQuery('.slide-img').fadeIn(speed);
            jQuery('.slide-img').promise().done(function () {
                jQuery('#the-image .description').html(response.description);
                jQuery('.photographer').html(response.photographer);
                jQuery(this).css('min-height', height);
                window.location.hash = image;
                jQuery('#current-image').html(image-deduct+1);
                if (!jQuery('#the-image').hasClass('active')) {
                    jQuery('#the-image').addClass('active');
                }

            });
        });
    }
}

function most_read_tabs() {
    jQuery('.najcitanije-widget').on('click', '.section-title', function(e) {
        var parent = jQuery(this).parents('.najcitanije-widget');
        parent.children('.tab').toggle();
        parent.find('.section-title').removeClass('active');
        jQuery(this).addClass('active');
    });
}

function track_outbound_link() {
    jQuery('a[target=_blank]').click(function() {
        var url = jQuery(this).attr('href');
        ga('newTracker.send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon'
        });
        ga('oldTracker.send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon'
        });
    });

    jQuery('.home article a').click(function() {
        var id = jQuery(this).attr('id');
        if (!id) {
            id = 'sidebar';
        }
        var url = jQuery(this).attr('href');
        ga('newTracker.send', 'event', 'crosslink', id, url, {
            'transport': 'beacon'
        });
        ga('oldTracker.send', 'event', 'crosslink', id, url, {
            'transport': 'beacon'
        });
    })
}


