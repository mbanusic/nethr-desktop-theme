<?php

// NET.HR -> Home page

get_header();
?>

	<div class="container cf">

		<?php
		get_template_part( 'templates/layout/home-top' );
		?>

		<div class="large-home-widget cf">
			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Widget', array(
					'zone'       => 'naslovnica-glavni',
					'cache_name' => 'glavni'
				)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php
				dynamic_sidebar( 'home_block_sidebar_main' );
				?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_main' );
		?>

		<div class="large-home-widget cf danas">

			<div class="section-header">
				<div class="section-titles">
					<a href="https://net.hr/kategorija/danas/"
					   class="active">Danas</a>
				</div>
			</div>

			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Widget', array(
					'zone'       => 'naslovnica-danas',
					'cache_name' => 'danas'
				)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php dynamic_sidebar( 'home_block_sidebar_danas' ); ?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_danas' );
		?>

		<div class="large-home-widget cf sport">

			<div class="section-header">
				<div class="section-titles">
					<a href="https://net.hr/kategorija/sport/"
					   class="active">Sport</a>
				</div>
			</div>

			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Widget', array(
					'zone'       => 'naslovnica-sport',
					'cache_name' => 'sport'
				)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php dynamic_sidebar( 'home_block_sidebar_sport' ); ?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_sport' );
		?>

		<div class="large-home-widget cf danas">

			<div class="section-header">
				<div class="section-titles">
					<a href="https://super1.net.hr" class="active">
						Super1
					</a>
				</div>
			</div>

			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Outside_Widget', array(
						'zone'       => '453',
					)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php dynamic_sidebar( 'home_block_sidebar_zena' ); ?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_zena' );
		?>

		<div class="large-home-widget cf hot">

			<div class="section-header">
				<div class="section-titles">
					<a href="https://net.hr/kategorija/hot/"
					   class="active">Hot</a>
				</div>
			</div>

			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Widget', array(
						'zone'       => 'naslovnica-hot',
						'cache_name' => 'hot'
					)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php dynamic_sidebar( 'home_block_sidebar_hot' ); ?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_hot' );
		?>

		<div class="large-home-widget cf magazin">

			<div class="section-header">
				<div class="section-titles">
					<a href="https://net.hr/kategorija/magazin/"
					   class="active">Magazin</a>
				</div>
			</div>

			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Widget', array(
						'zone'       => 'naslovnica-magazin',
						'cache_name' => 'magazin'
					)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php dynamic_sidebar( 'home_block_sidebar_magazin' ); ?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_magazin' );
		?>

		<div class="large-home-widget cf webcafe">

			<div class="section-header">
				<div class="section-titles">
					<a href="https://net.hr/kategorija/webcafe/"
					   class="active">Webcafe</a>
				</div>
			</div>

			<div class="left-content cf">
				<?php
				the_widget(
					'Nethr_Home_Large_Widget', array(
						'zone'       => 'naslovnica-webcafe',
						'cache_name' => 'webcafe'
					)
				);
				?>
			</div>
			<div class="widget-sidebar cf">
				<?php dynamic_sidebar( 'home_block_sidebar_webcafe' ); ?>
			</div>
		</div>

		<?php
		dynamic_sidebar( 'home_under_webcafe' );
		?>


	</div> <!-- end container -->


<?php

get_footer();

