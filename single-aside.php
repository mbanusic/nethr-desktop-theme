<?php
// NET.HR -> Single article
get_header();
?>
	<div class="container single aside cf">

		<div class="page-grid single-article">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="article-content" id="__xclaimwords_wrapper">
					<?php the_content(); ?>
				</div>

			<?php endwhile;  endif; ?>

		</div>

	</div>

<?php
get_footer();
