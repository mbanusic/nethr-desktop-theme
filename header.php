<!DOCTYPE html>
<html <?php language_attributes(); ?> id="top">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=1024, initial-scale=0.8, maximum-scale=1">

	<link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png">
	<?php
	if ( is_front_page() ) {
		?><meta http-equiv="refresh" content="1200"><?php
	}
	?>
	<?php wp_head(); ?>
	<!-- af7bf8cddd0fe677b5ecceef033ed47a -->
</head>

<body <?php body_class(); ?>>
<?php do_action( 'nethr_body_start' ); ?>

<div id="overlay"></div>

<div class="main-container">

	<?php
	// Call navbar
	get_template_part( 'templates/layout/navbar-2' ); ?>

	<div class="content-container cf">

		<div class="top-zone">
			<?php
            if ( is_front_page() ) {
                the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_homepage_billboard_v1' ) );
            }
            else {
	            the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_billboard_v1' ) );
            }
            ?>
		</div>
