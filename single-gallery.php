<?php
// NET.HR -> Single article -> Gallery
get_header();
?>
	<div class="container single gallery cf">

		<div class="page-grid single-article">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php
				global $post;
				$content = $post->post_content;
				preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );
				$attr         = shortcode_parse_atts( $matches[0][3] );
				$ids          = explode( ',', $attr['ids'] );
				?>
				<div class="gallery-top">

					<div class="gallery-top-item">
						<div class="big">
							<a href="#">
								<span id="current-image">1</span>/<?php echo intval( count( $ids ) ); ?>
							</a>
						</div>
						<div class="text">Fotografija</div>
					</div>

					<h3 class="overtitle"><?php $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
						if ( isset($titles['over_title'] ) ) {
							echo esc_html( $titles['over_title'] );
						}
						?></h3>

					<h1 class="title">
						<?php
						if ( isset( $titles['short_title'] ) && $titles['short_title'] ) {
							echo esc_html( $titles['short_title'] );
						}
						else  {
							the_title();
						} ?>
					</h1>
				</div>

				<div id="gallery" class="article-content" data-gallery="<?php the_ID(); ?>">

					<div id="the-image"
					     class="slide-img">

						<div class="gallery-image">
							<a href="#" class="arw arw-left"><i
									class="fa fa-angle-left"></i></a>
							<a href="#" class="arw arw-right"><i
									class="fa fa-angle-right"></i></a>
                                <div class="image-src">
                                </div>
						</div>

						<div class="gallery-meta cf">
                                <span class="socials">
                                    <?php get_template_part( 'templates/layout/socials-count' ); ?>
                                </span>

							<div class="author">
								<span class="photographer"></span></div>
						</div>

						<div class="description" id="__xclaimwords_wrapper">

						</div>

					</div>
					<?php
					$q = new WP_Query( array(
						'post_type' => 'attachment',
						'post__in' => $ids,
						'posts_per_page' => count( $ids ),
						'orderby' => 'post__in',
						'no_found_rows' => true,
						'post_status' => 'inherit',
					) );
					$out = array();
					while ($q->have_posts()) {
						$q->the_post();
						$post_id = get_the_ID();
						$out[] = array(
							'title' => get_the_title(),
							'description' => get_the_excerpt(),
							'image' => wp_get_attachment_image_src( $post_id, 'large' )[0],
							'photographer' => nethr_get_photographer( $post_id ),
							'link' => esc_url( get_post_meta( $post_id, 'image_link', true ) )
						);
					}
					wp_reset_query();
					$next_post = wpcom_vip_get_adjacent_post(true, '', true, 'post_format');
					?>
					<script>
						var photos = <?php echo wp_json_encode( $out ); ?>,
							next_post = <?php echo wp_json_encode( get_permalink( $next_post ) ); ?>;
					</script>


				</div>

			<?php
			endwhile;
			endif;

			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_intext_v1' ) );
			?>

		</div>


		<div class="sidebar gallery-sidebar">
			<?php dynamic_sidebar( 'sidebar-gallery' ) ?>
		</div>

	</div>


<?php
get_footer();
