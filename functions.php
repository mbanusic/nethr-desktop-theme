<?php

require_once( 'functions-shared.php' );

if ( ! isset( $content_width ) ) {
	$content_width = 669;
}

add_action( 'wp_enqueue_scripts', 'nethr_load_style' );

if ( ! function_exists( 'nethr_load_style' ) ) {
	function nethr_load_style() {
		wp_enqueue_style(
			'nethr-style', get_stylesheet_uri(), array(
				'nethr-font-awesome',
				'nethr-font-oswald',
				'nethr-font-source',
				'nethr-font-pt-sans',
				'nethr-font-pt-serif',
				'nethr-videojs'
			)
		);
		if ( is_single() ) {
			wp_enqueue_script( 'nethr-sticky-kit', get_template_directory_uri() . '/js/sticky-kit.js', array( 'jquery' ) );
		}
		wp_enqueue_script('nethr-expand-banner', 'https://scripts.net.hr/dfp/expand.js', array( 'jquery' ), '20181018');
	}
}

add_filter( 'pre_get_posts', 'nethr_pre_get_posts' );

/**
 * @param WP_Query $query
 */
if ( ! function_exists( 'nethr_pre_get_posts' ) ) {
	function nethr_pre_get_posts( $query ) {
		// affect only main query, other queries (e.g. in widgets) are specific and should not be filtered
		// we do not want to change anything in admin
		if ( is_admin() || ! $query->is_main_query() ) {
			return;
		}
		if ( $query->is_category( array( 444206542, 444206541, 444206540, 444206539, 444206537, 444206536 ) ) ) {
			$query->set( 'posts_per_page', 50 );
		}
		else {
			$query->set( 'posts_per_page', 20 );
		}
		if ( $query->is_post_type_archive( 'zena' ) || is_tax( 'zena-cat' ) ) {
			return $query;
		}
		if ( $query->is_post_type_archive( 'webcafe' ) ) {
			$query->set( 'post_type', array( 'post' ) );
			$query->set( 'category_name', 'webcafe' );
		}
		if ( is_archive() && ! is_tax( 'igrice' ) ) {
			if ( is_feed() ) {
				$query->set(
					'post_type', array(
						'post',
						'ispovijesti',
						'zena'
					)
				);
			}
			else {
				$query->set(
					'post_type', array(
					'post',
					'promo',
					'ispovijesti',
					'zena'
				)
				);
			}
			if ( is_category( 'komnetar' ) || is_category( 'cura-dana' ) || is_category( 'overkloking' ) || is_category( 'decko-dana' ) || is_category( 'vic-dana' ) || is_category( 'fora-dana' ) ) {
				$query->set( 'post_type', array( 'webcafe' ) );
			}
		}
	}

}

// add the banner inside of article content
add_filter( 'the_content', 'nethr_the_content', 9 );

function nethr_the_content( $content ) {
	// check if password protected
	if ( post_password_required() ) {
		return $content;
	}
	if ( is_page() || 'webcafe' === get_post_type() ) {
		return $content;
	}
    // check if we don't have ads
	$oglasi = get_post_meta( get_the_ID(), 'oglasi', true );
	if ( isset( $oglasi['adocean'] ) && 1 === intval( $oglasi['adocean'] ) ) {
		return $content;
	}
	$new_line = "\n";
	$parts   = explode( $new_line, $content, 12 );
	$return_content = '';
	for ( $i = 0; $i <= count( $parts ); $i++ ) {
		$return_content .= $parts[$i] . $new_line;
		if ( 2 === $i ) {
			ob_start();
			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_intext_v1' ) );
			$ad = ob_get_clean();

			$return_content .= $ad . $new_line;
		}
		if ( 6 === $i ) {
			ob_start();
			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_intext_v2' ) );
			$ad = ob_get_clean();
			$return_content .= $ad . $new_line;
		}
		if ( 10 === $i ) {
			ob_start();
			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_intext_v3' ) );
			$ad = ob_get_clean();
			$return_content .= $ad . $new_line;
		}
	}

	//append to end of content
	ob_start();
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_intext_v4' ) );
	$ad = ob_get_clean();
	$return_content .= $new_line . $ad;

	return $return_content;
}

//VIP: performance improvements for slow queries on the site
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

//VIP: Coauthors performance improvement see #67204-z
add_filter( 'coauthors_plus_should_query_post_author', '__return_false' );


add_action('nethr_body_start', 'nethr_cookie_disclaimer');

function nethr_cookie_disclaimer() {
	?><div class="disclaimer">
		<div class="container">
			<i class="fa fa-empire"></i>
			<div class="disclaimer-text">
				<div class="disclaimer-title">
					Pravilnik upotrebi kolačića
				</div>
				Portal Net.hr unaprijedio je politiku privatnosti i korištenja takozvanih cookiesa, u skladu s novom europskom regulativom. Cookiese koristimo kako bismo mogli pružati našu online uslugu, analizirati korištenje sadržaja, nuditi oglašivačka rješenja, kao i za ostale funkcionalnosti koje ne bismo mogli pružati bez cookiesa. Daljnjim korištenjem ovog portala pristajete na korištenje cookiesa. Ovdje možete saznati više o <a href="https://net.hr/pravila-privatnosti/">zaštiti privatnosti</a> i <a href="https://net.hr/politika-o-cookiejima/">postavkama cookiesa</a>
			</div>
			<div class="disclaimer-btn">
				<a href="#" onclick="tmg_hide_disclaimer()" class="dsc-btn">Shvaćam</a>
			</div>

		</div>
		<style>
			.disclaimer {
				display:none;
				background: #f5f5f5;
				padding: 80px 0;
				font-family: sans-serif;
				font-size: 18px;
			}

			.disclaimer a:not(.dsc-btn) {
				text-decoration: underline;
			}

			.disclaimer .container {
				display: flex;
				flex-wrap: nowrap;
				position: relative;
			}

			.disclaimer i {
				position: absolute;
				top: 5px;
				font-size: 72px;
				left: -80px;
				color: #999;
			}

			.disclaimer-title {
				font-family: "Oswald", Impact, Sans-Serif;
				font-size: 28px;
				font-weight: normal;
				margin-bottom: 5px;
				color: #333;
			}

			.disclaimer-text {
				padding-right: 20px;
			}

			.disclaimer-btn {
				text-align: center;
				display: flex;
				flex-direction: column;
				justify-content: center;
				border-left: 1px solid #333;
				padding-left: 20px;
			}

			.dsc-btn {
				display: inline-block;
				text-transform: uppercase;
				font-family: "Oswald", Impact, Sans-Serif;
				font-size: 18px;
				background: #f15a22;
				color: white;
				padding: 6px 0;
				width: 150px;
			}

			.dsc-btn:hover {
				background: black;
			}

			.tmg-no-cookie .disclaimer {
				display: block;
			}
		</style>
		<script>
            function tmg_hide_disclaimer() {
                document.body.className = document.body.className.replace(/\btmg-no-cookie\b/,'');
                return false;
            }
            if (document.cookie.indexOf("tmg_visited=") >= 0) {
                tmg_hide_disclaimer();
            }
            else {
                // set a new cookie
                expiry = new Date();
                expiry.setTime(expiry.getTime()+(365*24*60*60*1000)); // 1 year

                // Date()'s toGMTSting() method will format the date correctly for a cookie
                document.cookie = "tmg_visited=yes; domain=.net.hr; path=/; expires=" + expiry.toGMTString();
            }
		</script>
	</div><?php
}

