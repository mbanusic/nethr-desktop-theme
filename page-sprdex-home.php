<?php
/* Template Name: Sprdex page template */
?><!DOCTYPE html>
<html id="top">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<!-- Load Custom fonts -->
	<link href='https://fonts.googleapis.com/css?family=Oswald&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:600&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href="<?php echo esc_url( get_stylesheet_uri() ) ?>" rel="stylesheet">
</head>
<body>
<?php the_widget( 'Nethr_RSS_Home_Widget', array(
	'title' => 'Sprdex',
	'cache_name' => 'sprdex2',
	'feed_url' => 'http://sprdex.net.hr/feed/most-shared/',
	'url' => 'http://sprdex.net.hr',
	'logo' => 'https://adriaticmedianethr.files.wordpress.com/2016/01/sprdex-logo-final.png'
) ) ?>
</body>
</html>
