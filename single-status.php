<?php
// NET.HR -> Single article
get_header();
?>
    <div class="container single longform cf">

        <div class="section-header">
            <div class="section-titles">
                <?php
                $category      = nethr_get_the_category();
                $category_link = wpcom_vip_get_term_link( $category, 'category' );
                if ( !$category_link || true === is_wp_error( $category_link ) ) {
                    $category_link = '#';
                }
                ?>
                <a href="<?php echo esc_url( $category_link ); ?>"
                   class="active"><?php echo esc_html( $category->name ); ?></a>

            </div>
            <?php get_template_part( 'templates/layout/socials' ); ?>
        </div>

        <div class="page-grid single-article">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <div class="article-head">

                    <?php $titles = get_post_meta( get_the_ID(), 'extra_titles', true );  ?>
                    <h3 class="overtitle"><?php
                        if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                            echo esc_html( $titles['over_title'] );
                        }
                        ?></h3>
                    <h1 class="title"><?php the_title(); ?></h1>

                    <div class="article-meta cf">
                        <?php get_template_part( 'templates/layout/socials-count' ) ?>

                        <div class="metabox">
                            <span><?php echo esc_html( nethr_get_photographer() ) ?></span>
                            <span>Autor: <?php coauthors(); ?> </span>
                            <span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
                        </div>
                    </div>

                </div>

                <?php if( has_post_thumbnail() ) { ?>
                    <div class="featured-img thumb">
                        <?php
                        // The post thumbnail
                        the_post_thumbnail( 'longform-single' ); ?>
                    </div>
                <?php } ?>

                <div class="article-content" id="__xclaimwords_wrapper">
                    <?php the_content(); ?>
                    <div class="tags">
                        <?php the_tags( '', ' ', '' ); ?>
                    </div>
                </div>

                <div class="article-footer">
                    <div class="article-meta cf">
                        <?php get_template_part( 'templates/layout/socials-count' ) ?>

                        <div class="metabox">
                            <span><?php echo esc_html( nethr_get_photographer() ) ?></span>
                            <span>Autor: <?php coauthors() ?> </span>
                            <span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
                        </div>
                    </div>

                    <?php dynamic_sidebar( 'single-under' ) ?>
                </div>
            <?php endwhile;  endif; ?>

        </div>

        <?php dynamic_sidebar( 'single-footer' ) ?>
    </div>

<?php
get_footer();
