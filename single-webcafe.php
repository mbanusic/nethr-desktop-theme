<?php
// NET.HR -> Single article
get_header();

$category  = nethr_get_the_category();
$category_link = wpcom_vip_get_term_link( $category, 'category' );

$parent = get_category( $category->parent );

?>
	<div class="container single webcafe-single cf">

		<div class="section-header">
			<div class="yellow-title">
				<a href="<?php echo esc_url( $category_link ); ?>" class="active">
			    <?php
				   echo esc_html( $category->name );
			    if ( 'dnevni-horoskop' === $parent->slug ) {
				    echo esc_html( $category->description );
			    }
			    ?>
				</a>
			</div>
			<?php get_template_part( 'templates/layout/socials' ); ?>
		</div>

		<div class="page-grid single-article <?php if ( ! has_post_thumbnail() ) { echo 'text-article'; } ?>">
			<?php
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();

                $branding = intval( get_option( 'nethr_horoscope_branding', 0) );
					if ( 1 === $branding && 'dnevni-horoskop' === $parent->slug  ) {
						$url = get_option( 'nethr_horoscope_branding_url' );
						$image = get_option( 'nethr_horoscope_branding_image_small' );
					    ?>
						<a href="<?php echo esc_url( $url ) ?>" target="_blank">
							<img src="<?php echo esc_url( $image ) ?>" width="660"/>
						</a>
						<?php
					}

					nethr_get_template( 'templates/webcafe/img-article', array( 'parent' => $parent  ) ); ?>

					<div class="article-meta cf">
						<?php get_template_part( 'templates/layout/socials-count' ) ?>

						<div class="metabox">
						<span><?php echo esc_html( nethr_get_photographer() ) ?></span>
							<span>Autor: <?php coauthors() ?> </span>
							<span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
						</div>
					</div>

					<?php
					if ( 'dnevni-horoskop' === $parent->slug ) {
				    	nethr_get_template( 'templates/webcafe/horoskop-grid' );
				    }
				    dynamic_sidebar( 'single-under' );

			 	} } ?>


		<div id="__xclaimwords_wrapper"></div>
		</div>

		<div class="sidebar single-sidebar single-sidebar-1">
			<?php dynamic_sidebar( 'sidebar-single' ) ?>
		</div>

        <script>
            jQuery(document).ready(function() {
                jQuery('.single-sidebar').stick_in_parent({
                    offset_top: 100,
                    spacer: false
                });
            })
        </script>


	</div>

<?php
get_footer();
