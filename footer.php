<?php
// Get wallpapers -> ALLWAYS inside Content container
if (is_front_page()) {
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_homepage_wallpaper_left' ) );
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_homepage_wallpaper_right' ) );
}
else {
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_wallpaper_left' ) );
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_desktop_wallpaper_right' ) );
}
?>

</div> <!-- END Content container -->

<footer>

	<?php the_widget( 'Nethr_Footer_Categories_Widget' ) ?>

	<div class="footer-bottom cf">

		<div class="container cf">
			<a href="<?php echo esc_url( home_url() ); ?>">
				<div class="logo">
					<h1>Net.hr</h1>
				</div>
			</a>

			<div class="meta">
				Sva prava pridržana.
				<?php global $wp;
				$current_url = trailingslashit( home_url( add_query_arg( array(), $wp->request ) ) ); ?>
				<a href="<?php echo esc_url( site_url( 'uvjeti_koristenja' ) ); ?>">Uvjeti korištenja</a> |
				<a href="<?php echo esc_url( site_url( 'marketing' ) ); ?>">Marketing</a> |
				<a href="<?php echo esc_url( site_url( 'impressum' ) ); ?>">Impressum</a> |
				<a href="mailto:vijesti@portal.net.hr?subject=Net.hr - kontakt">Kontakt</a> |
				<a href="<?php echo esc_url( $current_url . '?ak_action=accept_mobile' ); ?>">Mobilna verzija</a>
			</div>

			<div class="wp-vip">
				<i class="fa fa-wordpress"></i>
				<?php echo vip_powered_wpcom(); ?>
			</div>
		</div>
	</div>

</footer>

<!-- /1092744/nethr/nethr_desktop_scripts -->
<div id='div-gpt-ad-1539078936758-0'>
    <script>
        googletag.cmd.push(function() { googletag.display('nethr_desktop_scripts'); });
    </script>
</div>

</div> <!-- END Main container -->

<?php if( is_front_page() ) { ?>
	<a href="#top" id="back_home" class="top">
		<i class="fa  fa-arrow-up"></i> Povratak na Vrh
	</a>
<?php } else { ?>
	<a href="<?php echo esc_url( home_url() ); ?>" id="back_home">
		<i class="fa  fa-arrow-left"></i> Povratak na Net.hr
	</a>
<?php } ?>

<?php wp_footer(); ?>

</body>
</html>