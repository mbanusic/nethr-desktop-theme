<?php
// NET.HR -> Category
get_header();
?>
    <div class="container category igrice cf">

        <div class="section-header">
            <div class="section-titles">
                <a class="active">Igrice</a>
            </div>
            <?php get_template_part( 'templates/layout/socials' ); ?>
        </div>

        <div class="page-grid">

            <section class="feed cf">
                <?php
                $games = z_get_zone_query( 'igrice-2', array( 'posts_per_page' => 1 ) );
                if ( $games->have_posts() ) {
                    while ( $games->have_posts() ) {
                        $games->the_post();
                        if ( 0 === $games->current_post ) { ?>

                            <?php
                            $category = get_the_terms( $post, 'igrice' );
                            $category = $category[0];
                            ?>

                            <article class="games-lead">
                                <div class="thumb">
                                    <?php the_post_thumbnail( 'single' ); ?>
                                </div>
                                <div class="text">
                                    <div class="overtitle"><?php echo esc_html( $category->name ); ?></div>
                                    <div class="description"><?php the_title(); ?></div>
                                    <a href="<?php the_permalink(); ?>" class="button">Pokreni igru</a>
                                </div>
                            </article>

                        <?php
                        }
                        else {
                            get_template_part( 'templates/articles/article-2-special' );
                        }
                    }
                }
                wp_reset_postdata();
                ?>
            </section>

            <div class="net-games">
                <img id="net_gamepad" width="106" height="74" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/net_gamepad.png"/>
                <div class="text cf">
                    <h1>Net igrice</h1>
	                <?php wp_nav_menu( array(
		                'theme_location' => 'igrice-menu',
		                'container' => 'div',
		                'container_class' => 'category-links',
		                'items_wrap' => '%3$s',
		                'walker' => new Nethr_Link_Menu()
	                ) ); ?>
                </div>
            </div>

	        <section class="feed feed-2 cf">
		        <?php
		        $games = new WP_Query( array(
			        'posts_per_page' => 20,
			        'post_type' => 'igre'
		        ) );
		        if ( $games->have_posts() ) {
			        while ( $games->have_posts() ) {
				        $games->the_post();
				        get_template_part( 'templates/articles/article-2-special' );
			        }
		        }
		        wp_reset_postdata();
		        ?>
	        </section>

            <div class="article-navigation">
                <?php posts_nav_link( '&nbsp;&nbsp; &diams; &nbsp;&nbsp;',
                    '<i class="fa fa-angle-left"></i> NOVIJE IGRE' ,
                    'STARIJE STARIJE <i class="fa fa-angle-right"></i>' );
                ?>
            </div>

        </div>


        <div class="sidebar single-sidebar single-sidebar-1">
            <?php dynamic_sidebar( 'sidebar-web-igrice' ) ?>
        </div>

        <?php dynamic_sidebar( 'under-category' ) ?>
    </div>

<?php
get_footer();