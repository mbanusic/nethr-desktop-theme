<?php
/* Template Name: Telegraf page template */
?><!DOCTYPE html>
<html id="top">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<!-- Load Custom fonts -->
	<link href='https://fonts.googleapis.com/css?family=Oswald&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:600&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

	<style type="text/css">
		/* Net.hr Telegraf Home widget style */
		html {margin:0;padding:0;border:0;}body, div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, code, del, dfn, em, img, q, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, dialog, figure, footer, header, hgroup, nav, section {margin:0;padding:0;border:0;font-weight:inherit;font-style:inherit;font-size:100%;font-family:inherit;vertical-align:baseline; list-style: none; }

		/* Clearfix */
		.cf:before,
		.cf:after { content: " "; display: table; }
		.cf:after { clear: both; }
		.cf { *zoom: 1; /* IE 6/7 only */ }

		.nethr-widget-home {
			border-bottom: 1px solid #ddd;
			width: 940px;
			height: 300px;
			color: #fff;
			overflow: hidden;
		}
		.nethr-widget-logo:hover { margin-top: 0px; }
		.nethr-widget-home a {
			color: #fff;
			text-decoration: none;
			-webkit-transition: all 0.3s;
			-moz-transition: all 0.3s;
			-ms-transition: all 0.3s;
			-o-transition: all 0.3s;
			transition: all 0.3s;
		}
		.nethr-widget-head a:hover {
			text-shadow: 0 0 20px #fff;
		}
		.nethr-widget-head {
			height: 30px;
			background: #ce4b0c;
			position: relative;
			font-family: 'Source Sans Pro', 'Helvetica Neue', 'Arial', sans-serif;
			font-size: 14px;
			line-height: 30px;
			text-transform: uppercase;
			color: #fff;
			position: relative;
		}
		.nethr-widget-logo {
			position: absolute;
			top: -5px;
			-webkit-transition: top 0.3s;
			-moz-transition: top 0.3s;
			-ms-transition: top 0.3s;
			-o-transition: top 0.3s;
			transition: top 0.3s;
		}
		.nethr-widget-logo:hover { top: 0px; }
		.nethr-widget-head .menu {  padding-left: 60px; }
		.nethr-widget-head .menu li { float: left; }
		.nethr-widget-head .menu li a {
			display: block;
			line-height: 30px;
			padding: 0 15px;
		}

		.nethr-widget-body { margin: 12px -10px 0 0; }
		.nethr-widget-body article {
			width: 20%;
			float: left;
		}
		.nethr-widget-body article:first-child { width: 40%; }
		.nethr-widget-body article .inner {
			height: 245px;
			padding-right: 10px;
			box-sizing: border-box;
			-o-box-sizing: border-box;
			-ms-box-sizing: border-box;
			-moz-box-sizing: border-box;
			-webkit-box-sizing: border-box;
		}
		.nethr-widget-body article .thumb {
			position: relative;
			width: 100%;
			height: 100%;
			border-top: 3px solid #f95905;
			background-size: cover;
			-o-background-size: cover;
			-ms-background-size: cover;
			-moz-background-size: cover;
			-webkit-background-size: cover;
			background-position: center;
		}
		.nethr-widget-body .article-text {
			padding: 10px;
			position: absolute;
			z-index: 5;
			bottom: 0;
			left: 0;
			width: 100%;
			box-sizing: border-box;
			-o-box-sizing: border-box;
			-ms-box-sizing: border-box;
			-moz-box-sizing: border-box;
			-webkit-box-sizing: border-box;
		}

		article .article-text .overtitle {
			display: inline-block;
			background: #f95905;
			font-family: 'Source Sans Pro', 'helvetica neue', arial, sans-serif;
			font-size: 12px;
			text-transform: uppercase;
			line-height: 12px;
			padding: 3px 10px;
			margin-left: -10px;
			margin-bottom: 5px;
		}
		article .article-text .title {
			font-family: 'Oswald', sans-serif;
			font-size: 16px;
			line-height: 1.2em;
		}
		article:first-child .title {
			font-size: 26px;
			text-transform: uppercase;
		}

		.article-shadow {
			width: 100%;
			height: 100%;
			position: absolute;
			bottom: 0;
			left: 0;
			background: url('<?php echo esc_url( get_template_directory_uri() ); ?>/img/article1_bg.png');
			background-size: 100% 100%;
			z-index: 1;
			-webkit-transition: all 0.3s;
			-moz-transition: all 0.3s;
			-ms-transition: all 0.3s;
			-o-transition: all 0.3s;
			transition: all 0.3s;
		}
		.article-triangle {
			width: 0;
			height: 0;
			border-bottom: 15px solid transparent;
			border-right: 15px solid #f95905;
			position: absolute;
			-webkit-transition: all 0.3s;
			-moz-transition: all 0.3s;
			-ms-transition: all 0.3s;
			-o-transition: all 0.3s;
			transition: all 0.3s;
			top: 0;
			right: 0;
		}
		article:hover .article-triangle {
			border-bottom: 25px solid transparent;
			border-right: 25px solid #f95905;
		}
		article:hover .article-shadow {
			background-size: 100% 130%;
		}

		/* Color code */
		.danas .thumb { border-top: 3px solid #d90000 !important; }
		.danas .article-text .overtitle { background: #d90000; }
		.danas .article-triangle { border-right: 15px solid #d90000; }
		.danas:hover .article-triangle { border-right: 25px solid #d90000; }

		.sport .thumb { border-top: 3px solid #0059b2 !important; }
		.sport .article-text .overtitle { background: #0059b2; }
		.sport .article-triangle { border-right: 15px solid #0059b2; }
		.sport:hover .article-triangle { border-right: 25px solid #0059b2; }

		.hot .thumb { border-top: 3px solid #d900d9 !important; }
		.hot .article-text .overtitle { background: #d900d9; }
		.hot .article-triangle { border-right: 15px solid #d900d9; }
		.hot:hover .article-triangle { border-right: 25px solid #d900d9; }

		.magazin .thumb { border-top: 3px solid #8eb200 !important; }
		.magazin .article-text .overtitle { background: #8eb200; }
		.magazin .article-triangle { border-right: 15px solid #8eb200; }
		.magazin:hover .article-triangle { border-right: 25px solid #8eb200; }

		.tehnoklik .thumb { border-top: 3px solid #00a3d9 !important; }
		.tehnoklik .article-text .overtitle { background: #00a3d9; }
		.tehnoklik .article-triangle { border-right: 15px solid #00a3d9; }
		.tehnoklik:hover .article-triangle { border-right: 25px solid #00a3d9; }

		.auto .thumb { border-top: 3px solid #b70000 !important; }
		.auto .article-text .overtitle { background: #b70000; }
		.auto .article-triangle { border-right: 15px solid #b70000; }
		.auto:hover .article-triangle { border-right: 25px solid #b70000; }

	</style>
</head>
<body>
<div class="nethr-widget-home">
    <div class="nethr-widget-head cf">
        <a href="<?php echo esc_url( home_url() ); ?>?utm_source=Telegraf&utm_medium=Widget%20970px&utm_campaign=Widget%20rss%20970" target="_blank">
            <img class="nethr-widget-logo"
                 src="https://adriaticmedianethr.files.wordpress.com/2016/02/nethr_telegraf_logo1.png"
                 width="102" height="38"
                />
        </a>
        <div class="menu cf">
            <?php wp_nav_menu( array( 'theme_location' => 'main-home' ) ); ?>
        </div>
    </div>

    <div class="nethr-widget-body cf">
        <?php
        $posts = wpcom_vip_top_posts_array( 2, 40 );
        $i = 1;
        foreach ( $posts as $one ) {
	        if ( $one['post_id'] ) {
		        $post_publish = get_the_time( 'U', $one['post_id']);
		        $time = current_time( 'timestamp' );
		        if ( $post_publish > ( $time - 2*24*3600 ) ) { // we only want posts published in last two days
	                $src = wp_get_attachment_image_src( get_post_thumbnail_id( $one['post_id'] ), 'article-1' );
			        $color = get_post_meta( $one['post_id'], 'nethr_color', true);
			        if ( !$color || $color === 1 ) {
				        $color = 'danas';
			        }
	                ?>

	                <article class="<?php echo esc_html( $color ); ?>">
	                    <div class="inner">

	                        <div class="thumb" style="background-image: url(<?php echo esc_url( $src[0] ); ?>);">
	                            <div class="article-text">
	                                <h3 class="overtitle">
	                                    <?php
	                                    $titles = get_post_meta( $one['post_id'], 'extra_titles', true );
	                                    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
	                                        echo esc_html( $titles['over_title'] );
	                                    }
	                                    else {
	                                        echo esc_html( $color );
	                                    }
	                                    ?>
	                                </h3>
	                                <h1 class="title">
	                                    <a href="<?php echo esc_url( $one['post_permalink'] . '?utm_source=Telegraf&utm_medium=Widget%20970px&utm_campaign=Widget%20rss%20970' ) ?>" target="_blank"><?php echo esc_html( $one['post_title'] ); ?></a>
	                                </h1>
	                            </div>
	                            <div class="article-shadow"></div>
	                           <div class="article-triangle"></div>
	                        </div>
	                    </div>

	                </article>

		            <?php
			        $i++;
			        if ( $i > 4 ) {
				        break; // we took 100 posts, but we need less, since we cannot separate older posts in stats.
			        }
	            }
	        }
        }
        ?>
    </div>
</div>

</body>
</html>
