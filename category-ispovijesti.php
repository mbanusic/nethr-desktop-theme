<?php
// NET.HR -> Category
get_header('ispovijesti');
?>
    <div class="container ispovijesti category cf">

	    <div class="ispovijesti-count">
		    <?php
		    $post_count = 0; $url = false;
		    $cat = get_category( get_query_var( 'cat' ) );
		    if ( $cat && !is_wp_error( $cat ) ) {
			    if ( 'ispovijesti' === $cat->slug ) {
				    $post_count = wp_count_posts( 'ispovijesti' );
				    $post_count = $post_count->publish;
			    }
			    else {
				    $post_count = $cat->category_count;
			    }
			    $url = wpcom_vip_get_term_link( $cat, 'category' );
		    }
		    if ( $post_count && $url ) {
			    ?>
			    Trenutno imamo <a href="<?php echo esc_url( $url ); ?>"
				    class="highlight"><?php echo esc_html( $post_count ); ?>
			    ispovijesti </a><?php

			    if ( $cat && ! is_wp_error( $cat ) && 'ispovijesti' != $cat->slug  ) {
				    echo esc_html( 'u kategoriji ' . $cat->name );
			    }
			    ?>
		    <?php }
		    ?>
	    </div>

        <div class="page-grid cf">

                <?php
                //main feed
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post(); ?>

                        <article class="article-ispovijest cf">

                            <?php the_category(); ?>

                            <a href="<?php the_permalink(); ?>">
                                <h2 class="title"><?php the_title(); ?></h2>
                            </a>
                            <div class="article-meta cf">
                                <span>
                                    Korisnik: <?php echo esc_html( get_post_meta( get_the_ID(), 'autor', true ) ); ?>
                                </span>
                                <span>
                                    Vrijeme: <?php the_time(); ?>
                                </span>
                                <span>
                                    <i class="fa fa-comment"></i>
                                    <block class="fb-comments-count" data-href="<?php the_permalink(); ?>"></block>
                                    Komentara
                                </span>
                            </div>
                            <div class="fb-like" data-href="<?php the_permalink(); ?>"
                                 data-layout="standard"
                                 data-action="like"
                                 data-show-faces="false"
                                 data-share="true">

                            </div>
                        </article>

                        <?php
                    }
                    wp_reset_postdata();
                }
                ?>

            <div class="article-navigation">
	            <?php next_posts_link( 'JOŠ ISPOVIJESTI <i class="fa fa-angle-right"></i>' ); ?>
            </div>

        </div>


        <div class="sidebar single-sidebar single-sidebar-1">
            <?php dynamic_sidebar( 'ispovijesti_sidebar' ) ?>
        </div>

    </div>



<?php
get_footer();
