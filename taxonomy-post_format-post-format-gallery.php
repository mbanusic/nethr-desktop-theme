<?php

// Gallery

get_header(); ?>

	<div class="container gallery cf">

		<h1 class="page-title">Fotogalerije</h1>

		<div class="page-grid gallery-articles cf">
			<?php
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					get_template_part( 'templates/articles/article-4' );
				}
			}
			?>
			<div class="article-navigation">
				<?php next_posts_link( 'JOŠ VIJESTI <i class="fa fa-angle-right"></i>' ); ?>
		    </div>
		    
		</div>
		<div class="sidebar gallery-sidebar">
			<?php dynamic_sidebar( 'sidebar-gallery' ) ?>
		</div>

	</div>

<?php get_footer();