<div class="section-header home-top cf">

	<div class="bottom-part">
		<div class="today"><?php echo esc_html( date_i18n( 'l, j. F' ) ); ?></div>
		<?php the_widget( 'Nethr_Ticker_Widget' ); ?>
	</div>

</div>