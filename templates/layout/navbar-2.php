<?php
//default values for navbar, for everything unless the special cases below
$header_klasa = 'home';
$header_link  = home_url();
$header_text  = 'Net.hr';
$menu = 'home';

if ( is_single() || is_category() ) {
    $cat = nethr_get_top_category();
    $header_klasa = 'category ' . $cat->slug;
    $menu = $cat->slug;
    if ( 'crna-kronika' === $cat->slug || 'net-tv' === $cat->slug ) {
        $menu = 'danas';
    }
}
?>

<div class="top-menu">
    <div class="container">
        <?php wp_nav_menu( array( 'theme_location' => 'header' ) ); ?>
    </div>
</div>


<header class="<?php echo esc_attr( $header_klasa ); ?>">
    <div class="container">

        <div class="head">
            <div class="logo">
                <a href="<?php echo esc_url( $header_link ) ?>">
                    <img src="<?php echo esc_url ( get_template_directory_uri() ) ?>/img/net_colored_logo.svg" height="40"/>
                </a>
            </div>
            <div class="nav-search">
                <?php the_widget('Nethr_Google_Search'); ?>
            </div>

            <div class="freemail">
                <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/icons/net_ico_freemail.svg" height="22"/> Freemail
                <a href="https://freemail.net.hr/" target="_blank" class="button button-small">Prijavi se</a>
            </div>
        </div>

        <div class="navbar-menu">

            <div class="small-logo">
                <a href="<?php echo esc_url( $header_link ) ?>">
                    <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/net_icon.svg" height="24"/>
                </a>
            </div>

            <div id="hamburger">
                <div class="bar bar-top"></div>
                <div class="bar bar-middle"></div>
                <div class="bar bar-bottom"></div>
            </div>

            <div class="drop-menu">
                <?php the_widget( 'Nethr_Footer_Categories_Widget' ) ?>
            </div>

            <?php wp_nav_menu( array( 'theme_location' => 'main-'. esc_html( $menu ) ) ); ?>

            <div class="nav-icons">
                <div class="icon">
                    <a href="https://sanjarica.net.hr/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/icons/net_ico_sanjarica.svg" height="22"/> Sanjarica
                    </a>
                </div>
                <div class="icon">
                    <a href="https://finirecepti.net.hr/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/icons/net_ico_recepti.svg" height="22"/> Recepti
                    </a>
                </div>
                <div class="icon">
                    <a href="http://webshop.net.hr/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/icons/net_ico_webshop.svg" height="22"/> Webshop
                    </a>
                </div>
            </div>
        </div>

    </div>
</header>

<div id="overlay-dark"></div>