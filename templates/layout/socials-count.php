<div class="socials socials-count">

	<span>
		<a href="https://www.facebook.com/dialog/share?app_id=103100136402693&display=popup&href=<?php echo urlencode( get_the_permalink() ); ?>&redirect_uri=<?php echo urlencode( get_the_permalink() ); ?>" target="_blank" class="fb" data-social-network="Facebook" data-social-action="share" data-social-target="<?php the_permalink(); ?>"><i
				class="fa fa-facebook"></i></a>
		<!--<label class="count-no"></label>-->
	</span>

	<span>
		<a href="https://twitter.com/intent/tweet?counturl=<?php echo urlencode( get_the_permalink() ) ?>&text=<?php echo urlencode( get_the_title() ); ?>&url=<?php echo urlencode( get_the_permalink() ) ?>&via=Nethr" target="_blank" class="tw" data-social-network="Twitter" data-social-target="<?php the_permalink() ?>" data-social-action="tweet"><i
				class="fa fa-twitter"></i></a>
		<!--<label class="count-no"></label>-->
	</span>

	<span>
	<a href="https://plus.google.com/share?hl=hr&url=<?php echo urlencode( get_the_permalink() ) ?>" target="_blank" class="go" data-social-network="GooglePlus" data-social-action="share" data-social-target="<?php the_permalink(); ?>"><i
				class="fa fa-google-plus"></i></a>
		<!--<label class="count-no"></label>-->
	</span>

</div>