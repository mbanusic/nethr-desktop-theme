<?php
//default values for navbar, for everything unless the special cases below
$header_klasa = 'home';
$header_link  = home_url();
$header_text  = 'Net.hr';
$menu = 'home';

if ( is_single() || is_category() ) {
	$cat = nethr_get_top_category();
	$header_klasa = 'category ' . $cat->slug;
	$menu = $cat->slug;
	if ( 'crna-kronika' === $cat->slug || 'net-tv' === $cat->slug ) {
		$menu = 'danas';
	}
}
?>
<header class="<?php echo esc_attr( $header_klasa ); ?>">

	<div class="top-navbar">
		<div class="container cf">

			<div class="top-menu">
				<?php wp_nav_menu( array( 'theme_location' => 'header' ) ); ?>
			</div>

		</div>
	</div>

	<div class="main-navbar">
		<div class="container cf">

			<div id="navbar-left"><?php // The orange arrow thingy   ?></div>

			<a href="<?php echo esc_url( $header_link ) ?>">
				<div class="logo">
					<h1><?php echo esc_html( $header_text ) ?></h1>

					<div class="logo-bg-tip"></div>
				</div>
			</a>

			<div class="menu cf">
				<?php
				wp_nav_menu( array( 'theme_location' => 'main-'. esc_html( $menu ) ) );
				?>
			</div>

			<form id="google-search" action="<?php echo esc_url( site_url('pretrazivanje') ); ?>">
				<input type="hidden" name="cx" value="partner-pub-2317149376955370:wn57ucrd4ll" />
				<input type="hidden" name="cof" value="FORID:10" />
				<input type="hidden" name="ie" value="UTF-8" />
				<input type="text" name="q" size="31" placeholder="Google pretraživanje" />
				<i class="fa fa-search"></i>
			</form>

		</div>
	</div>

</header>