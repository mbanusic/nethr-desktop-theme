<header class="zena">

    <div class="top-navbar">
        <div class="container cf">

            <div class="top-menu">
                <?php wp_nav_menu( array( 'theme_location' => 'header' ) ); ?>
            </div>

        </div>
    </div>

    <div class="main-navbar">
        <div class="container cf">
            <div class="logo">
                <a href="https://net.hr/zena">
                    <?php if ( is_single() && 'gallery' === get_post_format() ) { ?>
                        <img src="<?php echo esc_url( get_template_directory_uri() . '/img/zenanet_logo_white.svg' ); ?>" height="84"/>
                    <?php } else { ?>
                        <img src="<?php echo esc_url( get_template_directory_uri() . '/img/zenanet_logo.svg' ); ?>" height="84"/>
                    <?php } ?>
                </a>
            </div>
            <div class="social-zena">
                <a href="https://www.facebook.com/zenanet.hr/" target="_blank"><i class="fa fa-facebook"></i> Facebook</a>
                <a href="https://www.instagram.com/zenanet/" target="_blank"><i class="fa fa-instagram"></i> Instagram</a>
            </div>
            <div class="navbar-menu cf">
                <a href="https://net.hr/zena">
                    <div class="small-logo"></div>
                </a>
                <?php wp_nav_menu( array( 'theme_location' => 'zena-net' ) ); ?>
            </div>
        </div>

</header>

<div id="overlay-dark"></div>