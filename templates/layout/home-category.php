<?php /*
  ==============================================================================================================
  Section -> Categories
  ==============================================================================================================
 */
$category = $options['category'];
$cat = wpcom_vip_get_category_by_slug($category);
$permalink = wpcom_vip_get_term_link( $category, 'category' );
$cat_title = $cat->name;
$banner = $options['banner'];
?>
<section class="home-category <?php echo esc_attr( $category ); ?> cf">
	<div class="section-header">
		<div class="section-titles">
			<a href="<?php echo esc_url( $permalink ); ?>"
			   class="active"><?php echo esc_html( $cat_title ); ?></a>
		</div>
	</div>
		<?php
		the_widget( 'Nethr_Category_Five_Widget', array( 'location' => 'home', 'category' => $category ) );
		the_widget( 'Nethr_Banner_Widget', array( 'size' => $banner ) );
		?>
				<div class="grid-2 cf">

					<div class="article-num-text cf">
						<?php
						nethr_get_template( 'templates/articles/article-num-title', array( 'data' => $options['num_data'] ) );
						?></div>
				</div>
				<div class="grid-item-3">
					<?php
					the_widget( 'Nethr_Category_Four_Widget', array( 'location' => $category.'2' ) );
					?>
				</div>
	<?php // Color article - Big
	the_widget( 'Nethr_Color_Article_Widget', array( 'location' => $category, 'post' => $options['color'] ) );
	?>
</section>