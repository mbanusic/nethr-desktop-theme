<?php
if ( is_front_page() ) {
	$fb  = 'https://www.facebook.com/Net.hr';
	$tw  = 'https://twitter.com/Nethr/';
	$rss = get_feed_link();
}
else {
	$category = nethr_get_top_category();
	if ( $category->term_id ) {
		$rss = get_category_feed_link( $category->term_id );
	}
	else {
		$rss  = get_feed_link();
	}
	switch ( $category->slug ) {
		case 'danas':
			$fb   = 'https://www.facebook.com/Danas.hr';
			$tw   = 'https://twitter.com/danashr';
			break;
		case 'sport':
			$fb   = 'https://www.facebook.com/Sportski.net';
			$tw   = 'https://twitter.com/sportski_net';
			break;
		case 'webcafe':
			$fb   = 'https://www.facebook.com/Webcafe.hr';
			$tw   = 'https://twitter.com/Nethr/';
			break;
		case 'hot':
			$fb   = 'https://www.facebook.com/Hot.hr';
			$tw   = 'https://twitter.com/hot__hr';
			break;
		case 'magazin':
			$fb   = 'https://www.facebook.com/Magazin.hr';
			$tw   = 'https://twitter.com/magazinhr';
			break;
		case 'tehnoklik':
			$fb   = 'https://www.facebook.com/Net.hr';
			$tw   = 'https://twitter.com/Nethr/';
			break;
		case 'auto':
			$fb   = 'https://www.facebook.com/Net.hr';
			$tw   = 'https://twitter.com/Nethr/';
			break;
		default:
			$fb   = 'https://www.facebook.com/Net.hr';
			$tw   = 'https://twitter.com/Nethr/';
			break;
	}

}
?>

<div class="socials">
	<?php if ( ! is_front_page() && ! has_post_format( 'gallery' ) ) { ?>
		<div class="c2action"><!-- as bg --></div>
	<?php } ?>
	<a href="<?php echo esc_url( $fb ); ?>" target="_blank" class="fb" data-social-network="Facebook" data-social-action="click" data-social-target="<?php echo esc_url( $fb ); ?>"><i
			class="fa fa-facebook"></i></a>
	<a href="<?php echo esc_url( $tw ); ?>" target="_blank" class="tw" data-social-network="Twitter" data-social-action="click" data-social-target="<?php echo esc_url( $tw ); ?>"><i
			class="fa fa-twitter"></i></a>
	<a href="<?php echo esc_url( $rss ); ?>" target="_blank" class="go"><i
			class="fa fa-rss"></i></a>
	<?php if ( is_single() && has_post_format( 'gallery' ) ) { ?>
		<div class="c2action gallery"><!-- as bg --></div>
	<?php } ?>

</div>