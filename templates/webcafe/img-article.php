<div class="featured-img">

		<a href="<?php echo esc_url( get_permalink( get_adjacent_post( true, '', false ) ) ); ?>" class="arrow arrow-left">
			<i class="fa fa-angle-left"></i>
		</a>

	<?php
	if ( has_post_thumbnail() && ( isset($options['parent']) ) && 'dnevni-horoskop' != $options['parent']->slug ) {
		the_post_thumbnail( 'single-raw' );
	} ?>

	<a href="<?php echo esc_url( get_permalink( get_adjacent_post( true, '', true ) ) ); ?>" class="arrow arrow-right">
		<i class="fa fa-angle-right"></i>
	</a>
</div>

<div class="article-bottom">
	<h1 class="title"><?php the_title(); ?></h1>
	<?php the_content();  ?>

</div>