<?php
$data = wp_cache_get( 'horoskop-grid', 'nethr_widgets' );
if ( !$data ) {
	ob_start();
	?>
	<div class="horoskop-grid grid-4 cf">
		<?php

		$cat_id = wpcom_vip_get_category_by_slug( 'dnevni-horoskop' );
		$terms  = get_terms( 'category', array( 'parent' => $cat_id->term_id ) );

		foreach ( $terms as $cat ) {

			$args = array(
				'post_type'      => array( 'webcafe' ),
				'cat'            => $cat->term_id,
				'posts_per_page' => 1,
				'no_found_rows'  => true,
				'posts_status'   => 'publish'
			);
			$q    = new WP_Query( $args );
			if ( $q->have_posts() ) {
				while ( $q->have_posts() ) {
					$q->the_post(); ?>

					<article
						class="grid-article dnevni-horoskop <?php echo esc_html( $cat->slug ); ?>">
						<div class="inner">
							<a href="<?php the_permalink(); ?>">
								<div class="thumb"></div>
								<h2 class="title"><?php echo esc_html( $cat->name ); ?></h2>
							</a>
						</div>
					</article>

				<?php }
				wp_reset_postdata();
			}
		} ?>
	</div>
	<?php
	$data = ob_get_clean();
	wp_cache_set( 'horoskop-grid', $data, 'nethr_widgets', HOUR_IN_SECONDS );
}

echo $data;