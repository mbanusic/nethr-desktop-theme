<?php $category = nethr_get_the_category( get_the_ID() ); ?>
<article class="article-2 colored <?php echo esc_attr( nethr_category_color() ); ?>">
	<div class="inner cf">

		<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>">
			<div class="thumb">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( 'feed-2' );
                }
                else {
                    ?><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed-2.gif"/>
                <?php } ?>
            </div>

			<div class="article-text">
				<h2 class="overtitle <?php echo esc_attr( nethr_category_color() ); ?>">
					<?php
					$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
					if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
						echo esc_html( $titles['over_title'] );
					} else {
						echo esc_html( $category->name );
					}
					?>
				</h2>

				<h1 class="title"><?php the_title(); ?></h1>
			</div>
		</a>

	</div>
</article>