<article class="article-1 article-gallery <?php echo esc_attr( get_post_format() ); ?>">
	<div class="inner">

		<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>">
			<div class="thumb">
				<?php the_post_thumbnail( 'article-1' ); ?>
			</div>

			<div class="article-text">
				<h1 class="title">
					<?php
					$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
					if ( isset( $options['gallery_view'] ) && $options['gallery_view'] && isset( $titles['short_title'] ) && $titles['short_title'] ) {
						echo esc_html( $titles['short_title'] );
					} else {
						the_title();
					}
					?></h1>
			</div>

			<div class="icon"><?php // as background ?></div>
		</a>

	</div>
</article>