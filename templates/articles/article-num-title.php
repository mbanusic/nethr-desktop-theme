<?php $data = $options['data']; ?>
<article class="article-num-title cf">
	<div class="inner">

		<div class="number-container">
			<div class="number overtitle"><?php echo esc_html( $data['number'] ); ?></div>
			<div class="skew-left"></div>
			<div class="skew-right"></div>
		</div>
		<div class="number-title title">
			<a href="<?php echo esc_url( $data['url'] ); ?>">
				<span><?php echo esc_html( $data['text'] ); ?></span>
			</a>
		</div>

	</div>
</article>