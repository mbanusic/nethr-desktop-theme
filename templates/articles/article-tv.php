<article class="article-feed article-tv <?php echo esc_attr( get_post_format() ); ?> cf">
    <div class="inner cf">
        <?php
        $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
        if ( isset( $titles['sticker'] ) && $titles['sticker'] ) { ?>
            <a href="#" class="sticker">
                <?php
                echo esc_html( $titles['sticker'] )
                ?>
            </a>
        <?php } ?>

        <a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>">
            <div class="thumb">
                <?php

                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'feed-2' );
                    } else { ?>
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed-2.gif"/>
                    <?php
                    }
                    ?><div class="icon"></div><?php

                ?>
                <?php
                if ( 'gallery' === get_post_format() ) {
                    ?><div class="article-icon"><i class="fa fa-camera"></i> Foto
                    </div><?php
                } ?>
            </div>
        </a>
        <?php $category = nethr_get_the_category( get_the_ID() ); ?>
        <div class="article-text">
            <a href="<?php the_permalink(); ?>">
                <h2 class="overtitle <?php echo esc_attr( nethr_category_color() ); ?>">
                    <?php
                    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                        echo esc_html( $titles['over_title'] );
                    } else {
                        echo esc_html( $category->name );
                    } ?>
                </h2>

                <h1 class="title"><?php the_title(); ?></h1>
            </a>

            <p class="undertitle"><?php the_time(); ?>
                <a href="<?php esc_url( wpcom_vip_get_term_link( $category, 'category' ) ) ?>"><?php echo esc_html( $category->name );?></a>
            </p>
        </div>

    </div>
</article>