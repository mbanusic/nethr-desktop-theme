<article class="article-3 cf <?php echo esc_attr( get_post_format() ) ?>">
	<div class="inner cf">

		<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>">

			<div class="thumb">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( 'thumb' );
                }
                else {
                    ?><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-3-alt.gif"/>
                <?php } ?>

				<?php
				if ( 'gallery' === get_post_format() ) { ?>
					<div class="article-icon"><i class="fa fa-camera"></i> Foto
					</div>
				<?php
				}
				if ( 'video' === get_post_format() ) { ?>
					<div class="article-icon"><i class="fa fa-play"></i> Video
					</div>
				<?php
				}
				?>

			</div>

			<div class="article-text">
				<?php $category = nethr_get_the_category( get_the_ID() ); ?>
				<h2 class="overtitle <?php echo esc_attr( nethr_category_color() ); ?>">
					<?php
					$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
					if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
						echo esc_html( $titles['over_title'] );
					} else {
						echo esc_html( $category->name );
					}
					?>
				</h2>

				<h1 class="title">
					<?php
					if ( isset( $options['short'] ) && 'yes' === $options['short'] && isset( $titles['short_title'] ) && $titles['short_title'] ) {
						echo esc_html( $titles['short_title'] );
					} else {
						the_title();
					}
					?></h1>
			</div>
		</a>

	</div>
</article>