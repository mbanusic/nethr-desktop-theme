<?php
/*
 * Large Article - used on Home Large Widget
 */
?>
<article class="article-1 breaking-format breaking-4 cf">

        <div class="shadow"></div>
        <a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>"
	        <?php if (isset($options['zone']) && isset($options['position']) && $options['zone'] && $options['position']) {
		        echo 'id="' . $options['zone'] . '-' . $options['position'] . '"';
	        }  ?>
        >
            <?php
            // Custom sticker: Lokalni izbori 2017
            if( has_term('lokalni-izbori-2017', 'post_tag') ) {
                ?><img class="custom-sticker" src="<?php echo esc_url( get_template_directory_uri() . '/img/lok_izb_2017.svg' ) ?>" width="90" height="90" /><?php
            }
            // Post thumbnail
            $img_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'breaking-3' );
            ?>
            <div class="thumb" style="background-image: url('<?php echo esc_url( $img_url[0] ); ?>');">
            </div>

            <div class="article-text">
                <?php $category = nethr_get_the_category( get_the_ID() ); ?>
                <h2 class="overtitle <?php echo esc_attr( nethr_category_color() ); ?>">
                    <?php
                    $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
                    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                        echo esc_html( $titles['over_title'] );
                    } else {
                        echo esc_html( $category->name );
                    }
                    ?>
                </h2>

                <h1 class="title">
                    <?php
                        if ( isset( $options['short'] ) && 'yes' === $options['short'] ){
                            echo esc_html( $titles['short_title'] );
                        }
                        else {
                            the_title();
                        }
                    ?>
                </h1>

                <p class="subtitle"><?php echo esc_html( nethr_get_excerpt( 50 ) ); ?></p>

            </div>

            <div class="icon"><?php // as background ?></div>
        </a>

</article>
