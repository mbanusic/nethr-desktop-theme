<article class="article-feed zena-feed <?php echo esc_attr( get_post_format() ); ?> cf">
    <div class="inner cf">
        <?php
        $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
        if ( isset( $titles['sticker'] ) && $titles['sticker'] ) { ?>
            <a href="#" class="sticker">
                <?php
                echo esc_html( $titles['sticker'] )
                ?>
            </a>
        <?php } ?>

        <a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>">
            <div class="thumb">
                <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'feed-2' );
                    } else { ?>
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed-2.gif"/>
                        <?php
                    }
                    // if gallery
                    if ( 'gallery' === get_post_format() ) {
                        ?><div class="article-icon"><i class="fa fa-camera"></i> Foto
                        </div><?php
                    }
                    else if( 'video' === get_post_format() ) {
                        ?><div class="article-icon"><i class="fa fa-video-camera"></i> Video
                        </div><?php
                    }
                    else if( 'status' === get_post_format() ) {
                        ?>
                        <div class="article-icon"><i class="fa fa-instagram"></i> Instagram
                        </div><?php
                    }
                ?>
            </div>
        </a>
        <div class="article-text">
            <a href="<?php the_permalink(); ?>">
                <h2 class="overtitle zena">
                    <?php
                    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                        echo esc_html( $titles['over_title'] );
                    } ?>
                </h2>

                <h1 class="title"><?php the_title(); ?></h1>
            </a>

            <p class="undertitle">
                <?php nethr_intro_excerpt() ?>
            </p>
        </div>

    </div>
</article>