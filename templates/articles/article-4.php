<article class="article-4 <?php echo esc_html( get_post_format() ) ?>">
	<div class="inner cf">
		<?php
		$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
		if ( isset( $titles['sticker'] ) && $titles['sticker'] ) { ?>

			<a href="#" class="sticker sticker-right">
				<?php
				echo esc_html( $titles['sticker'] );
				?>
			</a>
		<?php } ?>
		<a href="<?php the_permalink(); ?>" target="<?php echo esc_attr( nethr_permalink_target() ) ?>">
			<div class="thumb">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( 'feed-2' );
                }
                else {
                    ?><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/dummy-feed-2.gif"/>
                <?php } ?>
			</div>

			<div class="article-text">
				<?php $category = nethr_get_the_category( get_the_ID() ); ?>
				<h2 class="overtitle <?php echo esc_attr( nethr_category_color()); ?>">
					<?php
					if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
						echo esc_html( $titles['over_title'] );
					}
					else {
						echo esc_html( $category->name );
					}
					?>
				</h2>

				<h1 class="title"><?php the_title(); ?></h1>
			</div>
		</a>

	</div>
</article>