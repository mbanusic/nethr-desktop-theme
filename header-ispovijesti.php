<!DOCTYPE html>
<html <?php language_attributes(); ?> id="top">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png">
    <?php
    if ( is_home() || is_page( 'home' ) ) {
        ?><meta http-equiv="refresh" content="1200"><?php
    }
    ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'nethr_body_start' ); ?>

<div id="overlay"></div>

<div class="main-container">

        <header class="ispovijesti">
            <div class="header-title" style="background-image: url( '<?php echo esc_url( get_option( 'nethr_ispovijesti_header', '' ) ) ?>' );">
                <div class="container">
                    <div class="top-navbar">
                        <div class="top-menu cf">
                            <a href="<?php echo esc_url( home_url() ); ?>" style="float: left;">
                                <i class="fa fa-long-arrow-left"></i> Povratak na Net.hr
                            </a>
                            <?php wp_nav_menu( array( 'theme_location' => 'header' ) ); ?>
                        </div>
                    </div>
                    <a href="<?php echo esc_url( site_url( 'kategorija/ispovijesti' ) ) ?>">
                        <img src="https://adriaticmedianethr.files.wordpress.com/2016/06/ispovijesti_logo22.png" width="323">
                    </a>
                    <div class="categories cf">
                        <?php
                        wp_nav_menu( array( 'theme_location' => 'header-ispovijesti' ) );
                        ?>
                    </div>
                    <a href="<?php echo esc_url( site_url( 'ostavi-ispovijest' ) ); ?>" id="ostavi-ispovijest">Ostavi ispovijest</a>
                </div>
            </div>
        </header>

    <div class="content-container cf">