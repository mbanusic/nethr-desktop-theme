<?php
get_header();
$tag = get_query_var( 'tag' );

?>
	<div class="container archive cf">

		<div class="section-header">
				<?php
				$tag = wpcom_vip_get_term_by( 'slug', $tag, 'post_tag' );
				if ( $tag ) {
				$tag_link = get_term_link( $tag );
				?>
				<div class="yellow-title">
					<a href="<?php echo esc_url( $tag_link ); ?>"
					   class="active"><?php echo esc_html( $tag->name ) ?></a>
				</div>
				<?php } ?>
			<?php get_template_part( 'templates/layout/socials' ); ?>

		</div>

		<div class="page-grid">

			<section class="feed cf">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						get_template_part( 'templates/articles/article-feed' );
					}
				}
				wp_reset_postdata();
				?>
				<div class="article-navigation">
			        <?php next_posts_link( 'JOŠ VIJESTI <i class="fa fa-angle-right"></i>' ); ?>
			    </div>
			</section>

		</div>


		<div class="sidebar single-sidebar single-sidebar-1">

			<?php dynamic_sidebar( 'sidebar-category' ) ?>

		</div>

	</div>


<?php
get_footer();
