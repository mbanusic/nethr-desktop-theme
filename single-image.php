<?php
// NET.HR -> Single article
get_header();
?>
	<div class="container single cf">

		<div class="section-header">
			<div class="section-titles">
				<?php
				$category      = nethr_get_the_category();
				$category_link = wpcom_vip_get_term_link( $category, 'category' );
				?>
				<a href="<?php echo esc_url( $category_link ); ?>"
				   class="active"><?php echo esc_html( $category->name ); ?></a>
			</div>
		</div>

		<div class="page-grid single-article">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php $titles = get_post_meta( get_the_ID(), 'extra_titles', true );  ?>
				<h3 class="overtitle"><?php
				if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
					echo esc_html( $titles['over_title'] );
				}
					?></h3>
				<h1 class="title"><?php the_title(); ?></h1>

				<div class="article-meta cf">
					<?php get_template_part( 'templates/layout/socials_count' ) ?>

					<div class="metabox">
					<span><?php echo esc_html( nethr_get_photographer() ); ?></span>
						<span>Autor: <?php coauthors(); ?> </span>
						<span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
					</div>
				</div>

				<div class="featured-img">
					<?php the_post_thumbnail( 'single' ); ?>
				</div>

				<div class="article-content"><?php the_content(); ?></div>

				<div class="article-meta cf">
					<?php get_template_part( 'templates/layout/socials_count' ) ?>

					<div class="metabox">
					<span><?php echo esc_html( nethr_get_photographer() ) ?></span>
						<span>Autor: <?php coauthors() ?> </span>
						<span><i class="fa fa-clock-o"></i> <?php the_time(); ?></span>
					</div>
				</div>
			<?php endwhile;  endif; ?>

			<?php dynamic_sidebar( 'single-under' ) ?>


		</div>

		<div class="sidebar single-sidebar single-sidebar-1">
			<?php dynamic_sidebar( 'sidebar-single' ) ?>
		</div>


	</div>

<?php
get_footer();
