<?php
// NET.HR -> Page
get_header();
?>
	<div class="container page impressum wide cf">

		<div class="section-header">
			<div class="section-titles">
				<h2 class="section-title"><?php the_title(); ?></h2>
			</div>
			<?php get_template_part( 'templates/layout/socials' ); ?>
		</div>


			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="featured-img">
					<?php the_post_thumbnail( 'single' ); ?>
				</div>

				<div class="article-content">

					<div class="staff section cf">
						<?php the_content(); ?>
					</div>
					<div class="section half left">
						<strong>Nakladnik</strong>
						<h1>Telegram media grupa d.o.o.</h1>
						<p>
                            Miran Pavić, član Uprave</br>
                            Zagreb, Pavletićeva 1</br>
                            OIB: 36974788949</br>
                            Društvo je upisano u sudski registar Trgovačkog suda u Zagrebu, pod brojem MBS: 080341252,
							IBAN: HR73 23600001101437160 Zagrebačka banka d.d.
                            Temeljni kapital društva iznosi 3.700.000,00 kuna i uplaćen je u cijelosti
						</p>
						</br>
						<strong>Kontakt</strong></br>
						<a href="mailto:vijesti@portal.net.hr" target="_blank">vijesti@portal.net.hr</a>
					</div>

					<div class="section half left">
						<small>
						<p>
							Svi poslani materijali, fotografije, videouraci, tekstovi i drugi slični materijali, odnosno drugi autorski radovi (u daljnjem tekstu: Materijali) mogu, ali ne moraju biti objavljeni.
						</p>
						<p>
							Pošiljatelji materijala prihvaćaju da za poslane materijale neće biti isplaćen nikakav honorar. Sve osobe koje nam pošalju materijal, jamče pod punom kaznenom i materijalnom odgovornošću da isti predstavlja isključivo njihovo vlasništvo te da na isti polažu autorska prava.
						</p>
						<p>
							Portal www.net.hr poštuje intelektualno vlasništvo i autorska prava drugih.
						</p>
							Pravila korištenja svih naših portala i servisa su definirana u uvjetima korištenja.
						</p>
							Kršenje uvjeta korištenja možete prijaviti na e-mail adresu: zloupotreba@portal.net.hr
						</p>
					</small>
					<img src="<?php echo esc_url( get_template_directory_uri() ) ?>/temp/logo_superbrands.jpg"/>
					<img style="position: relative; top: -17px;" src="<?php echo esc_url( get_template_directory_uri() ) ?>/temp/logo_iab.jpg"/>
					</div>

				</div>

			<?php endwhile;  endif; ?>

		<div id="__xclaimwords_wrapper"></div>


	</div>

<?php
get_footer();
