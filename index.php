<?php
get_header();

?>
	<div class="container archive cf">

		<div class="section-header">
			<div class="yellow-title">
				<a href="https://net.hr"
				   class="active">Net.hr</a>
			</div>

			<?php get_template_part( 'templates/layout/socials' ); ?>

		</div>

		<div class="page-grid">

			<section class="feed cf">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						get_template_part( 'templates/articles/article-feed' );
					}
				}
				wp_reset_postdata();
				?>
				<div class="article-navigation">
					<?php next_posts_link( 'JOŠ VIJESTI <i class="fa fa-angle-right"></i>' ); ?>
				</div>
			</section>

		</div>


		<div class="sidebar single-sidebar single-sidebar-1">

			<?php dynamic_sidebar( 'sidebar-category' ) ?>

		</div>

	</div>


<?php
get_footer();
